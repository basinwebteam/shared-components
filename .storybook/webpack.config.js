const path = require("path");
const include = path.resolve(__dirname, '../');

module.exports = {
    // Add '.ts' and '.tsx' as resolvable extensions.
    resolve: {
      extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            {
              test: /\.tsx?$/,
              loader: 'ts-loader',
              exclude: /node_modules/,
              include
            },
            {
              test: /\.scss$/,
              loaders: [
                {
                  loader: "style-loader"
                },
                {
                  loader: "css-loader"
                },
                {
                  loader: "sass-loader",
                  options: {
                    includePaths: [path.resolve(__dirname, '../node_modules/')]
                  }
                }
              ],
              include: path.resolve(__dirname, '../')
            },
            {
              test: /\.css$/,
              loaders: ["style-loader", "css-loader"],
              include: path.resolve(__dirname, '../')
            }
        ]
    }
  };