import { configure } from '@storybook/react';
import { setDefaults } from '@storybook/addon-info';
import BoardOfDirectors from '../src/components/BoardOfDirectors'

setDefaults({
  maxPropsIntoLine: 1,
  maxPropArrayLength: 1,
  propTablesExclude: [BoardOfDirectors]
});

const req = require.context('../src/', true, /story.tsx?$/)

function loadStories() {
  req.keys().forEach((filename) => req(filename))
}

configure(loadStories, module);
