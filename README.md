# Documentation

## Purpose

This creates a re-usable library of components. The src directory contains the following:

* *components* - Simple components (most without state) that can be used in any project that uses React. These contain story.tsx files that demo how to use them and style.scss files for their specific styles.

* *DrupalContainers* - Contains components that interact with Drupal's API and most often display with a similar named component from the components directory mentioned above. You can use these individually or you can use the paragraph or data view component which is a large switch statement that will try to use the correct component depending on the data sent to it by Drupal's API.

* *DrupalAPI* - Functions for requesting data from Drupal

* *DrupalContentTypes* - Names of content types used in Drupal. This is created to store the names in one place as variables instead of using strings throughout the code.

## Basin Install Instructions

* To use this project, make sure to install Storybook `npm i -g @storybook/cli`

* Also install your node_modules. `yarn install` or `npm install`

## To Run Dev

Run following command. View `localhost:6006` in your browser. This launches StoryBook. You can modify the components & story.tsx files in `/src/components/`. This will auto-reload on changes.

`yarn dev`

## To build & push changes

For this code to be reusable, we create a build directory at the root level where we compile all our code to. The `/src/index.ts` file contains the code that gets built. The `/build` directory is the directory that will be used when you import this code into other projects.

Run the following command to build & then push with Git

`yarn build`

## To run tests

Please write tests as much as possible. Look at other tests for examples. If nothing, look for some snapshot tests that will create a copy of the rendered output to compare future changes against. If you make changes to code that effect the rendered output, the tests will inform you. You can choose to update the snpashot by typing `u` in the terminal as prompted.

`yarn test` or `yarn test-watch`

## Troubleshooting

If you receive an error related to the SASS modules requiring vcbuild.exe, run the following as an administrator: `npm install --global --production windows-build-tools`
