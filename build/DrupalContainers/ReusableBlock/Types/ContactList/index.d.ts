import * as IAxios from "axios/index";
import * as React from "react";
import * as Page from "../../../../DrupalContentTypes/PageInterface";
declare class ContactList extends React.Component<IContactListProps, IContactListState> {
    constructor(props: IContactListProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IContactListProps, prevState: IContactListState): void;
    private getAnchor(name, number?);
    private getDataFromApi(id);
    updateStateWithContacts(data: IContactData): void;
    getImageUrl(image: Page.IField_Image, includedData: Array<IIncludedContact | Page.IField_File>): string;
    getBiography(bio: Page.IWYSIWYG): string;
}
export default ContactList;
export interface IContactListProps {
    siteDomain: string;
    blockId: string;
}
export interface IContactListState {
    contacts: Array<IContactListContact>;
}
export interface IContactListContact {
    name: string;
    image: string;
    bio: string;
}
export interface IAxiosResponseContactList extends IAxios.AxiosResponse {
    data: IContactData;
}
export interface IContactData extends Page.IPage {
    data: Page.IPageData & {
        relationships: Page.IRelationships & {
            field_contact: Page.ITypeMultipleData;
        };
    };
    included: Array<IIncludedContact | Page.IField_File>;
}
export interface IIncludedContact extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_biography: Page.IWYSIWYG;
        field_employee_e_mail: string;
        field_first_name: string;
        field_last_name: string;
        field_employee_phone_number: [string];
        field_employee_title: string;
    };
    relationships: Page.IRelationships & {
        field_image: Page.IField_Image;
    };
}
