import * as IAxios from "axios/index";
import * as React from "react";
import * as Page from "../../../../DrupalContentTypes/PageInterface";
declare class EmployeeContactInfo extends React.Component<IProps, IState> {
    constructor(props: IProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IProps, prevState: IState): void;
    private getPhoneNumbers();
    private getBiography();
    private getContactInfo();
    private findEmployeeContactFromIncludedData(employeeContactId);
    private queryEmployeeContact(id);
    private queryBlockAndEmployeeContact();
    private setEmployeeContact(employee);
    private getDefaultState();
}
export default EmployeeContactInfo;
export interface IProps {
    blockData: IReusableContactBlockData | Page.IPageData | null;
    blockId: string;
    includedPageData: Page.IPageData[];
    siteDomain: string;
}
export interface IState {
    title: string;
    employeeTitle: string;
    employeeName: string;
    employeeEmail: string;
    employeePhoneNumber: Array<string | undefined>;
    biography: string;
}
export interface IAxiosResponseContactInfo extends IAxios.AxiosResponse {
    data: IContactInfo;
}
export interface IContactInfo extends Page.IPage {
    included: [IEmployeeContactData];
}
export interface IContactInfoData extends Page.IPageData {
    relationships: IContactInfoRelationships;
}
export interface IContactInfoRelationships extends Page.IRelationships {
    field_employee_contact: Page.IType;
}
export interface IEmployeeContactData extends Page.IPageData {
    attributes: IEmployeeContactAttribute;
    relationships: Page.IRelationships;
}
export interface IEmployeeContactAttribute extends Page.IAttributes {
    field_biography: string;
    field_employee_e_mail: string;
    field_employee_name: string;
    field_employee_phone_number: [string];
    field_employee_title: string;
}
export interface IReusableContactBlockData extends Page.IPageData {
    relationships: IReusableContactBlockRelationship;
}
export interface IReusableContactBlockRelationship extends Page.IRelationships {
    field_employee_contact: Page.IType;
}
