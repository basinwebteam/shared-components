"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable:max-line-length - Long URL in getContactInfo
var axios_1 = require("axios");
var React = require("react");
var EmployeeContactInfo = (function (_super) {
    __extends(EmployeeContactInfo, _super);
    function EmployeeContactInfo(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this.getDefaultState();
        return _this;
    }
    EmployeeContactInfo.prototype.render = function () {
        return (React.createElement("div", { className: "contentBlock" },
            React.createElement("div", { className: "aside-block" },
                React.createElement("div", { className: "aside-header" }, this.state.title),
                React.createElement("div", { className: "aside-body" },
                    React.createElement("p", null,
                        this.state.employeeName,
                        " ",
                        React.createElement("br", null),
                        this.state.employeeTitle,
                        " ",
                        React.createElement("br", null),
                        this.getPhoneNumbers(),
                        this.getBiography()),
                    React.createElement("p", null,
                        React.createElement("a", { href: "/Contact/", title: "Contact Page" }, "Or use our Contact Form"))))));
    };
    EmployeeContactInfo.prototype.componentDidMount = function () {
        this.getContactInfo();
    };
    EmployeeContactInfo.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props.blockId !== prevProps.blockId) {
            this.getContactInfo();
        }
    };
    EmployeeContactInfo.prototype.getPhoneNumbers = function () {
        if (this.state.employeePhoneNumber.length > 0) {
            return (React.createElement("span", { dangerouslySetInnerHTML: { __html: this.state.employeePhoneNumber.join("<br />") } }));
        }
        return "";
    };
    EmployeeContactInfo.prototype.getBiography = function () {
        if (this.state.biography !== null && this.state.biography.length > 0) {
            return (React.createElement("div", { dangerouslySetInnerHTML: { __html: "<br />" + this.state.biography } }));
        }
        return "";
    };
    EmployeeContactInfo.prototype.getContactInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var employee, employeeContactId, employeeBlock;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(typeof this.props.blockData === "undefined" || this.props.blockData == null)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.queryBlockAndEmployeeContact()];
                    case 1:
                        employee = _a.sent();
                        return [3 /*break*/, 5];
                    case 2:
                        employeeContactId = this.props.blockData.relationships.field_employee_contact.data.id;
                        employeeBlock = this.findEmployeeContactFromIncludedData(employeeContactId);
                        if (!(typeof employeeBlock !== "undefined")) return [3 /*break*/, 3];
                        employee = employeeBlock.attributes;
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.queryEmployeeContact(employeeContactId)];
                    case 4:
                        employee = _a.sent();
                        _a.label = 5;
                    case 5:
                        if (typeof employee !== "undefined") {
                            this.setEmployeeContact(employee);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    EmployeeContactInfo.prototype.findEmployeeContactFromIncludedData = function (employeeContactId) {
        return this.props.includedPageData.find(function (contact) {
            return (employeeContactId === contact.id);
        });
    };
    EmployeeContactInfo.prototype.queryEmployeeContact = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/node/basin_contacts/" + id + "?include=field_employee_contact")];
                    case 1:
                        response = _a.sent();
                        if (typeof response.data.data !== "undefined") {
                            return [2 /*return*/, response.data.data.attributes];
                        }
                        return [2 /*return*/, undefined];
                }
            });
        });
    };
    EmployeeContactInfo.prototype.queryBlockAndEmployeeContact = function () {
        return __awaiter(this, void 0, void 0, function () {
            var employeeContact;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/block_content/employee_contact_info/" + this.props.blockId + "?include=field_employee_contact")];
                    case 1:
                        employeeContact = _a.sent();
                        if (employeeContact.data.included.length > 0) {
                            return [2 /*return*/, employeeContact.data.included[0].attributes];
                        }
                        return [2 /*return*/, undefined];
                }
            });
        });
    };
    EmployeeContactInfo.prototype.setEmployeeContact = function (employee) {
        this.setState({
            title: employee.title,
            employeeTitle: employee.field_employee_title,
            employeeName: employee.field_employee_name,
            employeeEmail: employee.field_employee_e_mail,
            employeePhoneNumber: employee.field_employee_phone_number,
            biography: employee.field_biography,
        });
    };
    EmployeeContactInfo.prototype.getDefaultState = function () {
        /* This tries to get the block and related child data if its part of the props blockData value.

        If any of it is missing, this returns an empty state. */
        try {
            var employeeContactId = this.props.blockData.relationships.field_employee_contact.data.id;
            var employeeBlock = this.findEmployeeContactFromIncludedData(employeeContactId);
            return {
                title: employeeBlock.attributes.title,
                employeeTitle: employeeBlock.attributes.field_employee_title,
                employeeName: employeeBlock.attributes.field_employee_name,
                employeeEmail: employeeBlock.attributes.field_employee_e_mail,
                employeePhoneNumber: employeeBlock.attributes.field_employee_phone_number,
                biography: employeeBlock.attributes.field_biography,
            };
        }
        catch (e) {
            return {
                title: "",
                employeeTitle: "",
                employeeName: "",
                employeeEmail: "",
                employeePhoneNumber: [],
                biography: "",
            };
        }
    };
    return EmployeeContactInfo;
}(React.Component));
exports.default = EmployeeContactInfo;
//# sourceMappingURL=index.js.map