import * as React from "react";
declare const AsideBlock: React.StatelessComponent<IPropsAsideBlock>;
export default AsideBlock;
export interface IPropsAsideBlock {
    title?: string;
    content: JSX.Element[] | JSX.Element;
}
