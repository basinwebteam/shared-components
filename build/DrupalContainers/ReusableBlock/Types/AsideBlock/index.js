"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Title_1 = require("../../../../components/Title");
var AsideBlock = function (_a) {
    var content = _a.content, title = _a.title;
    return (React.createElement("div", { className: "contentBlock" },
        title && (React.createElement(Title_1.default, null, title)),
        content));
};
exports.default = AsideBlock;
//# sourceMappingURL=index.js.map