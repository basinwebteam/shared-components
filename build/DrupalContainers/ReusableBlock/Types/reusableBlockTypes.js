"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EMPLOYEE_CONTACT_INFO = "block_content--employee_contact_info";
exports.BASIC_BLOCK = "block_content--basic";
exports.ENERGY_PORTFOLIO_CHART = "block_content--energy_portfolio_interactive_pie";
exports.CONTACT_LIST = "block_content--contact_list";
//# sourceMappingURL=reusableBlockTypes.js.map