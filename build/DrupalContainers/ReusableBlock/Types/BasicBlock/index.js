"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var AsideBlock_1 = require("../AsideBlock");
var Title_1 = require("../../../Title");
var BasicParagraph_1 = require("../../../BasicParagraph");
var Image_1 = require("../../../Image");
// TODO: Turn into stateful component that queries block data if props.blockData or props.includedPageData is missing
exports.BasicBlock = function (props) {
    if (props.useTitle && props.blockData.attributes.field_unique_block_title) {
        return (React.createElement(AsideBlock_1.default, { title: props.blockData.attributes.field_unique_block_title, content: getContent(props) }));
    }
    return React.createElement(AsideBlock_1.default, { content: getContent(props) });
};
var getContent = function (props) {
    var fieldContentData = props.blockData.relationships.field_content.data;
    return fieldContentData.map(function (data, i) {
        if (typeof data === "undefined") {
            throw Error("The fieldContentData array in the Basic Block component has a data item that is undefined.");
        }
        switch (data.type) {
            case ("paragraph--title"):
                return (React.createElement("div", { key: data.id, className: "aside-header" },
                    React.createElement(Title_1.TitleContainer, { paragraphId: data.id, titleParagraphData: props.includedPageData.find(function (p) { return p.id === data.id; }), siteDomain: props.siteDomain })));
            case ("paragraph--basic_paragraph"):
                return (React.createElement("div", { key: data.id, className: "aside-body" },
                    React.createElement(BasicParagraph_1.BasicParagraph, { paragraphId: data.id, data: props.includedPageData.find(function (p) { return p.id === data.id; }), siteDomain: props.siteDomain })));
            case ("paragraph--image"):
                return (React.createElement("div", { key: data.id, className: "aside-body" },
                    React.createElement(Image_1.default, { includedRelationships: props.includedPageData, paragraphId: data.id, siteDomain: props.siteDomain, className: "inlineImageLarge", imageFileFieldName: "field_image", imageParagraphName: "image" })));
            default:
                return (React.createElement("div", { key: i, className: "missing-basic-block-type" }));
        }
    });
};
exports.default = exports.BasicBlock;
//# sourceMappingURL=index.js.map