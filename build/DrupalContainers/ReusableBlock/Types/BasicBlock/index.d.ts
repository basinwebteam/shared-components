import * as React from "react";
import * as Page from "../../../../DrupalContentTypes/PageInterface";
export declare const BasicBlock: React.StatelessComponent<IProps>;
export default BasicBlock;
export interface IProps {
    useTitle: boolean;
    blockData: IBasicBlockData;
    blockId: string;
    includedPageData: Page.IPageData[];
    siteDomain: string;
}
export interface IState {
    title: string;
}
export interface IBasicBlockData extends Page.IPageData {
    relationships: IBasicBlockRelationships;
    attributes: IBasicBlockAttributes;
}
export interface IBasicBlockAttributes extends Page.IAttributes {
    field_unique_block_title: string;
}
export interface IBasicBlockRelationships extends Page.IRelationships {
    field_content: Page.IFieldContent;
}
