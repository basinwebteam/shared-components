import { AxiosResponse } from "axios";
import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
/**
 * ReusableBlock
 */
export declare class ReusableBlock extends React.Component<IReusableBlockProps, IReusableBlockState> {
    constructor(props: IReusableBlockProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IReusableBlockProps, prevState: IReusableBlockState): void;
    private updateComponent();
    private getChildComponent();
    private getBlock();
    private getBlockContent(block);
    private queryReusableBlock(id);
    private getDefaultState();
}
export default ReusableBlock;
export interface IReusableBlockProps {
    includedRelationships: Page.IPageData[];
    paragraphId: string;
    siteDomain: string;
}
export interface IReusableBlockState {
    type: string;
    id: string;
    childBlockData: Page.IPageData | null;
    includedRelationships: Page.IPageData[];
}
export interface IReusableBlockIncludedData extends Page.IPageData {
    relationships: IReusableBlockRelationships;
}
export interface IReusableBlockRelationships extends Page.IRelationships {
    field_block: Page.IType;
}
export interface IAxiosResponseBlock extends AxiosResponse {
    data: Page.IPage;
}
