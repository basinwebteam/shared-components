"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var React = require("react");
var BasicBlock_1 = require("./Types/BasicBlock");
var EmployeeContactInfo_1 = require("./Types/EmployeeContactInfo");
var EnergyPortfolioPieChart_1 = require("../EnergyPortfolioPieChart");
var ContactList_1 = require("./Types/ContactList");
var Type = require("./Types/reusableBlockTypes");
/**
 * ReusableBlock
 */
var ReusableBlock = (function (_super) {
    __extends(ReusableBlock, _super);
    function ReusableBlock(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this.getDefaultState();
        return _this;
    }
    ReusableBlock.prototype.render = function () {
        return (React.createElement("div", { className: "reusable-block " + this.state.type }, this.getChildComponent()));
    };
    ReusableBlock.prototype.componentDidMount = function () {
        this.updateComponent();
    };
    ReusableBlock.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props !== prevProps) {
            this.updateComponent();
        }
    };
    ReusableBlock.prototype.updateComponent = function () {
        var block = this.getBlock();
        if (block) {
            this.getBlockContent(block);
        }
    };
    ReusableBlock.prototype.getChildComponent = function () {
        if (this.state.childBlockData === null) {
            return (React.createElement("div", { className: "reusable-block-missing-child-block-data" }));
        }
        switch (this.state.type) {
            case (Type.BASIC_BLOCK):
                return (React.createElement(BasicBlock_1.BasicBlock, { key: this.state.id, blockId: this.state.id, blockData: this.state.childBlockData, includedPageData: this.props.includedRelationships, useTitle: true, siteDomain: this.props.siteDomain }));
            case (Type.CONTACT_LIST):
                return (React.createElement(ContactList_1.default, { key: this.state.id, blockId: this.state.id, siteDomain: this.props.siteDomain }));
            case (Type.EMPLOYEE_CONTACT_INFO):
                return (React.createElement(EmployeeContactInfo_1.default, { key: this.state.id, blockId: this.state.id, blockData: this.state.childBlockData, includedPageData: this.props.includedRelationships, siteDomain: this.props.siteDomain }));
            case Type.ENERGY_PORTFOLIO_CHART:
                return (React.createElement(EnergyPortfolioPieChart_1.default, { key: this.state.id, id: this.state.id, siteDomain: this.props.siteDomain }));
            default:
                return (React.createElement("div", { className: "Default-{this.state.type}" }));
        }
    };
    ReusableBlock.prototype.getBlock = function () {
        var _this = this;
        if (!this.props.includedRelationships) {
            return null;
        }
        var block = this.props.includedRelationships.find(function (item) {
            return (item.id === _this.props.paragraphId);
        });
        if (!block) {
            return null;
        }
        return block.relationships.field_block;
    };
    ReusableBlock.prototype.getBlockContent = function (block) {
        return __awaiter(this, void 0, void 0, function () {
            var content, contentRequest, includedRelationships;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        content = this.props.includedRelationships.find(function (item) { return item.id === block.data.id; });
                        if (!(typeof content === "undefined")) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.queryReusableBlock(this.props.paragraphId)];
                    case 1:
                        contentRequest = _a.sent();
                        includedRelationships = this.state.includedRelationships;
                        includedRelationships.push.apply(includedRelationships, contentRequest);
                        this.setState({
                            type: block.data.type,
                            id: block.data.id,
                            childBlockData: contentRequest.find(function (item) { return item.id === block.data.id; }),
                            includedRelationships: includedRelationships,
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        this.setState({
                            type: content.type,
                            id: content.id,
                            childBlockData: content,
                            includedRelationships: this.state.includedRelationships,
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    ReusableBlock.prototype.queryReusableBlock = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/paragraph/reusable_block/" + id + "?include=field_block,field_block.field_content")];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response.data.included || []];
                }
            });
        });
    };
    ReusableBlock.prototype.getDefaultState = function () {
        /*
            This tries to get this block and the child block's data.
            If any of it is missing, this returns an empty state.
         */
        var emptyState = {
            type: "",
            id: "",
            childBlockData: null,
            includedRelationships: this.props.includedRelationships,
        };
        try {
            var block_1 = this.getBlock();
            if (!block_1) {
                return emptyState;
            }
            var content_1 = this.props.includedRelationships.find(function (item) { return item.id === block_1.data.id; });
            var childBlockData = this.props.includedRelationships.find(function (item) {
                return (item.id === content_1.id);
            });
            return {
                type: content_1.type,
                id: content_1.id,
                childBlockData: childBlockData,
                includedRelationships: this.props.includedRelationships,
            };
        }
        catch (e) {
            return emptyState;
        }
    };
    return ReusableBlock;
}(React.Component));
exports.ReusableBlock = ReusableBlock;
exports.default = ReusableBlock;
//# sourceMappingURL=index.js.map