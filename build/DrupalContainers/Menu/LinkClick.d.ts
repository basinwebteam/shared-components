export declare const linkClick: (e: Event) => boolean;
export declare const showMobileMenu: () => void;
export declare const resetMenu: () => void;
export default linkClick;
