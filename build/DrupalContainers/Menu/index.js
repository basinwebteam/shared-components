"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Menu_1 = require("../../components/Menu");
var React = require("react");
var MenuContainer = (function (_super) {
    __extends(MenuContainer, _super);
    function MenuContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.linkClickHandler = _this.linkClickHandler.bind(_this);
        return _this;
    }
    MenuContainer.prototype.render = function () {
        return (React.createElement(Menu_1.Menu, { links: this.props.menuLinks, widthToChange: 840, linkHandler: this.linkClickHandler }));
    };
    MenuContainer.prototype.linkClickHandler = function (url, key, e) {
        if (typeof window !== "undefined") {
            window.location.href = e.target.href;
        }
    };
    return MenuContainer;
}(React.Component));
exports.MenuContainer = MenuContainer;
exports.default = MenuContainer;
/*
    <Link as={"/about-us"} href={"/page?id=81de6009-3f9e-46e6-b243-3d043d7727e6"}>
        <a>Annual Meeting</a>
    </Link>
*/
//# sourceMappingURL=index.js.map