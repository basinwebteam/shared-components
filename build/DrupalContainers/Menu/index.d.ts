import { ILinks } from "../../components/Menu";
import * as React from "react";
export declare class MenuContainer extends React.Component<IMenuContainerProps> {
    constructor(props: IMenuContainerProps);
    render(): JSX.Element;
    linkClickHandler(url: string, key: string, e: React.MouseEvent<HTMLElement>): void;
}
export default MenuContainer;
export interface IMenuContainerProps {
    menuLinks: ILinks[];
}
