"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.linkClick = function (e) {
    switch (e.type) {
        case "touchstart":
        default:
            return false;
        case "touchend":
        case "click":
            var target = e.target;
            var isChildLink = (linkIsChildLink(target));
            if (isChildLink) {
                exports.resetMenu();
                return true; // Navigate to the link that was clicked
            }
            else {
                showDesktopMenuDropDown(target);
                return false;
            }
    }
};
var linkIsChildLink = function (element) {
    var childClasses = [
        "menu-overview",
        "level2",
        "level3",
        "level4",
    ];
    var classMatch = false;
    childClasses.forEach(function (className) {
        classMatch = (element.parentElement && element.parentElement.classList.contains(className)) ? true : classMatch;
    });
    return classMatch;
};
var isDesktopMode = function () { return !isMobileMode(); };
var isMobileMode = function () { return (document.querySelector(".mobile-menu").clientWidth > 0); };
function showDesktopMenuDropDown(menuItem) {
    var menuItemParent = menuItem.parentElement;
    var subMenu = (menuItemParent !== null) ? menuItemParent.querySelector("ul") : null;
    if (menuItemParent && subMenu && isDesktopMode()) {
        if (closeMenuOnSecondClick(menuItem)) {
            return false;
        }
        refreshMenu();
        menuItem.classList.add("clicked");
        // Animate Height
        // Get Last sub nav's height
        var menuHeightOldElement = document.querySelector(".mega-menu li.level1 ul.visible");
        var menuHeightOld = (menuHeightOldElement === null) ? 0 : menuHeightOldElement.clientHeight;
        // Remove visible class from last sub nav
        removeClassNameFromElements("visible");
        var menuHeightNew = subMenu.clientHeight;
        subMenu.style.height = menuHeightOld.toString();
        subMenu.classList.add("visible");
        subMenu.style.height = menuHeightNew + "px";
        var visElement = document.querySelector(".mega-menu ul.visible");
        visElement.style.height = "auto";
    }
}
exports.showMobileMenu = function () {
    var menu = document.querySelector(".mega-menu");
    if (menu !== null) {
        menu.classList.add("display-important");
        menu.classList.add("menu-open");
        refreshMenu();
        showMe(document.querySelector(".mega-menu.menu-open .level1"));
        showMe(document.querySelector(".mega-menu.menu-open .level1 a"));
        hideMe(document.querySelector(".mega-menu.menu-open .level1 ul"));
    }
};
var removeClassNameFromElements = function (className) {
    var visibleElements = document.querySelectorAll(".mega-menu ." + className);
    if (visibleElements.length > 0) {
        for (var i = 0; i < visibleElements.length; i++) {
            visibleElements[i].classList.remove(className);
        }
    }
};
var closeMenuOnSecondClick = function (menuItem) {
    if (menuItem.parentElement !== null) {
        var childMenu = menuItem.parentElement.getElementsByClassName("visible");
        if (childMenu.length > 0) {
            exports.resetMenu();
            return true;
        }
    }
    return false;
};
exports.resetMenu = function () {
    if (typeof window !== "undefined") {
        if (typeof window.Event === "function") {
            var event_1 = new Event("reset_mobile_menu");
            if (typeof window !== "undefined") {
                window.dispatchEvent(event_1);
            }
        }
        else {
            /**
             * IE 11 Pollyfill for new Events
             * TODO:
             * Should be moved elsewhere but
             * should be using redux instead of events here
             */
            var CustomEvent_1 = function (event, params) {
                if (params === void 0) { params = { bubbles: false, cancelable: false, detail: undefined }; }
                params = params || { bubbles: false, cancelable: false, detail: undefined };
                var evt = document.createEvent("CustomEvent");
                evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
                return evt;
            };
            CustomEvent_1.prototype = window.Event.prototype;
            window.CustomEvent = CustomEvent_1;
            var IeEvent = CustomEvent_1("reset_mobile_menu");
            window.dispatchEvent(IeEvent);
        }
    }
};
var refreshMenu = function () {
    addRemoveMobileDesktopClasses();
    var children = document.querySelectorAll(".mega-menu li.level1 > ul");
    if (isDesktopMode()) {
        for (var i = 0; i < children.length; i++) {
            styleDesktopSubMenu(children[i]);
        }
    }
    else {
        for (var i = 0; i < children.length; i++) {
            removeStyleDesktopSubMenu(children[i]);
        }
    }
};
var addRemoveMobileDesktopClasses = function () {
    var megaMenu = document.querySelector(".mega-menu");
    var nav = document.querySelector("nav");
    if (megaMenu && nav) {
        if (isDesktopMode()) {
            // remove any mobile menu scripts
            megaMenu.classList.remove("mobile");
            nav.classList.remove("display-important");
            nav.classList.remove("menu-open");
            megaMenu.classList.add("desktop");
            megaMenu.classList.remove("mobile");
            removeClassNameFromElements("clicked");
        }
        else {
            // Removed touched classes, important for when desktop menu is switched to mobile during landscape/portrait flip
            megaMenu.classList.remove("touched");
            megaMenu.classList.add("mobile");
            megaMenu.classList.remove("desktop");
        }
    }
};
var styleDesktopSubMenu = function (item) {
    var megaMenu = document.querySelector(".mega-menu");
    var topBarSection = document.querySelector(".top-bar-section");
    var dropdownMenuContainer = (item.parentElement &&
        item.parentElement.parentElement &&
        item.parentElement.parentElement.parentElement &&
        item.parentElement.parentElement.parentElement.parentElement) ? item.parentElement.parentElement.parentElement.parentElement : null;
    if (megaMenu && topBarSection && dropdownMenuContainer) {
        var pageWidth = topBarSection.clientWidth;
        var padding = (megaMenu.clientWidth - pageWidth) / 2;
        // No negative padding
        if (padding < 0) {
            padding = 0;
        }
        var listLeftOffset = dropdownMenuContainer.offsetLeft + padding;
        item.style.width = megaMenu.clientWidth.toString() + "px";
        item.style.paddingLeft = padding.toString() + "px";
        item.style.paddingRight = padding.toString() + "px";
        item.style.left = "-" + (listLeftOffset) + "px";
    }
};
var removeStyleDesktopSubMenu = function (item) {
    // return object to style css
    item.style.width = "100%";
    item.style.left = "0px";
    item.style.padding = "0px";
};
var hideMe = function (element) {
    element.classList.remove("showMe");
    element.classList.add("hideMe");
};
var showMe = function (element) {
    element.classList.remove("hideMe");
    element.classList.add("showMe");
};
exports.default = exports.linkClick;
//# sourceMappingURL=LinkClick.js.map