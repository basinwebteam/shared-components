"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var TopicBox_1 = require("../TopicBox");
exports.TopicBoxRowContainer = function (props) {
    var getTopicBoxes = function () {
        var includedRelationships = props.includedRelationships, paragraphId = props.paragraphId, siteDomain = props.siteDomain;
        var rowData = includedRelationships.find(function (paragraph) { return paragraph.id === paragraphId; });
        return rowData.relationships.field_topic_row.data.map(function (topicBox, i) {
            if (typeof topicBox !== "undefined") {
                return (React.createElement(TopicBox_1.default, { key: topicBox.id, paragraphId: topicBox.id, includedRelationships: includedRelationships, siteDomain: siteDomain }));
            }
            return "";
        });
    };
    return (React.createElement("div", { className: "row topic-box-row" }, getTopicBoxes()));
};
exports.TopicBoxRowContainer.displayName = "DrupalTopicBoxContainer";
exports.default = exports.TopicBoxRowContainer;
//# sourceMappingURL=index.js.map