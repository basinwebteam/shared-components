import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare const TopicBoxRowContainer: React.SFC<ITopicBoxRowProps>;
export default TopicBoxRowContainer;
export interface ITopicBoxRowProps {
    includedRelationships: Page.IPageData[];
    paragraphId: string;
    siteDomain: string;
}
export interface ITopicBoxRowData extends Page.IPageData {
    relationships: ITopicBoxDataRelationships;
}
export interface ITopicBoxDataRelationships extends Page.IRelationships {
    field_topic_row: Page.IFieldContent;
}
