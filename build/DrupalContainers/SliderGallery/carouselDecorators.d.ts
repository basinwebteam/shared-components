/// <reference types="nuka-carousel" />
import * as React from "react";
import { IDecorators } from "nuka-carousel";
export declare const LeftButton: React.StatelessComponent<IProps>;
declare const DefaultDecorators: IDecorators[];
export default DefaultDecorators;
export interface IProps {
    nextSlide: () => void;
    previousSlide: () => void;
    currentSlide: number;
    wrapAround: boolean;
    slidesToScroll: number;
    slideCount: number;
}
