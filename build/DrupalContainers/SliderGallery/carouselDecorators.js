"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.LeftButton = function (props) {
    var handleClick = function (e) {
        e.preventDefault();
        props.previousSlide();
    };
    var getButtonStyles = function (disabled) {
        return {
            border: 0,
            background: "rgba(0,159,223,0.75)",
            color: "white",
            padding: 16,
            outline: 0,
            opacity: disabled ? 0.3 : 1,
            cursor: "pointer",
        };
    };
    return (React.createElement("button", { className: "slick-prev", style: getButtonStyles(props.currentSlide === 0 && !props.wrapAround), onClick: handleClick },
        React.createElement("span", { className: "prev" }, "PREV")));
};
var RightButton = function (props) {
    var handleClick = function (e) {
        e.preventDefault();
        props.nextSlide();
    };
    var getButtonStyles = function (disabled) {
        return {
            border: 0,
            background: "rgba(0,159,223,0.75)",
            color: "white",
            padding: 16,
            outline: 0,
            opacity: disabled ? 0.3 : 1,
            cursor: "pointer",
        };
    };
    return (React.createElement("button", { className: "slick-next", style: getButtonStyles(props.currentSlide + props.slidesToScroll >= props.slideCount && !props.wrapAround), onClick: handleClick },
        React.createElement("span", { className: "next" }, "NEXT")));
};
var DefaultDecorators = [
    {
        component: exports.LeftButton,
        position: "CenterLeft",
    },
    {
        component: RightButton,
        position: "CenterRight",
    },
];
exports.default = DefaultDecorators;
//# sourceMappingURL=carouselDecorators.js.map