import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
import { ITopicBoxWebsiteLink } from "../TopicBox";
export declare class SliderGalleryContainer extends React.Component<ISliderGalContProps, ISliderGalContState> {
    constructor(props: ISliderGalContProps);
    displayName: string;
    render(): JSX.Element;
    defaultSlide(slide: ISlide, i: number): JSX.Element;
    componentDidMount(): void;
    updateComponent(queryIncludedData?: boolean): Promise<void>;
    getDefaultState(): ISliderGalContState;
    checkUrlForEntity(link: ITopicBoxWebsiteLink): boolean;
    checkUrlStringForEntity(link: string): boolean;
    getGalleryType(gallery: ISliderGalleryPageData): GalleryType | undefined;
    getUrl(link: ITopicBoxWebsiteLink): Promise<string | null>;
    getClassName(): string;
    getDecorator(): any;
}
export default SliderGalleryContainer;
export interface ISliderGalContProps {
    includedRelationships: Array<Page.IPageData | ISliderGalleryPageData | any>;
    paragraphId: string;
    siteDomain: string;
    carousel?: React.ComponentClass<any>;
    slide?: React.Component;
    decorators?: IDecorators[];
    className?: string;
    autoplay?: boolean;
}
export interface ICarouselProps {
    decorators?: IDecorators[];
    wrapAround?: boolean;
}
export interface ISliderGalContState {
    slides: ISlide[];
    galleryType?: GalleryType;
}
export interface ISlide {
    imageUrl: string;
    imageAlt: string;
    caption: string;
    link: string;
}
export interface ISliderGalleryData extends Page.IType {
    data: ISliderGalleryPageData;
}
export interface ISliderGalleryPageData extends Page.IPageData {
    relationships: ISliderGalleryRelationships;
}
export interface ISliderGalleryRelationships extends Page.IRelationships {
    field_image_gallery_type?: Page.IType;
    field_slider: {
        data: ISlideData[];
    };
}
export interface ISlideData extends Page.IPageData {
    attributes: ISlideAttributes;
    relationships: ISlideRelationships;
}
export interface ISlideAttributes extends Page.IAttributes {
    field_caption: {
        value: string;
        format: string;
    } | null;
    field_website_link?: ITopicBoxWebsiteLink;
}
export interface ISlideRelationships extends Page.IRelationships {
    field_image: Page.IField_Image;
}
export interface IDecorators {
    component?: React.StatelessComponent;
    position?: decoratorPosition;
    style?: React.CSSProperties;
}
export declare type decoratorPosition = "TopLeft" | "TopCenter" | "TopRight" | "CenterLeft" | "CenterCenter" | "CenterRight" | "BottomLeft" | "BottomCenter" | "BottomRight";
export declare type GalleryType = "Horizontal" | "Vertical";
