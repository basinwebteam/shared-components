"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.ArticleLeftButton = function (props) {
    var handleClick = function (e) {
        e.preventDefault();
        props.previousSlide();
    };
    return (React.createElement("button", { className: "slick-prev", style: getButtonStyles(props.currentSlide === 0 && !props.wrapAround), onClick: handleClick },
        React.createElement("span", { className: "prev" }, "PREV")));
};
exports.ArticleRightButton = function (props) {
    var handleClick = function (e) {
        e.preventDefault();
        props.nextSlide();
    };
    return (React.createElement("button", { className: "slick-next", style: getButtonStyles(props.currentSlide + props.slidesToScroll >= props.slideCount && !props.wrapAround), onClick: handleClick },
        React.createElement("span", { className: "next" }, "NEXT")));
};
var getButtonStyles = function (disabled) {
    return {
        border: 0,
        background: "#FFF",
        color: "rgba(0,159,223,0.75)",
        padding: "16px 4px",
        outline: 0,
        opacity: disabled ? 0.3 : 1,
        cursor: "pointer",
    };
};
var ArticlePhotoGalleryDecorators = [
    {
        component: exports.ArticleLeftButton,
        position: "BottomLeft",
    },
    {
        component: exports.ArticleRightButton,
        position: "BottomRight",
    },
];
exports.default = ArticlePhotoGalleryDecorators;
//# sourceMappingURL=articlePhotoGalleryDecoratorsVertical.js.map