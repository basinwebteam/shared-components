"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var sitemap_1 = require("../../DrupalAPI/sitemap");
var carouselDecorators_1 = require("./carouselDecorators");
var articlePhotoGalleryDecorators_1 = require("./articlePhotoGalleryDecorators");
var articlePhotoGalleryDecoratorsVertical_1 = require("./articlePhotoGalleryDecoratorsVertical");
//TODO: Replace Carousel with a TypeScript component...
var SliderGalleryContainer = (function (_super) {
    __extends(SliderGalleryContainer, _super);
    function SliderGalleryContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.displayName = "DrupalSliderGalleryContainer";
        _this.state = _this.getDefaultState();
        return _this;
    }
    SliderGalleryContainer.prototype.render = function () {
        var _this = this;
        var Carousel = this.props.carousel;
        var autoplay = this.props.autoplay === undefined ? true : this.props.autoplay;
        var galleryType = this.state.galleryType;
        var decorator = this.getDecorator();
        if (Carousel) {
            return (React.createElement("div", { className: this.getClassName() },
                React.createElement(Carousel, { decorators: decorator, wrapAround: true, autoplay: autoplay }, this.state.slides.map(function (slide, i) {
                    return _this.props.slide || _this.defaultSlide(slide, i);
                }))));
        }
        return React.createElement("div", { className: "slider-gallery" });
    };
    SliderGalleryContainer.prototype.defaultSlide = function (slide, i) {
        if (slide.link === "#" || slide.link === "") {
            return (React.createElement("span", { key: i },
                React.createElement("img", { alt: slide.imageAlt, src: slide.imageUrl }),
                React.createElement("div", { className: "caption", dangerouslySetInnerHTML: { __html: slide.caption } })));
        }
        else {
            return (React.createElement("a", { href: slide.link, key: i },
                React.createElement("img", { alt: slide.imageAlt, src: slide.imageUrl }),
                React.createElement("div", { className: "caption", dangerouslySetInnerHTML: { __html: slide.caption } })));
        }
    };
    SliderGalleryContainer.prototype.componentDidMount = function () {
        this.updateComponent();
    };
    SliderGalleryContainer.prototype.updateComponent = function (queryIncludedData) {
        if (queryIncludedData === void 0) { queryIncludedData = false; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var slides;
            return __generator(this, function (_a) {
                slides = this.state.slides.map(function (slide, i) { return __awaiter(_this, void 0, void 0, function () {
                    var url;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!this.checkUrlStringForEntity(slide.link)) return [3 /*break*/, 2];
                                return [4 /*yield*/, this.getUrl({ uri: slide.link })];
                            case 1:
                                url = _a.sent();
                                if (url !== null) {
                                    slide.link = url;
                                }
                                _a.label = 2;
                            case 2: return [2 /*return*/, slide];
                        }
                    });
                }); });
                Promise.all(slides).then(function (newSlides) {
                    _this.setState({ slides: newSlides });
                });
                return [2 /*return*/];
            });
        });
    };
    SliderGalleryContainer.prototype.getDefaultState = function () {
        var _this = this;
        /* This tries to get the block and related child data if its part of the props blockData value.

        If any of it is missing, this returns an empty state. */
        try {
            var relationships_1 = this.props.includedRelationships;
            var gallery = this.props.includedRelationships.find(function (p) { return p.id === _this.props.paragraphId; });
            var galleryData = gallery.relationships.field_slider.data;
            var slides = galleryData.map(function (slide) {
                var slideData = relationships_1.find(function (p) { return p.id === slide.id; });
                var imageFile = relationships_1.find(function (p) { return p.id === slideData.relationships.field_image.data.id; });
                return {
                    imageUrl: _this.props.siteDomain + imageFile.attributes.url,
                    imageAlt: slideData.relationships.field_image.data.meta.alt,
                    caption: slideData.attributes.field_caption ? slideData.attributes.field_caption.value : "",
                    link: slideData.attributes.field_website_link ? slideData.attributes.field_website_link.uri : "#",
                };
            });
            return {
                slides: slides,
                galleryType: this.getGalleryType(gallery)
            };
        }
        catch (e) {
            return {
                slides: [],
                galleryType: undefined
            };
        }
    };
    SliderGalleryContainer.prototype.checkUrlForEntity = function (link) {
        if (link !== null && link.uri !== null) {
            if (link.uri.substr(0, 11) === "entity:node") {
                return true;
            }
        }
        return false;
    };
    SliderGalleryContainer.prototype.checkUrlStringForEntity = function (link) {
        if (link.substr(0, 11) === "entity:node") {
            return true;
        }
        return false;
    };
    SliderGalleryContainer.prototype.getGalleryType = function (gallery) {
        if (gallery.relationships.field_image_gallery_type !== undefined) {
            if (gallery.relationships.field_image_gallery_type.data !== undefined) {
                switch (gallery.relationships.field_image_gallery_type.data.id) {
                    case ("921f9001-3bea-46e5-a6a4-a8262fedf907"):
                        return "Vertical";
                    case ("b4a047c2-0397-4c6c-a8e7-2318b7eb41cc"):
                    default:
                        return "Horizontal";
                }
            }
        }
        return undefined;
    };
    SliderGalleryContainer.prototype.getUrl = function (link) {
        return __awaiter(this, void 0, void 0, function () {
            var map, sitemap, matchedPage, newsSitemap, matchedNewsPage;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        map = this.props.siteDomain
                            .replace("https://", "")
                            .replace("//", "")
                            .replace(".com", "");
                        if (!this.checkUrlForEntity(link)) return [3 /*break*/, 3];
                        return [4 /*yield*/, sitemap_1.GetSiteMap(map)];
                    case 1:
                        sitemap = _a.sent();
                        matchedPage = sitemap.find(function (page) {
                            return page.attributes.nid.toString() === link.uri.substr(12);
                        });
                        if (typeof matchedPage !== "undefined") {
                            return [2 /*return*/, matchedPage.attributes.path.alias];
                        }
                        return [4 /*yield*/, sitemap_1.GetSiteMap("news")];
                    case 2:
                        newsSitemap = _a.sent();
                        matchedNewsPage = newsSitemap.find(function (page) {
                            return page.attributes.nid.toString() === link.uri.substr(12);
                        });
                        if (typeof matchedNewsPage !== "undefined") {
                            return [2 /*return*/, matchedNewsPage.attributes.path.alias];
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/, (link !== null && link.uri !== null) ? link.uri : null];
                }
            });
        });
    };
    SliderGalleryContainer.prototype.getClassName = function () {
        var className = 'slider-gallery';
        if (this.props.className) {
            className = " " + this.props.className;
        }
        if (this.state.galleryType) {
            className += " slider-gallery-article-" + this.state.galleryType.toLowerCase();
        }
        return className;
    };
    SliderGalleryContainer.prototype.getDecorator = function () {
        if (this.props.decorators) {
            return this.props.decorators;
        }
        if (this.state.galleryType === "Horizontal") {
            return articlePhotoGalleryDecorators_1.default;
        }
        if (this.state.galleryType === "Vertical") {
            return articlePhotoGalleryDecoratorsVertical_1.default;
        }
        return carouselDecorators_1.default;
    };
    return SliderGalleryContainer;
}(React.Component));
exports.SliderGalleryContainer = SliderGalleryContainer;
exports.default = SliderGalleryContainer;
//# sourceMappingURL=index.js.map