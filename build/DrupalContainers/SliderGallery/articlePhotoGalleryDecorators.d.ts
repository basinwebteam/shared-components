/// <reference types="nuka-carousel" />
import * as React from "react";
import { IDecorators } from "nuka-carousel";
export declare const ArticleLeftButton: React.StatelessComponent<IProps>;
export declare const ArticleRightButton: React.StatelessComponent<IProps>;
declare const ArticlePhotoGalleryDecorators: IDecorators[];
export default ArticlePhotoGalleryDecorators;
export interface IProps {
    nextSlide: () => void;
    previousSlide: () => void;
    currentSlide: number;
    wrapAround: boolean;
    slidesToScroll: number;
    slideCount: number;
}
