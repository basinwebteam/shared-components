import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare class ImageGalleryContainer extends React.Component<IImageGalleryProps, IImageGalleryState> {
    constructor(props: IImageGalleryProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IImageGalleryProps, prevState: IImageGalleryState): void;
    updateComponent(): void;
    private getImageDescription(id);
    private getFieldCaption(caption);
    private getImages();
    private getDefaultState();
}
export default ImageGalleryContainer;
export interface IImageGalleryProps {
    includedRelationships: Array<Page.IPageData | any>;
    paragraphId: string;
    siteDomain: string;
}
export interface IImageGalleryState {
    images: IImage[];
}
export interface IImage {
    id: string;
    description: string;
}
export interface IImageGalleryParagraph extends Page.IPageData {
    relationships: IImageGalleryRelationships;
}
export interface IImageGalleryRelationships extends Page.IRelationships {
    field_gallery_image: {
        data: Page.IDatum[];
    };
}
export interface IImageParagraph extends Page.IPageData {
    attributes: IImageAttributes;
}
export interface IImageAttributes extends Page.IAttributes {
    field_caption: Page.IWYSIWYG;
}
