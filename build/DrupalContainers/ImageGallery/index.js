"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Image_1 = require("../Image");
var ImageGalleryContainer = (function (_super) {
    __extends(ImageGalleryContainer, _super);
    function ImageGalleryContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this.getDefaultState();
        return _this;
    }
    ImageGalleryContainer.prototype.render = function () {
        var _this = this;
        if (this.state.images.length > 0) {
            return (React.createElement("div", { className: "row photo-gallery" }, this.state.images.map(function (image, i) { return (React.createElement("div", { className: "column small-6 medium-4 large-3 photo-gallery-image", key: image.id + i },
                React.createElement(Image_1.default, { paragraphId: image.id, includedRelationships: _this.props.includedRelationships, siteDomain: _this.props.siteDomain }),
                React.createElement("div", { className: "photo-gallery-image-description", style: { minHeight: "5rem" }, dangerouslySetInnerHTML: { __html: image.description } }))); })));
        }
        else {
            return (React.createElement("div", { className: "row photo-gallery" }));
        }
    };
    ImageGalleryContainer.prototype.componentDidMount = function () {
        if (this.state.images.length === 0) {
            this.updateComponent();
        }
    };
    ImageGalleryContainer.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateComponent();
        }
    };
    ImageGalleryContainer.prototype.updateComponent = function () {
        var images = this.getImages();
        this.setState(images);
    };
    ImageGalleryContainer.prototype.getImageDescription = function (id) {
        var image = this.props.includedRelationships.find(function (paragraph) { return id === paragraph.id; });
        return (typeof image !== "undefined") ? this.getFieldCaption(image.attributes.field_caption) : "";
    };
    ImageGalleryContainer.prototype.getFieldCaption = function (caption) {
        if (caption === null) {
            return "";
        }
        if (typeof caption === "string") {
            return caption;
        }
        if (caption.hasOwnProperty("value")) {
            return caption.value;
        }
        return "";
    };
    ImageGalleryContainer.prototype.getImages = function () {
        var _this = this;
        var ImageParagraph = this.props.includedRelationships.find(function (paragraph) { return paragraph.id === _this.props.paragraphId; });
        var images = ImageParagraph.relationships.field_gallery_image.data.map(function (image) {
            return {
                id: image.id,
                description: _this.getImageDescription(image.id),
            };
        });
        return {
            images: images,
        };
    };
    ImageGalleryContainer.prototype.getDefaultState = function () {
        try {
            return this.getImages();
        }
        catch (e) {
            return {
                images: new Array(),
            };
        }
    };
    return ImageGalleryContainer;
}(React.Component));
exports.ImageGalleryContainer = ImageGalleryContainer;
exports.default = ImageGalleryContainer;
//# sourceMappingURL=index.js.map