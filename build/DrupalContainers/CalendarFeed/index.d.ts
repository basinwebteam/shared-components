import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
import { IEventProps } from "../../components/CalendarFeed/Event";
export declare class CalendarContainer extends React.Component<ICalendarFeedProps, ICalendarFeedState> {
    constructor(props: ICalendarFeedProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: ICalendarFeedProps, prevState: ICalendarFeedState): void;
    updateComponent(): Promise<void>;
    private getFeedUrl();
}
export default CalendarContainer;
export interface ICalendarFeedProps {
    paragraphId: string;
    includedRelationships: Page.IPageData[];
}
export interface ICalendarFeedState {
    events: IEventProps[];
}
