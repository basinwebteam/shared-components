import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare const Paragraph: React.StatelessComponent<IParagraph>;
export declare const getParagraphs: (props: IParagraph) => (JSX.Element | undefined)[];
export declare function getParagraphData<T extends Page.IPageData>(id: string, relationships: T[]): T;
export default Paragraph;
export interface IParagraph {
    items: Array<Page.IDatum | undefined>;
    includedRelationships: Page.IPageData[];
    siteDomain: string;
    carousel?: React.ComponentClass<any>;
}
