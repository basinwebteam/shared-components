"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Type = require("../../DrupalContentTypes/ParagraphTypes");
var BasicParagraph_1 = require("../BasicParagraph");
var BasicBlock_1 = require("../ReusableBlock/Types/BasicBlock");
var Award_1 = require("../Award");
var Biography_1 = require("../Biography");
var Button_1 = require("../Button");
var BoardOfDirectors_1 = require("../BoardOfDirectors");
var CalendarFeed_1 = require("../CalendarFeed");
var DataView_1 = require("../DataView");
var Image_1 = require("../Image");
var ImageGallery_1 = require("../ImageGallery");
var ReusableBlock_1 = require("../ReusableBlock");
var SliderGallery_1 = require("../SliderGallery");
var Title_1 = require("../Title");
var TopicBox_1 = require("../TopicBox");
var TopicBoxRow_1 = require("../TopicBoxRow");
var VideoGallery_1 = require("../VideoGallery");
var Vimeo_1 = require("../Vimeo");
var YouTube_1 = require("../YouTube");
exports.Paragraph = function (props) { return (React.createElement("div", null, exports.getParagraphs(props))); };
exports.getParagraphs = function (props) {
    return props.items.map(function (paragraph, i) {
        if (typeof paragraph === "undefined") {
            return;
        }
        var key = paragraph.id + i;
        switch (paragraph.type) {
            case Type.BASIC_PARAGRAPH:
                return (React.createElement(BasicParagraph_1.BasicParagraph, { key: key, paragraphId: paragraph.id, data: getParagraphData(paragraph.id, props.includedRelationships), siteDomain: props.siteDomain }));
            case Type.AWARD:
                return (React.createElement(Award_1.default, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships, siteDomain: props.siteDomain }));
            case Type.BIOGRAPHY:
                return (React.createElement(Biography_1.default, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships, siteDomain: props.siteDomain }));
            case Type.BUTTON:
                return (React.createElement(Button_1.default, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships, siteDomain: props.siteDomain }));
            case Type.BOARD_OF_DIRECTORS:
                return (React.createElement(BoardOfDirectors_1.default, { key: key, paragraphId: paragraph.id, siteDomain: props.siteDomain }));
            case Type.DATA_VIEW:
                return (React.createElement(DataView_1.default, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships, siteDomain: props.siteDomain }));
            case Type.IMAGE:
                return (React.createElement(Image_1.default, { key: key, includedRelationships: props.includedRelationships, paragraphId: paragraph.id, siteDomain: props.siteDomain, className: "inlineImageLarge" }));
            case Type.IMAGE_ASIDE:
                return (React.createElement(Image_1.default, { key: key, includedRelationships: props.includedRelationships, paragraphId: paragraph.id, siteDomain: props.siteDomain, className: "inlineImage", imageFileFieldName: "field_image_aside", imageParagraphName: "image_aside" }));
            case Type.IMAGE_BANNER:
                return (React.createElement(Image_1.default, { key: key, includedRelationships: props.includedRelationships, paragraphId: paragraph.id, siteDomain: props.siteDomain, className: "image-banner" }));
            case Type.IMAGE_GALLERY:
                return (React.createElement(ImageGallery_1.default, { key: key, includedRelationships: props.includedRelationships, siteDomain: props.siteDomain, paragraphId: paragraph.id }));
            case Type.INCONTENT_IMAGE_GALLERY:
                return (React.createElement(SliderGallery_1.SliderGalleryContainer, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships, siteDomain: props.siteDomain, carousel: props.carousel, autoplay: false }));
            case Type.RESUABLE_BLOCK:
                return (React.createElement(ReusableBlock_1.default, { key: key, paragraphId: paragraph.id, siteDomain: props.siteDomain, includedRelationships: props.includedRelationships }));
            case Type.SHAREPOINT_CALENDAR:
                return (React.createElement(CalendarFeed_1.default, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships }));
            case Type.SLIDER_GALLERY:
                if (props.carousel) {
                    return (React.createElement(SliderGallery_1.SliderGalleryContainer, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships, siteDomain: props.siteDomain, carousel: props.carousel }));
                }
            case Type.TITLE:
                return (React.createElement(Title_1.TitleContainer, { key: key, paragraphId: paragraph.id, titleParagraphData: getParagraphData(paragraph.id, props.includedRelationships), siteDomain: props.siteDomain }));
            case Type.TOPIC_BOX_ROW:
                return (React.createElement(TopicBoxRow_1.default, { key: key, paragraphId: paragraph.id, siteDomain: props.siteDomain, includedRelationships: props.includedRelationships }));
            case Type.TOPIC_BOX:
                return (React.createElement(TopicBox_1.default, { key: key, paragraphId: paragraph.id, siteDomain: props.siteDomain, includedRelationships: props.includedRelationships }));
            case Type.UNIQUE_BASIC_BLOCK:
                return (React.createElement(BasicBlock_1.BasicBlock, { key: key, useTitle: true, blockId: paragraph.id, blockData: getParagraphData(paragraph.id, props.includedRelationships), includedPageData: props.includedRelationships, siteDomain: props.siteDomain }));
            case Type.VIMEO:
                return (React.createElement(Vimeo_1.default, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships }));
            case Type.VIDEO_GALLERY:
                return (React.createElement(VideoGallery_1.default, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships, siteDomain: props.siteDomain }));
            case Type.YOUTUBE:
                return (React.createElement(YouTube_1.default, { key: key, paragraphId: paragraph.id, includedRelationships: props.includedRelationships }));
            default:
                return (React.createElement("div", { className: paragraph.type, key: i }));
        }
    });
};
function getParagraphData(id, relationships) {
    return relationships.find(function (paragraph) {
        return paragraph.id === id;
    });
}
exports.getParagraphData = getParagraphData;
exports.default = exports.Paragraph;
//# sourceMappingURL=index.js.map