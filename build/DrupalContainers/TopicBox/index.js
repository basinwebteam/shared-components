"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var React = require("react");
var Type = require("../../DrupalContentTypes/ParagraphTypes");
var BasicParagraph_1 = require("../BasicParagraph");
var sitemap_1 = require("../../DrupalAPI/sitemap");
var Image_1 = require("../Image");
var TopicBox_1 = require("../../components/TopicBox");
var TopicBoxContainer = (function (_super) {
    __extends(TopicBoxContainer, _super);
    function TopicBoxContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.displayName = "DrupalTopicBoxContainer";
        _this.state = _this.getDefaultState();
        return _this;
    }
    TopicBoxContainer.prototype.render = function () {
        var className = this.getColumnClassWidth();
        return (React.createElement(TopicBox_1.default, { title: this.state.title, className: className, image: this.getImage(), paragraph: this.getBasicParagraph(), color: this.state.titleColor.toLowerCase(), url: this.state.url }));
    };
    TopicBoxContainer.prototype.getColumnClassWidth = function () {
        switch (this.state.width) {
            case (0):
            default:
                return "medium-4";
            case (1):
                return "medium-8";
            case (2):
                return "small-12";
        }
    };
    TopicBoxContainer.prototype.getBasicParagraph = function () {
        var _this = this;
        if (this.state.basicParagraphId !== null) {
            var data = this.state.includedRelationships.find(function (paragraph) { return paragraph.id === _this.state.basicParagraphId; });
            return (React.createElement(BasicParagraph_1.BasicParagraph, { paragraphId: this.state.basicParagraphId, data: data, siteDomain: this.props.siteDomain }));
        }
        return "";
    };
    TopicBoxContainer.prototype.getImage = function () {
        if (this.state.imageId !== null) {
            return (React.createElement(Image_1.default, { className: "image-topic-box", paragraphId: this.state.imageId, siteDomain: this.props.siteDomain, includedRelationships: this.state.includedRelationships, imageParagraphName: "image_topic_box" }));
        }
        return "";
    };
    TopicBoxContainer.prototype.componentDidMount = function () {
        this.updateComponent(true);
    };
    TopicBoxContainer.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateComponent();
        }
    };
    TopicBoxContainer.prototype.updateComponent = function (queryIncludedData) {
        if (queryIncludedData === void 0) { queryIncludedData = false; }
        return __awaiter(this, void 0, void 0, function () {
            var topicBox, url;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        topicBox = this.getTopicBoxData();
                        if (!(typeof topicBox === "undefined")) return [3 /*break*/, 1];
                        this.queryTopicBoxData();
                        return [3 /*break*/, 4];
                    case 1:
                        if (!queryIncludedData) return [3 /*break*/, 2];
                        if (this.state.imageId === null || this.state.basicParagraphId === null) {
                            this.queryTopicBoxData();
                        }
                        return [3 /*break*/, 4];
                    case 2:
                        if (!this.checkUrlStringForEntity(this.state.url)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.getUrl({ uri: this.state.url })];
                    case 3:
                        url = _a.sent();
                        if (url !== null) {
                            this.setState({ url: url });
                        }
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    TopicBoxContainer.prototype.queryTopicBoxData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, topicBox, includedRelationships, _a, imageData, basicParagraphData, titleColor, titleColorData, url;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/topicbox/" + this.props.paragraphId)];
                    case 1:
                        response = _b.sent();
                        topicBox = response.data.data;
                        includedRelationships = this.state.includedRelationships;
                        if (typeof response.data.included !== "undefined") {
                            includedRelationships.push.apply(includedRelationships, response.data.included);
                        }
                        _a = this.getImageAndBasicParagraphData(topicBox), imageData = _a.imageData, basicParagraphData = _a.basicParagraphData;
                        if (topicBox.relationships.field_title_color.data !== null) {
                            titleColorData = this.props.includedRelationships.find(function (p) { return p.id === topicBox.relationships.field_title_color.data.id; });
                            titleColor = (typeof titleColorData !== "undefined") ? titleColorData.attributes.name : this.state.titleColor;
                        }
                        else {
                            titleColor = "Blue";
                        }
                        return [4 /*yield*/, this.getUrl(topicBox.attributes.field_website_link)];
                    case 2:
                        url = _b.sent();
                        this.setState({
                            title: topicBox.attributes.field_title,
                            titleColor: titleColor,
                            width: topicBox.attributes.field_topic_box_width,
                            url: url || "",
                            imageId: (typeof imageData !== "undefined") ? imageData.id : this.state.imageId,
                            basicParagraphId: (typeof basicParagraphData !== "undefined") ? basicParagraphData.id : this.state.basicParagraphId,
                            includedRelationships: includedRelationships,
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    TopicBoxContainer.prototype.getDefaultState = function () {
        try {
            var topicBox_1 = this.getTopicBoxData();
            var _a = this.getImageAndBasicParagraphData(topicBox_1), imageData = _a.imageData, basicParagraphData = _a.basicParagraphData;
            var titleColorData = this.props.includedRelationships.find(function (p) { return p.id === topicBox_1.relationships.field_title_color.data.id; });
            var titleColor = (typeof titleColorData !== "undefined") ? titleColorData.attributes.name : null;
            return {
                title: topicBox_1.attributes.field_title,
                width: topicBox_1.attributes.field_topic_box_width,
                titleColor: titleColor,
                url: (topicBox_1.attributes.field_website_link !== null) ? topicBox_1.attributes.field_website_link.uri : null,
                imageId: (typeof imageData !== "undefined") ? imageData.id : null,
                basicParagraphId: (typeof basicParagraphData !== "undefined") ? basicParagraphData.id : null,
                includedRelationships: this.props.includedRelationships,
            };
        }
        catch (e) {
            return {
                title: "",
                width: 1,
                titleColor: "Blue",
                url: "",
                imageId: "",
                basicParagraphId: "",
                includedRelationships: this.props.includedRelationships,
            };
        }
    };
    TopicBoxContainer.prototype.checkUrlForEntity = function (link) {
        if (link !== null && link.uri !== null) {
            if (link.uri.substr(0, 11) === "entity:node") {
                return true;
            }
        }
        return false;
    };
    TopicBoxContainer.prototype.checkUrlStringForEntity = function (link) {
        if (link.substr(0, 11) === "entity:node") {
            return true;
        }
        return false;
    };
    TopicBoxContainer.prototype.getUrl = function (link) {
        return __awaiter(this, void 0, void 0, function () {
            var map, sitemap, matchedPage, newsSitemap, matchedNewsPage;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        map = this.props.siteDomain
                            .replace("https://", "")
                            .replace("//", "")
                            .replace(".com", "");
                        if (!this.checkUrlForEntity(link)) return [3 /*break*/, 3];
                        return [4 /*yield*/, sitemap_1.GetSiteMap(map)];
                    case 1:
                        sitemap = _a.sent();
                        matchedPage = sitemap.find(function (page) {
                            return page.attributes.nid.toString() === link.uri.substr(12);
                        });
                        if (typeof matchedPage !== "undefined") {
                            return [2 /*return*/, matchedPage.attributes.path.alias];
                        }
                        return [4 /*yield*/, sitemap_1.GetSiteMap("news")];
                    case 2:
                        newsSitemap = _a.sent();
                        matchedNewsPage = newsSitemap.find(function (page) {
                            return page.attributes.nid.toString() === link.uri.substr(12);
                        });
                        if (typeof matchedNewsPage !== "undefined") {
                            return [2 /*return*/, matchedNewsPage.attributes.path.alias];
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/, (link !== null && link.uri !== null) ? link.uri : null];
                }
            });
        });
    };
    TopicBoxContainer.prototype.getTopicBoxData = function () {
        var _this = this;
        return this.props.includedRelationships.find(function (p) { return p.id === _this.props.paragraphId; });
    };
    TopicBoxContainer.prototype.getImageAndBasicParagraphData = function (topicBox) {
        var imageData = topicBox.relationships.field_content.data.find(function (data) { return data.type === Type.IMAGE_TOPIC_BOX; });
        var basicParagraphData = topicBox.relationships.field_content.data.find(function (data) { return data.type === Type.BASIC_PARAGRAPH; });
        return { imageData: imageData, basicParagraphData: basicParagraphData };
    };
    return TopicBoxContainer;
}(React.Component));
exports.TopicBoxContainer = TopicBoxContainer;
exports.default = TopicBoxContainer;
//# sourceMappingURL=index.js.map