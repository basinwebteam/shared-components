import { AxiosResponse } from "axios";
import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare class TopicBoxContainer extends React.Component<ITopicBoxProps, ITopicBoxState> {
    constructor(props: ITopicBoxProps);
    displayName: string;
    render(): JSX.Element;
    getColumnClassWidth(): "medium-4" | "medium-8" | "small-12";
    getBasicParagraph(): "" | JSX.Element;
    getImage(): "" | JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: ITopicBoxProps, prevState: ITopicBoxState): void;
    updateComponent(queryIncludedData?: boolean): Promise<void>;
    private queryTopicBoxData();
    private getDefaultState();
    private checkUrlForEntity(link);
    private checkUrlStringForEntity(link);
    private getUrl(link);
    private getTopicBoxData();
    private getImageAndBasicParagraphData(topicBox);
}
export default TopicBoxContainer;
export interface ITopicBoxProps {
    includedRelationships: Page.IPageData[];
    paragraphId: string;
    siteDomain: string;
}
export interface ITopicBoxState {
    title: string;
    titleColor: TitleColor;
    width: number;
    url: string;
    imageId: string;
    basicParagraphId: string;
    includedRelationships: Page.IPageData[];
}
export interface ITopicBoxData extends Page.IPageData {
    attributes: ITopicBoxAttributes;
    relationships: ITopicBoxRelationships;
}
export interface ITopicBoxAttributes extends Page.IAttributes {
    field_title: string;
    field_topic_box_width: number;
    field_website_link: ITopicBoxWebsiteLink;
}
export interface ITopicBoxWebsiteLink {
    uri: string;
    title?: string;
}
export interface ITaxonomyData extends Page.IPageData {
    attributes: ITaxonomyAttributes;
}
export interface ITaxonomyAttributes extends Page.IAttributes {
    name: TitleColor;
}
export interface ITopicBoxRelationships extends Page.IRelationships {
    field_content: {
        data: Page.IData[];
    };
    field_title_color: {
        data: Page.IData;
    };
}
export interface IAxiosResponseTopicBox extends AxiosResponse {
    data: {
        data: ITopicBoxData;
        included: Page.IPageData[];
    };
}
export declare type TitleColor = "Red" | "Green" | "Orange" | "Blue" | "Gray";
