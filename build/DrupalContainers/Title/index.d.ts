import { AxiosResponse } from "axios";
import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare class TitleContainer extends React.Component<IProps, IState> {
    private xhrCancel;
    constructor(props: IProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentWillUnmount(): void;
    componentDidUpdate(prevProps: IProps, prevState: IState): void;
    getData(): void;
    queryData(id: string): Promise<void>;
    getDefaultState(): IState;
}
export default TitleContainer;
export interface IProps {
    titleParagraphData: ITitleData;
    paragraphId: string;
    siteDomain: string;
}
export interface IState {
    text: string;
}
export interface IAxiosDataResponse extends AxiosResponse {
    data: ITitleResponse;
}
export interface ITitleResponse extends Page.IType {
    data: ITitleData;
}
export interface ITitleData extends Page.IPageData {
    attributes: ITitleAttributes;
}
export interface ITitleAttributes extends Page.IAttributes {
    field_title: string;
}
