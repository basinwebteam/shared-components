import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
import { IImageFileData } from "../Image";
export declare class BiographyContainer extends React.Component<IPropsBiography, IStateBiography> {
    constructor(props: IPropsBiography);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IPropsBiography, prevState: IStateBiography): void;
    private getBiography();
    private getImage();
    private getBiographyData();
    private queryParagraph();
    private queryBiography(id);
    private findParagraphData();
    private findBiographyData(paragraphData);
    private getPhoneNumbers();
    private getDefaultState();
}
export default BiographyContainer;
export interface IPropsBiography {
    paragraphId: string;
    includedRelationships: Page.IPageData[];
    siteDomain: string;
}
export interface IStateBiography {
    title: string;
    employeeTitle: string;
    firstName: string;
    lastName: string;
    employeeEmail: string;
    employeePhoneNumber: Array<string | undefined>;
    biography: string;
    includedImageData: Array<Page.IPageData | IImageFileData>;
    contactId: string;
    showImage: boolean;
}
