"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var React = require("react");
var Image_1 = require("../Image");
var Biography_1 = require("../../components/Biography");
var BiographyContainer = (function (_super) {
    __extends(BiographyContainer, _super);
    function BiographyContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this.getDefaultState();
        return _this;
    }
    BiographyContainer.prototype.render = function () {
        return (React.createElement(Biography_1.default, { firstName: this.state.firstName, lastName: this.state.lastName, title: this.state.employeeTitle, biographyDescription: this.getBiography(), emailAddress: this.state.employeeEmail, phoneNumber: this.getPhoneNumbers() }, (this.state.showImage) ? this.getImage() : ""));
    };
    BiographyContainer.prototype.componentDidMount = function () {
        this.getBiographyData();
    };
    BiographyContainer.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.getBiographyData();
        }
    };
    BiographyContainer.prototype.getBiography = function () {
        if (this.state.biography !== null && this.state.biography.length > 0) {
            return this.state.biography;
        }
        return "";
    };
    BiographyContainer.prototype.getImage = function () {
        if (this.state.showImage) {
            return (React.createElement("figure", null,
                React.createElement(Image_1.ImageContainer, { paragraphId: this.state.contactId, includedRelationships: this.state.includedImageData, siteDomain: this.props.siteDomain })));
        }
        return "";
    };
    BiographyContainer.prototype.getBiographyData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var paragraph, queriedData, paragraphResponse, biography, biographyResponse;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        paragraph = this.findParagraphData();
                        queriedData = new Array();
                        if (!(typeof paragraph === "undefined")) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.queryParagraph()];
                    case 1:
                        paragraphResponse = _a.sent();
                        paragraph = paragraphResponse.data;
                        queriedData = paragraphResponse.included;
                        _a.label = 2;
                    case 2:
                        biography = this.findBiographyData(paragraph);
                        if (!(typeof biography === "undefined" ||
                            typeof biography.relationships.field_image === "undefined")) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.queryBiography(paragraph.relationships.field_biography_reference.data.id)];
                    case 3:
                        biographyResponse = _a.sent();
                        biography = biographyResponse.data;
                        queriedData = biographyResponse.included;
                        queriedData.push(biographyResponse.data);
                        _a.label = 4;
                    case 4:
                        this.setState({
                            employeeTitle: biography.attributes.field_employee_title,
                            firstName: biography.attributes.field_first_name,
                            lastName: biography.attributes.field_last_name,
                            employeeEmail: biography.attributes.field_employee_e_mail,
                            employeePhoneNumber: biography.attributes.field_employee_phone_number,
                            biography: (biography.attributes.field_biography !== null) ? biography.attributes.field_biography.value : "",
                            contactId: biography.id,
                            includedImageData: (queriedData.length === 0) ? this.props.includedRelationships : queriedData,
                            showImage: (biography.relationships.field_image.data !== null) ? true : false,
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    BiographyContainer.prototype.queryParagraph = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/paragraph/biography/" + this.props.paragraphId + "?include=field_biography_reference,field_biography_reference.field_image")];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response.data];
                }
            });
        });
    };
    BiographyContainer.prototype.queryBiography = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/node/basin_contacts/" + id + "?include=field_image")];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response.data];
                }
            });
        });
    };
    BiographyContainer.prototype.findParagraphData = function () {
        var _this = this;
        return this.props.includedRelationships.find(function (includedData) {
            return includedData.id === _this.props.paragraphId;
        });
    };
    BiographyContainer.prototype.findBiographyData = function (paragraphData) {
        return this.props.includedRelationships.find(function (includedData) {
            return includedData.id === paragraphData.relationships.field_biography_reference.data.id;
        });
    };
    BiographyContainer.prototype.getPhoneNumbers = function () {
        if (this.state.employeePhoneNumber !== null && this.state.employeePhoneNumber.length > 0) {
            return this.state.employeePhoneNumber.join(" or ");
        }
        return "";
    };
    BiographyContainer.prototype.getDefaultState = function () {
        /*
            This tries to get the biography paragraph data and related contact data.
            If any of it is missing, this returns an empty state.
        */
        try {
            var paragraphData = this.findParagraphData();
            var bio = this.findBiographyData(paragraphData);
            if (typeof bio.relationships.field_image === "undefined") {
                throw Error("Field Image data missing");
            }
            return {
                title: bio.attributes.title,
                employeeTitle: bio.attributes.field_employee_title,
                firstName: bio.attributes.field_first_name,
                lastName: bio.attributes.field_last_name,
                employeeEmail: bio.attributes.field_employee_e_mail,
                employeePhoneNumber: bio.attributes.field_employee_phone_number,
                biography: bio.attributes.field_biography.value,
                contactId: bio.id,
                includedImageData: this.props.includedRelationships,
                showImage: bio.relationships.field_image.data !== null ? true : false,
            };
        }
        catch (e) {
            return {
                title: "",
                employeeTitle: "",
                firstName: "",
                lastName: "",
                employeeEmail: "",
                employeePhoneNumber: [],
                biography: "",
                contactId: "",
                includedImageData: this.props.includedRelationships,
                showImage: false,
            };
        }
    };
    return BiographyContainer;
}(React.Component));
exports.BiographyContainer = BiographyContainer;
exports.default = BiographyContainer;
//# sourceMappingURL=index.js.map