import { AxiosResponse } from "axios";
import { IYouTubePlaylistData } from "../../DrupalContainers/VideoGallery";
import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare class RelatedVideos extends React.Component<IRelatedVideosProps, IRelatedVideosState> {
    modalVisibleStyle: React.CSSProperties;
    modalHiddenStyle: React.CSSProperties;
    constructor(props: IRelatedVideosProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IRelatedVideosProps, prevState: IRelatedVideosState): void;
    showVideo(videoId: string): void;
    closeVideo(): void;
    updateComponent(): Promise<void>;
    queryData(): Promise<IYouTubePlaylistData[]>;
    private getRelatedVideosFeed();
}
export default RelatedVideos;
export interface IRelatedVideosProps {
    paragraphId?: string;
    includedRelationships?: Page.IPageData[];
    videos?: string[];
}
export interface IRelatedVideosState {
    videos: IRelatedVideoItem[];
    showModal: boolean;
    activeVideoId: string;
}
export interface IRelatedVideoItem {
    title: string;
    id: string;
    thumbnail: string;
}
export interface IRelatedVideoParagraph extends Page.IPageData {
    attributes: IRelatedVideoParagraphAttributes;
    relationships: IRelatedVideoParagraphRelationships;
    type: string;
}
export interface IRelatedVideoParagraphAttributes extends Page.IAttributes {
    field_video_playlist_id: string;
}
export interface IRelatedVideoParagraphRelationships extends Page.IRelationships {
    field_tags: Page.ITypeMultipleData;
}
export interface IAxiosRelatedVideosResponse extends AxiosResponse {
    data: IYouTubePlaylistData[];
}
