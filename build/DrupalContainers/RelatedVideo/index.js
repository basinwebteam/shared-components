"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var YouTube_1 = require("../../components/YouTube");
var React = require("react");
var Types = require("../../DrupalContentTypes/ParagraphTypes");
var RelatedVideos = (function (_super) {
    __extends(RelatedVideos, _super);
    function RelatedVideos(props) {
        var _this = _super.call(this, props) || this;
        _this.modalVisibleStyle = { display: "block", opacity: 1, visibility: "inherit", maxWidth: "100%", height: "100%", width: "100%", background: "rgba(0,0,0,.7)", position: "fixed", top: 0 };
        _this.modalHiddenStyle = { display: "none", opacity: 1, visibility: "hidden" };
        _this.state = {
            videos: new Array(),
            showModal: false,
            activeVideoId: "",
        };
        _this.closeVideo = _this.closeVideo.bind(_this);
        return _this;
    }
    //TODO: Remove tslint disable & fix the lambda. 
    //tslint:disable:jsx-no-lambda
    RelatedVideos.prototype.render = function () {
        var _this = this;
        if (this.state.videos.length > 0) {
            return (React.createElement("div", { className: "row", id: "relatedVideos" },
                React.createElement("div", { className: "column" },
                    React.createElement("h2", { className: "related-vids-header" }, "Related Videos")),
                React.createElement("div", { className: "vid-list column" },
                    React.createElement("div", { className: "videoPlaylist" },
                        React.createElement("div", { className: "videoList", id: "playlist-0" },
                            React.createElement("ul", { className: "vid-gal-list-item block-grid-4" }, this.state.videos.slice(0, 6).map(function (video) { return (React.createElement("li", { key: video.id },
                                React.createElement("a", { className: "vid-gal-list-link rel-vid-item", onClick: function () { return _this.showVideo(video.id); }, href: "#reveal-video-wrapper" },
                                    React.createElement("div", { className: "rel-vid-icon", "aria-label": "Play Button" }),
                                    React.createElement("div", { className: "rel-vid-thumb" },
                                        React.createElement("img", { src: video.thumbnail }),
                                        React.createElement("div", { className: "rel-vid-title" }, video.title))))); }))))),
                React.createElement("div", { className: "reveal-modal", id: "reveal-video-wrapper", onClick: this.closeVideo, style: this.state.showModal ? this.modalVisibleStyle : this.modalHiddenStyle },
                    React.createElement("div", { className: "video-player-wrapper", style: { position: "relative", maxWidth: "72rem", margin: "0 auto" } },
                        React.createElement("div", { id: "videoPlayer", className: "flex-video", style: { display: "block" } }, this.state.showModal && this.state.activeVideoId !== "" && React.createElement(YouTube_1.YouTubeVideo, { link: this.state.activeVideoId })),
                        React.createElement("a", { className: "close-reveal-modal", onClick: this.closeVideo, style: { top: "-12px", right: "-35px" } }, "\u00D7")))));
        }
        else {
            return (React.createElement("div", { className: "row", id: "relatedVideos" }));
        }
    };
    RelatedVideos.prototype.componentDidMount = function () {
        this.updateComponent();
    };
    RelatedVideos.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateComponent();
        }
    };
    RelatedVideos.prototype.showVideo = function (videoId) {
        this.setState({
            showModal: true,
            activeVideoId: videoId,
        });
    };
    RelatedVideos.prototype.closeVideo = function () {
        this.setState({
            showModal: false,
        });
    };
    RelatedVideos.prototype.updateComponent = function () {
        return __awaiter(this, void 0, void 0, function () {
            var videos, activeVideoId, videoData, videoIds, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.props.videos !== undefined && this.props.videos.length > 0)) return [3 /*break*/, 2];
                        videoIds = this.props.videos
                            .map(function (v) { return YouTube_1.getVideoId(v); })
                            .join(",");
                        return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/youtube/videosById/" + videoIds)];
                    case 1:
                        response = _a.sent();
                        videoData = response.data.items;
                        videos = videoData.map(function (video) { return ({
                            title: video.snippet.title,
                            id: video.id,
                            thumbnail: video.snippet.thumbnails.default.url,
                        }); });
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.queryData()];
                    case 3:
                        videoData = _a.sent();
                        videos = videoData.map(function (video) {
                            return {
                                title: video.title,
                                id: video.id,
                                thumbnail: video.thumbnailStandard,
                            };
                        });
                        _a.label = 4;
                    case 4:
                        activeVideoId = (videos.length > 0) ? videos[0].id : "";
                        this.setState({
                            videos: videos,
                            activeVideoId: activeVideoId,
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    RelatedVideos.prototype.queryData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get(this.getRelatedVideosFeed())];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response.data];
                }
            });
        });
    };
    RelatedVideos.prototype.getRelatedVideosFeed = function () {
        var includedRelationships = this.props.includedRelationships || [];
        var paragraphId = this.props.paragraphId || [];
        if (this.props.includedRelationships !== undefined && this.props.paragraphId !== undefined) {
        }
        var paragraph = includedRelationships.find(function (p) { return p.id === paragraphId; });
        if (paragraph.type === Types.RELATED_VIDEO) {
            return "//api.bepc.com/api/youtube/videosByPlaylist/" + paragraph.attributes.field_video_playlist_id;
        }
        else {
            var tags = paragraph.relationships.field_tags.data.map(function (tag) { return includedRelationships.find(function (p) { return p.id === tag.id; }); });
            return "//api.bepc.com/api/youtube/videosByTag/" + tags.map(function (tag) { return tag.attributes.name; }).join(",");
        }
    };
    return RelatedVideos;
}(React.Component));
exports.RelatedVideos = RelatedVideos;
exports.default = RelatedVideos;
//# sourceMappingURL=index.js.map