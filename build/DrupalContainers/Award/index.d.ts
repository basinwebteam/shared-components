import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare const AwardContainer: React.SFC<AwardProps>;
export declare function GetAwardContent(AwardParagraph: AwardPageData, includedRelationships: Array<AwardIncludedRelationships>, siteDomain: string): IAwardWinner[];
export declare function GetWinnerData(winnerData: Page.IDatum, includedRelationships: Array<AwardIncludedRelationships>, siteDomain: string): IAwardWinner | undefined;
export declare function GetWinnerPhoto(winner: FieldWinnerData, includedRelationships: Array<AwardIncludedRelationships>, siteDomain: string): string;
export default AwardContainer;
export interface AwardProps {
    includedRelationships: Array<Page.IPageData | AwardPageData | any>;
    paragraphId: string;
    siteDomain: string;
}
export interface AwardPageData extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_title: string;
    };
    relationships: Page.IRelationships & {
        field_winners: {
            data: Page.IDatum[];
        };
    };
}
export interface FieldWinnerData extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_date: string;
        field_title: string;
        field_year: number;
    };
    relationships: Page.IRelationships & {
        field_image: Page.IField_Image;
    };
}
export interface IAwardWinner {
    name: string;
    image: string;
    year: number;
}
export declare type AwardIncludedRelationships = Page.IPageData | AwardPageData | FieldWinnerData | Page.IField_File | any;
