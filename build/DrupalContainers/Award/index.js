"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.AwardContainer = function (_a) {
    var includedRelationships = _a.includedRelationships, paragraphId = _a.paragraphId, siteDomain = _a.siteDomain;
    var AwardParagraph = includedRelationships.find(function (p) { return p.id === paragraphId; });
    var winners = GetAwardContent(AwardParagraph, includedRelationships, siteDomain);
    if (AwardParagraph) {
        return (React.createElement("ul", { className: "small-block-grid-2 large-block-grid-3" }, winners.map(function (_a, i) {
            var name = _a.name, year = _a.year, image = _a.image;
            return (React.createElement("li", { key: i },
                React.createElement("img", { src: image }),
                React.createElement("div", null, name),
                React.createElement("div", null, year)));
        })));
    }
    else {
        return React.createElement("span", { className: "award-paragraph-empty" });
    }
};
// Expects content to be in includedRelationships. 
// Doesn't query content if it's missing
function GetAwardContent(AwardParagraph, includedRelationships, siteDomain) {
    return AwardParagraph.relationships.field_winners.data
        .map(function (data) { return GetWinnerData(data, includedRelationships, siteDomain); })
        .filter(function (awards) { return awards !== undefined; });
}
exports.GetAwardContent = GetAwardContent;
function GetWinnerData(winnerData, includedRelationships, siteDomain) {
    var winner = includedRelationships.find(function (p) { return p.id === winnerData.id; });
    if (winner) {
        return {
            name: winner.attributes.field_title,
            year: winner.attributes.field_year,
            image: GetWinnerPhoto(winner, includedRelationships, siteDomain)
        };
    }
    return undefined;
}
exports.GetWinnerData = GetWinnerData;
function GetWinnerPhoto(winner, includedRelationships, siteDomain) {
    var image = includedRelationships.find(function (p) { return p.id === winner.relationships.field_image.data.id; });
    if (image) {
        return siteDomain + image.attributes.url;
    }
    return "";
}
exports.GetWinnerPhoto = GetWinnerPhoto;
exports.default = exports.AwardContainer;
//# sourceMappingURL=index.js.map