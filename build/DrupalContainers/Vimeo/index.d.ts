import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare const Vimeo: React.StatelessComponent<IVimeoProps>;
export default Vimeo;
export interface IVimeoProps {
    includedRelationships: Page.IPageData[];
    paragraphId: string;
}
export interface IVimeoData extends Page.IPageData {
    attributes: IVimeoAttributes;
}
export interface IVimeoAttributes extends Page.IAttributes {
    field_vimeo_video: string;
}
