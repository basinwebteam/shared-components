"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Vimeo_1 = require("../../components/Vimeo");
exports.Vimeo = function (props) { return (React.createElement(Vimeo_1.default, { link: getVideoUrl(props) })); };
var getVideoUrl = function (props) {
    var data = props.includedRelationships.find(function (item) { return item.id === props.paragraphId; });
    return data.attributes.field_vimeo_video;
};
exports.default = exports.Vimeo;
//# sourceMappingURL=index.js.map