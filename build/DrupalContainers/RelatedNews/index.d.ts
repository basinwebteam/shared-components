import { AxiosResponse } from "axios";
import * as React from "react";
export declare class RelatedNews extends React.Component<IRelatedNewsProps, IRelatedNewsState> {
    constructor(props: IRelatedNewsProps);
    render(): JSX.Element;
    getContent(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IRelatedNewsProps, prevState: IRelatedNewsState): void;
    updateComponent(): Promise<void>;
    queryData(): Promise<INewsItems[]>;
}
export default RelatedNews;
export interface IRelatedNewsProps {
    sites: string;
    relatedTagIds: string;
    excludeTitle: string;
}
export interface IRelatedNewsState {
    news: INewsItems[];
}
export interface INewsItems {
    title: string;
    field_publish_to_sites: string;
    field_related_news: string;
    path: string;
    uuid: string;
}
export interface IAxiosRelatedNewsResponse extends AxiosResponse {
    data: INewsItems[];
}
