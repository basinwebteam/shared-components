import { AxiosResponse } from "axios";
import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
import { IVideoPlaylistProps as IPlaylistProps } from "../../components/VideoGallery/Playlist";
export declare class VideoGallery extends React.Component<IVideoGalleryProps, IVideoGalleryState> {
    constructor(props: IVideoGalleryProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IVideoGalleryProps, prevState: IVideoGalleryState): void;
    updateComponent(autoplay?: boolean): Promise<void>;
    playVideoClickHandler(videoId: string): void;
    private getVideoPlaylists();
    private getVideoGalleryParagraph();
    private getVideoGalleriesData(videoGalleryParagraph);
    private getVideosFromYouTubePlaylist(paragraphId);
    private getVideosFromYoutubeUrlCollection(paragraphId);
    private getVideosFromYouTubeTags(paragraphId);
    private getConcatenatedTagNamesFromCMS(paragraphId);
}
export default VideoGallery;
export interface IVideoGalleryProps {
    paragraphId: string;
    includedRelationships: Page.IPageData[];
    siteDomain: string;
}
export interface IVideoGalleryState {
    currentVideoId: string;
    autoplay: boolean;
    playlists: IPlaylistProps[];
}
export interface IVideoGallery extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_default_video: string;
    };
    relationships: Page.IRelationships & {
        field_gallery_playlist_videos: Page.ITypeMultipleData;
    };
}
export interface IVideoGalleryItem extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_title: string;
    };
    relationships: Page.IRelationships & {
        field_video_gallery_videos: Page.IType;
    };
}
export interface IYouTubeCollection extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_video_youtube: string[];
    };
}
export interface IYouTubeCollectionAxiosResponse extends AxiosResponse {
    data: IYouTubeAPIVideosById;
}
export interface IYouTubeAPIVideosById {
    kind: string;
    etag: string;
    nextPageToken: null;
    regionCode: null;
    pageInfo: {
        totalResults: number;
        resultsPerPage: number;
    };
    items: IYouTubeAPIVideoItems[];
}
export interface IYouTubeAPIVideoItems {
    kind: string;
    etag: string;
    id: string;
    snippet: {
        publishedAt: string;
        channelId: string;
        title: string;
        description: string;
        thumbnails: {
            default: {
                url: string;
                width: number;
                height: number;
            };
            medium: {
                url: string;
                width: number;
                height: number;
            };
            high: {
                url: string;
                width: number;
                height: number;
            };
        };
        channelTitle: string;
        tags: string[];
        liveBroadcastContent: string;
    };
}
export interface IRelatedVideoTagTaxonomyResponse extends AxiosResponse {
    data: {
        data: IRelatedVideoTagTaxonomyData[];
    };
}
export interface IRelatedVideoTagTaxonomyData extends Page.IPageData {
    attributes: Page.IAttributes & {
        name: string;
    };
}
export interface IRelatedVideoTagsParagraph extends Page.IPageData {
    relationships: Page.IRelationships & {
        field_tags: Page.ITypeMultipleData;
    };
}
export interface IYouTubePlaylistResponse extends AxiosResponse {
    data: IYouTubePlaylistData[];
}
export interface IYouTubePlaylistData {
    id: string;
    title: string;
    description: string;
    publishedAt: string;
    thumbnailDefault: string;
    thumbnailStandard: string;
    thumbnailMaxRes: string;
    tag: string;
}
export interface IRelatedVideoPlaylistParagraph extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_video_playlist_id: string;
    };
}
