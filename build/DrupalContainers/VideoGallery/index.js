"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var React = require("react");
var YouTube_1 = require("../../components/YouTube");
var Playlist_1 = require("../../components/VideoGallery/Playlist");
var TableOfContents_1 = require("../../components/VideoGallery/TableOfContents");
var VideoGallery = (function (_super) {
    __extends(VideoGallery, _super);
    function VideoGallery(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            currentVideoId: "",
            autoplay: false,
            playlists: [],
        };
        _this.playVideoClickHandler = _this.playVideoClickHandler.bind(_this);
        return _this;
    }
    VideoGallery.prototype.render = function () {
        return (React.createElement("section", { className: "videoGallery" },
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "videoPlayerWrapper", id: "videoPlayer" },
                    React.createElement(YouTube_1.YouTubeVideo, { autoplay: this.state.autoplay, link: this.state.currentVideoId })),
                React.createElement(TableOfContents_1.VideoTOC, { playlists: this.state.playlists.map(function (playlist, id) {
                        return {
                            id: id.toString(),
                            title: playlist.title,
                        };
                    }) })),
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "vid-list column" },
                    React.createElement("div", { className: "videoPlaylist" }, this.state.playlists
                        .map(function (playlist, i) { return React.createElement(Playlist_1.Playlist, __assign({ key: i }, playlist, { instance: i })); }))))));
    };
    VideoGallery.prototype.componentDidMount = function () {
        this.updateComponent();
    };
    VideoGallery.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateComponent(true);
        }
    };
    VideoGallery.prototype.updateComponent = function (autoplay) {
        if (autoplay === void 0) { autoplay = false; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var playlists, defaultVideo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getVideoPlaylists()];
                    case 1:
                        playlists = _a.sent();
                        defaultVideo = this.props.includedRelationships.find(function (paragraph) { return paragraph.id === _this.props.paragraphId; }).attributes.field_default_video;
                        this.setState({
                            autoplay: autoplay,
                            currentVideoId: defaultVideo,
                            playlists: playlists,
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    VideoGallery.prototype.playVideoClickHandler = function (videoId) {
        this.setState({
            autoplay: true,
            currentVideoId: videoId,
        });
    };
    VideoGallery.prototype.getVideoPlaylists = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var videoGalleriesData, playlists;
            return __generator(this, function (_a) {
                videoGalleriesData = this.getVideoGalleriesData(this.getVideoGalleryParagraph());
                playlists = Promise.all(videoGalleriesData.map(function (gallery) { return __awaiter(_this, void 0, void 0, function () {
                    var videos, _a;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                videos = [];
                                _a = gallery.relationships.field_video_gallery_videos.data.type;
                                switch (_a) {
                                    case ("paragraph--related_video_playlist"): return [3 /*break*/, 1];
                                    case ("paragraph--related_video_tags"): return [3 /*break*/, 3];
                                    case ("paragraph--video_youtube_url_collection"): return [3 /*break*/, 5];
                                }
                                return [3 /*break*/, 7];
                            case 1: return [4 /*yield*/, this.getVideosFromYouTubePlaylist(gallery.relationships.field_video_gallery_videos.data.id)];
                            case 2:
                                videos = _b.sent();
                                return [3 /*break*/, 7];
                            case 3: return [4 /*yield*/, this.getVideosFromYouTubeTags(gallery.relationships.field_video_gallery_videos.data.id)];
                            case 4:
                                videos = _b.sent();
                                return [3 /*break*/, 7];
                            case 5: return [4 /*yield*/, this.getVideosFromYoutubeUrlCollection(gallery.relationships.field_video_gallery_videos.data.id)];
                            case 6:
                                videos = _b.sent();
                                return [3 /*break*/, 7];
                            case 7: return [2 /*return*/, {
                                    title: gallery.attributes.field_title,
                                    videos: videos,
                                    playVideoClickHandler: this.playVideoClickHandler,
                                    showMorePlaylistClickHandler: function () { console.log("test 1"); },
                                }];
                        }
                    });
                }); }));
                return [2 /*return*/, playlists];
            });
        });
    };
    VideoGallery.prototype.getVideoGalleryParagraph = function () {
        var _this = this;
        return this.props.includedRelationships.find(function (paragraph) { return paragraph.id === _this.props.paragraphId; });
    };
    VideoGallery.prototype.getVideoGalleriesData = function (videoGalleryParagraph) {
        var _this = this;
        return videoGalleryParagraph.relationships.field_gallery_playlist_videos.data
            .map(function (data) { return _this.props.includedRelationships
            .find(function (includedItem) { return includedItem.id === data.id; }); });
    };
    VideoGallery.prototype.getVideosFromYouTubePlaylist = function (paragraphId) {
        return __awaiter(this, void 0, void 0, function () {
            var playlistParagraph, youtubeVideos;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        playlistParagraph = this.props.includedRelationships.find(function (paragraph) { return paragraph.id === paragraphId; });
                        return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/youtube/videosByPlaylist/" + playlistParagraph.attributes.field_video_playlist_id)];
                    case 1:
                        youtubeVideos = _a.sent();
                        return [2 /*return*/, youtubeVideos.data.map(function (video, i) {
                                return {
                                    id: video.id + i + paragraphId,
                                    title: video.title,
                                    url: video.id,
                                    thumb: video.thumbnailStandard,
                                };
                            })];
                }
            });
        });
    };
    VideoGallery.prototype.getVideosFromYoutubeUrlCollection = function (paragraphId) {
        return __awaiter(this, void 0, void 0, function () {
            var queries, collectionParagraph, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        queries = [];
                        collectionParagraph = this.props.includedRelationships.find(function (paragraph) { return paragraph.id === paragraphId; });
                        collectionParagraph.attributes.field_video_youtube.map(function (url) {
                            var data = axios_1.default.get("//api.bepc.com/api/youtube/videosById/" + YouTube_1.getVideoId(url));
                            queries.push(data);
                        });
                        return [4 /*yield*/, axios_1.default.all(queries)];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response
                                .filter(function (video) { return video.data.items.length > 0; })
                                .map(function (video, i) {
                                var item = video.data.items[0];
                                return {
                                    id: item.id + i + paragraphId,
                                    title: item.snippet.title,
                                    url: item.id,
                                    thumb: item.snippet.thumbnails.medium.url,
                                };
                            })];
                }
            });
        });
    };
    VideoGallery.prototype.getVideosFromYouTubeTags = function (paragraphId) {
        return __awaiter(this, void 0, void 0, function () {
            var concatenatedTags, youtubeVideos;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getConcatenatedTagNamesFromCMS(paragraphId)];
                    case 1:
                        concatenatedTags = _a.sent();
                        return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/youtube/videosByTag/" + concatenatedTags)];
                    case 2:
                        youtubeVideos = _a.sent();
                        return [2 /*return*/, youtubeVideos.data.map(function (video, i) {
                                return {
                                    id: video.id + i + paragraphId,
                                    title: video.title,
                                    url: video.id,
                                    thumb: video.thumbnailStandard,
                                };
                            })];
                }
            });
        });
    };
    VideoGallery.prototype.getConcatenatedTagNamesFromCMS = function (paragraphId) {
        return __awaiter(this, void 0, void 0, function () {
            var relatedVideoTags, paragraphData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/taxonomy_term/related_videos/")];
                    case 1:
                        relatedVideoTags = _a.sent();
                        paragraphData = this.props.includedRelationships.find(function (paragraph) { return paragraph.id === paragraphId; });
                        return [2 /*return*/, paragraphData.relationships.field_tags.data
                                .reduce(function (prev, cur) {
                                var tagData = relatedVideoTags.data.data.find(function (data) { return data.id === cur.id; });
                                if (typeof tagData !== "undefined") {
                                    return (prev.length > 0) ? "," + tagData.attributes.name : tagData.attributes.name;
                                }
                                return "";
                            }, "")];
                }
            });
        });
    };
    return VideoGallery;
}(React.Component));
exports.VideoGallery = VideoGallery;
exports.default = VideoGallery;
//# sourceMappingURL=index.js.map