"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var React = require("react");
var EnergyPortfolioPieChart_1 = require("../../components/EnergyPortfolioPieChart");
var EnergyPortfolioResourceDetails_1 = require("../../components/EnergyPortfolioResourceDetails");
var EnergyPortfolioResourceTable_1 = require("../../components/EnergyPortfolioResourceTable");
var Image_1 = require("../../components/Image");
var YouTube_1 = require("../../components/YouTube");
var BasicParagraph_1 = require("../BasicParagraph");
var EnergyPortfolioPieChartContainer = (function (_super) {
    __extends(EnergyPortfolioPieChartContainer, _super);
    function EnergyPortfolioPieChartContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            generationData: [],
            generationRenewableData: [],
            details: {
                name: "Resource Details",
                photo: "",
                plants: [],
                videos: "",
                briefInfo: "",
            },
            explanationParagraph: {
                paragraphId: "",
                siteDomain: _this.props.siteDomain,
                data: undefined
            },
            operatedGenSummer: "",
            operatedGenWinter: "",
            ownedGenSummer: "",
            ownedGenWinter: "",
            purchasedGenSummer: "",
            purchasedGenWinter: "",
        };
        _this.linkClickHandler = _this.linkClickHandler.bind(_this);
        return _this;
    }
    EnergyPortfolioPieChartContainer.prototype.render = function () {
        if (this.state.generationData.length > 0) {
            return (React.createElement("div", { className: "row" },
                React.createElement("div", { className: "small-12 medium-9 column" },
                    React.createElement(EnergyPortfolioPieChart_1.default, { size: [600, 450], generationData: this.state.generationData, generationRenewableData: this.state.generationRenewableData, clickHandler: this.linkClickHandler }),
                    React.createElement(BasicParagraph_1.BasicParagraph, { siteDomain: this.props.siteDomain, paragraphId: this.state.explanationParagraph.paragraphId, data: this.state.explanationParagraph.data }),
                    React.createElement(EnergyPortfolioResourceTable_1.default, { data: this.state.generationData, operatedGenSummer: this.state.operatedGenSummer, operatedGenWinter: this.state.operatedGenWinter, ownedGenSummer: this.state.ownedGenSummer, ownedGenWinter: this.state.ownedGenWinter, purchasedGenSummer: this.state.purchasedGenSummer, purchasedGenWinter: this.state.purchasedGenWinter })),
                React.createElement("div", { className: "small-12 medium-3 column" },
                    React.createElement(EnergyPortfolioResourceDetails_1.EnergyPortfolioResourceDetails, { name: this.state.details.name, photo: this.state.details.photo, plants: this.state.details.plants, videos: this.state.details.videos, briefInfo: this.state.details.briefInfo }))));
        }
        return React.createElement("div", { className: "pie-chart-loading" });
    };
    EnergyPortfolioPieChartContainer.prototype.componentDidMount = function () {
        this.getData(this.props.id);
    };
    EnergyPortfolioPieChartContainer.prototype.getData = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var includedFields, energyPortfolioData, generationData, generationRenewableData, explanationParagraphDataField, explanationParagraphId, explanationParagraphData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        includedFields = [
                            "field_energy_type",
                            "field_energy_type.field_image",
                            // "field_energy_type.field_body",
                            "field_energy_type.field_plant",
                            // "field_energy_type.field_video_youtube",
                            "field_data_explanation"
                        ];
                        return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/block_content/energy_portfolio_interactive_pie/" + id + "/?include=" + includedFields.join(","))];
                    case 1:
                        energyPortfolioData = _a.sent();
                        generationData = this.modelGenerationData(energyPortfolioData.data);
                        generationRenewableData = this.modelGenerationData(energyPortfolioData.data, true);
                        explanationParagraphDataField = energyPortfolioData.data.data.relationships.field_data_explanation.data;
                        explanationParagraphId = Array.isArray(explanationParagraphDataField) && explanationParagraphDataField.length > 0 ? explanationParagraphDataField[0].id : "";
                        explanationParagraphData = energyPortfolioData.data.included.find(function (p) { return p.id === explanationParagraphId; });
                        this.setState({
                            generationData: generationData,
                            generationRenewableData: generationRenewableData,
                            explanationParagraph: {
                                paragraphId: explanationParagraphId,
                                siteDomain: this.props.siteDomain,
                                data: explanationParagraphData
                            },
                            operatedGenSummer: energyPortfolioData.data.data.attributes.field_operated_generation_summer,
                            operatedGenWinter: energyPortfolioData.data.data.attributes.field_operated_generation_winter,
                            ownedGenSummer: energyPortfolioData.data.data.attributes.field_owned_generation_summer,
                            ownedGenWinter: energyPortfolioData.data.data.attributes.field_owned_generation_winter,
                            purchasedGenSummer: energyPortfolioData.data.data.attributes.field_purchased_gen_summer,
                            purchasedGenWinter: energyPortfolioData.data.data.attributes.field_purchased_gen_winter,
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    EnergyPortfolioPieChartContainer.prototype.modelGenerationData = function (data, renewables) {
        var _this = this;
        if (renewables === void 0) { renewables = false; }
        var genData = data.data.relationships.field_energy_type.data.map(function (p) {
            var energy = data.included.find(function (portfolio) { return portfolio.id == p.id; });
            if (energy !== undefined) {
                return {
                    name: energy.attributes.field_type_name_short,
                    mw: parseFloat(energy.attributes.field_megawatts),
                    mwSummer: parseFloat(energy.attributes.field_megawatts_summer),
                    longName: energy.attributes.field_type_name_long,
                    photo: _this.getPhoto(energy.relationships.field_image, data.included),
                    plants: _this.getPlants(energy.relationships.field_plant, data.included),
                    videos: _this.getVideos(energy.attributes.field_video_youtube || []),
                    briefInfo: _this.getBriefInfo(energy)
                };
            }
            throw new Error("Unable to find Portfolio id " + p.id + " for Energy Portfolio Pie Chart.");
        });
        if (renewables) {
            return genData.filter(function (d) { return d.name === "Renewables"; });
        }
        else {
            return genData.filter(function (d) { return d.name !== "Renewables"; });
        }
    };
    EnergyPortfolioPieChartContainer.prototype.getBriefInfo = function (body) {
        if (body.attributes.field_body === null) {
            return "";
        }
        return (React.createElement(BasicParagraph_1.BasicParagraph, { siteDomain: this.props.siteDomain, paragraphId: body.id, data: body }));
    };
    EnergyPortfolioPieChartContainer.prototype.getPhoto = function (paragraph, includedData) {
        if (paragraph.data == null) {
            return "";
        }
        var url = includedData.find(function (data) { return data.id === paragraph.data.id; });
        if (url === undefined) {
            return "";
        }
        return (React.createElement(Image_1.default, { altText: paragraph.data.meta.alt, url: this.props.siteDomain + url.attributes.url }));
    };
    EnergyPortfolioPieChartContainer.prototype.getPlants = function (plants, includedData) {
        if (plants.data.length === 0) {
            return [];
        }
        return plants.data.map(function (plant) {
            var plantInfo = includedData.find(function (d) { return plant.id === d.id; });
            if (plantInfo === undefined) {
                throw new Error("Plant info undefined for plant " + plant.id);
            }
            return {
                name: plantInfo.attributes.field_facility_name,
                url: plantInfo.attributes.path.alias
            };
        });
    };
    EnergyPortfolioPieChartContainer.prototype.getVideos = function (videos) {
        return (React.createElement("div", null, videos.map(function (v, i) { return (React.createElement("p", { key: i },
            React.createElement(YouTube_1.default, { link: v }))); })));
    };
    EnergyPortfolioPieChartContainer.prototype.linkClickHandler = function (name) {
        var resource = this.state.generationData.find(function (r) { return r.name === name; });
        if (resource === undefined) {
            resource = this.state.generationRenewableData.find(function (r) { return r.name === name; });
        }
        if (resource !== undefined) {
            this.setState({
                details: {
                    name: resource.longName,
                    photo: resource.photo || "",
                    plants: resource.plants || [],
                    videos: resource.videos || "",
                    briefInfo: resource.briefInfo || ""
                }
            });
        }
    };
    return EnergyPortfolioPieChartContainer;
}(React.Component));
exports.EnergyPortfolioPieChartContainer = EnergyPortfolioPieChartContainer;
exports.default = EnergyPortfolioPieChartContainer;
//# sourceMappingURL=index.js.map