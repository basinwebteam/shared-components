import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
import { GenerationData, GenerationDataPlants } from "../../components/EnergyPortfolioPieChart";
import { IEPResourceDetailsProps } from "../../components/EnergyPortfolioResourceDetails";
import { IVideoGalleryItem, IYouTubeCollection } from "../VideoGallery";
import { IBasicParagraphProps } from "../BasicParagraph";
export declare class EnergyPortfolioPieChartContainer extends React.Component<IPropsEnergyPortfolio, IStateEnergyPortfolio> {
    constructor(props: IPropsEnergyPortfolio);
    render(): JSX.Element;
    componentDidMount(): void;
    getData(id: string): Promise<void>;
    modelGenerationData(data: IEnergyPortfolio, renewables?: boolean): GenerationData[];
    getBriefInfo(body: IEnergyPortfolioParagraph): any;
    getPhoto(paragraph: Page.IField_Image, includedData: Page.IPageData[]): "" | JSX.Element;
    getPlants(plants: Page.ITypeMultipleData, includedData: Page.IPageData[]): GenerationDataPlants[];
    getVideos(videos: string[]): JSX.Element;
    linkClickHandler(name: string): void;
}
export default EnergyPortfolioPieChartContainer;
export interface IPropsEnergyPortfolio {
    id: string;
    siteDomain: string;
}
export interface IStateEnergyPortfolio {
    generationData: GenerationData[];
    generationRenewableData: GenerationData[];
    details: IEPResourceDetailsProps;
    explanationParagraph: IBasicParagraphProps;
    operatedGenSummer: string;
    operatedGenWinter: string;
    ownedGenSummer: string;
    ownedGenWinter: string;
    purchasedGenSummer: string;
    purchasedGenWinter: string;
}
export interface IAxiosResponseEnergyPortfolioData {
    data: IEnergyPortfolio;
}
export interface IEnergyPortfolio {
    data: IEnergyPortfolioData;
    included: Array<Page.IPageData | IEnergyPortfolioParagraph | IYouTubeCollection | IVideoGalleryItem | Page.IParagraphFile>;
}
export interface IEnergyPortfolioData extends Page.IPageData {
    attributes: Page.IAttributes & {
        info: string;
        field_operated_generation_summer: string;
        field_operated_generation_winter: string;
        field_owned_generation_summer: string;
        field_owned_generation_winter: string;
        field_purchased_gen_summer: string;
        field_purchased_gen_winter: string;
    };
    relationships: Page.IRelationships & {
        field_energy_type: Page.ITypeMultipleData;
        field_data_explanation: Page.ITypeMultipleData;
    };
}
export interface IEnergyPortfolioParagraph extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_body: Page.IWYSIWYG;
        field_megawatts: string;
        field_megawatts_summer: string;
        field_type_name_long: string;
        field_type_name_short: string;
        field_video_youtube: string[];
    };
    relationships: Page.IRelationships & {
        field_image: Page.IType;
        field_plant: Page.ITypeMultipleData;
    };
}
export interface IAxiosResponseRelatedVideoTags {
    data: IRelatedVideoTags;
}
export interface IRelatedVideoTags {
    data: IRelatedVideoTagData[];
    included: Page.IPageData & {
        attributes: Page.IAttributes & {
            name: string;
        };
    };
}
export interface IRelatedVideoTagData extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_video_limit: number;
    };
    relationships: Page.IRelationships & {
        field_tags: Page.ITypeMultipleData;
    };
}
export interface IFacilityData extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_city: string;
        field_facility_capacity: string;
        field_facility_name: string;
        field_facility_phone: string;
        field_physical_location_city: string;
        field_physical_location_state: string;
        field_state: string;
        field_street_address: string;
        field_zip_code: string;
        path: {
            alias: string;
        };
    };
}
