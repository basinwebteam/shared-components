"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var React = require("react");
var BoardOfDirectors_1 = require("../../components/BoardOfDirectors");
var BoardOfDirectorsContainer = (function (_super) {
    __extends(BoardOfDirectorsContainer, _super);
    function BoardOfDirectorsContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.displayName = "DrupalBoardOfDirectorsContainer";
        _this.state = _this.getDefaultState();
        return _this;
    }
    BoardOfDirectorsContainer.prototype.render = function () {
        return (React.createElement(BoardOfDirectors_1.BoardOfDirectors, { boards: this.state.boards }));
    };
    BoardOfDirectorsContainer.prototype.componentDidMount = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getData()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    BoardOfDirectorsContainer.prototype.componentDidUpdate = function (prevProps, prevState) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.props.paragraphId !== prevProps.paragraphId)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.getData()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    BoardOfDirectorsContainer.prototype.getData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var response, boardIds, boards;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/paragraph/board_of_directors_list/" +
                            this.props.paragraphId +
                            "?include=" +
                            "field_board," +
                            "field_board.field_content," +
                            "field_board.field_content.field_biography_reference," +
                            "field_board.field_content.field_biography_reference.field_image")];
                    case 1:
                        response = _a.sent();
                        boardIds = response.data.data.relationships.field_board.data.map(function (board) { return board.id; });
                        boards = boardIds.map(function (id) {
                            var board = response.data.included.find(function (paragraph) { return paragraph.id === id; });
                            var members = board.relationships.field_content.data.map(function (member) {
                                return _this.getBoardMemberData(member.id, response.data.included);
                            });
                            return {
                                name: board.attributes.title,
                                members: members,
                            };
                        });
                        this.setState({
                            boards: boards,
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    BoardOfDirectorsContainer.prototype.getBoardMemberData = function (memberId, includedData) {
        var memberData = includedData.find(function (paragraph) { return paragraph.id === memberId; });
        var _a = this.getBoardMemberNameAndDescription(memberData.relationships.field_biography_reference.data.id, includedData), name = _a.name, description = _a.description;
        return {
            name: name,
            description: description,
            image: this.getBoardMemberImage(memberData.relationships.field_biography_reference.data.id, includedData),
            title: memberData.attributes.field_title,
            district: memberData.attributes.field_co_op_district.toString(),
        };
    };
    BoardOfDirectorsContainer.prototype.getBoardMemberNameAndDescription = function (id, includedData) {
        var memberBioReference = includedData.find(function (paragraph) { return paragraph.id === id; });
        var description = typeof memberBioReference.attributes.field_biography === "string" ? memberBioReference.attributes.field_biography :
            memberBioReference.attributes.field_biography ?
                memberBioReference.attributes.field_biography.value :
                "";
        return {
            name: memberBioReference.attributes.field_first_name + " " + memberBioReference.attributes.field_last_name,
            description: description
        };
    };
    BoardOfDirectorsContainer.prototype.getBoardMemberImage = function (id, includedData) {
        var memberBioReference = includedData.find(function (paragraph) { return paragraph.id === id; });
        var imageId = memberBioReference.relationships.field_image.data.id;
        var imageData = includedData.find(function (paragraph) { return paragraph.id === imageId; });
        return this.props.siteDomain + imageData.attributes.url;
    };
    BoardOfDirectorsContainer.prototype.getDefaultState = function () {
        return {
            boards: [
                {
                    name: "",
                    members: [
                        {
                            name: "",
                            image: "",
                            title: "",
                            district: "",
                            description: "",
                        },
                    ],
                },
            ],
        };
    };
    return BoardOfDirectorsContainer;
}(React.Component));
exports.BoardOfDirectorsContainer = BoardOfDirectorsContainer;
exports.default = BoardOfDirectorsContainer;
//# sourceMappingURL=index.js.map