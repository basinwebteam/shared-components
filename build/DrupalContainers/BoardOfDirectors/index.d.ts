import * as IAxios from "axios/index";
import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
import { IBoard, IPeople } from "../../components/BoardOfDirectors";
export declare class BoardOfDirectorsContainer extends React.Component<IBoardDirContainerProps, IBoardDirContainerState> {
    constructor(props: IBoardDirContainerProps);
    displayName: string;
    render(): JSX.Element;
    componentDidMount(): Promise<void>;
    componentDidUpdate(prevProps: IBoardDirContainerProps, prevState: IBoardDirContainerState): Promise<void>;
    getData(): Promise<void>;
    getBoardMemberData(memberId: string, includedData: Array<IncludedParagraphTypes>): IPeople;
    getBoardMemberNameAndDescription(id: string, includedData: Array<IncludedParagraphTypes>): {
        name: string;
        description: string;
    };
    getBoardMemberImage(id: string, includedData: Array<IncludedParagraphTypes>): string;
    getDefaultState(): {
        boards: {
            name: string;
            members: {
                name: string;
                image: string;
                title: string;
                district: string;
                description: string;
            }[];
        }[];
    };
}
export default BoardOfDirectorsContainer;
export interface IBoardDirContainerProps {
    paragraphId: string;
    siteDomain: string;
}
export interface IBoardDirContainerState {
    boards: IBoard[];
}
export interface IBoardOfDirectorsListResponse extends IAxios.AxiosResponse {
    data: Page.IPage & {
        data: Page.IPageData & {
            attributes: Page.IAttributes & {
                field_include_show_all_link: boolean;
            };
            relationships: Page.IRelationships & {
                field_board: Page.ITypeMultipleData;
            };
        };
        included: Array<Page.IPageData | IParagraphBoardMember>;
    };
}
export interface IParagraphBoardMember extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_title: string;
        field_co_op_district: number;
    };
    relationships: Page.IRelationships & {
        field_biography_reference: Page.IType;
    };
}
export interface IParagraphBoardOfDirectors extends Page.IPageData {
    attributes: Page.IAttributes & {
        title: string;
    };
    relationships: Page.IRelationships & {
        field_content: Page.ITypeMultipleData;
    };
}
export interface IParagraphBiographyRef extends Page.IPageData {
    attributes: Page.IAttributes & {
        title: string;
        field_biography: Page.IWYSIWYG;
        field_employee_e_mail: string;
        field_employee_phone_number: string;
        field_employee_title: string;
        field_first_name: string;
        field_last_name: string;
    };
    relationships: Page.IRelationships & {
        field_image: Page.IField_Image;
    };
}
export declare type IncludedParagraphTypes = IParagraphBoardMember | IParagraphBoardOfDirectors | IParagraphBiographyRef | Page.IParagraphFile;
