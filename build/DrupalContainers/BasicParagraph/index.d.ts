import { AxiosResponse } from "axios";
import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare class BasicParagraph extends React.Component<IBasicParagraphProps, IBasicParagraphState> {
    constructor(props: IBasicParagraphProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IBasicParagraphProps, prevState: IBasicParagraphState): void;
    getData(): Promise<void>;
    queryData(): Promise<string>;
    getDefaultState(): IBasicParagraphState;
}
export default BasicParagraph;
export interface IBasicParagraphProps {
    paragraphId: string;
    data: IBasicParagraphPageData | undefined;
    siteDomain: string;
}
export interface IBasicParagraphState {
    html: string;
}
export interface IAxiosBasicParagraphResponse extends AxiosResponse {
    data: IBasicParagraphData;
}
export interface IBasicParagraphData extends Page.IType {
    data: IBasicParagraphPageData;
}
export interface IBasicParagraphPageData extends Page.IPageData {
    attributes: IBasicParagraphAttributes;
}
export interface IBasicParagraphAttributes extends Page.IAttributes {
    field_body: IFieldBody;
}
export interface IFieldBody {
    value: string;
}
