"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var React = require("react");
var BasicParagraph = (function (_super) {
    __extends(BasicParagraph, _super);
    function BasicParagraph(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this.getDefaultState();
        return _this;
    }
    BasicParagraph.prototype.render = function () {
        return (React.createElement("div", { dangerouslySetInnerHTML: { __html: this.state.html } }));
    };
    BasicParagraph.prototype.componentDidMount = function () {
        this.getData();
    };
    BasicParagraph.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.getData();
        }
    };
    BasicParagraph.prototype.getData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (!(typeof this.props.data === "undefined")) return [3 /*break*/, 2];
                        _a = this.setState;
                        _b = {};
                        return [4 /*yield*/, this.queryData()];
                    case 1:
                        _a.apply(this, [(_b.html = _c.sent(),
                                _b)]);
                        return [3 /*break*/, 3];
                    case 2:
                        this.setState({
                            html: this.props.data.attributes.field_body.value,
                        });
                        _c.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    BasicParagraph.prototype.queryData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/paragraph/basic_paragraph/" + this.props.paragraphId)];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response.data.data.attributes.field_body.value];
                }
            });
        });
    };
    BasicParagraph.prototype.getDefaultState = function () {
        /* This tries to get the block and related child data if its part of the props blockData value.

        If any of it is missing, this returns an empty state. */
        try {
            var html = this.props.data ? this.props.data.attributes.field_body.value : "";
            return {
                html: html,
            };
        }
        catch (e) {
            return {
                html: "",
            };
        }
    };
    return BasicParagraph;
}(React.Component));
exports.BasicParagraph = BasicParagraph;
exports.default = BasicParagraph;
//# sourceMappingURL=index.js.map