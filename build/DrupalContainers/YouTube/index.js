"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var YouTube_1 = require("../../components/YouTube");
exports.YouTube = function (props) { return (React.createElement(YouTube_1.default, { link: getVideoUrl(props) })); };
var getVideoUrl = function (props) {
    var data = props.includedRelationships.find(function (item) { return item.id === props.paragraphId; });
    return data.attributes.field_video_youtube;
};
exports.default = exports.YouTube;
//# sourceMappingURL=index.js.map