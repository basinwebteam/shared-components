import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare const YouTube: React.StatelessComponent<IYouTubeProps>;
export default YouTube;
export interface IYouTubeProps {
    includedRelationships: Page.IPageData[];
    paragraphId: string;
}
export interface IYouTubeData extends Page.IPageData {
    attributes: IYouTubeAttributes;
}
export interface IYouTubeAttributes extends Page.IAttributes {
    field_video_youtube: string;
}
