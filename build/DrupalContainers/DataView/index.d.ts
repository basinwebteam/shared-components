import { AxiosResponse } from "axios";
import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
/**
 * DataView
 */
export declare class DataView extends React.Component<IDataViewProps, IDataViewState> {
    constructor(props: IDataViewProps);
    render(): JSX.Element;
    private getChildComponent();
    componentDidMount(): void;
    componentDidUpdate(prevProps: IDataViewProps, prevState: IDataViewState): void;
    private updateComponent();
    private getView();
    private getViewContent(view);
    private getChildViewContent(id);
    private getRestUrl(childViewContent);
    private getDefaultState();
}
export default DataView;
export interface IDataViewProps {
    includedRelationships: any[];
    paragraphId: string;
    siteDomain: string;
    dev?: boolean;
}
export interface IDataViewState {
    type: string;
    id: string;
    restUrl: string;
}
export interface IIncludedData extends Page.IPageData {
    relationships: IRelationships;
}
export interface IRelationships extends Page.IRelationships {
    field_view: Page.IType;
}
export interface IAxiosResponseChildView extends AxiosResponse {
    data: IAxiosResponseChildViewData;
}
export interface IAxiosResponseChildViewData extends Page.IPage {
    data: IViewData;
}
export interface IViewData extends Page.IPageData {
    attributes: IViewDataAttributes;
}
export interface IViewDataAttributes extends Page.IAttributes {
    display: IViewDisplay;
    id: string;
}
export interface IViewDisplay {
    rest_export_1: IViewDisplayObject;
}
export interface IViewDisplayObject {
    display_options: IViewDisplayOptions;
}
export interface IViewDisplayOptions {
    path: string;
}
