import { AxiosResponse } from "axios";
import * as React from "react";
export declare class DataViewWrapper extends React.Component<IProps, IState> {
    constructor(props: IProps);
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IProps, prevState: IState): void;
    updateComponent(): Promise<void>;
}
export default DataViewWrapper;
export interface IProps {
    url: string;
    siteDomain: string;
}
export interface IState {
    data: any[];
}
export interface IAxiosResponseView extends AxiosResponse {
    data: any;
}
