"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var CalendarFeed_1 = require("../../../../components/CalendarFeed");
exports.RetireeEventView = function (_a) {
    var _b = _a.data, data = _b === void 0 ? [] : _b;
    return (React.createElement(CalendarFeed_1.default, { events: data, className: "retiree-events" }));
};
exports.default = exports.RetireeEventView;
//# sourceMappingURL=index.js.map