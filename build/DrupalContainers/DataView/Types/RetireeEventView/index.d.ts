import * as React from "react";
import { IEventProps } from "../../../../components/CalendarFeed/Event";
export declare const RetireeEventView: React.SFC<IProps>;
export default RetireeEventView;
export interface IProps {
    data?: IEventProps[];
}
