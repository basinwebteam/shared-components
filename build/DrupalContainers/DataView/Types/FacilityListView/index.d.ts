import * as React from "react";
export declare const FacilityListView: React.SFC<IProps>;
export declare const columns: any;
export declare const getData: (data: IMLViewData[]) => {
    key: number;
    name: string;
    state: string;
    type: string;
    fuel: string;
    capacity: string;
    path: string;
}[];
export default FacilityListView;
export interface IProps {
    data?: IMLViewData[];
}
export interface IMLViewData {
    key?: number;
    name: string;
    state: string;
    class: string;
    type: string;
    capacity: string;
    path: string;
}
