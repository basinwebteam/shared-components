"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var antd_1 = require("antd");
exports.FacilityListView = function (_a) {
    var _b = _a.data, data = _b === void 0 ? [] : _b;
    return (React.createElement("div", { id: "facilities_list_wrapper", className: "facility-list-view dataTables_wrapper no-footer" }, data.length > 0 &&
        React.createElement(antd_1.Table, { dataSource: exports.getData(data), columns: exports.columns, pagination: false })));
};
exports.FacilityListView.displayName = "FacilityListView";
exports.columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: function (a, b) { return a.name.localeCompare(b.name); },
        onFilter: function (value, record) { return record.name.indexOf(value) === 0; },
        render: function (text, record) {
            return (React.createElement("a", { href: record.path }, text));
        }
    },
    {
        title: 'State',
        dataIndex: 'state',
        key: 'state',
        sorter: function (a, b) {
            var first = a.state || "";
            var second = b.state || "";
            return first.localeCompare(second);
        },
        onFilter: function (value, record) { return record.state.indexOf(value) === 0; },
    },
    {
        title: 'Type',
        dataIndex: 'type',
        key: 'type',
        sorter: function (a, b) { return a.type.localeCompare(b.type); },
        onFilter: function (value, record) { return record.type.indexOf(value) === 0; },
    },
    {
        title: 'Fuel',
        dataIndex: 'fuel',
        key: 'fuel',
        sorter: function (a, b) { return a.fuel.localeCompare(b.fuel); },
        onFilter: function (value, record) { return record.fuel.indexOf(value) === 0; },
    },
    {
        title: 'Capacity',
        dataIndex: 'capacity',
        key: 'capacity',
        sorter: function (a, b) { return a.capacity.localeCompare(b.capacity); },
        onFilter: function (value, record) { return record.capacity.indexOf(value) === 0; },
    },
];
exports.getData = function (data) {
    var x = data
        .map(function (d, i) { return ({
        key: i,
        name: d.name,
        state: d.state,
        type: d.class,
        fuel: d.type,
        capacity: d.capacity,
        path: d.path
    }); });
    return x;
};
exports.default = exports.FacilityListView;
//# sourceMappingURL=index.js.map