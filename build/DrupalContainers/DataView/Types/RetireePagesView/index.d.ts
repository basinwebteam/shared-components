import * as React from "react";
import { AxiosResponse } from "axios";
import * as Page from "../../../../DrupalContentTypes/PageInterface";
export declare class RetireePagesView extends React.Component<IRetireePagesProps, IRetireePagesState> {
    constructor(props: IRetireePagesProps);
    render(): JSX.Element;
    componentDidUpdate(prevProps: IRetireePagesProps, prevState: IRetireePagesState): void;
    getPagesContent(): Promise<void>;
}
export default RetireePagesView;
export interface IRetireePagesProps {
    data?: {
        uuid: string;
    }[];
    domain: string;
}
export interface IRetireePagesState {
    pages: IRetireeBasicPage[];
}
export interface IMultiplePagesResponse extends AxiosResponse {
    data: IRetireeBasicPage;
}
export interface IRetireeBasicPage extends Page.IPage {
    data: Page.IPageData & {
        relationships: Page.IRelationships & {
            field_content: Page.IFieldContent;
        };
    };
}
