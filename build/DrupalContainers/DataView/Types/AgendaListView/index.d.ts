import * as React from "react";
export declare const AgendaListView: React.SFC<IProps>;
export default AgendaListView;
export interface IProps {
    data?: IMLViewData[];
}
export interface IMLViewData {
    start_day: string;
    title: string;
    start_time: string;
    desc: string;
    end_time: string;
    more_link: string[] | boolean;
    location: string;
    speaker: string[];
}
