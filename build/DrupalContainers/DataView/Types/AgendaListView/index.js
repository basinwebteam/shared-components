"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.AgendaListView = function (_a) {
    var _b = _a.data, data = _b === void 0 ? [] : _b;
    return (React.createElement("div", { id: "agenda_view", className: "agenda-wrapper" }, getBody(data)));
};
exports.AgendaListView.displayName = "AgendaListView";
var getBody = function (data) {
    if (data === void 0) { data = []; }
    var days = sortByDay(data);
    return days.map(function (day, i) { return (React.createElement("div", { className: "agenda-day-wrapper", key: i },
        React.createElement("div", { className: "agenda-day" }, day[0].start_day),
        React.createElement("div", { className: "agenda-items" }, getDaysAgenda(day)))); });
};
var getDaysAgenda = function (dayAgenda) {
    return dayAgenda.map(function (agenda, i) {
        var endTime = (agenda.end_time !== "") ? (React.createElement("span", { className: "end-time" },
            " - ",
            agenda.end_time)) : "";
        return (React.createElement("div", { className: "agenda-item row", key: i },
            React.createElement("div", { className: "medium-3 agenda-time columns medium-text-center" },
                React.createElement("span", { className: "start-time" }, agenda.start_time),
                endTime),
            React.createElement("div", { className: "medium-9 agenda-detail columns" },
                React.createElement("div", { className: "agenda-title" }, agenda.title),
                React.createElement("div", { className: "agenda-location" },
                    React.createElement("i", null, agenda.location)),
                getAgendaSpeaker(agenda.speaker),
                React.createElement("div", { className: "agenda-desc", dangerouslySetInnerHTML: { __html: agenda.desc } }),
                getAgendaLink(agenda.more_link))));
    });
};
var getAgendaSpeaker = function (speakers) {
    if (speakers.length > 0) {
        return (React.createElement("div", { className: "agenda-speaker" },
            "Speaker/s: ",
            speakers.join(", ")));
    }
    return;
};
var getAgendaLink = function (link) {
    if (link === void 0) { link = false; }
    if (typeof link === "boolean") {
        return "";
    }
    return (link.length > 0) ? (React.createElement("div", { className: "agenda-link" },
        React.createElement("a", { href: link[0] }, "More"))) : "";
};
var sortByDay = function (data) {
    if (data === void 0) { data = []; }
    if (data.length === 0) {
        return [];
    }
    var agendas = [];
    var dayAgenda = [];
    data.forEach(function (a) {
        if (dayAgenda.length !== 0 && dayAgenda[0].start_day !== a.start_day) {
            agendas.push(dayAgenda);
            dayAgenda = [];
            dayAgenda.push(a);
            return;
        }
        return dayAgenda.push(a);
    });
    agendas.push(dayAgenda);
    return agendas;
};
exports.default = exports.AgendaListView;
//# sourceMappingURL=index.js.map