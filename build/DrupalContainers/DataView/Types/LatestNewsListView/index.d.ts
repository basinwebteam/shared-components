import * as React from "react";
export declare const LatestNewsListView: React.SFC<IProps>;
export declare const getBody: (data?: IMLViewData[]) => JSX.Element[];
export default LatestNewsListView;
export interface IProps {
    data?: IMLViewData[];
}
export interface IMLViewData {
    title: string;
    url: string;
    publishDate: string;
    summary: string;
    newsClass?: string;
}
