"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.LatestNewsListView = function (_a) {
    var _b = _a.data, data = _b === void 0 ? [] : _b;
    return (React.createElement("div", { className: "news-list-wrapper" },
        React.createElement("ul", { className: "news-list" }, exports.getBody(data))));
};
exports.LatestNewsListView.defaultProps = {
    data: []
};
exports.LatestNewsListView.displayName = "LatestNewsListView";
exports.getBody = function (data) {
    if (data === void 0) { data = []; }
    return data.map(function (_a, i) {
        var url = _a.url, title = _a.title, summary = _a.summary, publishDate = _a.publishDate, _b = _a.newsClass, newsClass = _b === void 0 ? "" : _b;
        newsClass = newsClass !== "" ? newsClass + " - " : "";
        return (React.createElement("li", { key: i },
            React.createElement("a", { href: url, title: title },
                React.createElement("div", { className: "news-card" },
                    React.createElement("div", { className: "title" }, title),
                    React.createElement("div", { className: "summary" }, summary),
                    React.createElement("div", { className: "dateline" },
                        React.createElement("span", { className: "no-link-color" },
                            publishDate,
                            " - ",
                            newsClass),
                        "Read Article")))));
    });
};
exports.default = exports.LatestNewsListView;
//# sourceMappingURL=index.js.map