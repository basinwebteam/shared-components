"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var antd_1 = require("antd");
exports.MemberListView = function (_a) {
    var _b = _a.data, data = _b === void 0 ? [] : _b;
    return (React.createElement("div", { className: "member-list-view" }, data.length > 0 &&
        React.createElement(antd_1.Table, { dataSource: exports.getData(data), columns: exports.columns, pagination: false })));
};
exports.MemberListView.displayName = "MemberListView";
exports.columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: function (a, b) { return a.name.localeCompare(b.name); },
        onFilter: function (value, record) { return record.name.indexOf(value) === 0; },
        render: function (text, record) { return (React.createElement("a", { href: record.url }, text)); }
    },
    {
        title: 'Class',
        dataIndex: 'class',
        key: 'class',
        sorter: function (a, b) {
            var first = a.class || "";
            var second = b.class || "";
            return first.localeCompare(second);
        },
        onFilter: function (value, record) { return record.class.indexOf(value) === 0; },
    },
    {
        title: 'City',
        dataIndex: 'city',
        key: 'city',
        sorter: function (a, b) { return a.city.localeCompare(b.city); },
        onFilter: function (value, record) { return record.city.indexOf(value) === 0; },
    },
    {
        title: 'State',
        dataIndex: 'state',
        key: 'state',
        sorter: function (a, b) { return a.state.localeCompare(b.state); },
        onFilter: function (value, record) { return record.state.indexOf(value) === 0; },
    },
];
exports.getData = function (data) {
    var x = data
        .map(function (d, i) { return ({
        key: i,
        name: d.name,
        url: d.url,
        dist: d.dist,
        class: d.coopclass,
        city: d.city,
        state: d.state,
    }); });
    return x;
};
exports.default = exports.MemberListView;
//# sourceMappingURL=index.js.map