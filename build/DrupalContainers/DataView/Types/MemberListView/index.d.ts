import * as React from "react";
export declare const MemberListView: React.SFC<IProps>;
export declare const columns: any;
export declare const getData: (data: IMLViewData[]) => {
    key: number;
    name: string;
    url: string;
    dist: string;
    class: string;
    city: string;
    state: string;
}[];
export default MemberListView;
export interface IProps {
    data?: IMLViewData[];
}
export interface IMLViewData {
    name: string;
    url: string;
    dist: string;
    coopclass: string;
    city: string;
    state: string;
}
