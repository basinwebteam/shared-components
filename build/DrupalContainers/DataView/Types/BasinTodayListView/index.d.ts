import * as React from "react";
export declare const BasinTodayListView: React.SFC<IProps>;
export declare const getFeature: (data?: IMLViewData[], domain?: string) => JSX.Element[];
export declare const getThumbnails: (data?: IMLViewData[], domain?: string) => JSX.Element[];
export declare const getImageThumbnail: (image?: string, domain?: string) => string;
export declare const getFeaturedImage: (image?: string, domain?: string) => string;
export default BasinTodayListView;
export interface IProps {
    data?: IMLViewData[];
    domain?: string;
}
export interface IMLViewData {
    title: string;
    url: string;
    publishDate: string;
    summary: string;
    byline: string;
    btThumb: string;
    thumbImg: string;
    field_news_top_image: string;
    sticky: BooleanString | string;
    promote?: BooleanString | string;
}
export declare type BooleanString = "False" | "True";
