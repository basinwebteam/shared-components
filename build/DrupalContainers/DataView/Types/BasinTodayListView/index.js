"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.BasinTodayListView = function (_a) {
    var _b = _a.data, data = _b === void 0 ? [] : _b, _c = _a.domain, domain = _c === void 0 ? "" : _c;
    return (React.createElement("div", { className: "basin-today" },
        exports.getFeature(data, domain),
        React.createElement("div", { className: "btoday row" },
            React.createElement("ul", { className: "medium-block-grid-2 large-block-grid-3" }, exports.getThumbnails(data, domain)))));
};
exports.BasinTodayListView.displayName = "BasinTodayListView";
exports.getFeature = function (data, domain) {
    if (data === void 0) { data = []; }
    if (domain === void 0) { domain = ""; }
    var foundPromoted = false;
    return data
        .filter(function (news) {
        if (foundPromoted === false && news.sticky === "True") {
            foundPromoted = true;
            return true;
        }
        return false;
    })
        .map(function (news, i) {
        var image = exports.getFeaturedImage(news.field_news_top_image, domain);
        return (React.createElement("div", { className: "btoday-feature-card", key: i },
            React.createElement("a", { href: news.url, title: news.title },
                React.createElement("div", { className: "btoday-feature-img" },
                    React.createElement("img", { src: "" + image })),
                React.createElement("div", { className: "btoday-feature-title" }, news.title),
                React.createElement("div", { className: "btoday-feature-summary" }, news.summary),
                React.createElement("div", { className: "btoday-feature-dateline" },
                    news.byline,
                    " - ",
                    news.publishDate))));
    });
};
exports.getThumbnails = function (data, domain) {
    if (data === void 0) { data = []; }
    if (domain === void 0) { domain = ""; }
    var foundPromoted = false;
    return data
        .filter(function (news) {
        if (foundPromoted === false && news.sticky === "True") {
            foundPromoted = true;
            return false;
        }
        return true;
    })
        .map(function (news, i) {
        var image = exports.getImageThumbnail(news.btThumb, domain);
        return (React.createElement("li", { key: i },
            React.createElement("a", { href: news.url, title: news.title },
                React.createElement("div", { className: "btoday-card" },
                    React.createElement("div", { className: "btoday-thumb", dangerouslySetInnerHTML: { __html: image } }),
                    React.createElement("div", { className: "btoday-title" }, news.title),
                    React.createElement("div", { className: "btoday-summary" }, news.summary),
                    React.createElement("div", { className: "btoday-dateline" },
                        news.byline,
                        " - ",
                        news.publishDate)))));
    });
};
// Useful for images that aren't hosted on the local site
exports.getImageThumbnail = function (image, domain) {
    if (image === void 0) { image = ""; }
    if (domain === void 0) { domain = ""; }
    return domain !== "" ? image.replace('src="/', "src=\"" + domain + "/") : image;
};
// Useful for images that aren't hosted on the local site
exports.getFeaturedImage = function (image, domain) {
    if (image === void 0) { image = ""; }
    if (domain === void 0) { domain = ""; }
    return domain !== "" ? domain + image : image;
};
exports.default = exports.BasinTodayListView;
//# sourceMappingURL=index.js.map