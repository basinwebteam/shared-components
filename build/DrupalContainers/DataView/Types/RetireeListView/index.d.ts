import * as React from "react";
export declare const RetireeListView: React.SFC<IProps>;
export declare const orderByPromoted: (a: IMLViewData, b: IMLViewData) => 1 | -1;
export declare const getImageThumbnail: (image?: string, domain?: string) => string;
export default RetireeListView;
export interface IProps {
    data?: IMLViewData[];
    domain?: string;
}
export interface IMLViewData {
    title: string;
    url: string;
    publishDate: string;
    image: string;
    content: string;
    sticky: BooleanString | string;
    promote?: BooleanString | string;
}
export declare type BooleanString = "False" | "True";
