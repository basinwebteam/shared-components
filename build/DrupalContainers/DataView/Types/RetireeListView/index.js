"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.RetireeListView = function (_a) {
    var _b = _a.data, data = _b === void 0 ? [] : _b, _c = _a.domain, domain = _c === void 0 ? "" : _c;
    return (React.createElement("div", { className: "retiree-dataview" }, data
        .sort(exports.orderByPromoted)
        .map(function (news, i) {
        var image = exports.getImageThumbnail(news.image, domain);
        return (React.createElement("div", { className: "retiree-article", key: i },
            React.createElement("h2", { className: "retiree-article-title" }, news.title),
            React.createElement("div", { className: "retiree-article-dateline", dangerouslySetInnerHTML: { __html: news.publishDate } }),
            React.createElement("div", { className: "retiree-article-body", dangerouslySetInnerHTML: { __html: news.content } }),
            image !== "" && React.createElement("div", { className: "retiree-article-img", dangerouslySetInnerHTML: { __html: image } })));
    })));
};
exports.RetireeListView.displayName = "RetireeListView";
exports.orderByPromoted = function (a, b) {
    if (a.promote === "True") {
        return -1;
    }
    return 1;
};
// Useful for images that aren't hosted on the local site
exports.getImageThumbnail = function (image, domain) {
    if (image === void 0) { image = ""; }
    if (domain === void 0) { domain = ""; }
    return domain !== "" ? image.replace('src="/', "src=\"" + domain + "/") : image;
};
exports.default = exports.RetireeListView;
//# sourceMappingURL=index.js.map