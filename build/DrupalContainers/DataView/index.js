"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var React = require("react");
var Type = require("./Types/dataViewTypes");
var Wrapper_1 = require("./Wrapper");
var AgendaListView_1 = require("./Types/AgendaListView");
var BasinTodayListView_1 = require("./Types/BasinTodayListView");
var FacilityListView_1 = require("./Types/FacilityListView");
var LatestNewsListView_1 = require("./Types/LatestNewsListView");
var MemberListView_1 = require("./Types/MemberListView");
var RetireeEventView_1 = require("./Types/RetireeEventView");
var RetireePagesView_1 = require("./Types/RetireePagesView");
/**
 * DataView
 */
var DataView = (function (_super) {
    __extends(DataView, _super);
    function DataView(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this.getDefaultState();
        return _this;
    }
    DataView.prototype.render = function () {
        return (React.createElement("div", { className: "data-view " + this.state.type },
            React.createElement(Wrapper_1.default, { url: this.state.restUrl, siteDomain: this.props.siteDomain }, this.getChildComponent())));
    };
    DataView.prototype.getChildComponent = function () {
        var dev = this.props.dev || false;
        switch (this.state.type) {
            case (Type.MEMBER_LIST):
                return React.createElement(MemberListView_1.default, null);
            case (Type.FACILITY_LIST):
                return React.createElement(FacilityListView_1.default, null);
            case (Type.FEDERAL_LEGISLATION_NEWS_ARTICLES):
            case (Type.LATEST_NEWS_BEPC):
            case (Type.LATEST_NEWS_DGC):
            case (Type.NEWS_RELEASES_BEPC):
            case (Type.NEWS_RELEASES_DGC):
            case (Type.NEWS_BRIEFS_BEPC):
            case (Type.NEWS_BRIEFS_DGC):
            case (Type.NEWS_ABOUT_BEPC):
            case (Type.NEWS_ABOUT_DGC):
            case (Type.STATE_LEGISLATION_NEWS_ARTICLES):
                return React.createElement(LatestNewsListView_1.default, null);
            case (Type.BASIN_TODAY_LIST):
                return React.createElement(BasinTodayListView_1.default, { domain: dev ? this.props.siteDomain : "" });
            case (Type.AGENDA_LIST):
                return React.createElement(AgendaListView_1.default, null);
            case (Type.RETIREES_EVENTS):
                return React.createElement(RetireeEventView_1.default, null);
            case (Type.RETIREES_RETIREMENTS):
            case (Type.RETIREES_THROWBACK):
            case (Type.RETIREES_NEWS):
            case (Type.RETIREES_HOME):
                return React.createElement(RetireePagesView_1.default, { domain: this.props.siteDomain });
            default:
                return (React.createElement("div", { className: "DefaultDataViewComponent {this.state.type}" }));
        }
    };
    DataView.prototype.componentDidMount = function () {
        this.updateComponent();
    };
    DataView.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props !== prevProps) {
            this.updateComponent();
        }
    };
    DataView.prototype.updateComponent = function () {
        var block = this.getView();
        if (block) {
            this.getViewContent(block);
        }
    };
    DataView.prototype.getView = function () {
        var _this = this;
        var view = this.props.includedRelationships.find(function (item) {
            return (item.id === _this.props.paragraphId);
        });
        if (typeof view === "undefined") {
            return false;
        }
        return view.relationships.field_view;
    };
    DataView.prototype.getViewContent = function (view) {
        return __awaiter(this, void 0, void 0, function () {
            var content;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        content = this.props.includedRelationships.find(function (item) { return item.id === view.data.id; });
                        if (!(typeof content === "undefined")) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.getChildViewContent(view.data.id)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        this.setState({
                            type: content.attributes.id,
                            id: content.id,
                            restUrl: this.getRestUrl(content),
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    DataView.prototype.getChildViewContent = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/view/view/" + id)];
                    case 1:
                        response = _a.sent();
                        this.setState({
                            type: response.data.data.attributes.id,
                            id: response.data.data.id,
                            restUrl: this.getRestUrl(response.data.data),
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    DataView.prototype.getRestUrl = function (childViewContent) {
        try {
            return childViewContent.attributes.display.rest_export_1.display_options.path + "?_format=json";
        }
        catch (e) {
            return "";
        }
    };
    DataView.prototype.getDefaultState = function () {
        /*
            This tries to get this view and the child view's data.

            If any of it is missing, this returns an empty state.
         */
        try {
            var view_1 = this.getView();
            if (!view_1) {
                throw Error("View not found");
            }
            var content = this.props.includedRelationships.find(function (item) { return item.id === view_1.data.id; });
            var restUrl = this.getRestUrl(content);
            return {
                type: content.attributes.id,
                id: content.id,
                restUrl: restUrl,
            };
        }
        catch (e) {
            return {
                type: "",
                id: "",
                restUrl: "",
            };
        }
    };
    return DataView;
}(React.Component));
exports.DataView = DataView;
exports.default = DataView;
//# sourceMappingURL=index.js.map