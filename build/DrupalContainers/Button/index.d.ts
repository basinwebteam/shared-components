import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
export declare class ButtonContainer extends React.Component<ButtonContainerProps, ButtonContainerState> {
    constructor(props: ButtonContainerProps);
    displayName: string;
    componentDidMount(): Promise<void>;
    render(): JSX.Element;
    getButtonData(paragraphId: string, includedRelationships: Page.IPageData[], siteDomain: string): Promise<{
        url: string;
        title: string;
    }>;
}
export default ButtonContainer;
export interface ButtonContainerState {
    title: string;
    url: string;
}
export interface ButtonContainerProps {
    paragraphId: string;
    includedRelationships: Page.IPageData[];
    siteDomain: string;
}
export interface ButtonRelationships extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_website_link: Page.ILinkField;
    };
}
