import { AxiosResponse } from "axios";
import * as React from "react";
import * as Page from "../../DrupalContentTypes/PageInterface";
/**
 * Tests TODO
 * Without default props sent
 * With default props sent
 */
export declare class ImageContainer extends React.Component<IImageContainerProps, IImageContainerState> {
    constructor(props: IImageContainerProps);
    displayName: string;
    defaultProps: Partial<IImageContainerProps>;
    render(): JSX.Element;
    componentDidMount(): void;
    componentDidUpdate(prevProps: IImageContainerProps, prevState: IImageContainerState): void;
    updateImage(): Promise<void>;
    queryImage(id: string): Promise<void>;
    getFieldCaption(image: IImageData | undefined): string;
    queryFile(id: string): Promise<string>;
    private getAltTextFromImageData(image);
    private getFileIdFromImageData(image);
    private findImageFile(fileId);
    private findImage();
    private findImageLink(image);
    private findDefaultImageLink(image);
    private getDefaultState();
}
export default ImageContainer;
export interface IImageContainerProps {
    includedRelationships: Array<Page.IPageData | IImageFileData>;
    paragraphId: string;
    siteDomain: string;
    imageParagraphName?: string;
    imageFileFieldName?: imageFileFieldName;
    className?: string;
}
export interface IDefaultPropsImageContainer {
    imageParagraphName: string;
    imageFileFieldName: imageFileFieldName;
}
export interface IImageContainerState {
    altText: string;
    url: string;
    caption: string;
    link: string | null;
}
export interface IAxiosResponseImage extends AxiosResponse {
    data: IImageInfo;
}
export interface IAxiosResponseImageFile extends AxiosResponse {
    data: IImageFileInfo;
}
export interface IImageInfo extends Page.IPage {
    data: IImageData;
    included: IImageFileData[];
}
export interface IImageData extends Page.IPageData {
    attributes: IAttributes;
    relationships: IImageDataRelationships;
}
export interface IAttributes extends Page.IAttributes {
    field_caption?: string | {
        value: string;
        format: string;
    };
    field_website_link?: Page.ILinkField;
}
export interface IImageDataRelationships extends Page.IRelationships {
    field_image?: Page.IField_Image;
    field_image_aside?: Page.IField_Image;
}
export interface IImageFileInfo {
    data: IImageFileData;
}
export interface IImageFileData extends Page.IPageData {
    attributes: IIncludedAttributes;
}
export interface IIncludedAttributes extends Page.IAttributes {
    uri: string;
    url: string;
}
export declare type PropsWithDefaults = IImageContainerProps & IDefaultPropsImageContainer;
export declare type imageFileFieldName = "field_image" | "field_image_aside";
