"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var React = require("react");
var sitemap_1 = require("../../DrupalAPI/sitemap");
var Image_1 = require("../../components/Image");
/**
 * Tests TODO
 * Without default props sent
 * With default props sent
 */
var ImageContainer = (function (_super) {
    __extends(ImageContainer, _super);
    function ImageContainer(props) {
        var _this = _super.call(this, props) || this;
        _this.displayName = "DrupalImageContainer";
        _this.defaultProps = {
            imageFileFieldName: "field_image",
            imageParagraphName: "image"
        };
        _this.state = _this.getDefaultState();
        return _this;
    }
    ImageContainer.prototype.render = function () {
        var _a = this.state, url = _a.url, altText = _a.altText, caption = _a.caption, link = _a.link;
        if (url === "") {
            return React.createElement("span", { className: "empty-image-placeholder" });
        }
        if (link) {
            return (React.createElement("a", { href: link },
                React.createElement(Image_1.default, { url: url, altText: altText, caption: caption, className: (this.props.className) ? this.props.className : "" })));
        }
        return (React.createElement(Image_1.default, { url: url, altText: altText, caption: caption, className: (this.props.className) ? this.props.className : "" }));
    };
    ImageContainer.prototype.componentDidMount = function () {
        this.updateImage();
    };
    ImageContainer.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateImage();
        }
    };
    ImageContainer.prototype.updateImage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var paragraphId, image, link, fileId, file, url;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        paragraphId = this.props.paragraphId;
                        image = this.findImage();
                        return [4 /*yield*/, this.findImageLink(image)];
                    case 1:
                        link = _a.sent();
                        if (!(typeof image === "undefined")) return [3 /*break*/, 2];
                        this.queryImage(paragraphId);
                        return [3 /*break*/, 6];
                    case 2:
                        fileId = this.getFileIdFromImageData(image);
                        file = this.findImageFile(fileId);
                        url = "";
                        if (!(typeof file === "undefined")) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.queryFile(fileId)];
                    case 3:
                        url = _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        url = this.props.siteDomain + file.attributes.url;
                        _a.label = 5;
                    case 5:
                        this.setState({
                            url: url,
                            altText: this.getAltTextFromImageData(image),
                            caption: this.getFieldCaption(image),
                            link: link
                        });
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ImageContainer.prototype.queryImage = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/json/paragraph/" + this.props.imageParagraphName + "/" + id + "/?include=" + this.props.imageFileFieldName)];
                    case 1:
                        response = _a.sent();
                        this.setState({
                            url: this.props.siteDomain + response.data.included[0].attributes.url,
                            altText: this.getAltTextFromImageData(response.data.data),
                            caption: this.getFieldCaption(response.data.data),
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    ImageContainer.prototype.getFieldCaption = function (image) {
        var defaultCaption = (typeof this.state !== "undefined") ? this.state.caption : "";
        if (typeof image === "undefined") {
            return defaultCaption;
        }
        if (!image.attributes.hasOwnProperty("field_caption")) {
            return defaultCaption;
        }
        if (typeof image.attributes.field_caption === null) {
            return defaultCaption;
        }
        if (typeof image.attributes.field_caption === "string") {
            return image.attributes.field_caption || defaultCaption;
        }
        if (typeof image.attributes.field_caption === "object" && image.attributes.field_caption !== null) {
            return image.attributes.field_caption.value || defaultCaption;
        }
        return defaultCaption;
    };
    ImageContainer.prototype.queryFile = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (id === "") {
                            return [2 /*return*/, ""];
                        }
                        return [4 /*yield*/, axios_1.default.get("//api.bepc.com/api/cms/file/" + id)];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, this.props.siteDomain + response.data.data.attributes.url];
                }
            });
        });
    };
    ImageContainer.prototype.getAltTextFromImageData = function (image) {
        var defaultTxt = (typeof this.state !== "undefined") ? this.state.altText : "";
        if (typeof image === "undefined") {
            return defaultTxt;
        }
        if (typeof image.relationships.field_image !== "undefined" &&
            image.relationships.field_image.data !== null) {
            return image.relationships.field_image.data.meta.alt;
        }
        if (typeof image.relationships.field_image_aside !== "undefined" &&
            image.relationships.field_image_aside.data !== null) {
            return image.relationships.field_image_aside.data.meta.alt;
        }
        return defaultTxt;
    };
    ImageContainer.prototype.getFileIdFromImageData = function (image) {
        if (typeof image === "undefined") {
            return "";
        }
        if (typeof image.relationships.field_image !== "undefined" &&
            image.relationships.field_image.data !== null) {
            return image.relationships.field_image.data.id;
        }
        if (typeof image.relationships.field_image_aside !== "undefined" &&
            image.relationships.field_image_aside.data !== null) {
            return image.relationships.field_image_aside.data.id;
        }
        return "";
    };
    ImageContainer.prototype.findImageFile = function (fileId) {
        if (fileId === "") {
            return undefined;
        }
        return this.props.includedRelationships.find(function (paragraph) { return paragraph.id === fileId; });
    };
    ImageContainer.prototype.findImage = function () {
        var _this = this;
        return this.props.includedRelationships.find(function (paragraph) { return paragraph.id === _this.props.paragraphId; });
    };
    ImageContainer.prototype.findImageLink = function (image) {
        return __awaiter(this, void 0, void 0, function () {
            var link, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        link = this.findDefaultImageLink(image);
                        if (!link) return [3 /*break*/, 2];
                        return [4 /*yield*/, sitemap_1.getUrl(link, this.props.siteDomain)];
                    case 1:
                        _a = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        _a = null;
                        _b.label = 3;
                    case 3: return [2 /*return*/, _a];
                }
            });
        });
    };
    ImageContainer.prototype.findDefaultImageLink = function (image) {
        if (typeof image === "undefined") {
            return null;
        }
        if (typeof image.attributes.field_website_link === "undefined") {
            return null;
        }
        if (!image.attributes.field_website_link) {
            return null;
        }
        return image.attributes.field_website_link.uri;
    };
    ImageContainer.prototype.getDefaultState = function () {
        /*
            This tries to get the image and related file if its part of the props paragraphData value.

            If any of it is missing, this returns an empty state.
        */
        var image = this.findImage();
        var fileId = this.getFileIdFromImageData(image);
        var file = this.findImageFile(fileId);
        var caption = this.getFieldCaption(image);
        var link = this.findDefaultImageLink(image);
        if (typeof file !== "undefined") {
            return {
                altText: this.getAltTextFromImageData(image),
                url: this.props.siteDomain + file.attributes.url,
                caption: caption,
                link: link,
            };
        }
        return {
            altText: "",
            url: "",
            caption: "",
            link: link,
        };
    };
    return ImageContainer;
}(React.Component));
exports.ImageContainer = ImageContainer;
exports.default = ImageContainer;
//# sourceMappingURL=index.js.map