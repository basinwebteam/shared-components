/**
 * COMPONENTS
 *
 * Base Components with no HTTP requests & minimal logic.
 * These can be used on any site & do not contain logic specific to Drupal or some other site.
 */
import * as components from "./components/index";
export declare const Components: typeof components;
/**
 * DRUPAL CONTAINERS
//
*/
import * as containers from "./DrupalContainers/index";
export declare const DrupalContainers: typeof containers;
/**
 * Drupal Types
 */
import * as paragraphType from "./DrupalContentTypes/ParagraphTypes";
export declare const ParagraphType: typeof paragraphType;
/**
 * Drupal Interfaces
 */
export { IPath, IAttributes, IAttributesTrimmed, IData, ILinks, IType, ITypeMultipleData, ITypeSingleData, IUid, IRevisionUid, IMenuLink, IModerationState, IMeta, IDatum, IFieldContent, IScheduledUpdate, IRelationships, IPageData, IPage, IPageDataTrimmed, IField_Image, IField_File, IField_FileAttributes, IWYSIWYG, IParagraphFile, ITaxonomyData, ITaxonomyAttributes } from "./DrupalContentTypes/PageInterface";
