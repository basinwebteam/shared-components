"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.YouTubeVideo = function (props) {
    var autoplay = (props.autoplay) ? "1" : "0";
    return (React.createElement("div", { className: "video-youtube", style: styles.videoWrapper },
        React.createElement("iframe", { style: styles.iframe, width: "560", height: "315", src: "https://www.youtube.com/embed/" + exports.getVideoId(props.link) + "?rel=0&autoplay=" + autoplay, frameBorder: "0", allowFullScreen: true })));
};
var styles = {
    videoWrapper: {
        position: "relative",
        paddingBottom: "56.25%",
        paddingTop: "25px",
        height: 0,
    },
    iframe: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
    },
};
exports.getVideoId = function (linkStringArray) {
    var link = "";
    if (Array.isArray(linkStringArray)) {
        link = linkStringArray[0];
    }
    else {
        link = linkStringArray;
    }
    if (link.startsWith("https://youtu.be/")) {
        return link.substr(17, 11);
    }
    if (link.startsWith("https://www.youtube.com/watch?v=")) {
        return link.substr(32, 11);
    }
    if (link.startsWith("https://www.youtube.com/embed")) {
        return link.substr(30, 11);
    }
    return link;
};
exports.default = exports.YouTubeVideo;
//# sourceMappingURL=index.js.map