import * as React from "react";
export declare const YouTubeVideo: React.StatelessComponent<IYouTubeProps>;
export declare const getVideoId: (linkStringArray: string | string[]) => string;
export default YouTubeVideo;
export interface IYouTubeProps {
    link: string;
    autoplay?: boolean;
}
