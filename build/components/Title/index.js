"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.Title = function (props) { return (React.createElement("h2", { className: "paragraph-title" }, props.children)); };
exports.Title.displayName = "Title";
exports.default = exports.Title;
//# sourceMappingURL=index.js.map