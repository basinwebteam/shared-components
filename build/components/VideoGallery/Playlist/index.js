"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Thumbnail_1 = require("../Thumbnail");
exports.Playlist = function (props) {
    var videos = props.videos.map(function (video) { return (__assign({ playVideoClickHandler: props.playVideoClickHandler }, video)); });
    return (React.createElement("div", { className: "videoList", id: "playlist-" + props.instance },
        React.createElement("h2", null, props.title),
        React.createElement("ul", { className: "vid-gal-list-item block-grid-4" }, videos.map(function (video, i) {
            return React.createElement(Thumbnail_1.default, { key: props.instance.toString() + i.toString(), id: video.id, title: video.title, url: video.url, thumb: video.thumb, playVideoClickHandler: video.playVideoClickHandler });
        }))));
};
exports.Playlist.displayName = "Playlist";
exports.default = exports.Playlist;
//# sourceMappingURL=index.js.map