import * as React from "react";
export declare const Playlist: React.StatelessComponent<IVideoPlaylistProps>;
export default Playlist;
export interface IVideoPlaylistProps {
    instance: number;
    title: string;
    videos: IVideo[];
    playVideoClickHandler: (videoId: string) => void;
    showMorePlaylistClickHandler?: () => void;
}
export interface IVideo {
    id: string;
    title: string;
    url: string;
    thumb: string;
}
