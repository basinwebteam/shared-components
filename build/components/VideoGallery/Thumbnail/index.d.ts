import * as React from "react";
export declare const Thumbnail: React.StatelessComponent<IProps>;
export default Thumbnail;
export interface IProps {
    id: string;
    title: string;
    url: string;
    thumb: string;
    playVideoClickHandler: (videoId: string) => void;
}
