"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.Thumbnail = function (_a) {
    var id = _a.id, title = _a.title, url = _a.url, thumb = _a.thumb, playVideoClickHandler = _a.playVideoClickHandler;
    var clickHandler = function () { return playVideoClickHandler(url); };
    return (React.createElement("li", { key: id },
        React.createElement("a", { className: "vid-gal-list-link rel-vid-item", onClick: clickHandler, href: "#videoPlayer" },
            React.createElement("div", { className: "rel-vid-icon", "aria-label": "Play Button" }),
            React.createElement("div", { className: "rel-vid-thumb" },
                React.createElement("img", { "data-source": thumb, src: thumb }),
                React.createElement("small", null, title)))));
};
exports.Thumbnail.displayName = "Thumbnail";
exports.default = exports.Thumbnail;
//# sourceMappingURL=index.js.map