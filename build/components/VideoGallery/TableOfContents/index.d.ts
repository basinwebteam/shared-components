import * as React from "react";
export declare const VideoTOC: React.StatelessComponent<IVideoTOCProps>;
export default VideoTOC;
export interface IVideoTOCProps {
    playlists: IPlaylist[];
}
export interface IPlaylist {
    id: string;
    title: string;
}
