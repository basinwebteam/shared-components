"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.VideoTOC = function (props) {
    return (React.createElement("div", { className: "videoFeed" }, props.playlists.map(function (item, i) {
        return React.createElement("a", { key: i, className: "button expand", href: "#playlist-" + item.id }, item.title);
    })));
};
exports.VideoTOC.displayName = "VideoTOC";
exports.default = exports.VideoTOC;
//# sourceMappingURL=index.js.map