import * as React from 'react';
import { ILinks } from "../../Menu";
export declare class MenuLink extends React.Component<PropsWithDefaultsMenuLink, IMenuLinkState> {
    constructor(props: PropsWithDefaultsMenuLink);
    static defaultProps: Partial<IMenuLinkProps>;
    render(): JSX.Element;
    listItemClass(): string;
    handleLinkClick(e: React.MouseEvent<HTMLElement>): boolean;
    getLinks(links: Array<ILinks>): JSX.Element[];
    private isActive();
    private activeIdBelongsToDescendant();
    private findDescendantIfContainsActiveId(children);
    private getChildren();
}
export default MenuLink;
export interface IPropsRequired {
    id: string;
    path: string;
    title: string;
    onLinkClick: (e: React.MouseEvent<HTMLElement>, id: string) => boolean;
    onBackClick: (e: React.MouseEvent<HTMLElement>) => void;
}
export interface IMenuLinkProps extends IPropsRequired {
    activeId?: string;
    children?: Array<ILinks>;
    className?: string;
    activeForce?: boolean;
}
export interface IMenuLinkState {
    active: boolean;
    activeChildren: boolean;
}
export interface IDefaultMenuLinkProps {
    activeId: string;
    children: Array<ILinks>;
    className?: string;
    activeForce: boolean;
}
export declare type PropsWithDefaultsMenuLink = IMenuLinkProps & IDefaultMenuLinkProps;
