"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var MenuLink = (function (_super) {
    __extends(MenuLink, _super);
    function MenuLink(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            active: _this.isActive(),
            activeChildren: false
        };
        _this.handleLinkClick = _this.handleLinkClick.bind(_this);
        return _this;
    }
    MenuLink.prototype.render = function () {
        return (React.createElement("li", { className: this.listItemClass() },
            React.createElement("a", { href: this.props.path, onClick: this.handleLinkClick },
                this.props.title,
                this.props.children.length > 0 ? React.createElement("span", { className: "more" }) : ""),
            this.props.children.length > 0 ? this.getChildren() : ""));
    };
    MenuLink.prototype.listItemClass = function () {
        var classList = ["menu-link"];
        classList.push(this.props.className ? this.props.className : "");
        classList.push(this.isActive() ? "active" : "");
        classList.push(this.activeIdBelongsToDescendant() ? "active-parent" : "");
        classList.push(this.props.activeForce ? "active-forced" : "");
        return classList.join(" ").trim();
    };
    MenuLink.prototype.handleLinkClick = function (e) {
        e.stopPropagation();
        e.preventDefault();
        return this.props.onLinkClick(e, this.props.id);
    };
    MenuLink.prototype.getLinks = function (links) {
        var _this = this;
        return links.map(function (link, i) {
            var url = link.url, title = link.title, key = link.key, _a = link.children, children = _a === void 0 ? [] : _a;
            return (React.createElement(MenuLink, { key: i, id: key, path: url, title: title, children: children, activeId: _this.props.activeId, activeForce: false, onLinkClick: _this.props.onLinkClick, onBackClick: _this.props.onBackClick }));
        });
    };
    MenuLink.prototype.isActive = function () {
        return this.props.activeId === this.props.id;
    };
    MenuLink.prototype.activeIdBelongsToDescendant = function () {
        var descendant = this.findDescendantIfContainsActiveId(this.props.children);
        if (typeof descendant !== "undefined") {
            return true;
        }
        return false;
    };
    MenuLink.prototype.findDescendantIfContainsActiveId = function (children) {
        var _this = this;
        return children.find(function (child) {
            if (child.key === _this.props.activeId) {
                return true;
            }
            if (typeof child.children !== "undefined") {
                var descendant = _this.findDescendantIfContainsActiveId(child.children);
                if (typeof descendant !== "undefined") {
                    return true;
                }
            }
            return false;
        });
    };
    MenuLink.prototype.getChildren = function () {
        var title = this.props.title + " Overview";
        var _a = this.props, onBackClick = _a.onBackClick, path = _a.path, children = _a.children;
        return (React.createElement("ul", null,
            React.createElement("li", { className: "back", onClick: onBackClick },
                React.createElement("span", null, "Back")),
            React.createElement("li", { className: "menu-overview" },
                React.createElement("a", { href: path }, title),
                React.createElement("ul", { className: "menu-inner-wrapper" },
                    React.createElement("li", null,
                        React.createElement("ul", null, this.getLinks(children)))))));
    };
    MenuLink.defaultProps = {
        activeId: "",
        className: "",
        children: []
    };
    return MenuLink;
}(React.Component));
exports.MenuLink = MenuLink;
exports.default = MenuLink;
//# sourceMappingURL=index.js.map