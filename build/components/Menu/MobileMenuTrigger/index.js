"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.MobileMenuTrigger = function (props) { return (React.createElement("div", { className: "mobile-menu-trigger-wrapper", style: { display: props.display ? "block" : "none" } },
    React.createElement("div", { className: "mobile-menu-trigger", title: "Menu Button", onClick: props.onClick },
        React.createElement("span", null),
        "Menu"))); };
//# sourceMappingURL=index.js.map