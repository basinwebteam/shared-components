import * as React from "react";
export declare const MobileMenuTrigger: React.StatelessComponent<IProps>;
export interface IProps {
    display: boolean;
    onClick: () => void;
}
