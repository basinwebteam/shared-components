"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var MenuLink_1 = require("./MenuLink");
var MobileMenuTrigger_1 = require("./MobileMenuTrigger");
var Menu = (function (_super) {
    __extends(Menu, _super);
    function Menu(props) {
        var _this = _super.call(this, props) || this;
        _this.defaultProps = {
            widthToChange: 840,
            linkHandler: function (url, key, e) {
                if (typeof window !== "undefined") {
                    window.location.href = e.target.href;
                }
            }
        };
        _this.state = {
            menuOpen: false,
            menuVersion: _this.determineMenuVersion(),
            activeId: "",
        };
        _this.toggleMenuOpenClosed = _this.toggleMenuOpenClosed.bind(_this);
        _this.closeMenu = _this.closeMenu.bind(_this);
        _this.clickLink = _this.clickLink.bind(_this);
        _this.clickBack = _this.clickBack.bind(_this);
        _this.resizeEventListener = _this.resizeEventListener.bind(_this);
        return _this;
    }
    Menu.prototype.render = function () {
        return (React.createElement("div", { className: this.menuClass() },
            React.createElement(MobileMenuTrigger_1.MobileMenuTrigger, { display: this.state.menuVersion === "Mobile", onClick: this.toggleMenuOpenClosed }),
            React.createElement("nav", { role: "navigation" },
                React.createElement("ul", null, this.getLinks()))));
    };
    Menu.prototype.menuClass = function () {
        var classList = ["mega-menu"];
        classList.push(this.state.menuVersion.toLowerCase());
        if (this.state.menuOpen) {
            classList.push("menu-open");
        }
        if (this.state.activeId) {
            classList.push("default");
        }
        return classList.join(" ");
    };
    Menu.prototype.componentDidMount = function () {
        if (typeof window !== "undefined") {
            window.addEventListener("resize", this.resizeEventListener);
            window.addEventListener("reset_mobile_menu", this.closeMenu);
        }
    };
    Menu.prototype.componentWillUnmount = function () {
        if (typeof window !== "undefined") {
            window.removeEventListener("resize", this.resizeEventListener);
            window.removeEventListener("reset_mobile_menu", this.closeMenu);
        }
    };
    Menu.prototype.resizeEventListener = function () {
        this.setState({
            menuVersion: this.determineMenuVersion()
        });
    };
    Menu.prototype.getLinks = function () {
        var _this = this;
        return this.props.links.map(function (link, i) {
            var url = link.url, title = link.title, key = link.key, _a = link.children, children = _a === void 0 ? [] : _a;
            return (React.createElement(MenuLink_1.default, { key: i, id: key, path: url, title: title, children: children, activeId: _this.state.activeId, className: "top-menu-item-" + i, activeForce: _this.state.activeId === "", onLinkClick: _this.clickLink, onBackClick: _this.clickBack }));
        });
    };
    Menu.prototype.clickLink = function (e, key) {
        switch (e.type) {
            case "touchstart":
            default:
                return false;
            case "touchend":
            case "click":
                //target could be a span or a tag on mobile. Make sure it's the a tag.
                var target = e.target;
                if (target.classList.contains("more")) {
                    target = target.parentElement;
                }
                if (this.state.menuVersion === "Mobile") {
                    if (this.hasChildren(target)) {
                        this.setState({ activeId: key });
                        return false;
                    }
                }
                if (this.state.menuVersion === "Large") {
                    if (this.hasNoAncestors(key) && this.isNotOverviewLink(target)) {
                        this.setState({ activeId: key });
                        return false;
                    }
                }
                if (this.props.linkHandler) {
                    this.props.linkHandler(target.href, key, e);
                }
                return false;
        }
    };
    Menu.prototype.clickBack = function (e) {
        var parentId = this.findParentId(this.props.links, this.state.activeId);
        this.setState({ activeId: parentId });
    };
    Menu.prototype.findParentId = function (links, idToFind, parentId) {
        var _this = this;
        if (parentId === void 0) { parentId = ""; }
        links.find(function (link) {
            if (idToFind === link.key) {
                return true;
            }
            if (typeof link.children !== "undefined") {
                parentId = _this.findParentId(link.children, idToFind);
                if (parentId !== "") {
                    return true;
                }
            }
            return false;
        });
        return parentId;
    };
    Menu.prototype.hasChildren = function (linkElement) {
        return linkElement.children.length > 0
            && linkElement.children[0].classList.contains("more");
    };
    Menu.prototype.isNotOverviewLink = function (linkElement) {
        return linkElement.childElementCount !== 0;
    };
    Menu.prototype.hasNoAncestors = function (key) {
        return typeof this.props.links.find(function (link) { return link.key === key; }) !== "undefined";
    };
    Menu.prototype.toggleMenuOpenClosed = function () {
        this.setState({
            menuOpen: !this.state.menuOpen,
            activeId: "",
        });
    };
    Menu.prototype.closeMenu = function () {
        this.setState({
            menuOpen: false,
            activeId: "",
        });
    };
    Menu.prototype.determineMenuVersion = function () {
        var widthToChange = this.props.widthToChange;
        if (typeof window !== "undefined") {
            return window.innerWidth >= widthToChange ? "Large" : "Mobile";
        }
        return "Large";
    };
    return Menu;
}(React.Component));
exports.Menu = Menu;
exports.default = Menu;
//# sourceMappingURL=index.js.map