import * as React from 'react';
export declare class Menu extends React.Component<IMenuProps, IMenuState> {
    defaultProps: Partial<IMenuProps>;
    constructor(props: IMenuProps);
    render(): JSX.Element;
    menuClass(): string;
    componentDidMount(): void;
    componentWillUnmount(): void;
    resizeEventListener(): void;
    getLinks(): JSX.Element[];
    clickLink(e: React.MouseEvent<HTMLElement>, key: string): boolean;
    clickBack(e: React.MouseEvent<HTMLElement>): void;
    findParentId(links: ILinks[], idToFind: string, parentId?: string): string;
    hasChildren(linkElement: HTMLElement): boolean;
    isNotOverviewLink(linkElement: HTMLElement): boolean;
    hasNoAncestors(key: string): boolean;
    toggleMenuOpenClosed(): void;
    closeMenu(): void;
    determineMenuVersion(): MenuVersion;
}
export default Menu;
export interface IMenuProps {
    links: Array<ILinks>;
    widthToChange?: number;
    linkHandler?: (url: string, key: string, e: React.MouseEvent<HTMLElement>) => void;
}
export interface IMenuState {
    menuOpen: boolean;
    menuVersion: MenuVersion;
    activeId: string;
}
export interface IDefaultProps {
    widthToChange: number;
    linkHandler: (url: string, key: string, e: React.MouseEvent<HTMLElement>) => void;
}
export interface ILinks {
    key: string;
    title: string;
    url: string;
    children?: Array<ILinks>;
}
export declare type MenuVersion = "Large" | "Mobile";
export declare type PropsWithDefaults = IMenuProps & IDefaultProps;
