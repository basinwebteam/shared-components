import * as React from "react";
export declare const Biography: React.StatelessComponent<IBiographyProps>;
export default Biography;
export interface IBiographyProps {
    firstName: string;
    lastName: string;
    title: string;
    biographyDescription: string;
    emailAddress?: string;
    phoneNumber?: string;
}
