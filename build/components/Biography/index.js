"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.Biography = function (props) {
    var emailAddress = props.emailAddress || "";
    var phoneNumber = props.phoneNumber || "";
    var wrapperClass = props.children ? "biography biography-plus" : "biography";
    return (React.createElement("div", { className: wrapperClass },
        React.createElement("p", { className: "bio-name" },
            props.firstName,
            " ",
            props.lastName),
        props.children,
        React.createElement("div", { className: "bio-info" },
            React.createElement("p", { className: "bio-title" }, props.title),
            emailAddress !== "" && (React.createElement("div", { className: "bio-email" },
                "Email: ",
                React.createElement("a", { href: "mailto:" + emailAddress },
                    props.firstName,
                    " ",
                    props.lastName))),
            phoneNumber !== "" && (React.createElement("div", { className: "bio-phone" },
                "Phone: ",
                phoneNumber)),
            React.createElement("div", { className: "bio-description", dangerouslySetInnerHTML: { __html: props.biographyDescription } }))));
};
exports.Biography.displayName = "Biography";
exports.default = exports.Biography;
//# sourceMappingURL=index.js.map