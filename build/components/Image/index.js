"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.Image = function (props) {
    var className = (props.className) ? props.className : "";
    if (props.caption) {
        return (React.createElement("figure", { className: className },
            React.createElement("img", { src: props.url, alt: props.altText }),
            React.createElement("figcaption", { dangerouslySetInnerHTML: { __html: props.caption } })));
    }
    return React.createElement("img", { className: className, src: props.url, alt: props.altText });
};
exports.Image.displayName = "Image";
exports.default = exports.Image;
//# sourceMappingURL=index.js.map