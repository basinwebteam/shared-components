import * as React from "react";
export declare const Image: React.SFC<IImageProps>;
export default Image;
export interface IImageProps {
    altText: string;
    url: string;
    caption?: string;
    className?: string;
}
