"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Grid_1 = require("./Grid");
var Menu_1 = require("./Menu");
var Table_1 = require("./Table");
var Bio_1 = require("./Bio");
var BoardOfDirectors = (function (_super) {
    __extends(BoardOfDirectors, _super);
    function BoardOfDirectors(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this.getDefaultState();
        _this.showBio = _this.showBio.bind(_this);
        _this.closeBio = _this.closeBio.bind(_this);
        return _this;
    }
    BoardOfDirectors.prototype.render = function () {
        var _this = this;
        var currentBoard = this.props.boards[this.state.currentBoard];
        var boardMenuBoards = this.getBoardMenuBoardsProperty();
        currentBoard.members = currentBoard.members.map(function (_a) {
            var name = _a.name, image = _a.image, title = _a.title, district = _a.district, description = _a.description;
            return ({
                name: name,
                image: image,
                title: title,
                district: district,
                description: description,
                onClick: function () { return _this.showBio(name, image, title, district, description); }
            });
        });
        var bio = this.state.currentBoardMember;
        return (React.createElement("div", { className: "board-of-directors" },
            React.createElement("div", { className: "row" },
                React.createElement("div", { className: "medium-6 large-4 columns" },
                    React.createElement(Menu_1.default, { boards: boardMenuBoards, activeIndex: this.state.currentBoard }),
                    React.createElement(Table_1.default, { name: currentBoard.name, people: currentBoard.members })),
                React.createElement("div", { className: "medium-6 large-8 columns" },
                    React.createElement(Grid_1.default, { people: currentBoard.members }))),
            React.createElement(Bio_1.default, { name: bio.name, title: bio.title, image: bio.image, district: bio.district, description: bio.description, className: this.state.showBoardMember ? "show" : "hide", onClose: this.closeBio })));
    };
    BoardOfDirectors.prototype.getBoardMenuBoardsProperty = function () {
        var _this = this;
        return this.props.boards.map(function (board, i) {
            return ({
                name: board.name,
                onClick: function () {
                    _this.setState({
                        currentBoard: i
                    });
                    _this.scrollToTopIfMobile();
                }
            });
        });
    };
    BoardOfDirectors.prototype.scrollToTopIfMobile = function () {
        if (window.pageYOffset > 200) {
            window.scrollTo(0, 0);
        }
    };
    BoardOfDirectors.prototype.closeBio = function () {
        this.setState(function () { return ({ showBoardMember: false }); });
    };
    BoardOfDirectors.prototype.showBio = function (name, image, title, district, description) {
        if (description === void 0) { description = ""; }
        this.setState(function (prevState, _a) {
            var boards = _a.boards;
            return ({
                currentBoardMember: {
                    name: name,
                    title: title,
                    image: image,
                    district: district,
                    description: description
                },
                showBoardMember: true
            });
        });
    };
    BoardOfDirectors.prototype.getDefaultState = function () {
        if (this.props.boards.length > 0) {
            return {
                currentBoard: 0,
                currentBoardMember: {
                    name: "",
                    title: "",
                    image: "",
                    district: "",
                    description: ""
                },
                showBoardMember: false,
            };
        }
        return {
            currentBoard: 0,
            currentBoardMember: {
                name: "",
                title: "",
                image: "",
                district: "",
                description: ""
            },
            showBoardMember: false,
        };
    };
    return BoardOfDirectors;
}(React.Component));
exports.BoardOfDirectors = BoardOfDirectors;
exports.default = BoardOfDirectors;
//# sourceMappingURL=index.js.map