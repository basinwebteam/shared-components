import * as React from "react";
export declare class BoardOfDirectors extends React.Component<IBoardOfDirectorsProps, IBoardOfDirectorsState> {
    constructor(props: IBoardOfDirectorsProps);
    state: {
        currentBoard: number;
        currentBoardMember: {
            name: string;
            title: string;
            image: string;
            district: string;
            description: string;
        };
        showBoardMember: boolean;
    };
    render(): JSX.Element;
    getBoardMenuBoardsProperty(): {
        name: string;
        onClick: () => void;
    }[];
    scrollToTopIfMobile(): void;
    closeBio(): void;
    showBio(name: string, image: string, title: string, district: string, description?: string): void;
    getDefaultState(): {
        currentBoard: number;
        currentBoardMember: {
            name: string;
            title: string;
            image: string;
            district: string;
            description: string;
        };
        showBoardMember: boolean;
    };
}
export default BoardOfDirectors;
export interface IBoardOfDirectorsProps {
    boards: IBoard[];
}
export interface IBoardOfDirectorsState {
    currentBoard: number;
    currentBoardMember: ICurrentBoardMember;
    showBoardMember: boolean;
}
export interface IPeople {
    name: string;
    image: string;
    title: string;
    district: string;
    description?: string;
    onClick?: () => void;
}
export interface ICurrentBoardMember {
    name: string;
    title: string;
    image: string;
    district: string;
    description: string;
}
export interface IBoard {
    name: string;
    members: IPeople[];
    onClick?: () => void;
}
