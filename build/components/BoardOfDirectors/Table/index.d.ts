import * as React from "react";
export declare const Table: React.StatelessComponent<IProps>;
export default Table;
export interface IProps {
    name: string;
    people: IPeople[];
}
export interface IPeople {
    title: string;
    name: string;
    district: string;
    onClick?: () => void;
}
