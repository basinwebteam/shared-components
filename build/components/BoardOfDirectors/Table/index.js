"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.Table = function (_a) {
    var people = _a.people, name = _a.name, props = __rest(_a, ["people", "name"]);
    return (React.createElement("div", { className: "row" },
        React.createElement("div", { className: "medium-12 column" },
            React.createElement("div", { className: "board-table" },
                React.createElement("h3", null,
                    name,
                    " Board"),
                React.createElement("table", null,
                    React.createElement("thead", null,
                        React.createElement("tr", null,
                            React.createElement("th", null, "Title"),
                            React.createElement("th", null, "Name"),
                            React.createElement("th", null, "District"))),
                    React.createElement("tbody", null, people.map(function (_a, i) {
                        var title = _a.title, name = _a.name, district = _a.district, onClick = _a.onClick;
                        return (React.createElement("tr", { key: i },
                            React.createElement("td", { className: "title" }, title),
                            React.createElement("td", { onClick: onClick, className: "name" }, name),
                            React.createElement("td", { className: "district" }, district == "0" ? "External" : district)));
                    })))))));
};
exports.default = exports.Table;
//# sourceMappingURL=index.js.map