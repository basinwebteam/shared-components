import * as React from "react";
export declare const Bio: React.StatelessComponent<IProps>;
export default Bio;
export interface IProps {
    name: string;
    title: string;
    image: string;
    district: string;
    description: string;
    className?: string;
    onClose?: () => void;
}
