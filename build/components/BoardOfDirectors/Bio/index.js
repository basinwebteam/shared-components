"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.Bio = function (_a) {
    var name = _a.name, title = _a.title, image = _a.image, district = _a.district, description = _a.description, _b = _a.className, className = _b === void 0 ? "" : _b, _c = _a.onClose, onClose = _c === void 0 ? (function () { return (""); }) : _c;
    var onClick = function (event) {
        var target = event.target;
        if (target.classList.contains("board-bio-wrapper") ||
            target.classList.contains("close-button")) {
            onClose();
        }
    };
    return (React.createElement("div", { className: "board-bio-wrapper " + className, onClick: onClick },
        React.createElement("div", { className: "board-bio" },
            React.createElement("button", { className: "close-button", onClick: onClick, type: "button", "aria-label": "Close alert" },
                React.createElement("span", { "aria-hidden": "true" }, "\u00D7")),
            React.createElement("span", { className: "board-bio-pic" },
                React.createElement("img", { src: image })),
            React.createElement("span", { className: "board-bio-details" },
                React.createElement("div", { className: "name" }, name),
                React.createElement("div", { className: "district" }, district == "0" ? "External" : "District " + district),
                React.createElement("div", { className: "description", dangerouslySetInnerHTML: { __html: description } })))));
};
exports.default = exports.Bio;
//# sourceMappingURL=index.js.map