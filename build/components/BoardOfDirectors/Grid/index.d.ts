import * as React from "react";
export declare const Grid: React.StatelessComponent<IProps>;
export default Grid;
export interface IProps {
    people: IPeople[];
}
export interface IPeople {
    name: string;
    image: string;
    district: string;
    onClick?: () => void;
}
