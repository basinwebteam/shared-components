"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.Grid = function (_a) {
    var people = _a.people, props = __rest(_a, ["people"]);
    return (React.createElement("div", { className: "board-grid" },
        React.createElement("ul", { className: "small-block-grid-2 large-block-grid-3" }, people.map(function (_a, i) {
            var name = _a.name, district = _a.district, image = _a.image, onClick = _a.onClick;
            return (React.createElement("li", { className: "board-member", onClick: onClick, key: i },
                React.createElement("img", { src: image }),
                React.createElement("div", { className: "dir-name" }, name),
                React.createElement("div", { className: "dir-district" }, district == "0" ? "External" : "District " + district)));
        }))));
};
exports.default = exports.Grid;
//# sourceMappingURL=index.js.map