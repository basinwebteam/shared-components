"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.BoardMenu = function (props) {
    var isActive = function (index, activeIndex) { return activeIndex === index ? "disabled" : ""; };
    var className = props.className || "button expand";
    return (React.createElement("div", { className: "row" },
        React.createElement("div", { className: "medium-12 column" },
            React.createElement("div", { className: "board-menu" },
                React.createElement("ul", null, props.boards.map(function (_a, i) {
                    var name = _a.name, onClick = _a.onClick;
                    return React.createElement("li", { className: className + " " + isActive(i, props.activeIndex), onClick: onClick, key: i }, name);
                }))))));
};
exports.default = exports.BoardMenu;
//# sourceMappingURL=index.js.map