import * as React from "react";
export declare const BoardMenu: React.StatelessComponent<IProps>;
export default BoardMenu;
export interface IProps {
    boards: IBoard[];
    activeIndex: number;
    className?: string;
}
export interface IBoard {
    name: string;
    onClick?: () => void;
}
