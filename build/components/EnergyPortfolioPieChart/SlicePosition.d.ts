export declare class SlicePosition {
    position: number;
    /**
     * Sets a pie slices position as a number between 0 and 6.28(tau).
     * 0 and 6.28 are the top center of the pie. 3.14(pi) is the bottom center.
     * @param startAngle Start angle of an arc
     * @param endAngle End angle of an arc
     */
    constructor(startAngle: number, endAngle: number);
    /**
     * Determines a pie slices position in circle.
     * Positions are not evenly split. Left & right side are given more value.
     * Adjust points array as needed.
     * Useful for placing multiple lines of text outside a pie chart.
     */
    toList(): string;
    /**
     * Returns a value from a closure based on the slice position
     */
    returnValue(positionsToReturnValue: slicePositionsList): any;
    /**
     * Does slice lay on left half of pie
     */
    isLeftHalf(): boolean;
    /**
     * Does slice lay on right half of pie
     */
    isRightHalf(): boolean;
}
export default SlicePosition;
export interface slicePositionsList {
    'top': (() => any);
    'right': (() => any);
    'bottom': (() => any);
    'left': (() => any);
}
