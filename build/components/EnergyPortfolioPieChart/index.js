"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var d3_scale_1 = require("d3-scale");
var d3_shape_1 = require("d3-shape");
var d3_format_1 = require("d3-format");
var d3_selection_1 = require("d3-selection");
var SlicePosition_1 = require("./SlicePosition");
var EnergyPortfolioPieChart = (function (_super) {
    __extends(EnergyPortfolioPieChart, _super);
    function EnergyPortfolioPieChart(props) {
        var _this = _super.call(this, props) || this;
        _this.basinColors = [
            [
                '#D22630',
                '#dc575f',
                '#009FDF',
                '#ED8B00',
                '#004967',
                '#78BE20',
                '#b3b5b6',
                '#4b4d4f',
            ],
            [
                '#86181f',
                '#985900',
                '#4d7a14',
                '#00668f',
            ]
        ];
        _this.generationPieColor = d3_scale_1.scaleOrdinal().range(_this.basinColors[0]);
        _this.generationRenewablesPieColor = d3_scale_1.scaleOrdinal().range(_this.basinColors[1]);
        _this.basinColorsCombined = _this.basinColors[0].concat(_this.basinColors[1]);
        _this.generationColorsCombined = d3_scale_1.scaleOrdinal().range(_this.basinColorsCombined);
        _this.textStyle = {
            'font-family': 'nimbus-sans, Helvetica, Arial, sans-serif',
            'font-weight': 'bold',
        };
        _this.orderedGenData = new Array();
        _this.orderResources();
        _this.totalMW = _this.orderedGenData.reduce(function (a, b) { return a + b.mw; }, 0);
        _this.renewablePieEndAngle = Math.round(_this.props.generationRenewableData[0].mw / _this.totalMW * 360);
        _this.chartRadius = Math.min(_this.props.size[0], _this.props.size[1]) / 2.4;
        return _this;
    }
    EnergyPortfolioPieChart.prototype.render = function () {
        var _this = this;
        return (React.createElement("div", null,
            React.createElement("div", { className: "small-12 column" },
                React.createElement("svg", { ref: function (node) { return _this.node = node; }, width: this.props.size[0], height: this.props.size[1], style: { cursor: "pointer" } })),
            React.createElement("div", { className: "small-12 column", id: "js-at-a-glance-chart-legend" },
                React.createElement("div", null),
                React.createElement("ul", null)),
            React.createElement("div", { className: "small-12 column" },
                React.createElement("p", null,
                    React.createElement("strong", null,
                        "Total = ",
                        d3_format_1.format(".1f")(this.totalMW),
                        " MW")))));
    };
    EnergyPortfolioPieChart.prototype.componentDidMount = function () {
        this.createChart();
    };
    EnergyPortfolioPieChart.prototype.orderResources = function () {
        var _this = this;
        [
            "Wind",
            "Recovered",
            "Coal",
            "Hydro",
            "Nuclear",
            "Natural",
            "Oil",
            "Unspecified"
        ].forEach(function (resource) { return _this.addResource(resource); });
    };
    EnergyPortfolioPieChart.prototype.addResource = function (name) {
        var resource = this.props.generationData.find(function (d) { return d.name.startsWith(name); });
        if (resource !== undefined) {
            this.orderedGenData.push(resource);
        }
    };
    EnergyPortfolioPieChart.prototype.createChart = function () {
        var _this = this;
        var node = this.node;
        var generationArc = d3_shape_1.arc()
            .outerRadius(this.chartRadius - 10)
            .innerRadius(0);
        var generationRenewableArc = d3_shape_1.arc()
            .outerRadius(this.chartRadius - 50)
            .innerRadius(0);
        var generationPie = d3_shape_1.pie()
            .sort(null)
            .value(function (data) { return data.mw; });
        // Create a partial pie to lay over generationPie
        var generationRenewablePie = d3_shape_1.pie()
            .sort(null)
            .value(function (data) { return data.mw; })
            .startAngle(0)
            .endAngle(this.renewablePieEndAngle * (Math.PI / 180));
        var generationSVG = d3_selection_1.select(node)
            .attr('viewBox', "20 0 " + this.props.size[0] + " " + this.props.size[1] + " ")
            .attr('preserveAspectRatio', 'xMinYMin meet')
            .append('g')
            .attr('transform', "translate( " + this.props.size[0] / 2 + ", " + this.props.size[1] / 2 + " )");
        var generationPieSlices = generationSVG.selectAll('.generationSlice')
            .data(generationPie(this.orderedGenData))
            .enter()
            .append('g')
            .attr('class', 'generationSlice');
        generationPieSlices.append("path")
            .attr('d', generationArc)
            .style("fill", function (d) { return _this.generationPieColor(d.data.name); });
        function translateByNameHack(d, axis, line) {
            if (d.data.name == 'Oil' && axis == 'x' && line == 1) {
                return -25;
            }
            if (d.data.name == 'Oil' && axis == 'x' && line == 2) {
                return -15;
            }
            if (d.data.name == 'Oil' && axis == 'y' && line == 1) {
                return 3;
            }
            if (d.data.name == 'Unspecified' && axis == 'x' && line == 2) {
                return 23;
            }
            return 0;
        }
        var generationPieSliceLine1Attributes = {
            "transform": function (d) {
                var slicePosition = new SlicePosition_1.SlicePosition(d.startAngle, d.endAngle);
                var yPaddingAmount = slicePosition.returnValue({
                    'top': function () { return -24; },
                    'bottom': function () { return 0; },
                    'left': function () { return 0; },
                    'right': function () { return 0; },
                });
                var c = generationArc.centroid(d);
                return "translate(" + (c[0] * 2.1 + (5) + (translateByNameHack(d, 'x', 1))) + "," + (c[1] * 2.1 + yPaddingAmount + (translateByNameHack(d, 'y', 1))) + ")";
            },
            "text-anchor": function (d) {
                var slicePosition = new SlicePosition_1.SlicePosition(d.startAngle, d.endAngle);
                return slicePosition.returnValue({
                    'top': function () { return 'middle'; },
                    'bottom': function () { return 'middle'; },
                    'left': function () { return 'end'; },
                    'right': function () { return 'start'; },
                });
            },
            "dy": ".35em",
        };
        //Small Devices
        generationPieSlices.append("text")
            .attr("transform", generationPieSliceLine1Attributes.transform)
            .attr("text-anchor", generationPieSliceLine1Attributes["text-anchor"])
            .attr("dy", generationPieSliceLine1Attributes.dy)
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .classed('hide-for-medium-up', true)
            .text(function (d) {
            return _this.round10((d.data.mw / _this.totalMW) * 100, -1) + '%';
        });
        //Larger Devices
        generationPieSlices.append("text")
            .attr("transform", generationPieSliceLine1Attributes.transform)
            .attr("text-anchor", generationPieSliceLine1Attributes["text-anchor"])
            .attr("dy", generationPieSliceLine1Attributes.dy)
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .classed('hide-for-small-only', true)
            .text(function (d) {
            return d.data.name;
        });
        generationPieSlices.append("text")
            .attr("transform", function (d) {
            var mwPercentage = _this.round10((d.data.mw / _this.totalMW) * 100, -1) + '%';
            //Get Y Padding
            var slicePosition = new SlicePosition_1.SlicePosition(d.startAngle, d.endAngle);
            var yPaddingAmount = slicePosition.returnValue({
                'top': function () { return -24; },
                'bottom': function () { return 0; },
                'left': function () { return 0; },
                'right': function () { return 0; },
            });
            //Get X Padding
            var xPaddingAmount = (d.data.name.length - mwPercentage.length) ? Math.round((d.data.name.length - mwPercentage.length) / 2) : 0;
            if (slicePosition.isLeftHalf()) {
                xPaddingAmount = xPaddingAmount * -1;
            }
            var c = generationArc.centroid(d);
            return "translate(" + (c[0] * 2.1 + (xPaddingAmount * 5) + (translateByNameHack(d, 'x', 2))) + "," + (c[1] * 2.1 + 16 + yPaddingAmount + (translateByNameHack(d, 'y', 2))) + ")";
        })
            .attr("dy", ".35em")
            .attr("text-anchor", function (d) {
            var slicePosition = new SlicePosition_1.SlicePosition(d.startAngle, d.endAngle);
            //return slicePosition.isLeftHalf() ? "end" : "start"
            return slicePosition.returnValue({
                'top': function () { return 'middle'; },
                'bottom': function () { return 'middle'; },
                'left': function () { return 'end'; },
                'right': function () { return 'start'; },
            });
        })
            .classed('hide-for-small-only', true)
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .text(function (d) {
            var mwPercentage = _this.round10((d.data.mw / _this.totalMW) * 100, -1) + '%';
            return mwPercentage;
        });
        var generationRenewablePieSlices = generationSVG.selectAll('.generationRenewableSlice')
            .data(generationRenewablePie(this.props.generationRenewableData))
            .enter()
            .append('g')
            .attr('class', 'generationRenewableSlice');
        generationRenewablePieSlices.append("path")
            .attr('d', generationRenewableArc)
            .style("fill", function (d) { return _this.generationRenewablesPieColor(d.data.name); });
        generationRenewablePieSlices.append("text")
            .attr("transform", function (d) {
            var c = generationArc.centroid(d);
            return "translate(" + c[0] * 1.2 + "," + c[1] * 1.2 + ")";
        })
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .style('fill', '#FFF')
            .classed('hide-for-small-only', true)
            .text(function (d) {
            return d.data.name;
        });
        generationRenewablePieSlices.append("text")
            .attr("transform", function (d) {
            var c = generationArc.centroid(d);
            return "translate(" + c[0] * 1.2 + "," + (c[1] * 1.2 + 16) + ")";
        })
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .style('fill', '#FFF')
            .text(function (d) {
            return _this.round10((d.data.mw / _this.totalMW) * 100, -1) + '%';
        });
        var legendRectSize = 14;
        var legendSpacing = 4;
        var legendContainer = d3_selection_1.select('#js-at-a-glance-chart-legend');
        legendContainer.select('div')
            .style('font-weight', 'bold')
            .text('Maximum winter capability in MW');
        legendContainer.select('ul')
            .style('margin-left', 0);
        var legendList = d3_selection_1.select('#js-at-a-glance-chart-legend ul');
        var legend = legendList.selectAll('.legend')
            .data(this.orderedGenData.concat(this.props.generationRenewableData))
            .enter()
            .append('li')
            .attr('class', 'legend')
            .style('float', 'left')
            .style('list-style-type', 'none')
            .style('margin-right', '25px');
        legend.append('div')
            .style('width', legendRectSize + 'px')
            .style('height', legendRectSize + 'px')
            .style('display', 'inline-block')
            .style('margin-right', '5px')
            .style('background-color', function (d) { return _this.generationColorsCombined(d.mw); });
        legend.append('span')
            .style('font-weight', 'bold')
            .style('font-size', '14px')
            .text(function (d) {
            return d.longName + ' - ' + d.mw.toLocaleString();
        });
        legend.on("click", function (data) { return _this.dispatchEvent(data); });
        generationRenewablePieSlices.on('click', function (data) { return _this.dispatchEvent(data.data); });
        generationPieSlices.on('click', function (data) { return _this.dispatchEvent(data.data); });
    };
    EnergyPortfolioPieChart.prototype.dispatchEvent = function (data) {
        this.props.clickHandler(data.name);
    };
    EnergyPortfolioPieChart.prototype.decimalAdjust = function (type, value, exp) {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    };
    EnergyPortfolioPieChart.prototype.round10 = function (value, exp) {
        return this.decimalAdjust('round', value, exp);
    };
    EnergyPortfolioPieChart.prototype.floor10 = function (value, exp) {
        return this.decimalAdjust('floor', value, exp);
    };
    EnergyPortfolioPieChart.prototype.ceil10 = function (value, exp) {
        return this.decimalAdjust('ceil', value, exp);
    };
    return EnergyPortfolioPieChart;
}(React.Component));
exports.EnergyPortfolioPieChart = EnergyPortfolioPieChart;
exports.default = EnergyPortfolioPieChart;
//# sourceMappingURL=index.js.map