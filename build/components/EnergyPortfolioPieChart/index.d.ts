import * as React from "react";
import { ScaleOrdinal } from 'd3-scale';
export declare class EnergyPortfolioPieChart extends React.Component<IPropsEnergyPortfolio> {
    node: SVGElement;
    basinColors: string[][];
    generationPieColor: ScaleOrdinal<string, {}>;
    generationRenewablesPieColor: ScaleOrdinal<string, {}>;
    basinColorsCombined: string[];
    generationColorsCombined: ScaleOrdinal<string, {}>;
    textStyle: {
        'font-family': string;
        'font-weight': string;
    };
    totalMW: number;
    chartRadius: number;
    renewablePieEndAngle: number;
    orderedGenData: GenerationData[];
    constructor(props: IPropsEnergyPortfolio);
    render(): JSX.Element;
    componentDidMount(): void;
    orderResources(): void;
    addResource(name: string): void;
    createChart(): void;
    dispatchEvent(data: GenerationData): void;
    decimalAdjust(type: string, value: any, exp: number): number;
    round10(value: any, exp: number): number;
    floor10(value: any, exp: number): number;
    ceil10(value: any, exp: number): number;
}
export default EnergyPortfolioPieChart;
export interface IPropsEnergyPortfolio {
    size: number[];
    generationData: GenerationData[];
    generationRenewableData: GenerationData[];
    clickHandler: (name: string) => void;
}
export interface GenerationData {
    name: string;
    mw: number;
    mwSummer?: number;
    longName: string;
    photo?: JSX.Element | string;
    videos?: JSX.Element | string;
    plants?: GenerationDataPlants[];
    briefInfo: JSX.Element | string;
}
export interface GenerationDataPlants {
    name: string;
    url: string;
}
