"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SlicePosition = (function () {
    /**
     * Sets a pie slices position as a number between 0 and 6.28(tau).
     * 0 and 6.28 are the top center of the pie. 3.14(pi) is the bottom center.
     * @param startAngle Start angle of an arc
     * @param endAngle End angle of an arc
     */
    function SlicePosition(startAngle, endAngle) {
        this.position = (startAngle + endAngle) / 2;
    }
    /**
     * Determines a pie slices position in circle.
     * Positions are not evenly split. Left & right side are given more value.
     * Adjust points array as needed.
     * Useful for placing multiple lines of text outside a pie chart.
     */
    SlicePosition.prototype.toList = function () {
        var points = [
            .78,
            2.75,
            3.53,
            5.75
        ];
        if (this.position >= points[3] || (this.position >= 0 && this.position <= points[0])) {
            return 'top';
        }
        if (this.position > points[0] && this.position < points[1]) {
            return 'right';
        }
        if (this.position >= points[1] && this.position <= points[2]) {
            return 'bottom';
        }
        if (this.position > points[2] && this.position < points[3]) {
            return 'left';
        }
        return '';
    };
    /**
     * Returns a value from a closure based on the slice position
     */
    SlicePosition.prototype.returnValue = function (positionsToReturnValue) {
        var position = this.toList();
        return positionsToReturnValue[position]();
    };
    /**
     * Does slice lay on left half of pie
     */
    SlicePosition.prototype.isLeftHalf = function () {
        if (this.position < Math.PI) {
            return false;
        }
        return true;
    };
    /**
     * Does slice lay on right half of pie
     */
    SlicePosition.prototype.isRightHalf = function () {
        if (this.position < Math.PI) {
            return true;
        }
        return false;
    };
    return SlicePosition;
}());
exports.SlicePosition = SlicePosition;
exports.default = SlicePosition;
//# sourceMappingURL=SlicePosition.js.map