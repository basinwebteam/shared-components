import * as React from "react";
export declare const VimeoVideo: React.StatelessComponent<IVimeoProps>;
export default VimeoVideo;
export interface IVimeoProps {
    link: string;
}
