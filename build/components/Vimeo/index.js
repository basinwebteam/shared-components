"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.VimeoVideo = function (props) { return (React.createElement("div", { className: "video-vimeo", style: styles.videoWrapper },
    React.createElement("iframe", { style: styles.iframe, allowFullScreen: true, frameBorder: "0", height: "281", src: props.link, width: "500" }))); };
var styles = {
    videoWrapper: {
        position: "relative",
        paddingBottom: "56.25%",
        paddingTop: "25px",
        height: 0,
    },
    iframe: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
    },
};
exports.default = exports.VimeoVideo;
//# sourceMappingURL=index.js.map