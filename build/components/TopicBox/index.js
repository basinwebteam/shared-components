"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.TopicBox = function (_a) {
    var title = _a.title, _b = _a.className, className = _b === void 0 ? "" : _b, _c = _a.image, image = _c === void 0 ? "" : _c, _d = _a.paragraph, paragraph = _d === void 0 ? "" : _d, _e = _a.color, color = _e === void 0 ? "blue" : _e, _f = _a.url, url = _f === void 0 ? null : _f;
    if (url === null) {
        return (React.createElement("div", { className: "column box " + className },
            React.createElement("div", { className: "box-title title-" + color }, title),
            image,
            paragraph));
    }
    else {
        return (React.createElement("div", { className: "column box " + className },
            React.createElement("a", { href: url },
                React.createElement("div", { className: "box-title title-" + color }, title),
                image),
            paragraph));
    }
};
exports.TopicBox.displayName = "TopicBox";
exports.default = exports.TopicBox;
//# sourceMappingURL=index.js.map