import * as React from "react";
export declare const TopicBox: React.StatelessComponent<IProps>;
export default TopicBox;
export interface IProps {
    title: string;
    url?: string;
    color?: string;
    className?: string;
    image?: any;
    paragraph?: any;
}
