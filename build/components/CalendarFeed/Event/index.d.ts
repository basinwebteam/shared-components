import * as React from "react";
export declare const Event: React.SFC<IEventProps>;
export default Event;
export interface IEventProps {
    id: string;
    date: string;
    title: string;
    location?: string;
    description: string;
    link?: string;
}
