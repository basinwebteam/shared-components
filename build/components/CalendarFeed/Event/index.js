"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
var React = require("react");
exports.Event = function (_a) {
    var id = _a.id, date = _a.date, title = _a.title, _b = _a.location, location = _b === void 0 ? "" : _b, _c = _a.description, description = _c === void 0 ? "" : _c;
    var mdate = moment(date);
    return (React.createElement("div", { className: "row event-item", id: "id" + id },
        React.createElement("div", { className: "small-12 large-2 columns" },
            React.createElement("div", { className: "event-date-box" },
                React.createElement("span", { className: "event-month" }, mdate.format("MMM")),
                React.createElement("span", { className: "event-day" }, mdate.format("D")))),
        React.createElement("div", { className: "small-12 large-10 columns" },
            React.createElement("p", { className: "event-title" }, title),
            React.createElement("p", { className: "event-date-string" }, mdate.format("ddd, MMM D, YYYY")),
            location !== "" && React.createElement("p", { className: "event-location" }, location),
            description !== "" && React.createElement("p", { className: "event-desc", dangerouslySetInnerHTML: { __html: description } }))));
};
exports.default = exports.Event;
//# sourceMappingURL=index.js.map