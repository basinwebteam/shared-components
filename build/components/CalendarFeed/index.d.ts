import * as React from "react";
import { IEventProps } from "./Event";
export declare const CalendarFeed: React.SFC<ICalendarFeedProps>;
export default CalendarFeed;
export declare const displayEvents: (events: IEventProps[]) => JSX.Element[];
export interface ICalendarFeedProps {
    events: IEventProps[];
    className?: string;
}
