"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var Event_1 = require("./Event");
exports.CalendarFeed = function (_a) {
    var events = _a.events, _b = _a.className, className = _b === void 0 ? "" : _b;
    var wrapperClassName = "cal-holder" + className;
    return (React.createElement("div", { className: wrapperClassName },
        React.createElement("div", { className: "calendarList" }, exports.displayEvents(events))));
};
exports.CalendarFeed.displayName = "CalendarFeed";
exports.default = exports.CalendarFeed;
exports.displayEvents = function (events) { return (events.map(function (event, i) {
    if (event.link && event.link !== "") {
        return (React.createElement("a", { href: event.link },
            React.createElement(Event_1.Event, { key: event.id + i, id: event.id, date: event.date, title: event.title, location: event.location, description: event.description })));
    }
    else {
        return (React.createElement(Event_1.Event, { key: event.id + i, id: event.id, date: event.date, title: event.title, location: event.location, description: event.description }));
    }
})); };
//# sourceMappingURL=index.js.map