import * as React from "react";
import { GenerationData } from "../EnergyPortfolioPieChart";
export declare const EnergyPortfolioResourceTable: React.SFC<IEPResourceResourceTableProps>;
export declare function calculateResourceData(data: GenerationData[]): CalculatedData;
export default EnergyPortfolioResourceTable;
export interface IEPResourceResourceTableProps {
    data: GenerationData[];
    operatedGenSummer: string;
    operatedGenWinter: string;
    ownedGenSummer: string;
    ownedGenWinter: string;
    purchasedGenSummer: string;
    purchasedGenWinter: string;
}
export interface CalculatedData {
    totalSummerMW: number;
    totalWinterMW: number;
    resources: CalculatedDataResource[];
}
export interface CalculatedDataResource {
    name: string;
    mw: number;
    mwSummer: number;
    summerCap: number;
    winterCap: number;
}
