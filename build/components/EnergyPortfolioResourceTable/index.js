"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var d3_format_1 = require("d3-format");
exports.EnergyPortfolioResourceTable = function (props) {
    var data = calculateResourceData(props.data);
    return (React.createElement("div", null,
        React.createElement("h2", null, "Basin Electric Generation Portfolio"),
        React.createElement("table", null,
            React.createElement("thead", null,
                React.createElement("tr", null,
                    React.createElement("th", null, "Total"),
                    React.createElement("th", null, "Summer (MW)"),
                    React.createElement("th", null, "Winter (MW)"),
                    React.createElement("th", null, "Summer Capacity (%)"),
                    React.createElement("th", null, "Winter Capacity (%)"))),
            React.createElement("tbody", null,
                data.resources.map(function (_a, i) {
                    var name = _a.name, mw = _a.mw, mwSummer = _a.mwSummer, summerCap = _a.summerCap, winterCap = _a.winterCap;
                    return (React.createElement("tr", { key: i },
                        React.createElement("td", null, name),
                        React.createElement("td", { className: "text-right" }, d3_format_1.format(",")(mwSummer)),
                        React.createElement("td", { className: "text-right" }, d3_format_1.format(",")(mw)),
                        React.createElement("td", { className: "text-right" },
                            d3_format_1.format(".1f")(summerCap * 100),
                            "%"),
                        React.createElement("td", { className: "text-right" },
                            d3_format_1.format(".1f")(winterCap * 100),
                            "%")));
                }),
                React.createElement("tr", null,
                    React.createElement("td", null,
                        React.createElement("strong", null, "Total generation")),
                    React.createElement("td", { className: "text-right" },
                        React.createElement("strong", null, d3_format_1.format(",.1f")(data.totalSummerMW))),
                    React.createElement("td", { className: "text-right" },
                        React.createElement("strong", null, d3_format_1.format(",.1f")(data.totalWinterMW))),
                    React.createElement("td", null, "\u00A0"),
                    React.createElement("td", null, "\u00A0")),
                React.createElement("tr", null,
                    React.createElement("td", null, "Owned Generation"),
                    React.createElement("td", { className: "text-right" }, d3_format_1.format(",.1f")(parseFloat(props.ownedGenSummer))),
                    React.createElement("td", { className: "text-right" }, d3_format_1.format(",.1f")(parseFloat(props.ownedGenWinter))),
                    React.createElement("td", { className: "text-right" },
                        d3_format_1.format(".1f")(parseFloat(props.ownedGenSummer) / data.totalSummerMW * 100),
                        "%"),
                    React.createElement("td", { className: "text-right" },
                        d3_format_1.format(".1f")(parseFloat(props.ownedGenWinter) / data.totalWinterMW * 100),
                        "%")),
                React.createElement("tr", null,
                    React.createElement("td", null, "Purchased Generation"),
                    React.createElement("td", { className: "text-right" }, d3_format_1.format(",.1f")(parseFloat(props.purchasedGenSummer))),
                    React.createElement("td", { className: "text-right" }, d3_format_1.format(",.1f")(parseFloat(props.purchasedGenWinter))),
                    React.createElement("td", { className: "text-right" },
                        d3_format_1.format(".1f")(parseFloat(props.purchasedGenSummer) / data.totalSummerMW * 100),
                        "%"),
                    React.createElement("td", { className: "text-right" },
                        d3_format_1.format(".1f")(parseFloat(props.purchasedGenWinter) / data.totalWinterMW * 100),
                        "%")),
                React.createElement("tr", null,
                    React.createElement("td", null, "Operated Generation"),
                    React.createElement("td", { className: "text-right" }, d3_format_1.format(",")(parseFloat(props.operatedGenSummer))),
                    React.createElement("td", { className: "text-right" }, d3_format_1.format(",")(parseFloat(props.operatedGenWinter))),
                    React.createElement("td", null, "\u00A0"),
                    React.createElement("td", null, "\u00A0"))))));
};
function calculateResourceData(data) {
    var totalSummerMW = data.reduce(function (a, b) { return (a + (b.mwSummer || 0)); }, 0);
    var totalWinterMW = data.reduce(function (a, b) { return (a + (b.mw || 0)); }, 0);
    var resources = data.map(function (d) { return ({
        name: d.name,
        mw: d.mw,
        mwSummer: d.mwSummer || 0,
        summerCap: (d.mwSummer || 0) / totalSummerMW,
        winterCap: (d.mw || 0) / totalWinterMW
    }); });
    return {
        totalSummerMW: totalSummerMW,
        totalWinterMW: totalWinterMW,
        resources: resources
    };
}
exports.calculateResourceData = calculateResourceData;
exports.default = exports.EnergyPortfolioResourceTable;
//# sourceMappingURL=index.js.map