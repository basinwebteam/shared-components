"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.Button = function (props) {
    var className = (props.className) ? props.className : "default";
    return (React.createElement("div", null,
        React.createElement("a", { href: props.url, className: "button " + className }, props.title)));
};
exports.Button.displayName = "Button";
exports.default = exports.Button;
//# sourceMappingURL=index.js.map