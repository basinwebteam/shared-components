import * as React from "react";
export declare const Button: React.StatelessComponent<IProps>;
export default Button;
export interface IProps {
    url: string;
    title: string;
    className?: string;
}
