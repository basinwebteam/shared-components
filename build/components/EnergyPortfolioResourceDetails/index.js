"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
exports.EnergyPortfolioResourceDetails = function (_a) {
    var name = _a.name, photo = _a.photo, plants = _a.plants, briefInfo = _a.briefInfo, videos = _a.videos;
    return (React.createElement("div", { className: "aside" },
        React.createElement("h2", { className: "paragraph-title" }, "" + name),
        React.createElement("div", { className: "aside-body" },
            photo,
            React.createElement("div", null,
                createPlantsContent(plants, name),
                name === "Resource Details" && (React.createElement("p", null, "Click a resource on the pie chart to view details.")),
                briefInfo,
                videos))));
};
function createPlantsContent(plants, name) {
    if (plants.length > 0) {
        return (React.createElement("div", null,
            React.createElement("h3", { style: { marginTop: "1em" } }, name + " Facilities"),
            React.createElement("div", { className: "aside-body" },
                React.createElement("ul", null, plants.map(function (p, i) { return (React.createElement("li", { key: i },
                    React.createElement("a", { href: p.url }, p.name))); })))));
    }
    return "";
}
exports.createPlantsContent = createPlantsContent;
exports.default = exports.EnergyPortfolioResourceDetails;
//# sourceMappingURL=index.js.map