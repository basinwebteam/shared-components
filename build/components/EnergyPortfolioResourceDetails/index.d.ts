import * as React from "react";
import { GenerationDataPlants } from "../EnergyPortfolioPieChart";
export declare const EnergyPortfolioResourceDetails: React.SFC<IEPResourceDetailsProps>;
export declare function createPlantsContent(plants: GenerationDataPlants[], name: string): "" | JSX.Element;
export default EnergyPortfolioResourceDetails;
export interface IEPResourceDetailsProps {
    name: string;
    photo: JSX.Element | string;
    videos: JSX.Element | string;
    plants: GenerationDataPlants[];
    briefInfo: JSX.Element | string;
}
