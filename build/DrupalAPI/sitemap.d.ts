import { AxiosResponse } from "axios";
export declare const GetNodes: (url: string) => Promise<IDrupalJsonApiNodeData[]>;
export declare const GetSiteMap: (map?: string, useCache?: boolean) => Promise<IDrupalJsonApiNodeData[]>;
export declare const getUrl: (link: string, siteDomain: string) => Promise<string>;
export declare const checkUrlForEntity: (link: string) => boolean;
export interface IDrupalJsonApiNodeResponse extends AxiosResponse {
    data: IDrupalJsonApiNodeObj;
}
export interface IDrupalJsonApiNodeObj {
    "data": IDrupalJsonApiNodeData[];
    "links": any;
}
export interface IDrupalJsonApiNodeData {
    "type": string;
    "id": string;
    "attributes": IDrupalJsonApiNodeAttributes;
}
export interface IDrupalJsonApiNodeAttributes {
    "nid": number;
    "uuid": string;
    "vid": number;
    "langcode": string;
    "status": boolean;
    "title": string;
    "created": number;
    "changed": number;
    "promote": boolean;
    "sticky": boolean;
    "revision_timestamp": number;
    "revision_translation_affected": boolean;
    "default_langcode": boolean;
    "path": IDrupalJsonApiNodePathAlias;
    "field_meta_tags": string;
}
export interface IDrupalJsonApiNodePathAlias {
    "alias": string;
    "pid": number;
}
