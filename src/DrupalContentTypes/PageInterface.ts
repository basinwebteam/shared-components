export interface IPath
{
    alias: string,
    pid: number,
}

export interface IAttributes
{
    nid: number,
    uuid: string,
    vid: number,
    langcode: string,
    status: boolean,
    title: string,
    created: number,
    changed: number,
    promote: boolean,
    sticky: boolean,
    revision_timestamp: number,
    revision_log: object,
    revision_translation_affected: boolean,
    default_langcode: boolean,
    path: IPath,
    field_meta_tags: string,
}
export interface IAttributesTrimmed
{
    path: IPath,
}

export interface IData
{
    type: string,
    id: string,
}

export interface ILinks
{
    self?: string,
    related?: string,
}

export interface IType
{
    data: IData,
    links: ILinks,
}

export interface ITypeMultipleData
{
    data: IData[],
    links: ILinks,
}

export interface ITypeSingleData
{
    data: IData,
    links: ILinks,
}

export interface IUid extends IType {}

export interface IRevisionUid extends IType {}

export interface IMenuLink
{
    data: any,
}

export interface IModerationState extends IMenuLink {}

export interface IMeta
{
    target_revision_id: string,
}

export interface IDatum
{
    type: string,
    id: string,
    meta: IMeta,
}

export interface ILinkField {
    title: string,
    uri: string,
    options: any[]
}

export interface IFieldContent
{
    data: Array<IDatum|undefined>,
    links: ILinks,
}

export interface IScheduledUpdate
{
    object: any[],
}

export interface IRelationships
{
    type: IType,
    uid: IUid,
    revision_uid: IRevisionUid,
    menu_link: IMenuLink,
    moderation_state: IModerationState,
    scheduled_update: IScheduledUpdate,
}

export interface IPageData
{
    type: string,
    id: string,
    attributes: IAttributes,
    relationships: IRelationships,
    links: ILinks,
}

export interface IPage
{
    data: IPageData,
    links?: ILinks,
    included?: Array<IPageData|any>,
}

export interface IPageDataTrimmed
{
    type: string,
    id: string,
    attributes: IAttributesTrimmed,
}

// Field Interfaces - (Should maybe live elsewhere)
export interface IField_Image {
    data: {
        type: string,
        id: string,
        meta: {
            alt: string,
            title: string,
            width: string,
            height: string
        }
    },
    links: ILinks
}

export interface IField_File extends IPageData {
    attributes: IField_FileAttributes
}

export interface IField_FileAttributes extends IAttributes {
    uri: string,
    url: string
}

export type IWYSIWYG = string | null | {
    value: string,
    format: string,
}

export interface IParagraphFile extends IPageData {
    attributes: IAttributes & {
        filename: string,
        uri: string,
        filemime: string,
        filesize: number,
        status: boolean,
        created: number,
        changed: number,
        url: string
    }
}

// Taxonomy Interfaces - (Should maybe live somewhere else)

export interface ITaxonomyData extends IPageData {
    attributes: ITaxonomyAttributes,
}

export interface ITaxonomyAttributes extends IAttributes{
    tid: number,
    name: string,
    weight: number,
}

