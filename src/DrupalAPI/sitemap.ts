import {AxiosResponse, default as axios} from "axios"

export const GetNodes = async (url: string) => await axios.get(url)
    .then((result: IDrupalJsonApiNodeResponse) => result.data.data)

export const GetSiteMap = async (map = "basinelectric", useCache = true) => {
    // TODO Add Api call that gets all types & merges them into one array
    if (typeof window !== "undefined" && useCache) {
        const sitemap = sessionStorage.getItem("sitemap" + map)
        if (sitemap !== null) {
            return JSON.parse(sitemap) as IDrupalJsonApiNodeData[]
        }
    }

    const pages = await GetNodes(`https://api.bepc.com/api/cms/sitemap/${map}/${useCache}`)
    if (typeof window !== "undefined") {
        sessionStorage.setItem("sitemap" + map, JSON.stringify([...pages]))
    }
    return [...pages]
}

export const getUrl = async (link: string, siteDomain: string) => {
    if (checkUrlForEntity(link)) {
        const map = siteDomain
            .replace("https://", "")
            .replace("//", "")
            .replace(".com", "")
        const sitemap = await GetSiteMap(map)
        const matchedPage = sitemap.find((page) =>
            page.attributes.nid.toString() === link.substr(12))
        if (typeof matchedPage !== "undefined") {
            return matchedPage.attributes.path.alias
        }

        const newsSitemap = await GetSiteMap("news")
        const matchedNewsPage = newsSitemap.find((page) =>
            page.attributes.nid.toString() === link.substr(12))
        if (typeof matchedNewsPage !== "undefined") {
            return matchedNewsPage.attributes.path.alias
        }

    }
    return (link !== null) ? link : ""
}

export const checkUrlForEntity = (link: string) => {
    if (link.substr(0, 11) === "entity:node") {
        return true
    }
    return false
}

export interface IDrupalJsonApiNodeResponse extends AxiosResponse {
    data: IDrupalJsonApiNodeObj
}

export interface IDrupalJsonApiNodeObj {
    "data": IDrupalJsonApiNodeData[],
    "links": any
}

export interface IDrupalJsonApiNodeData {
    "type": string,
    "id": string,
    "attributes": IDrupalJsonApiNodeAttributes
}

export interface IDrupalJsonApiNodeAttributes {
    "nid": number,
    "uuid": string,
    "vid": number,
    "langcode": string,
    "status": boolean,
    "title": string,
    "created": number,
    "changed": number,
    "promote": boolean,
    "sticky": boolean,
    "revision_timestamp": number,
    "revision_translation_affected": boolean,
    "default_langcode": boolean,
    "path": IDrupalJsonApiNodePathAlias,
    "field_meta_tags": string
}

export interface IDrupalJsonApiNodePathAlias {
    "alias": string,
    "pid": number
}
