import { AxiosResponse, default as axios } from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import * as Type from "./Types/dataViewTypes"
import DataViewWrapper from "./Wrapper"
import AgendaListView from "./Types/AgendaListView"
import BasinTodayListView from "./Types/BasinTodayListView"
import FacilityListView from "./Types/FacilityListView"
import LatestNewsListView from "./Types/LatestNewsListView"
import MemberListView from "./Types/MemberListView"
import RetireeEventView from "./Types/RetireeEventView"
import RetireePagesView from "./Types/RetireePagesView"


/**
 * DataView
 */
export class DataView extends React.Component<IDataViewProps, IDataViewState> {
    constructor(props: IDataViewProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    public render() {
        return (
            <div className={`data-view ${this.state.type}`}>
                <DataViewWrapper
                    url={this.state.restUrl}
                    siteDomain={this.props.siteDomain}
                >
                    {this.getChildComponent()}
                </DataViewWrapper>
            </div>
        )
    }

    private getChildComponent() {
        const dev = this.props.dev || false
        switch (this.state.type) {
            case (Type.MEMBER_LIST):
                return <MemberListView />
            case (Type.FACILITY_LIST):
                return <FacilityListView />
            case (Type.FEDERAL_LEGISLATION_NEWS_ARTICLES):
            case (Type.LATEST_NEWS_BEPC):
            case (Type.LATEST_NEWS_DGC):
            case (Type.NEWS_RELEASES_BEPC):
            case (Type.NEWS_RELEASES_DGC):
            case (Type.NEWS_BRIEFS_BEPC):
            case (Type.NEWS_BRIEFS_DGC):
            case (Type.NEWS_ABOUT_BEPC):
            case (Type.NEWS_ABOUT_DGC):
            case (Type.STATE_LEGISLATION_NEWS_ARTICLES):
                return <LatestNewsListView />
            case (Type.BASIN_TODAY_LIST):
                return <BasinTodayListView domain={dev ? this.props.siteDomain : ""} />
            case (Type.AGENDA_LIST):
                return <AgendaListView />
            case (Type.RETIREES_EVENTS):
                return <RetireeEventView />
            case (Type.RETIREES_RETIREMENTS):
            case (Type.RETIREES_THROWBACK):
            case (Type.RETIREES_NEWS):
            case (Type.RETIREES_HOME):
                return <RetireePagesView domain={this.props.siteDomain} />
            default:
                return (<div className={`DefaultDataViewComponent {this.state.type}`} />)
        }
    }

    public componentDidMount() {
        this.updateComponent()
    }

    public componentDidUpdate(prevProps: IDataViewProps, prevState: IDataViewState) {
        if (this.props !== prevProps) {
            this.updateComponent()
        }
    }

    private updateComponent() {
        const block = this.getView()
        if (block) {
            this.getViewContent(block)
        }
    }

    private getView() {
        const view = this.props.includedRelationships.find((item) =>
            (item.id === this.props.paragraphId)) as IIncludedData
        if (typeof view === "undefined") {
            return false
        }
        return view.relationships.field_view
    }

    private async getViewContent(view: Page.IType) {
        const content = this.props.includedRelationships.find((item) => item.id === view.data.id) as IViewData

        if (typeof content === "undefined") {
            await this.getChildViewContent(view.data.id)
        } else {
            this.setState({
                type: content.attributes.id,
                id: content.id,
                restUrl: this.getRestUrl(content),
            })
        }
    }

    private async getChildViewContent(id: string) {
        const response = await axios.get
            (`//api.bepc.com/api/cms/json/view/view/${id}`) as IAxiosResponseChildView
        this.setState({
            type: response.data.data.attributes.id,
            id: response.data.data.id,
            restUrl: this.getRestUrl(response.data.data),
        })
    }

    private getRestUrl(childViewContent: IViewData) {
        try {
            return childViewContent.attributes.display.rest_export_1.display_options.path + "?_format=json"
        } catch (e) {
            return ""
        }
    }

    private getDefaultState() {
        /*
            This tries to get this view and the child view's data.

            If any of it is missing, this returns an empty state.
         */
        try {
            const view = this.getView()
            if (!view) {
                throw Error("View not found")
            }
            const content = this.props.includedRelationships.find((item) => item.id === view.data.id) as IViewData
            const restUrl = this.getRestUrl(content)

            return {
                type: content.attributes.id,
                id: content.id,
                restUrl,
            } as IDataViewState
        } catch (e) {
            return {
                type: "",
                id: "",
                restUrl: "",
            }
        }
    }
}

export default DataView

export interface IDataViewProps {
    includedRelationships: any[],
    paragraphId: string,
    siteDomain: string,
    dev?: boolean,
}

export interface IDataViewState {
    type: string
    id: string,
    restUrl: string
}

export interface IIncludedData extends Page.IPageData {
    relationships: IRelationships
}

export interface IRelationships extends Page.IRelationships {
    field_view: Page.IType
}

export interface IAxiosResponseChildView extends AxiosResponse {
    data: IAxiosResponseChildViewData
}

export interface IAxiosResponseChildViewData extends Page.IPage {
    data: IViewData
}

export interface IViewData extends Page.IPageData {
    attributes: IViewDataAttributes
}

export interface IViewDataAttributes extends Page.IAttributes {
    display: IViewDisplay,
    id: string, // Machine Name of view
}

export interface IViewDisplay {
    rest_export_1: IViewDisplayObject
}

export interface IViewDisplayObject {
    display_options: IViewDisplayOptions
}

export interface IViewDisplayOptions {
    path: string
}
