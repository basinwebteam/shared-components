import * as React from "react"

export const AgendaListView: React.SFC<IProps> = ({data=[]}) => (
    <div id="agenda_view" className="agenda-wrapper">
        {getBody(data)}
    </div>
)

AgendaListView.displayName = "AgendaListView"

const getBody = (data: IMLViewData[] = []) => {
    const days = sortByDay(data)
    return days.map((day, i) => (
        <div className="agenda-day-wrapper" key={i}>
            <div className="agenda-day">{day[0].start_day}</div>
            <div className="agenda-items">{getDaysAgenda(day)}</div>
        </div>
    ))
}

const getDaysAgenda = (dayAgenda: IMLViewData[]) => {
    return dayAgenda.map((agenda, i) => {
        const endTime = (agenda.end_time !== "") ? (<span className="end-time"> - {agenda.end_time}</span>) : ""
        
        return (
            <div className="agenda-item row" key={i}>
                <div className="medium-3 agenda-time columns medium-text-center">
                    <span className="start-time">{agenda.start_time}</span>
                    {endTime}
                </div>
                <div className="medium-9 agenda-detail columns">
                    <div className="agenda-title">{agenda.title}</div>
                    <div className="agenda-location"><i>{agenda.location}</i></div>
                    {getAgendaSpeaker(agenda.speaker)}
                    <div className="agenda-desc" dangerouslySetInnerHTML={{__html: agenda.desc}} />
                    {getAgendaLink(agenda.more_link)}
                </div>
            </div>
        )
    })
}

const getAgendaSpeaker = (speakers: string[]) => {
    if (speakers.length > 0) {
        return (
            <div className="agenda-speaker">Speaker/s: {speakers.join(", ")}</div>
        )
    }
    return
}

const getAgendaLink = (link: string[] | boolean = false) => {
    if(typeof link === "boolean") {
        return ""
    }
    return (link.length > 0) ? (<div className="agenda-link"><a href={link[0]}>More</a></div>) : ""
}

const sortByDay = (data: IMLViewData[] = []) => {
    if (data.length === 0) {
        return []
    }
    const agendas: IMLViewData[][] = []
    let dayAgenda: IMLViewData[] = []
    data.forEach((a) => {
        if (dayAgenda.length !== 0 && dayAgenda[0].start_day !== a.start_day) {
            agendas.push(dayAgenda)
            dayAgenda = []
            dayAgenda.push(a)
            return
        }

        return dayAgenda.push(a)
    })

    agendas.push(dayAgenda)
    return agendas
}

export default AgendaListView

export interface IProps {
    data?: IMLViewData[]
}

export interface IMLViewData {
    start_day: string,
    title: string,
    start_time: string,
    desc: string,
    end_time: string,
    more_link: string[] | boolean,
    location: string,
    speaker: string[],
}