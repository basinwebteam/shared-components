import axios from "axios"
import * as moxios from "moxios"
import * as React from "react"
import { shallow, mount } from 'enzyme'
import toJson from "enzyme-to-json"
import * as Page from "../../../../../DrupalContentTypes/PageInterface"

import DataView from "../../../../DataView"
import DataViewWrapper from "../../../Wrapper"
import AgendaListView from "../../../Types/AgendaListView"
import {pageData, viewData} from "./mocks"

describe("DataView - Agenda List", () => {
    const SITE_DOMAIN = "https://basinelectric.com"

    beforeEach(() => {
        moxios.install()
    })

    afterEach(() => {
        moxios.uninstall()
    })

    it("should render empty Agenda List View without data sent", () => {
        const element = mount(
            <DataView
                paragraphId="26d23f6d-757e-4b75-9136-418bb73a3a18"
                siteDomain={SITE_DOMAIN}
                includedRelationships={pageData.included}
            />
        )
        const agendaListViewComponent = element.childAt(0).childAt(0).childAt(0).childAt(0)

        expect(agendaListViewComponent.name()).toEqual("AgendaListView")
        expect(agendaListViewComponent.prop("data").length).toEqual(0)
        expect(agendaListViewComponent.text()).toEqual("")
    })

    it("should render Agenda List View with props sent", () => {
        const element = shallow(
            <AgendaListView data={viewData} />
        )
        expect(element.find(".agenda-title").length).toEqual(26)
        expect(toJson(element)).toMatchSnapshot()
    })

    // it("should render with data", async (done) => {
    //     const element = mount(
    //         <DataView
    //             paragraphId="26d23f6d-757e-4b75-9136-418bb73a3a18"
    //             siteDomain={SITE_DOMAIN}
    //             includedRelationships={pageData.included}
    //         />
    //     )
    //     const agendaListViewComponent = element.childAt(0).childAt(0).childAt(0).childAt(0)
    //     const dataViewWrapper = element.childAt(0).childAt(0).mount()
    //     moxios.wait(() => {
    //         let request = moxios.requests.mostRecent()
    //         request.respondWith({
    //             status: 200,
    //             response: viewData,
    //         }).then(() => {
    //             console.log(dataViewWrapper.state("data"))
    //             console.log(agendaListViewComponent.prop("data"))
    //             done()
    //         })
    //     })
        
    // })
})
