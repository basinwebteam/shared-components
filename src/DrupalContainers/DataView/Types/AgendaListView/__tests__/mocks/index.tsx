// tslint:disable:max-line-length

export const pageData = {
    "data": {
        "type": "node--basin_electric_basic_page",
        "id": "9850ae1c-f1fd-455a-9de5-a63f93db024d",
        "attributes": {
            "nid": 3006,
            "uuid": "9850ae1c-f1fd-455a-9de5-a63f93db024d",
            "vid": 12201,
            "langcode": "en",
            "status": true,
            "title": "Agenda",
            "created": 1501868968,
            "changed": 1504799003,
            "promote": false,
            "sticky": false,
            "revision_timestamp": 1504799003,
            "revision_log": null,
            "revision_translation_affected": true,
            "default_langcode": true,
            "path": {
                "alias": "/about-us/annual-meeting/agenda",
                "pid": 1691
            },
            "field_meta_tags": "a:0:{}",
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c1953c9b-859c-42d1-a2ae-a74572456c0c"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "1af84cb6-39cb-4bb8-8cf0-8701a789a0b5"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/revision_uid"
                }
            },
            "menu_link": {
                "data": null
            },
            "moderation_state": {
                "data": null
            },
            "field_aside_content": {
                "data": []
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "3f58d038-ba4c-4e77-941f-47c588edf342",
                        "meta": {
                            "target_revision_id": "34686"
                        }
                    },
                    {
                        "type": "paragraph--data_view",
                        "id": "26d23f6d-757e-4b75-9136-418bb73a3a18",
                        "meta": {
                            "target_revision_id": "34691"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/field_content"
                }
            },
            "field_related_videos": {
                "data": null
            },
            "field_web_layout": {
                "data": {
                    "type": "taxonomy_term--web_layouts",
                    "id": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/relationships/field_web_layout",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d/field_web_layout"
                }
            },
            "scheduled_update": {
                "data": []
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/9850ae1c-f1fd-455a-9de5-a63f93db024d?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_content.field_board%2Cfield_content.field_board.field_content%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--basic_paragraph",
            "id": "3f58d038-ba4c-4e77-941f-47c588edf342",
            "attributes": {
                "id": 10006,
                "uuid": "3f58d038-ba4c-4e77-941f-47c588edf342",
                "revision_id": 34686,
                "langcode": "en",
                "status": true,
                "created": 1504631633,
                "parent_id": "3006",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_body": {
                    "value": "<p>Tentative 2017 Annual Meeting Agenda</p>\r\n",
                    "format": "rich_text"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3f58d038-ba4c-4e77-941f-47c588edf342/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3f58d038-ba4c-4e77-941f-47c588edf342/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3f58d038-ba4c-4e77-941f-47c588edf342/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3f58d038-ba4c-4e77-941f-47c588edf342/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3f58d038-ba4c-4e77-941f-47c588edf342/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3f58d038-ba4c-4e77-941f-47c588edf342/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3f58d038-ba4c-4e77-941f-47c588edf342"
            }
        },
        {
            "type": "paragraph--data_view",
            "id": "26d23f6d-757e-4b75-9136-418bb73a3a18",
            "attributes": {
                "id": 9991,
                "uuid": "26d23f6d-757e-4b75-9136-418bb73a3a18",
                "revision_id": 34691,
                "langcode": "en",
                "status": true,
                "created": 1504206314,
                "parent_id": "3006",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "0b02c669-1e41-447a-bf90-b2fa878b58cf"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/26d23f6d-757e-4b75-9136-418bb73a3a18/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/26d23f6d-757e-4b75-9136-418bb73a3a18/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/26d23f6d-757e-4b75-9136-418bb73a3a18/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/26d23f6d-757e-4b75-9136-418bb73a3a18/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/26d23f6d-757e-4b75-9136-418bb73a3a18/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/26d23f6d-757e-4b75-9136-418bb73a3a18/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_view": {
                    "data": {
                        "type": "view--view",
                        "id": "9d995da7-041c-465c-b2f2-ed2f2ccc6f02"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/26d23f6d-757e-4b75-9136-418bb73a3a18/relationships/field_view",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/26d23f6d-757e-4b75-9136-418bb73a3a18/field_view"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/26d23f6d-757e-4b75-9136-418bb73a3a18"
            }
        },
        {
            "type": "taxonomy_term--web_layouts",
            "id": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb",
            "attributes": {
                "tid": 606,
                "uuid": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb",
                "langcode": "en",
                "name": "Full Page",
                "description": null,
                "weight": 0,
                "changed": 1502982012,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "9481b046-bd2c-4ef8-ba1d-acde28597ffc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb/vid"
                    }
                },
                "parent": {
                    "data": []
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb"
            }
        },
        {
            "type": "view--view",
            "id": "9d995da7-041c-465c-b2f2-ed2f2ccc6f02",
            "attributes": {
                "uuid": "9d995da7-041c-465c-b2f2-ed2f2ccc6f02",
                "langcode": "en",
                "status": true,
                "dependencies": {
                    "config": [
                        "field.storage.node.body",
                        "field.storage.node.field_end_date_time",
                        "field.storage.node.field_link_to_more_info",
                        "field.storage.node.field_location",
                        "field.storage.node.field_news_class",
                        "field.storage.node.field_news_thumb",
                        "field.storage.node.field_published_date",
                        "field.storage.node.field_speaker_presenter",
                        "field.storage.node.field_start_date_time",
                        "field.storage.node.field_summary",
                        "node.type.agenda_item",
                        "taxonomy.vocabulary.event_types"
                    ],
                    "content": [
                        "taxonomy_term:event_types:fbd7f576-1a86-46e4-9d1c-734e6d55c80d"
                    ],
                    "module": [
                        "datetime",
                        "image",
                        "link",
                        "node",
                        "rest",
                        "serialization",
                        "taxonomy",
                        "text",
                        "user"
                    ]
                },
                "id": "agenda_list",
                "label": "Agenda List",
                "module": "views",
                "description": "Lists all news types in order by published date",
                "tag": "",
                "base_table": "node_field_data",
                "base_field": "nid",
                "core": "8.x",
                "display": {
                    "default": {
                        "display_plugin": "default",
                        "id": "default",
                        "display_title": "Master",
                        "position": 0,
                        "display_options": {
                            "access": {
                                "type": "perm",
                                "options": {
                                    "perm": "access content"
                                }
                            },
                            "cache": {
                                "type": "tag",
                                "options": []
                            },
                            "query": {
                                "type": "views_query",
                                "options": {
                                    "disable_sql_rewrite": false,
                                    "distinct": false,
                                    "replica": false,
                                    "query_comment": "",
                                    "query_tags": []
                                }
                            },
                            "exposed_form": {
                                "type": "basic",
                                "options": {
                                    "submit_button": "Apply",
                                    "reset_button": false,
                                    "reset_button_label": "Reset",
                                    "exposed_sorts_label": "Sort by",
                                    "expose_sort_order": true,
                                    "sort_asc_label": "Asc",
                                    "sort_desc_label": "Desc"
                                }
                            },
                            "pager": {
                                "type": "mini",
                                "options": {
                                    "items_per_page": 15,
                                    "offset": 0,
                                    "id": 0,
                                    "total_pages": null,
                                    "expose": {
                                        "items_per_page": false,
                                        "items_per_page_label": "Items per page",
                                        "items_per_page_options": "5, 10, 25, 50",
                                        "items_per_page_options_all": false,
                                        "items_per_page_options_all_label": "- All -",
                                        "offset": false,
                                        "offset_label": "Offset"
                                    },
                                    "tags": {
                                        "previous": "‹‹",
                                        "next": "››"
                                    }
                                }
                            },
                            "style": {
                                "type": "grid",
                                "options": {
                                    "grouping": [],
                                    "columns": 3,
                                    "automatic_width": true,
                                    "alignment": "horizontal",
                                    "col_class_default": true,
                                    "col_class_custom": "",
                                    "row_class_default": true,
                                    "row_class_custom": ""
                                }
                            },
                            "row": {
                                "type": "fields"
                            },
                            "fields": {
                                "field_news_thumb": {
                                    "id": "field_news_thumb",
                                    "table": "node__field_news_thumb",
                                    "field": "field_news_thumb",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image",
                                    "settings": {
                                        "image_style": "",
                                        "image_link": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_summary": {
                                    "id": "field_summary",
                                    "table": "node__field_summary",
                                    "field": "field_summary",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 100,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": true,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "basic_string",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_news_class": {
                                    "id": "field_news_class",
                                    "table": "node__field_news_class",
                                    "field": "field_news_class",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "entity_reference_label",
                                    "settings": {
                                        "link": false
                                    },
                                    "group_column": "target_id",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "m/d/y"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "nothing": {
                                    "id": "nothing",
                                    "table": "views",
                                    "field": "nothing",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": true,
                                        "text": "{{ field_news_thumb }}<br />\n<h2>{{ title }}</h2>\n<p>{{ field_summary }}<br/>\n<span style=\"color:#ccc;\"> {{ field_news_class }} - {{ field_published_date }}</span></p>",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": true,
                                    "empty_zero": false,
                                    "hide_alter_empty": false,
                                    "plugin_id": "custom"
                                }
                            },
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "in",
                                    "value": {
                                        "agenda_item": "agenda_item"
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false,
                                        "argument": null
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                },
                                "field_event_type_target_id": {
                                    "id": "field_event_type_target_id",
                                    "table": "node__field_event_type",
                                    "field": "field_event_type_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "616": 616
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "event_types",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                }
                            },
                            "sorts": {
                                "field_start_date_time_value": {
                                    "id": "field_start_date_time_value",
                                    "table": "node__field_start_date_time",
                                    "field": "field_start_date_time_value",
                                    "relationship": "none",
                                    "group_type": "count_distinct",
                                    "admin_label": "",
                                    "order": "ASC",
                                    "exposed": false,
                                    "expose": {
                                        "label": ""
                                    },
                                    "granularity": "minute",
                                    "plugin_id": "datetime"
                                }
                            },
                            "title": "Agenda List",
                            "header": [],
                            "footer": [],
                            "empty": [],
                            "relationships": [],
                            "arguments": [],
                            "display_extenders": [],
                            "group_by": false
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url.query_args",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_class",
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    },
                    "rest_export_1": {
                        "display_plugin": "rest_export",
                        "id": "rest_export_1",
                        "display_title": "Rest-Agenda List",
                        "position": 2,
                        "display_options": {
                            "display_extenders": [],
                            "style": {
                                "type": "serializer",
                                "options": {
                                    "formats": {
                                        "json": "json"
                                    }
                                }
                            },
                            "defaults": {
                                "style": false,
                                "row": false,
                                "pager": false,
                                "fields": false
                            },
                            "row": {
                                "type": "data_field",
                                "options": {
                                    "field_options": {
                                        "field_start_date_time_1": {
                                            "alias": "start_day",
                                            "raw_output": false
                                        },
                                        "field_start_date_time": {
                                            "alias": "start_time",
                                            "raw_output": false
                                        },
                                        "field_end_date_time": {
                                            "alias": "end_time",
                                            "raw_output": false
                                        },
                                        "title": {
                                            "alias": "",
                                            "raw_output": true
                                        },
                                        "field_location": {
                                            "alias": "location",
                                            "raw_output": false
                                        },
                                        "body": {
                                            "alias": "desc",
                                            "raw_output": false
                                        },
                                        "field_link_to_more_info": {
                                            "alias": "more_link",
                                            "raw_output": true
                                        },
                                        "field_speaker_presenter": {
                                            "alias": "speaker",
                                            "raw_output": true
                                        }
                                    }
                                }
                            },
                            "pager": {
                                "type": "none",
                                "options": {
                                    "offset": 0
                                }
                            },
                            "fields": {
                                "field_start_date_time_1": {
                                    "id": "field_start_date_time_1",
                                    "table": "node__field_start_date_time",
                                    "field": "field_start_date_time",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "Day",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "l, F j"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "set_precision": false,
                                    "precision": 0,
                                    "decimal": ".",
                                    "format_plural": false,
                                    "format_plural_string": "1\u0003@count",
                                    "prefix": "",
                                    "suffix": "",
                                    "plugin_id": "field"
                                },
                                "field_start_date_time": {
                                    "id": "field_start_date_time",
                                    "table": "node__field_start_date_time",
                                    "field": "field_start_date_time",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "Start Time",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "g:i a"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_end_date_time": {
                                    "id": "field_end_date_time",
                                    "table": "node__field_end_date_time",
                                    "field": "field_end_date_time",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "<span>{{ field_end_date_time }}</span>",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "g:i a"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": false
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_location": {
                                    "id": "field_location",
                                    "table": "node__field_location",
                                    "field": "field_location",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": false
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "body": {
                                    "id": "body",
                                    "table": "node__body",
                                    "field": "body",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "text_default",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_link_to_more_info": {
                                    "id": "field_link_to_more_info",
                                    "table": "node__field_link_to_more_info",
                                    "field": "field_link_to_more_info",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "uri",
                                    "type": "link_separate",
                                    "settings": {
                                        "trim_length": 80,
                                        "url_only": false,
                                        "url_plain": false,
                                        "rel": "0",
                                        "target": "0"
                                    },
                                    "group_column": "options",
                                    "group_columns": {
                                        "options": "options"
                                    },
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_speaker_presenter": {
                                    "id": "field_speaker_presenter",
                                    "table": "node__field_speaker_presenter",
                                    "field": "field_speaker_presenter",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": false
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                }
                            },
                            "path": "rest-agenda-items",
                            "display_description": "Sends list of latest news articles"
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "request_format",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.body",
                                "config:field.storage.node.field_end_date_time",
                                "config:field.storage.node.field_link_to_more_info",
                                "config:field.storage.node.field_location",
                                "config:field.storage.node.field_speaker_presenter",
                                "config:field.storage.node.field_start_date_time"
                            ]
                        }
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/view/view/9d995da7-041c-465c-b2f2-ed2f2ccc6f02"
            }
        }
    ]
}

export const viewData = [
    {
        "start_day": "Tuesday, November 7",
        "start_time": "1:00 pm",
        "end_time": "",
        "title": "Resolutions Committee Meeting",
        "location": "Basin Electric Headquarters - 5th South",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Tuesday, November 7",
        "start_time": "4:00 pm",
        "end_time": "",
        "title": "District 9 Caucus",
        "location": "Ramkota Hotel - Governors Room",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Tuesday, November 7",
        "start_time": "6:00 pm",
        "end_time": "9:00 pm",
        "title": "Annual Meeting Reception",
        "location": "Basin Electric Headquarters Addition",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "6:30 am",
        "end_time": "",
        "title": "Registration Opens ",
        "location": "Exhibition Center foyer",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "7:00 am",
        "end_time": "",
        "title": "Members Breakfast",
        "location": "Exhibition Center Hall A",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "7:30 am",
        "end_time": "",
        "title": "Display booths open",
        "location": "Exhibition Center Hall A",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "8:00 am",
        "end_time": "",
        "title": "Welcome Message",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "8:00 am",
        "end_time": "",
        "title": "Annual Meeting Convenes",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "8:30 am",
        "end_time": "",
        "title": "President’s Message",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "8:45 am",
        "end_time": "",
        "title": "General Manager’s Report",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "9:15 am",
        "end_time": "",
        "title": "Keynote",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "10:25 am",
        "end_time": "",
        "title": "Break",
        "location": "",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "10:45 am",
        "end_time": "",
        "title": "Financial Report",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "11:30 am",
        "end_time": "",
        "title": "Members luncheon",
        "location": "Exhibition Center Hall A",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "1:00 pm",
        "end_time": "",
        "title": "Operational Excellence ",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "2:00 pm",
        "end_time": "",
        "title": "Supporting Membership Growth",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "3:00 pm",
        "end_time": "",
        "title": "Break",
        "location": "",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "3:20 pm",
        "end_time": "",
        "title": "Government Relations Report ",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "4:00 pm",
        "end_time": "",
        "title": "Resolutions Report",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "4:05 pm",
        "end_time": "",
        "title": "Bylaws Report ",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "4:15 pm",
        "end_time": "",
        "title": "New Business",
        "location": "Exhibition Center Hall B/C",
        "desc": "<ul>\n<li>Action on resolutions</li>\n<li>Action on bylaws</li>\n<li>Caucus report</li>\n<li>Old Business</li>\n</ul>\n",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "4:30 pm",
        "end_time": "",
        "title": "Adjournment",
        "location": "",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "6:00 pm",
        "end_time": "",
        "title": "Social",
        "location": "Exhibition Center Hall A",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Wednesday, November 8",
        "start_time": "7:00 pm",
        "end_time": "",
        "title": "Banquet",
        "location": "Exhibition Center Hall B/C",
        "desc": "",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Thursday, November 9",
        "start_time": "6:45 am",
        "end_time": "2:30 pm",
        "title": "Power Plant Tour",
        "location": "",
        "desc": "<p>Bus departs from exhibit hall entrance at 6:45 a.m.</p>\n",
        "more_link": false,
        "speaker": []
    },
    {
        "start_day": "Thursday, November 9",
        "start_time": "7:00 am",
        "end_time": "",
        "title": "Security and Response Services Breakfast",
        "location": "Prairie Rose Rooms 102/103",
        "desc": "",
        "more_link": false,
        "speaker": []
    }
]