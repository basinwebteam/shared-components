// tslint:disable:max-line-length

export const pageData = {
    "data": {
    "type": "node--basin_electric_basic_page",
    "id": "f80d692d-e719-499b-9246-c27c0012786e",
    "attributes": {
    "nid": 46,
    "uuid": "f80d692d-e719-499b-9246-c27c0012786e",
    "vid": 19936,
    "langcode": "en",
    "status": true,
    "title": "Members",
    "created": 1498848803,
    "changed": 1515452758,
    "promote": false,
    "sticky": false,
    "revision_timestamp": 1515452758,
    "revision_log": null,
    "revision_translation_affected": true,
    "default_langcode": true,
    "path": {
    "alias": "/about-us/members",
    "pid": 11
    },
    "field_meta_tags": "a:0:{}",
    "field_summary": null
    },
    "relationships": {
    "type": {
    "data": {
    "type": "node_type--node_type",
    "id": "c1953c9b-859c-42d1-a2ae-a74572456c0c"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/relationships/type",
    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/type"
    }
    },
    "uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/relationships/uid",
    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/uid"
    }
    },
    "revision_uid": {
    "data": {
    "type": "user--user",
    "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/relationships/revision_uid",
    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/revision_uid"
    }
    },
    "menu_link": {
    "data": null
    },
    "moderation_state": {
    "data": null
    },
    "field_aside_content": {
    "data": [
    {
    "type": "paragraph--unique_basic_block",
    "id": "e41a80a6-62d8-436c-8d9a-323be297d091",
    "meta": {
    "target_revision_id": "63156"
    }
    },
    {
    "type": "paragraph--unique_basic_block",
    "id": "4f730b88-a33d-4e49-9c8b-6c8ea82f4916",
    "meta": {
    "target_revision_id": "63166"
    }
    },
    {
    "type": "paragraph--unique_basic_block",
    "id": "07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94",
    "meta": {
    "target_revision_id": "63176"
    }
    }
    ],
    "links": {
    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/relationships/field_aside_content",
    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/field_aside_content"
    }
    },
    "field_content": {
    "data": [
    {
    "type": "paragraph--basic_paragraph",
    "id": "e30ac83b-d611-47d2-bfff-8a71cb721aec",
    "meta": {
    "target_revision_id": "63181"
    }
    },
    {
    "type": "paragraph--image",
    "id": "e34aed88-da38-4c56-b263-94a62790611d",
    "meta": {
    "target_revision_id": "63186"
    }
    },
    {
    "type": "paragraph--data_view",
    "id": "887a78f3-cd5c-4240-9246-bd9974637c5c",
    "meta": {
    "target_revision_id": "63191"
    }
    }
    ],
    "links": {
    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/relationships/field_content",
    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e/field_content"
    }
    },
    "field_related_videos": {
    "data": null
    },
    "field_web_layout": {
    "data": null
    },
    "scheduled_update": {
    "data": []
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e"
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f80d692d-e719-499b-9246-c27c0012786e?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_content.field_board%2Cfield_content.field_board.field_content%2Cfield_web_layout"
    },
    "included": [
    {
    "type": "paragraph--unique_basic_block",
    "id": "e41a80a6-62d8-436c-8d9a-323be297d091",
    "attributes": {
    "id": 96,
    "uuid": "e41a80a6-62d8-436c-8d9a-323be297d091",
    "revision_id": 63156,
    "langcode": "en",
    "status": true,
    "created": 1498849065,
    "parent_id": "46",
    "parent_type": "node",
    "parent_field_name": "field_aside_content",
    "behavior_settings": "a:0:{}",
    "default_langcode": true,
    "field_unique_block_title": "Our service area"
    },
    "relationships": {
    "type": {
    "data": {
    "type": "paragraphs_type--paragraphs_type",
    "id": "05040298-e668-41b5-a2ee-68c3a544c9be"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/e41a80a6-62d8-436c-8d9a-323be297d091/relationships/type",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/e41a80a6-62d8-436c-8d9a-323be297d091/type"
    }
    },
    "uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/e41a80a6-62d8-436c-8d9a-323be297d091/relationships/uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/e41a80a6-62d8-436c-8d9a-323be297d091/uid"
    }
    },
    "revision_uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/e41a80a6-62d8-436c-8d9a-323be297d091/relationships/revision_uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/e41a80a6-62d8-436c-8d9a-323be297d091/revision_uid"
    }
    },
    "moderation_state": {
    "data": null
    },
    "field_content": {
    "data": [
    {
    "type": "paragraph--basic_paragraph",
    "id": "8a11318f-44c1-4069-8fe8-04e7be12d79b",
    "meta": {
    "target_revision_id": "63151"
    }
    }
    ],
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/e41a80a6-62d8-436c-8d9a-323be297d091/relationships/field_content",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/e41a80a6-62d8-436c-8d9a-323be297d091/field_content"
    }
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/e41a80a6-62d8-436c-8d9a-323be297d091"
    }
    },
    {
    "type": "paragraph--unique_basic_block",
    "id": "4f730b88-a33d-4e49-9c8b-6c8ea82f4916",
    "attributes": {
    "id": 101,
    "uuid": "4f730b88-a33d-4e49-9c8b-6c8ea82f4916",
    "revision_id": 63166,
    "langcode": "en",
    "status": true,
    "created": 1498849077,
    "parent_id": "46",
    "parent_type": "node",
    "parent_field_name": "field_aside_content",
    "behavior_settings": "a:0:{}",
    "default_langcode": true,
    "field_unique_block_title": "The co-op difference"
    },
    "relationships": {
    "type": {
    "data": {
    "type": "paragraphs_type--paragraphs_type",
    "id": "05040298-e668-41b5-a2ee-68c3a544c9be"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/4f730b88-a33d-4e49-9c8b-6c8ea82f4916/relationships/type",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/4f730b88-a33d-4e49-9c8b-6c8ea82f4916/type"
    }
    },
    "uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/4f730b88-a33d-4e49-9c8b-6c8ea82f4916/relationships/uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/4f730b88-a33d-4e49-9c8b-6c8ea82f4916/uid"
    }
    },
    "revision_uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/4f730b88-a33d-4e49-9c8b-6c8ea82f4916/relationships/revision_uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/4f730b88-a33d-4e49-9c8b-6c8ea82f4916/revision_uid"
    }
    },
    "moderation_state": {
    "data": null
    },
    "field_content": {
    "data": [
    {
    "type": "paragraph--basic_paragraph",
    "id": "2dad6911-7a25-4b4c-a7a3-f8e9d41a5669",
    "meta": {
    "target_revision_id": "63161"
    }
    }
    ],
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/4f730b88-a33d-4e49-9c8b-6c8ea82f4916/relationships/field_content",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/4f730b88-a33d-4e49-9c8b-6c8ea82f4916/field_content"
    }
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/4f730b88-a33d-4e49-9c8b-6c8ea82f4916"
    }
    },
    {
    "type": "paragraph--unique_basic_block",
    "id": "07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94",
    "attributes": {
    "id": 106,
    "uuid": "07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94",
    "revision_id": 63176,
    "langcode": "en",
    "status": true,
    "created": 1498849103,
    "parent_id": "46",
    "parent_type": "node",
    "parent_field_name": "field_aside_content",
    "behavior_settings": "a:0:{}",
    "default_langcode": true,
    "field_unique_block_title": "Original incorporators"
    },
    "relationships": {
    "type": {
    "data": {
    "type": "paragraphs_type--paragraphs_type",
    "id": "05040298-e668-41b5-a2ee-68c3a544c9be"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94/relationships/type",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94/type"
    }
    },
    "uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94/relationships/uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94/uid"
    }
    },
    "revision_uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94/relationships/revision_uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94/revision_uid"
    }
    },
    "moderation_state": {
    "data": null
    },
    "field_content": {
    "data": [
    {
    "type": "paragraph--basic_paragraph",
    "id": "5169a03d-4028-4b9a-89be-9730d96252d1",
    "meta": {
    "target_revision_id": "63171"
    }
    }
    ],
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94/relationships/field_content",
    "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94/field_content"
    }
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/07b4e1b5-98ca-4db7-8aa0-87b1f1d5af94"
    }
    },
    {
    "type": "paragraph--basic_paragraph",
    "id": "e30ac83b-d611-47d2-bfff-8a71cb721aec",
    "attributes": {
    "id": 111,
    "uuid": "e30ac83b-d611-47d2-bfff-8a71cb721aec",
    "revision_id": 63181,
    "langcode": "en",
    "status": true,
    "created": 1498848823,
    "parent_id": "46",
    "parent_type": "node",
    "parent_field_name": "field_content",
    "behavior_settings": "a:0:{}",
    "default_langcode": true,
    "field_body": {
    "value": "<p>At Basin Electric, our focus is the person at the end of the line. Our member-owners are the reason we exist. That's the co-op difference.</p>\r\n\r\n<p>Rural electric cooperative pioneers in the Missouri River basin created Basin Electric in 1961 to provide supplemental wholesale power to their distribution cooperatives. Basin Electric's growth and success during the past 50-plus years is due to the commitment, support, and resolve of these visionary co-op leaders and their consumers.</p>\r\n\r\n<p>Basin Electric's 11-member&nbsp;<a href=\"https://www.basinelectric.com/About-Us/Organization/Board/index.html\">board of directors</a>&nbsp;are elected by our members. These directors have been elected to the boards of their local distribution systems and then, with the exception of District 9, to their respective intermediate generation and transmission systems. Many of our Class A members are generation and transmission (G&amp;T) electric cooperatives.</p>\r\n",
    "format": "rich_text"
    }
    },
    "relationships": {
    "type": {
    "data": {
    "type": "paragraphs_type--paragraphs_type",
    "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/e30ac83b-d611-47d2-bfff-8a71cb721aec/relationships/type",
    "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/e30ac83b-d611-47d2-bfff-8a71cb721aec/type"
    }
    },
    "uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/e30ac83b-d611-47d2-bfff-8a71cb721aec/relationships/uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/e30ac83b-d611-47d2-bfff-8a71cb721aec/uid"
    }
    },
    "revision_uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/e30ac83b-d611-47d2-bfff-8a71cb721aec/relationships/revision_uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/e30ac83b-d611-47d2-bfff-8a71cb721aec/revision_uid"
    }
    },
    "moderation_state": {
    "data": null
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/e30ac83b-d611-47d2-bfff-8a71cb721aec"
    }
    },
    {
    "type": "paragraph--image",
    "id": "e34aed88-da38-4c56-b263-94a62790611d",
    "attributes": {
    "id": 116,
    "uuid": "e34aed88-da38-4c56-b263-94a62790611d",
    "revision_id": 63186,
    "langcode": "en",
    "status": true,
    "created": 1498848873,
    "parent_id": "46",
    "parent_type": "node",
    "parent_field_name": "field_content",
    "behavior_settings": "a:0:{}",
    "default_langcode": true,
    "field_caption": null,
    "field_website_link": null
    },
    "relationships": {
    "type": {
    "data": {
    "type": "paragraphs_type--paragraphs_type",
    "id": "87ac238d-a191-4617-bb34-6a19f8436979"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/image/e34aed88-da38-4c56-b263-94a62790611d/relationships/type",
    "related": "http://cms.bepc.com/jsonapi/paragraph/image/e34aed88-da38-4c56-b263-94a62790611d/type"
    }
    },
    "uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/image/e34aed88-da38-4c56-b263-94a62790611d/relationships/uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/image/e34aed88-da38-4c56-b263-94a62790611d/uid"
    }
    },
    "revision_uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/image/e34aed88-da38-4c56-b263-94a62790611d/relationships/revision_uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/image/e34aed88-da38-4c56-b263-94a62790611d/revision_uid"
    }
    },
    "moderation_state": {
    "data": null
    },
    "field_image": {
    "data": {
    "type": "file--file",
    "id": "d3d46ce4-c0f3-454b-a98f-51d3f6d7624e",
    "meta": {
    "alt": "A map of the United States with our co-op locations highlighted. The table below contains their locations.",
    "title": "",
    "width": "710",
    "height": "731"
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/image/e34aed88-da38-4c56-b263-94a62790611d/relationships/field_image",
    "related": "http://cms.bepc.com/jsonapi/paragraph/image/e34aed88-da38-4c56-b263-94a62790611d/field_image"
    }
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/image/e34aed88-da38-4c56-b263-94a62790611d"
    }
    },
    {
    "type": "paragraph--data_view",
    "id": "887a78f3-cd5c-4240-9246-bd9974637c5c",
    "attributes": {
    "id": 5556,
    "uuid": "887a78f3-cd5c-4240-9246-bd9974637c5c",
    "revision_id": 63191,
    "langcode": "en",
    "status": true,
    "created": 1501790478,
    "parent_id": "46",
    "parent_type": "node",
    "parent_field_name": "field_content",
    "behavior_settings": "a:0:{}",
    "default_langcode": true,
    "field_title": null
    },
    "relationships": {
    "type": {
    "data": {
    "type": "paragraphs_type--paragraphs_type",
    "id": "0b02c669-1e41-447a-bf90-b2fa878b58cf"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/887a78f3-cd5c-4240-9246-bd9974637c5c/relationships/type",
    "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/887a78f3-cd5c-4240-9246-bd9974637c5c/type"
    }
    },
    "uid": {
    "data": {
    "type": "user--user",
    "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/887a78f3-cd5c-4240-9246-bd9974637c5c/relationships/uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/887a78f3-cd5c-4240-9246-bd9974637c5c/uid"
    }
    },
    "revision_uid": {
    "data": {
    "type": "user--user",
    "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/887a78f3-cd5c-4240-9246-bd9974637c5c/relationships/revision_uid",
    "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/887a78f3-cd5c-4240-9246-bd9974637c5c/revision_uid"
    }
    },
    "moderation_state": {
    "data": null
    },
    "field_view": {
    "data": {
    "type": "view--view",
    "id": "75f02a80-3b84-4293-85ad-9911df1b51cb"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/887a78f3-cd5c-4240-9246-bd9974637c5c/relationships/field_view",
    "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/887a78f3-cd5c-4240-9246-bd9974637c5c/field_view"
    }
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/887a78f3-cd5c-4240-9246-bd9974637c5c"
    }
    },
    {
    "type": "file--file",
    "id": "d3d46ce4-c0f3-454b-a98f-51d3f6d7624e",
    "attributes": {
    "fid": 5256,
    "uuid": "d3d46ce4-c0f3-454b-a98f-51d3f6d7624e",
    "langcode": "en",
    "filename": "BEPC-member-area-map-web.png",
    "uri": "public://images/BEPC-member-area-map-web.png",
    "filemime": "image/png",
    "filesize": 109244,
    "status": true,
    "created": 1503090503,
    "changed": 1503090503,
    "url": "/sites/CMS/files/images/BEPC-member-area-map-web.png"
    },
    "relationships": {
    "uid": {
    "data": {
    "type": "user--user",
    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/file/file/d3d46ce4-c0f3-454b-a98f-51d3f6d7624e/relationships/uid",
    "related": "http://cms.bepc.com/jsonapi/file/file/d3d46ce4-c0f3-454b-a98f-51d3f6d7624e/uid"
    }
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/file/file/d3d46ce4-c0f3-454b-a98f-51d3f6d7624e"
    }
    },
    {
    "type": "view--view",
    "id": "75f02a80-3b84-4293-85ad-9911df1b51cb",
    "attributes": {
    "uuid": "75f02a80-3b84-4293-85ad-9911df1b51cb",
    "langcode": "en",
    "status": true,
    "dependencies": {
    "config": [
    "field.storage.node.field_co_op_city",
    "field.storage.node.field_co_op_class",
    "field.storage.node.field_co_op_district",
    "field.storage.node.field_co_op_state",
    "field.storage.node.field_co_op_website_url",
    "node.type.member_co_op_details"
    ],
    "module": [
    "node",
    "options",
    "rest",
    "serialization",
    "user"
    ]
    },
    "id": "member_list",
    "label": "Member List",
    "module": "views",
    "description": "List of all of Basin's members",
    "tag": "",
    "base_table": "node_field_data",
    "base_field": "nid",
    "core": "8.x",
    "display": {
    "default": {
    "display_plugin": "default",
    "id": "default",
    "display_title": "Master",
    "position": 0,
    "display_options": {
    "access": {
    "type": "perm",
    "options": {
    "perm": "access content"
    }
    },
    "cache": {
    "type": "tag",
    "options": []
    },
    "query": {
    "type": "views_query",
    "options": {
    "disable_sql_rewrite": false,
    "distinct": false,
    "replica": false,
    "query_comment": "",
    "query_tags": []
    }
    },
    "exposed_form": {
    "type": "basic",
    "options": {
    "submit_button": "Apply",
    "reset_button": false,
    "reset_button_label": "Reset",
    "exposed_sorts_label": "Sort by",
    "expose_sort_order": true,
    "sort_asc_label": "Asc",
    "sort_desc_label": "Desc"
    }
    },
    "pager": {
    "type": "full",
    "options": {
    "items_per_page": 25,
    "offset": 0,
    "id": 0,
    "total_pages": null,
    "expose": {
    "items_per_page": false,
    "items_per_page_label": "Items per page",
    "items_per_page_options": "5, 10, 25, 50",
    "items_per_page_options_all": false,
    "items_per_page_options_all_label": "- All -",
    "offset": false,
    "offset_label": "Offset"
    },
    "tags": {
    "previous": "‹ Previous",
    "next": "Next ›",
    "first": "« First",
    "last": "Last »"
    },
    "quantity": 9
    }
    },
    "style": {
    "type": "table",
    "options": {
    "grouping": [],
    "row_class": "",
    "default_row_class": true,
    "override": true,
    "sticky": false,
    "caption": "List of Basin's members",
    "summary": "",
    "description": "",
    "columns": {
    "title": "title",
    "field_co_op_website_url": "field_co_op_website_url",
    "field_co_op_district": "field_co_op_district",
    "field_co_op_class": "field_co_op_class",
    "field_co_op_city": "field_co_op_city",
    "field_co_op_state": "field_co_op_state"
    },
    "info": {
    "title": {
    "sortable": true,
    "default_sort_order": "asc",
    "align": "",
    "separator": "",
    "empty_column": false,
    "responsive": ""
    },
    "field_co_op_website_url": {
    "sortable": false,
    "default_sort_order": "asc",
    "align": "",
    "separator": "",
    "empty_column": false,
    "responsive": ""
    },
    "field_co_op_district": {
    "sortable": true,
    "default_sort_order": "asc",
    "align": "",
    "separator": "",
    "empty_column": false,
    "responsive": ""
    },
    "field_co_op_class": {
    "sortable": true,
    "default_sort_order": "asc",
    "align": "",
    "separator": "",
    "empty_column": false,
    "responsive": ""
    },
    "field_co_op_city": {
    "sortable": false,
    "default_sort_order": "asc",
    "align": "",
    "separator": "",
    "empty_column": false,
    "responsive": ""
    },
    "field_co_op_state": {
    "sortable": true,
    "default_sort_order": "asc",
    "align": "",
    "separator": "",
    "empty_column": false,
    "responsive": ""
    }
    },
    "default": "title",
    "empty_table": false
    }
    },
    "row": {
    "type": "fields"
    },
    "fields": {
    "title": {
    "id": "title",
    "table": "node_field_data",
    "field": "title",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "Name",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": true,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "string",
    "settings": {
    "link_to_entity": true
    },
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_website_url": {
    "id": "field_co_op_website_url",
    "table": "node__field_co_op_website_url",
    "field": "field_co_op_website_url",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "",
    "exclude": true,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "string",
    "settings": {
    "link_to_entity": false
    },
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_district": {
    "id": "field_co_op_district",
    "table": "node__field_co_op_district",
    "field": "field_co_op_district",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "District",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "list_default",
    "settings": [],
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_class": {
    "id": "field_co_op_class",
    "table": "node__field_co_op_class",
    "field": "field_co_op_class",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "Class",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "list_default",
    "settings": [],
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_city": {
    "id": "field_co_op_city",
    "table": "node__field_co_op_city",
    "field": "field_co_op_city",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "City",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "string",
    "settings": {
    "link_to_entity": false
    },
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_state": {
    "id": "field_co_op_state",
    "table": "node__field_co_op_state",
    "field": "field_co_op_state",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "State",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "string",
    "settings": {
    "link_to_entity": false
    },
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    }
    },
    "filters": {
    "status": {
    "value": "1",
    "table": "node_field_data",
    "field": "status",
    "plugin_id": "boolean",
    "entity_type": "node",
    "entity_field": "status",
    "id": "status",
    "expose": {
    "operator": ""
    },
    "group": 1
    },
    "type": {
    "id": "type",
    "table": "node_field_data",
    "field": "type",
    "value": {
    "member_co_op_details": "member_co_op_details"
    },
    "entity_type": "node",
    "entity_field": "type",
    "plugin_id": "bundle"
    }
    },
    "sorts": {
    "title": {
    "id": "title",
    "table": "node_field_data",
    "field": "title",
    "order": "ASC",
    "entity_type": "node",
    "entity_field": "title",
    "plugin_id": "standard",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "exposed": false,
    "expose": {
    "label": ""
    }
    }
    },
    "title": "block-member-list",
    "header": [],
    "footer": [],
    "empty": [],
    "relationships": [],
    "arguments": [],
    "display_extenders": []
    },
    "cache_metadata": {
    "max-age": 0,
    "contexts": [
    "languages:language_content",
    "languages:language_interface",
    "url.query_args",
    "user.node_grants:view",
    "user.permissions"
    ],
    "tags": [
    "config:field.storage.node.field_co_op_city",
    "config:field.storage.node.field_co_op_class",
    "config:field.storage.node.field_co_op_district",
    "config:field.storage.node.field_co_op_state",
    "config:field.storage.node.field_co_op_website_url"
    ]
    }
    },
    "block_1": {
    "display_plugin": "block",
    "id": "block_1",
    "display_title": "Block-List of Members",
    "position": 1,
    "display_options": {
    "display_extenders": [],
    "use_more": true,
    "defaults": {
    "use_more": false,
    "use_more_always": false,
    "use_more_text": false
    },
    "use_more_always": false,
    "use_more_text": "more",
    "block_description": "List of Members",
    "display_description": ""
    },
    "cache_metadata": {
    "max-age": 0,
    "contexts": [
    "languages:language_content",
    "languages:language_interface",
    "url.query_args",
    "user.node_grants:view",
    "user.permissions"
    ],
    "tags": [
    "config:field.storage.node.field_co_op_city",
    "config:field.storage.node.field_co_op_class",
    "config:field.storage.node.field_co_op_district",
    "config:field.storage.node.field_co_op_state",
    "config:field.storage.node.field_co_op_website_url"
    ]
    }
    },
    "rest_export_1": {
    "display_plugin": "rest_export",
    "id": "rest_export_1",
    "display_title": "REST-List of Members",
    "position": 2,
    "display_options": {
    "display_extenders": [],
    "path": "rest-member-list",
    "style": {
    "type": "serializer",
    "options": {
    "formats": {
    "json": "json"
    }
    }
    },
    "defaults": {
    "style": false,
    "row": false,
    "filters": false,
    "filter_groups": false,
    "fields": false
    },
    "row": {
    "type": "data_field",
    "options": {
    "field_options": {
    "title": {
    "alias": "name",
    "raw_output": true
    },
    "field_co_op_website_url": {
    "alias": "url",
    "raw_output": true
    },
    "field_co_op_district": {
    "alias": "dist",
    "raw_output": true
    },
    "field_co_op_class": {
    "alias": "coopclass",
    "raw_output": true
    },
    "field_co_op_city": {
    "alias": "city",
    "raw_output": true
    },
    "field_co_op_state": {
    "alias": "state",
    "raw_output": true
    }
    }
    }
    },
    "pager": {
    "type": "some",
    "options": {
    "items_per_page": 0,
    "offset": 0
    }
    },
    "filters": {
    "status": {
    "value": "1",
    "table": "node_field_data",
    "field": "status",
    "plugin_id": "boolean",
    "entity_type": "node",
    "entity_field": "status",
    "id": "status",
    "expose": {
    "operator": ""
    },
    "group": 1
    },
    "type": {
    "id": "type",
    "table": "node_field_data",
    "field": "type",
    "value": {
    "member_co_op_details": "member_co_op_details"
    },
    "entity_type": "node",
    "entity_field": "type",
    "plugin_id": "bundle"
    }
    },
    "filter_groups": {
    "operator": "AND",
    "groups": {
    "1": "AND"
    }
    },
    "fields": {
    "title": {
    "id": "title",
    "table": "node_field_data",
    "field": "title",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "string",
    "settings": {
    "link_to_entity": true
    },
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_website_url": {
    "id": "field_co_op_website_url",
    "table": "node__field_co_op_website_url",
    "field": "field_co_op_website_url",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "string",
    "settings": {
    "link_to_entity": false
    },
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_district": {
    "id": "field_co_op_district",
    "table": "node__field_co_op_district",
    "field": "field_co_op_district",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "list_default",
    "settings": [],
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_class": {
    "id": "field_co_op_class",
    "table": "node__field_co_op_class",
    "field": "field_co_op_class",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "list_default",
    "settings": [],
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_city": {
    "id": "field_co_op_city",
    "table": "node__field_co_op_city",
    "field": "field_co_op_city",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "string",
    "settings": {
    "link_to_entity": false
    },
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    },
    "field_co_op_state": {
    "id": "field_co_op_state",
    "table": "node__field_co_op_state",
    "field": "field_co_op_state",
    "relationship": "none",
    "group_type": "group",
    "admin_label": "",
    "label": "",
    "exclude": false,
    "alter": {
    "alter_text": false,
    "text": "",
    "make_link": false,
    "path": "",
    "absolute": false,
    "external": false,
    "replace_spaces": false,
    "path_case": "none",
    "trim_whitespace": false,
    "alt": "",
    "rel": "",
    "link_class": "",
    "prefix": "",
    "suffix": "",
    "target": "",
    "nl2br": false,
    "max_length": 0,
    "word_boundary": true,
    "ellipsis": true,
    "more_link": false,
    "more_link_text": "",
    "more_link_path": "",
    "strip_tags": false,
    "trim": false,
    "preserve_tags": "",
    "html": false
    },
    "element_type": "",
    "element_class": "",
    "element_label_type": "",
    "element_label_class": "",
    "element_label_colon": false,
    "element_wrapper_type": "",
    "element_wrapper_class": "",
    "element_default_classes": true,
    "empty": "",
    "hide_empty": false,
    "empty_zero": false,
    "hide_alter_empty": true,
    "click_sort_column": "value",
    "type": "string",
    "settings": {
    "link_to_entity": false
    },
    "group_column": "value",
    "group_columns": [],
    "group_rows": true,
    "delta_limit": 0,
    "delta_offset": 0,
    "delta_reversed": false,
    "delta_first_last": false,
    "multi_type": "separator",
    "separator": ", ",
    "field_api_classes": false,
    "plugin_id": "field"
    }
    },
    "display_description": ""
    },
    "cache_metadata": {
    "max-age": -1,
    "contexts": [
    "languages:language_content",
    "languages:language_interface",
    "request_format",
    "user.node_grants:view",
    "user.permissions"
    ],
    "tags": [
    "config:field.storage.node.field_co_op_city",
    "config:field.storage.node.field_co_op_class",
    "config:field.storage.node.field_co_op_district",
    "config:field.storage.node.field_co_op_state",
    "config:field.storage.node.field_co_op_website_url"
    ]
    }
    }
    }
    },
    "links": {
    "self": "http://cms.bepc.com/jsonapi/view/view/75f02a80-3b84-4293-85ad-9911df1b51cb"
    }
    }
    ]
    }

export const viewData = [
    {
    "name": "Agralite Electric Cooperative",
    "url": "http://www.agralite.coop/",
    "dist": "1",
    "coopclass": "A",
    "city": "Benson",
    "state": "MN"
    },
    {
    "name": "Big Flat Electric Cooperative ",
    "url": "http://www.bigflatelectric.com/ ",
    "dist": "6",
    "coopclass": "C",
    "city": "Malta",
    "state": "MT"
    },
    {
    "name": "Big Horn Rural Electric Company ",
    "url": "http://www.bighornrea.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Basin",
    "state": "WY"
    },
    {
    "name": "Black Hills Electric Cooperative",
    "url": "http://www.bhec.com/ ",
    "dist": "7",
    "coopclass": "C",
    "city": "Custer",
    "state": "SD"
    },
    {
    "name": "Bon Homme Yankton Electric Association",
    "url": "http://www.byelectric.com/",
    "dist": "1",
    "coopclass": "C",
    "city": "Tabor",
    "state": "SD"
    },
    {
    "name": "Boone Valley Electric Cooperative",
    "url": "http://www.boonevalleyrec.com/",
    "dist": "11",
    "coopclass": "C",
    "city": "Renwick",
    "state": "IA"
    },
    {
    "name": "Burke-Divide Electric Cooperative ",
    "url": "http://www.bdec.coop/ ",
    "dist": "8",
    "coopclass": "C",
    "city": "Columbus",
    "state": "ND"
    },
    {
    "name": "Butler County REC",
    "url": "http://www.butlerrec.coop/ ",
    "dist": "11",
    "coopclass": "C",
    "city": "Allison",
    "state": "IA"
    },
    {
    "name": "Butte Electric Cooperative ",
    "url": "http://www.butteelectric.com/ ",
    "dist": "7",
    "coopclass": "C",
    "city": "Newell",
    "state": "SD"
    },
    {
    "name": "Calhoun County REC",
    "url": "http://www.calhounrec.coop/ ",
    "dist": "11",
    "coopclass": "C",
    "city": "Rockwell City",
    "state": "IA"
    },
    {
    "name": "Cam Wal Electric Cooperative ",
    "url": "http://www.cam-walnet.com/ ",
    "dist": "7",
    "coopclass": "C",
    "city": "Selby",
    "state": "SD"
    },
    {
    "name": "Capital Electric Cooperative",
    "url": "http://www.capitalelec.com/ ",
    "dist": "3",
    "coopclass": "C",
    "city": "Bismarck",
    "state": "ND"
    },
    {
    "name": "Carbon Power & Light ",
    "url": "http://www.carbonpower.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Saratoga",
    "state": "WY"
    },
    {
    "name": "Central Electric Cooperative ",
    "url": "http://www.centralec.coop/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Mitchell",
    "state": "SD"
    },
    {
    "name": "Central Montana Electric Power Cooperative ",
    "url": "http://www.cmepc.org/ ",
    "dist": "6",
    "coopclass": "A",
    "city": "Great Falls",
    "state": "MT"
    },
    {
    "name": "Central New Mexico Electric Cooperative ",
    "url": "http://www.cnmec.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Mountainair",
    "state": "NM"
    },
    {
    "name": "Central Power Electric Cooperative",
    "url": "http://www.centralpwr.com/ ",
    "dist": "3",
    "coopclass": "A",
    "city": "Minot",
    "state": "ND"
    },
    {
    "name": "Charles Mix Electric Association",
    "url": "http://cme.coop/",
    "dist": "1",
    "coopclass": "C",
    "city": "Lake Andes",
    "state": "SD"
    },
    {
    "name": "Cherry-Todd Electric Cooperative ",
    "url": "http://www.cherry-todd.com/ ",
    "dist": "7",
    "coopclass": "C",
    "city": "Mission",
    "state": "SD"
    },
    {
    "name": "Chimney Rock Public Power District",
    "url": "http://www.crppd.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Bayard",
    "state": "NE"
    },
    {
    "name": "City of Elk Point",
    "url": "http://www.elkpoint.org/ ",
    "dist": "1",
    "coopclass": "B",
    "city": "Elk Point",
    "state": "SD"
    },
    {
    "name": "Clay-Union Electric Corporation",
    "url": "http://www.clayunionelectric.coop/",
    "dist": "1",
    "coopclass": "C",
    "city": "Vermillion",
    "state": "SD"
    },
    {
    "name": "Codington-Clark Electric Cooperative",
    "url": "http://www.codingtonclarkelectric.coop/",
    "dist": "1",
    "coopclass": "C",
    "city": "Watertown",
    "state": "SD"
    },
    {
    "name": "Columbus Electric Cooperative ",
    "url": "http://www.columbusco-op.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Deming",
    "state": "NM"
    },
    {
    "name": "Continental Divide Electric Cooperative, Inc. ",
    "url": "http://www.cdec.coop/",
    "dist": "5",
    "coopclass": "C",
    "city": "Grants & Gallup",
    "state": "NM"
    },
    {
    "name": "Corn Belt Power Cooperative ",
    "url": "http://www.cbpower.coop/ ",
    "dist": "11",
    "coopclass": "A",
    "city": "Humboldt",
    "state": "IA"
    },
    {
    "name": "Crow Wing Power ",
    "url": "http://www.cwpower.com/ ",
    "dist": "9",
    "coopclass": "A",
    "city": "Brainerd",
    "state": "MN"
    },
    {
    "name": "Dakota Energy Cooperative ",
    "url": "http://dakotaenergy.coop/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Huron",
    "state": "SD"
    },
    {
    "name": "Dakota Valley Electric Cooperative ",
    "url": "http://www.dakotavalley.com/ ",
    "dist": "3",
    "coopclass": "C",
    "city": "Milnor",
    "state": "ND"
    },
    {
    "name": "Delta-Montrose Electric Association ",
    "url": "http://www.dmea.com/",
    "dist": "5",
    "coopclass": "C",
    "city": "Delta",
    "state": "CO"
    },
    {
    "name": "Douglas Electric Cooperative ",
    "url": "http://www.douglaselec.coop/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Armour",
    "state": "SD"
    },
    {
    "name": "East River Electric Power Cooperative ",
    "url": "http://www.eastriver.coop/ ",
    "dist": "1",
    "coopclass": "A",
    "city": "Madison",
    "state": "SD"
    },
    {
    "name": "Empire Electric Association",
    "url": "http://www.eea.coop/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Cortez",
    "state": "CO"
    },
    {
    "name": "Federated Rural Electric Association ",
    "url": "http://www.federatedrea.coop",
    "dist": "2",
    "coopclass": "C",
    "city": "Jackson",
    "state": "MN"
    },
    {
    "name": "FEM Electric Association",
    "url": "http://www.femelectric.coop/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Ipswich",
    "state": "SD"
    },
    {
    "name": "Fergus Electric Cooperative ",
    "url": "http://www.ferguselectric.coop/ ",
    "dist": "10",
    "coopclass": "C",
    "city": "Lewistown",
    "state": "MT"
    },
    {
    "name": "Flathead Electric Cooperative",
    "url": "http://www.flatheadelectric.com/ ",
    "dist": "1",
    "coopclass": "D",
    "city": "Kalispell & Libby ",
    "state": "MT"
    },
    {
    "name": "Franklin REC ",
    "url": "http://www.franklinrec.coop/ ",
    "dist": "11",
    "coopclass": "C",
    "city": "Hampton",
    "state": "IA"
    },
    {
    "name": "Garland Light & Power Company ",
    "url": "http://www.garlandpower.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Powell",
    "state": "WY"
    },
    {
    "name": "Goldenwest Electric Cooperative",
    "url": "http://www.montanaco-ops.com/?page=goldenwest ",
    "dist": "8",
    "coopclass": "C",
    "city": "Wibaux",
    "state": "MT"
    },
    {
    "name": "Grand Electric Cooperative ",
    "url": "http://www.grandelectric.coop/ ",
    "dist": "9",
    "coopclass": "A",
    "city": "Bison",
    "state": "SD"
    },
    {
    "name": "Grundy County REC",
    "url": "http://www.grundycountyrecia.com/ ",
    "dist": "11",
    "coopclass": "C",
    "city": "Grundy County",
    "state": "IA"
    },
    {
    "name": "Gunnison County Electric Association ",
    "url": "http://www.gcea.coop/",
    "dist": "5",
    "coopclass": "C",
    "city": "Gunnison",
    "state": "CO"
    },
    {
    "name": "H-D Electric Cooperative ",
    "url": "http://www.h-delectric.coop/",
    "dist": "1",
    "coopclass": "C",
    "city": "Clear Lake",
    "state": "SD"
    },
    {
    "name": "Harrison County Rural Electric Cooperative",
    "url": "http://www.hcrec.coop/ ",
    "dist": "4",
    "coopclass": "C",
    "city": "Woodbine",
    "state": "IA"
    },
    {
    "name": "High Plains Power Inc. ",
    "url": "http://www.highplainspower.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Thermopolis and Riverton",
    "state": "WY"
    },
    {
    "name": "High West Energy ",
    "url": "http://www.highwestenergy.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Pine Bluffs",
    "state": "WY"
    },
    {
    "name": "Highline Electric Association",
    "url": "http://www.hea.coop/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Holyoke",
    "state": "CO"
    },
    {
    "name": "Hill County Electric Cooperative ",
    "url": "http://www.hcelectric.com/ ",
    "dist": "6",
    "coopclass": "C",
    "city": "Havre",
    "state": "MT"
    },
    {
    "name": "Iowa Lakes Electric Cooperative ",
    "url": "http://www.ilec.coop/ ",
    "dist": "11",
    "coopclass": "C",
    "city": "Estherville",
    "state": "IA"
    },
    {
    "name": "Jemez Mountains Electric Cooperative ",
    "url": "http://www.jemezcoop.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Hernandez",
    "state": "NM"
    },
    {
    "name": "K. C. Electric Association ",
    "url": "http://www.kcelectric.coop/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Hugo",
    "state": "CO"
    },
    {
    "name": "KEM Electric Cooperative ",
    "url": "http://kemelectric.com/ ",
    "dist": "9",
    "coopclass": "A",
    "city": "Linton",
    "state": "ND"
    },
    {
    "name": "Kingsbury Electric Cooperative",
    "url": "http://www.kec-sd.coop/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "DeSmet",
    "state": "SD"
    },
    {
    "name": "L & O Power Cooperative",
    "url": "http://www.landopowercoop.com/ ",
    "dist": "2",
    "coopclass": "A",
    "city": "Rock Rapids",
    "state": "IA"
    },
    {
    "name": "La Plata Electric Association ",
    "url": "http://www.lpea.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Durango",
    "state": "CO"
    },
    {
    "name": "Lacreek Electric Association ",
    "url": "http://www.lacreek.com/",
    "dist": "7",
    "coopclass": "C",
    "city": "Martin",
    "state": "SD"
    },
    {
    "name": "Lake Region Electric Association ",
    "url": "http://www.lakeregion.coop/",
    "dist": "1",
    "coopclass": "C",
    "city": "Webster",
    "state": "SD"
    },
    {
    "name": "Lower Yellowstone Rural Electric Association",
    "url": "http://www.lyrec.com/ ",
    "dist": "8",
    "coopclass": "C",
    "city": "Sidney",
    "state": "MT"
    },
    {
    "name": "Lyon Rural Electric Cooperative ",
    "url": "http://www.lyonrec.coop/ ",
    "dist": "2",
    "coopclass": "C",
    "city": "Rock Rapids",
    "state": "IA"
    },
    {
    "name": "Lyon-Lincoln Electric Cooperative ",
    "url": "http://www.llec.coop/",
    "dist": "1",
    "coopclass": "C",
    "city": "Tyler",
    "state": "MN"
    },
    {
    "name": "Marias River Electric Cooperative ",
    "url": "http://www.mariasriverec.com/ ",
    "dist": "6",
    "coopclass": "C",
    "city": "Shelby",
    "state": "MT"
    },
    {
    "name": "McCone Electric Cooperative ",
    "url": "http://www.mcconeelectric.coop/ ",
    "dist": "8",
    "coopclass": "C",
    "city": "Circle",
    "state": "MT"
    },
    {
    "name": "McKenzie Electric Cooperative",
    "url": "http://www.mckenzieelectric.com/ ",
    "dist": "8",
    "coopclass": "C",
    "city": "Watford City",
    "state": "ND"
    },
    {
    "name": "McLean Electric Cooperative ",
    "url": "http://www.mcleanelectric.com/",
    "dist": "3",
    "coopclass": "C",
    "city": "Garrison",
    "state": "ND"
    },
    {
    "name": "Meeker Cooperative Light & Power Association",
    "url": "http://www.meeker.coop/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Litchfield",
    "state": "MN"
    },
    {
    "name": "Members 1st Power Cooperative ",
    "url": "http://members1stpower.coop/ ",
    "dist": "10",
    "coopclass": "A",
    "city": "Sundance",
    "state": "WY"
    },
    {
    "name": "Mid-Yellowstone Electric Cooperative ",
    "url": "http://www.uppermo.com/content/mid-yellowstone-electric ",
    "dist": "8",
    "coopclass": "C",
    "city": "Hysham",
    "state": "MT"
    },
    {
    "name": "Midland Power Cooperative ",
    "url": "http://www.midlandpower.coop/ ",
    "dist": "11",
    "coopclass": "C",
    "city": "Jefferson",
    "state": "IA"
    },
    {
    "name": "Midwest Electric Cooperative Corporation ",
    "url": "http://www.midwestecc.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Grant",
    "state": "NE"
    },
    {
    "name": "Minnesota Valley Cooperative Light & Power Association ",
    "url": "http://www.mnvalleyrec.com/ ",
    "dist": "9",
    "coopclass": "A",
    "city": "Montevideo",
    "state": "MN"
    },
    {
    "name": "Minnesota Valley Electric Cooperative ",
    "url": "http://www.mvec.net/",
    "dist": "9",
    "coopclass": "A",
    "city": "Jordan",
    "state": "MN"
    },
    {
    "name": "Mor-Gran-Sou Electric Cooperative",
    "url": "http://morgransou.com/ ",
    "dist": "9",
    "coopclass": "A",
    "city": "Flasher",
    "state": "ND"
    },
    {
    "name": "Mora-San Miguel Electric Cooperative",
    "url": "http://www.morasanmiguel.coop/",
    "dist": "5",
    "coopclass": "C",
    "city": "Mora",
    "state": "NM"
    },
    {
    "name": "Moreau-Grand Electric Cooperative ",
    "url": "http://www.mge.coop/ ",
    "dist": "7",
    "coopclass": "C",
    "city": "Timber Lake",
    "state": "SD"
    },
    {
    "name": "Morgan County Rural Electric Association ",
    "url": "http://www.mcrea.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Fort Morgan",
    "state": "CO"
    },
    {
    "name": "Mountain Parks Electric ",
    "url": "http://www.mpei.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Granby",
    "state": "CO"
    },
    {
    "name": "Mountain View Electric Association ",
    "url": "http://www.mvea.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Limon",
    "state": "CO"
    },
    {
    "name": "Mountrail-Williams Electric Cooperative ",
    "url": "http://www.mwec.com/ ",
    "dist": "8",
    "coopclass": "C",
    "city": "Williston",
    "state": "ND"
    },
    {
    "name": "Niobrara Electric Association ",
    "url": "http://niobrara-electric.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Lusk",
    "state": "WY"
    },
    {
    "name": "Nishnabotna Valley Rural Electric Cooperative ",
    "url": "http://www.nvrec.com ",
    "dist": "4",
    "coopclass": "C",
    "city": "Harlan",
    "state": "IA"
    },
    {
    "name": "North Central Electric Cooperative ",
    "url": "http://www.nceci.com/ ",
    "dist": "3",
    "coopclass": "C",
    "city": "Bottineau",
    "state": "ND"
    },
    {
    "name": "North Iowa Municipal Electric Cooperative Association ",
    "url": "http://www.nimeca.com/",
    "dist": "11",
    "coopclass": "C",
    "city": "Humboldt",
    "state": "IA"
    },
    {
    "name": "North West Rural Electric Cooperative",
    "url": "http://www.nwrec.com/ ",
    "dist": "4",
    "coopclass": "C",
    "city": "Orange City",
    "state": "IA"
    },
    {
    "name": "Northern Electric Cooperative ",
    "url": "http://www.northernelectric.coop/",
    "dist": "1",
    "coopclass": "C",
    "city": "Bath",
    "state": "SD"
    },
    {
    "name": "Northern Plains Electric ",
    "url": "http://www.nplains.com/",
    "dist": "3",
    "coopclass": "C",
    "city": "Carrington",
    "state": "ND"
    },
    {
    "name": "Northern Rio Arriba Electric Cooperative ",
    "url": "http://www.noraelectric.org/",
    "dist": "5",
    "coopclass": "C",
    "city": "Chama",
    "state": "NM"
    },
    {
    "name": "Northwest Iowa Power Cooperative ",
    "url": "http://www.nipco.coop/",
    "dist": "4",
    "coopclass": "A",
    "city": "LeMars",
    "state": "IA"
    },
    {
    "name": "Northwest Rural Public Power District ",
    "url": "http://www.nrppd.com/",
    "dist": "5",
    "coopclass": "C",
    "city": "Hay Springs",
    "state": "NE"
    },
    {
    "name": "NorVal Electric Cooperative ",
    "url": "http://www.norval.coop/ ",
    "dist": "6",
    "coopclass": "C",
    "city": "Glasgow & Opheim",
    "state": "MT"
    },
    {
    "name": "Oahe Electric Cooperative",
    "url": "http://www.oaheelectric.com/",
    "dist": "1",
    "coopclass": "C",
    "city": "Blunt",
    "state": "SD"
    },
    {
    "name": "Osceola Electric Cooperative",
    "url": "http://www.osceolaelectric.com/",
    "dist": "2",
    "coopclass": "C",
    "city": "Sibley",
    "state": "IA"
    },
    {
    "name": "Otero County Electric Cooperative ",
    "url": "http://www.ocec-inc.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Cloudcroft",
    "state": "NM"
    },
    {
    "name": "Panhandle Rural Electric Membership Association ",
    "url": "http://www.prema.coop/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Alliance",
    "state": "NE"
    },
    {
    "name": "Park Electric Cooperative ",
    "url": "http://parkelectric.coop/ ",
    "dist": "6",
    "coopclass": "C",
    "city": "Livingston",
    "state": "MT"
    },
    {
    "name": "Poudre Valley Rural Electric Association ",
    "url": "http://www.pvrea.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Fort Collins",
    "state": "CO"
    },
    {
    "name": "Powder River Energy Corporation ",
    "url": "http://precorp.coop/ ",
    "dist": "10",
    "coopclass": "C",
    "city": "Sundance",
    "state": "WY"
    },
    {
    "name": "Prairie Energy Cooperative ",
    "url": "http://www.prairieenergy.coop/ ",
    "dist": "11",
    "coopclass": "C",
    "city": "Clarion",
    "state": "IA"
    },
    {
    "name": "Raccoon Valley Electric Cooperative",
    "url": "http://www.rvec.coop/ ",
    "dist": "11",
    "coopclass": "C",
    "city": "Glidden",
    "state": "IA"
    },
    {
    "name": "Redwood Electric Cooperative ",
    "url": "http://redwoodelectric.com/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Clemens",
    "state": "MN"
    },
    {
    "name": "Renville-Sibley Cooperative Power Association ",
    "url": "http://www.renville-sibley.coop/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Danube",
    "state": "MN"
    },
    {
    "name": "Roosevelt Public Power District ",
    "url": "http://rooseveltppd.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Mitchell",
    "state": "NE"
    },
    {
    "name": "Rosebud Electric Cooperative",
    "url": "http://www.rosebudelectric.com/ ",
    "dist": "9",
    "coopclass": "A",
    "city": "Gregory",
    "state": "SD"
    },
    {
    "name": "Roughrider Electric Cooperative ",
    "url": "http://www.roughriderelectric.com/ ",
    "dist": "8",
    "coopclass": "C",
    "city": "Hazen",
    "state": "ND"
    },
    {
    "name": "Rushmore Electric Power Cooperative ",
    "url": "http://www.rushelec.com/ ",
    "dist": "7",
    "coopclass": "A",
    "city": "Rapid City",
    "state": "SD"
    },
    {
    "name": "San Isabel Electric Association ",
    "url": "http://www.siea.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Pueblo",
    "state": "CO"
    },
    {
    "name": "San Luis Valley Rural Electric Cooperative ",
    "url": "http://www.slvrec.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Monte Vista",
    "state": "CO"
    },
    {
    "name": "San Miguel Power Association ",
    "url": "http://www.smpa.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Nucla",
    "state": "CO"
    },
    {
    "name": "Sangre De Cristo Electric Association ",
    "url": "http://www.myelectric.coop/",
    "dist": "5",
    "coopclass": "C",
    "city": "Buena Vista",
    "state": "CO"
    },
    {
    "name": "Sheridan Electric Cooperative ",
    "url": "http://sheridanelectric.coop/ ",
    "dist": "8",
    "coopclass": "C",
    "city": "Medicine Lake",
    "state": "MT"
    },
    {
    "name": "Sierra Electric Cooperative Inc. ",
    "url": "http://www.sierraelectric.org/",
    "dist": "5",
    "coopclass": "C",
    "city": "Elephant Butte",
    "state": "NM"
    },
    {
    "name": "Sioux Valley Energy ",
    "url": "http://www.siouxvalleyenergy.com/ ",
    "dist": "2",
    "coopclass": "C",
    "city": "Colman",
    "state": "SD"
    },
    {
    "name": "Slope Electric Cooperative",
    "url": "http://www.slopeelectric.coop/ ",
    "dist": "8",
    "coopclass": "C",
    "city": "New England",
    "state": "ND"
    },
    {
    "name": "Socorro Electric Cooperative ",
    "url": "http://www.socorroelectric.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Socorro",
    "state": "NM"
    },
    {
    "name": "South Central Electric Association ",
    "url": "http://www.southcentralelectric.com/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "St. James",
    "state": "MN"
    },
    {
    "name": "Southeast Colorado Power Association ",
    "url": "http://secpa.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "La Junta",
    "state": "CO"
    },
    {
    "name": "Southeast Electric Cooperative ",
    "url": "http://www.seecoop.com/",
    "dist": "8",
    "coopclass": "C",
    "city": "Ekalaka",
    "state": "MT"
    },
    {
    "name": "Southeastern Electric Cooperative ",
    "url": "http://www.southeasternelectric.com/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Marion",
    "state": "SD"
    },
    {
    "name": "Southwestern Electric Cooperative",
    "url": "http://www.swec-coop.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Clayton",
    "state": "NM"
    },
    {
    "name": "Springer Electric Cooperative",
    "url": "http://www.springercoop.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Springer",
    "state": "NM"
    },
    {
    "name": "Sun River Electric Cooperative ",
    "url": "http://www.sunriverelectric.coop/ ",
    "dist": "6",
    "coopclass": "C",
    "city": "Fairfield",
    "state": "MT"
    },
    {
    "name": "Tongue River Electric Cooperative ",
    "url": "http://www.tongueriverelectric.com/ ",
    "dist": "10",
    "coopclass": "C",
    "city": "Ashland",
    "state": "MT"
    },
    {
    "name": "Traverse Electric Cooperative ",
    "url": "http://www.traverseelectric.com/",
    "dist": "1",
    "coopclass": "C",
    "city": "Wheaton",
    "state": "MN"
    },
    {
    "name": "Tri-State Generation and Transmission Association ",
    "url": "http://www.tristategt.org/ ",
    "dist": "5",
    "coopclass": "A",
    "city": "Denver",
    "state": "CO"
    },
    {
    "name": "Union County Electric Cooperative ",
    "url": "http://www.unioncounty.coop/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Elk Point",
    "state": "SD"
    },
    {
    "name": "United Power - Inc. ",
    "url": "http://www.unitedpower.com",
    "dist": "5",
    "coopclass": "C",
    "city": "Brighton",
    "state": "CO"
    },
    {
    "name": "Upper Missouri Power Cooperative ",
    "url": "http://uppermo.com/ ",
    "dist": "8",
    "coopclass": "A",
    "city": "Sidney",
    "state": "MT"
    },
    {
    "name": "Verendrye Electric Cooperative ",
    "url": "http://www.verendrye.com/ ",
    "dist": "3",
    "coopclass": "C",
    "city": "Velva",
    "state": "ND"
    },
    {
    "name": "West Central Electric Cooperative ",
    "url": "http://www.wce.coop/ ",
    "dist": "7",
    "coopclass": "C",
    "city": "Murdo",
    "state": "SD"
    },
    {
    "name": "West River Electric Association ",
    "url": "http://westriver.coop/ ",
    "dist": "7",
    "coopclass": "C",
    "city": "Wall",
    "state": "SD"
    },
    {
    "name": "Western Iowa Municipal Electric Association ",
    "url": "http://www.iamu.org/ ",
    "dist": "4",
    "coopclass": "C",
    "city": "Manning",
    "state": "IA"
    },
    {
    "name": "Western Iowa Power Cooperative ",
    "url": "http://www.wipco.com/ ",
    "dist": "4",
    "coopclass": "C",
    "city": "Denison",
    "state": "IA"
    },
    {
    "name": "Wheat Belt Public Power District ",
    "url": "http://www.wheatbelt.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Sidney",
    "state": "NE"
    },
    {
    "name": "Wheatland Rural Electric Association ",
    "url": "http://www.wheatlandrea.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Wheatland",
    "state": "WY"
    },
    {
    "name": "Whetstone Valley Electric Cooperative ",
    "url": "http://www.whetstone.coop/ ",
    "dist": "1",
    "coopclass": "C",
    "city": "Milbank",
    "state": "SD"
    },
    {
    "name": "White River Electric Association ",
    "url": "http://www.white-river-electric-association.org/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Meeker",
    "state": "CO"
    },
    {
    "name": "Woodbury County Rural Electric Cooperative",
    "url": "http://www.woodburyrec.com/ ",
    "dist": "4",
    "coopclass": "C",
    "city": "Moville",
    "state": "IA"
    },
    {
    "name": "Wright-Hennepin Cooperative Electric Association ",
    "url": "http://www.whe.org/ ",
    "dist": "9",
    "coopclass": "A",
    "city": "Rockford",
    "state": "MN"
    },
    {
    "name": "Wyrulec Company ",
    "url": "http://www.wyrulec.com/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Lingle",
    "state": "WY"
    },
    {
    "name": "Y-W Electric Association ",
    "url": "https://www.ywelectric.coop/ ",
    "dist": "5",
    "coopclass": "C",
    "city": "Akron",
    "state": "CO"
    },
    {
    "name": "Yellowstone Valley Electric Cooperative ",
    "url": "http://www.yvec.com/ ",
    "dist": "6",
    "coopclass": "C",
    "city": "Huntley",
    "state": "MT"
    }
    ]