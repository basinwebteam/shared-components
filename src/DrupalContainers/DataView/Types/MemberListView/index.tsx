import * as React from "react"
import { Table } from "antd"

export const MemberListView: React.SFC<IProps> = ({ data = [] }) => (
    <div className="member-list-view">
        {data.length > 0 &&
            <Table
                dataSource={getData(data)}
                columns={columns}
                pagination={false}
            />
        }
    </div>
)

MemberListView.displayName = "MemberListView"


export const columns: any = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: (a: any, b: any) => a.name.localeCompare(b.name),
        onFilter: (value: any, record: any) => record.name.indexOf(value) === 0,
        render: (text: string, record: IMLViewData) => (<a href={record.url}>{text}</a>)
    },
    {
        title: 'Class',
        dataIndex: 'class',
        key: 'class',
        sorter: (a: any, b: any) => {
            const first = a.class || ""
            const second = b.class || ""
            return first.localeCompare(second)
        },
        onFilter: (value: any, record: any) => record.class.indexOf(value) === 0,
    },
    {
        title: 'City',
        dataIndex: 'city',
        key: 'city',
        sorter: (a: any, b: any) => a.city.localeCompare(b.city),
        onFilter: (value: any, record: any) => record.city.indexOf(value) === 0,
    },
    {
        title: 'State',
        dataIndex: 'state',
        key: 'state',
        sorter: (a: any, b: any) => a.state.localeCompare(b.state),
        onFilter: (value: any, record: any) => record.state.indexOf(value) === 0,
    },
]

export const getData = (data: IMLViewData[]) => {
    const x = data
        .map((d, i) => ({
            key: i,
            name: d.name,
            url: d.url,
            dist: d.dist,
            class: d.coopclass,
            city: d.city,
            state: d.state,
        }))
    return x
}

export default MemberListView

export interface IProps {
    data?: IMLViewData[]
}

export interface IMLViewData {
    name: string,
    url: string,
    dist: string,
    coopclass: string,
    city: string,
    state: string,
}