// tslint:disable:max-line-length

export const pageData = {
    "data": {
        "type": "node--basin_electric_basic_page",
        "id": "cd34ded5-1f93-4e04-8b56-3ab2be02dbf4",
        "attributes": {
            "nid": 2571,
            "uuid": "cd34ded5-1f93-4e04-8b56-3ab2be02dbf4",
            "vid": 16621,
            "langcode": "en",
            "status": true,
            "title": "News Center",
            "created": 1501692237,
            "changed": 1508965448,
            "promote": false,
            "sticky": false,
            "revision_timestamp": 1508965448,
            "revision_log": null,
            "revision_translation_affected": true,
            "default_langcode": true,
            "path": {
                "alias": "/news-center",
                "pid": 3971
            },
            "field_meta_tags": "a:0:{}",
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c1953c9b-859c-42d1-a2ae-a74572456c0c"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/revision_uid"
                }
            },
            "menu_link": {
                "data": null
            },
            "moderation_state": {
                "data": null
            },
            "field_aside_content": {
                "data": []
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "85e390b0-5f10-45df-9ba3-88737a835f22",
                        "meta": {
                            "target_revision_id": "49051"
                        }
                    },
                    {
                        "type": "paragraph--data_view",
                        "id": "87a4c9bf-b981-4758-8c9a-dbb9bb0f2170",
                        "meta": {
                            "target_revision_id": "49056"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/field_content"
                }
            },
            "field_related_videos": {
                "data": null
            },
            "field_web_layout": {
                "data": {
                    "type": "taxonomy_term--web_layouts",
                    "id": "40f93ba8-666a-45d6-82f0-0c0e4503c363"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/relationships/field_web_layout",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4/field_web_layout"
                }
            },
            "scheduled_update": {
                "data": []
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/cd34ded5-1f93-4e04-8b56-3ab2be02dbf4?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_content.field_board%2Cfield_content.field_board.field_content%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--basic_paragraph",
            "id": "85e390b0-5f10-45df-9ba3-88737a835f22",
            "attributes": {
                "id": 4616,
                "uuid": "85e390b0-5f10-45df-9ba3-88737a835f22",
                "revision_id": 49051,
                "langcode": "en",
                "status": true,
                "created": 1501692248,
                "parent_id": "2571",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_body": {
                    "value": "<h2>Areas of interest</h2>\r\n\r\n<p><a href=\"https://www.basinelectric.com/News-Center/Publications/Basin-Today/\">Basin Today magazine&nbsp;articles</a>, <a href=\"https://www.basinelectric.com/News-Center/Publications/Annual-Report/\">annual report</a>, <a href=\"https://www.basinelectric.com/News-Center/Media-Resources/Tours/\">tour information</a> and <a href=\"https://www.basinelectric.com/News-Center/Media-Resources/Video-Gallery/\">video</a> galleries.</p>\r\n\r\n<h2>Media kit</h2>\r\n\r\n<p><a href=\"/sites/CMS/files/files/pdf/Fact-Sheets-Media-Kit/Basin-Electric-Media-Kit.pdf\">General Basin Electric media kit</a> (January 2016)</p>\r\n\r\n<h2>Latest News</h2>\r\n",
                    "format": "rich_text"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/85e390b0-5f10-45df-9ba3-88737a835f22/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/85e390b0-5f10-45df-9ba3-88737a835f22/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/85e390b0-5f10-45df-9ba3-88737a835f22/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/85e390b0-5f10-45df-9ba3-88737a835f22/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/85e390b0-5f10-45df-9ba3-88737a835f22/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/85e390b0-5f10-45df-9ba3-88737a835f22/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/85e390b0-5f10-45df-9ba3-88737a835f22"
            }
        },
        {
            "type": "paragraph--data_view",
            "id": "87a4c9bf-b981-4758-8c9a-dbb9bb0f2170",
            "attributes": {
                "id": 6146,
                "uuid": "87a4c9bf-b981-4758-8c9a-dbb9bb0f2170",
                "revision_id": 49056,
                "langcode": "en",
                "status": true,
                "created": 1501882550,
                "parent_id": "2571",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": "Latest News"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "0b02c669-1e41-447a-bf90-b2fa878b58cf"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/87a4c9bf-b981-4758-8c9a-dbb9bb0f2170/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/87a4c9bf-b981-4758-8c9a-dbb9bb0f2170/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/87a4c9bf-b981-4758-8c9a-dbb9bb0f2170/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/87a4c9bf-b981-4758-8c9a-dbb9bb0f2170/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/87a4c9bf-b981-4758-8c9a-dbb9bb0f2170/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/87a4c9bf-b981-4758-8c9a-dbb9bb0f2170/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_view": {
                    "data": {
                        "type": "view--view",
                        "id": "a535de45-565a-4e88-afe9-9dca9bd1cd3d"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/87a4c9bf-b981-4758-8c9a-dbb9bb0f2170/relationships/field_view",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/87a4c9bf-b981-4758-8c9a-dbb9bb0f2170/field_view"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/87a4c9bf-b981-4758-8c9a-dbb9bb0f2170"
            }
        },
        {
            "type": "taxonomy_term--web_layouts",
            "id": "40f93ba8-666a-45d6-82f0-0c0e4503c363",
            "attributes": {
                "tid": 611,
                "uuid": "40f93ba8-666a-45d6-82f0-0c0e4503c363",
                "langcode": "en",
                "name": "Page with aside Content",
                "description": null,
                "weight": 0,
                "changed": 1502982037,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "9481b046-bd2c-4ef8-ba1d-acde28597ffc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/40f93ba8-666a-45d6-82f0-0c0e4503c363/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/40f93ba8-666a-45d6-82f0-0c0e4503c363/vid"
                    }
                },
                "parent": {
                    "data": []
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/40f93ba8-666a-45d6-82f0-0c0e4503c363"
            }
        },
        {
            "type": "view--view",
            "id": "a535de45-565a-4e88-afe9-9dca9bd1cd3d",
            "attributes": {
                "uuid": "a535de45-565a-4e88-afe9-9dca9bd1cd3d",
                "langcode": "en",
                "status": true,
                "dependencies": {
                    "config": [
                        "field.storage.node.field_news_class",
                        "field.storage.node.field_news_thumb",
                        "field.storage.node.field_published_date",
                        "field.storage.node.field_summary",
                        "node.type.news_page",
                        "taxonomy.vocabulary.news_shared_domains"
                    ],
                    "content": [
                        "taxonomy_term:news_shared_domains:213e34f1-0b4a-4770-92f3-959fc1cf2def"
                    ],
                    "module": [
                        "datetime",
                        "image",
                        "node",
                        "rest",
                        "serialization",
                        "taxonomy",
                        "user"
                    ]
                },
                "id": "list_news",
                "label": "Latest News BEPC",
                "module": "views",
                "description": "Lists all news types in order by published date",
                "tag": "",
                "base_table": "node_field_data",
                "base_field": "nid",
                "core": "8.x",
                "display": {
                    "default": {
                        "display_plugin": "default",
                        "id": "default",
                        "display_title": "Master",
                        "position": 0,
                        "display_options": {
                            "access": {
                                "type": "perm",
                                "options": {
                                    "perm": "access content"
                                }
                            },
                            "cache": {
                                "type": "tag",
                                "options": []
                            },
                            "query": {
                                "type": "views_query",
                                "options": {
                                    "disable_sql_rewrite": false,
                                    "distinct": false,
                                    "replica": false,
                                    "query_comment": "",
                                    "query_tags": []
                                }
                            },
                            "exposed_form": {
                                "type": "basic",
                                "options": {
                                    "submit_button": "Apply",
                                    "reset_button": false,
                                    "reset_button_label": "Reset",
                                    "exposed_sorts_label": "Sort by",
                                    "expose_sort_order": true,
                                    "sort_asc_label": "Asc",
                                    "sort_desc_label": "Desc"
                                }
                            },
                            "pager": {
                                "type": "mini",
                                "options": {
                                    "items_per_page": 15,
                                    "offset": 0,
                                    "id": 0,
                                    "total_pages": null,
                                    "expose": {
                                        "items_per_page": false,
                                        "items_per_page_label": "Items per page",
                                        "items_per_page_options": "5, 10, 25, 50",
                                        "items_per_page_options_all": false,
                                        "items_per_page_options_all_label": "- All -",
                                        "offset": false,
                                        "offset_label": "Offset"
                                    },
                                    "tags": {
                                        "previous": "‹‹",
                                        "next": "››"
                                    }
                                }
                            },
                            "style": {
                                "type": "grid",
                                "options": {
                                    "grouping": [],
                                    "columns": 3,
                                    "automatic_width": true,
                                    "alignment": "horizontal",
                                    "col_class_default": true,
                                    "col_class_custom": "",
                                    "row_class_default": true,
                                    "row_class_custom": ""
                                }
                            },
                            "row": {
                                "type": "fields"
                            },
                            "fields": {
                                "field_news_thumb": {
                                    "id": "field_news_thumb",
                                    "table": "node__field_news_thumb",
                                    "field": "field_news_thumb",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image",
                                    "settings": {
                                        "image_style": "",
                                        "image_link": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_summary": {
                                    "id": "field_summary",
                                    "table": "node__field_summary",
                                    "field": "field_summary",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 100,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": true,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "basic_string",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_news_class": {
                                    "id": "field_news_class",
                                    "table": "node__field_news_class",
                                    "field": "field_news_class",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "entity_reference_label",
                                    "settings": {
                                        "link": false
                                    },
                                    "group_column": "target_id",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "m/d/y"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "nothing": {
                                    "id": "nothing",
                                    "table": "views",
                                    "field": "nothing",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": true,
                                        "text": "{{ field_news_thumb }}<br />\n<h2>{{ title }}</h2>\n<p>{{ field_summary }}<br/>\n<span style=\"color:#ccc;\"> {{ field_news_class }} - {{ field_published_date }}</span></p>",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": true,
                                    "empty_zero": false,
                                    "hide_alter_empty": false,
                                    "plugin_id": "custom"
                                }
                            },
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "value": {
                                        "news_page": "news_page"
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                },
                                "field_publish_to_sites_target_id": {
                                    "id": "field_publish_to_sites_target_id",
                                    "table": "node__field_publish_to_sites",
                                    "field": "field_publish_to_sites_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "336": 336
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_shared_domains",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                }
                            },
                            "sorts": {
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "order": "DESC",
                                    "exposed": false,
                                    "expose": {
                                        "label": ""
                                    },
                                    "granularity": "day",
                                    "plugin_id": "datetime"
                                }
                            },
                            "title": "Latest News",
                            "header": [],
                            "footer": [],
                            "empty": [],
                            "relationships": [],
                            "arguments": [],
                            "display_extenders": []
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url.query_args",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_class",
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    },
                    "block_1": {
                        "display_plugin": "block",
                        "id": "block_1",
                        "display_title": "Block-Latest News BEPC",
                        "position": 2,
                        "display_options": {
                            "display_extenders": [],
                            "style": {
                                "type": "grid",
                                "options": {
                                    "grouping": [],
                                    "columns": 3,
                                    "automatic_width": true,
                                    "alignment": "horizontal",
                                    "col_class_default": true,
                                    "col_class_custom": "",
                                    "row_class_default": true,
                                    "row_class_custom": ""
                                }
                            },
                            "defaults": {
                                "style": false,
                                "row": false,
                                "pager": false,
                                "fields": false
                            },
                            "row": {
                                "type": "fields"
                            },
                            "pager": {
                                "type": "some",
                                "options": {
                                    "items_per_page": 10,
                                    "offset": 0
                                }
                            },
                            "fields": {
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "m/d/y"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_summary": {
                                    "id": "field_summary",
                                    "table": "node__field_summary",
                                    "field": "field_summary",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 192,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": true,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "basic_string",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_news_class": {
                                    "id": "field_news_class",
                                    "table": "node__field_news_class",
                                    "field": "field_news_class",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "entity_reference_label",
                                    "settings": {
                                        "link": false
                                    },
                                    "group_column": "target_id",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "nothing": {
                                    "id": "nothing",
                                    "table": "views",
                                    "field": "nothing",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": true,
                                        "text": "<h2>{{ title }}</h2>\n<p> {{ field_summary }} </p>\n<p> {{ field_news_class }} - {{ field_published_date }}</p>",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": false,
                                    "plugin_id": "custom"
                                }
                            },
                            "block_description": "Latest News BEPC",
                            "allow": {
                                "items_per_page": true
                            },
                            "display_description": ""
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_class",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    },
                    "page_1": {
                        "display_plugin": "page",
                        "id": "page_1",
                        "display_title": "Page-Latest News BEPC",
                        "position": 1,
                        "display_options": {
                            "display_extenders": [],
                            "path": "latest-news-bepc",
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "in",
                                    "value": {
                                        "news_page": "news_page"
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false,
                                        "argument": null
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                },
                                "field_publish_to_sites_target_id": {
                                    "id": "field_publish_to_sites_target_id",
                                    "table": "node__field_publish_to_sites",
                                    "field": "field_publish_to_sites_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": [
                                        336
                                    ],
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_shared_domains",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                }
                            },
                            "defaults": {
                                "filters": false,
                                "filter_groups": false
                            },
                            "filter_groups": {
                                "operator": "AND",
                                "groups": {
                                    "1": "AND"
                                }
                            },
                            "exposed_block": true,
                            "display_description": ""
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url.query_args",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_class",
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    },
                    "rest_export_1": {
                        "display_plugin": "rest_export",
                        "id": "rest_export_1",
                        "display_title": "Rest-Latest News BEPC",
                        "position": 2,
                        "display_options": {
                            "display_extenders": [],
                            "style": {
                                "type": "serializer",
                                "options": {
                                    "formats": {
                                        "json": "json"
                                    }
                                }
                            },
                            "defaults": {
                                "style": false,
                                "row": false,
                                "pager": false,
                                "fields": false
                            },
                            "row": {
                                "type": "data_field",
                                "options": {
                                    "field_options": {
                                        "title": {
                                            "alias": "",
                                            "raw_output": true
                                        },
                                        "field_published_date": {
                                            "alias": "publishDate",
                                            "raw_output": false
                                        },
                                        "field_summary": {
                                            "alias": "summary",
                                            "raw_output": true
                                        },
                                        "field_news_class": {
                                            "alias": "newsClass",
                                            "raw_output": false
                                        },
                                        "path": {
                                            "alias": "url",
                                            "raw_output": false
                                        }
                                    }
                                }
                            },
                            "pager": {
                                "type": "some",
                                "options": {
                                    "items_per_page": 6,
                                    "offset": 0
                                }
                            },
                            "fields": {
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": false
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "F j"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_summary": {
                                    "id": "field_summary",
                                    "table": "node__field_summary",
                                    "field": "field_summary",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 192,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": true,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "basic_string",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_news_class": {
                                    "id": "field_news_class",
                                    "table": "node__field_news_class",
                                    "field": "field_news_class",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "entity_reference_label",
                                    "settings": {
                                        "link": false
                                    },
                                    "group_column": "target_id",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "path": {
                                    "id": "path",
                                    "table": "node",
                                    "field": "path",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "absolute": false,
                                    "entity_type": "node",
                                    "plugin_id": "node_path"
                                }
                            },
                            "path": "rest-latest-news-bepc",
                            "display_description": "Sends list of latest news articles"
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "request_format",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_class",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/view/view/a535de45-565a-4e88-afe9-9dca9bd1cd3d"
            }
        }
    ]
}

export const viewData = [
    {
        "title": "ND coal production in 2017 highest since 2009",
        "publishDate": "January 10",
        "summary": "Currently, North Dakota’s seven lignite-based power plants generate enough electricity to serve 2 million families in North Dakota and surrounding states.",
        "url": "/news-center/news-briefs/nd-coal-production-2017-highest-2009"
    },
    {
        "title": "Basin Electric’s Commitment to Community Campaign raises more than $184,000",
        "publishDate": "January 9",
        "summary": "The communities Basin Electric employees call home will be lifted by more than $184,000 raised during the co-op's 2017 Commitment to Community Campaign.",
        "url": "/news-center/news-briefs/basin-electrics-commitment-community-campaign-raises-more-184000"
    },
    {
        "title": "Registered nurse now located at Headquarters",
        "publishDate": "January 5",
        "summary": "Employees working at Headquarters can contact the onsite nurse for their daily medical needs and questions.",
        "url": "/news-center/news-briefs/registered-nurse-now-located-headquarters-0"
    },
    {
        "title": "Wind subsidiaries to merge into Basin Electric",
        "publishDate": "December 19",
        "summary": "Operations on the wind projects owned by the subsidiaries will not change.",
        "url": "/news-center/news-briefs/wind-subsidiaries-merge-basin-electric"
    },
    {
        "title": "Inaugural BE Leaders Program wraps up",
        "publishDate": "December 19",
        "summary": "Seventy-eight Basin Electric employees from all facilities dedicated 12 months to exploring various aspects of leadership through this newly created program.",
        "url": "/news-center/news-briefs/inaugural-be-leaders-program-wraps"
    },
    {
        "title": "Basin Electric surveys 2017 Annual Meeting attendees ",
        "publishDate": "December 19",
        "summary": "The survey was emailed to more than 1,000 attendees on Nov. 29. ",
        "url": "/news-center/news-briefs/basin-electric-surveys-2017-annual-meeting-attendees"
    },
    {
        "title": "Boards approve 2018 budgets",
        "publishDate": "December 19",
        "summary": "The consolidated net income after tax for Basin Electric and its subsidiaries is estimated to be $72.9 million.  ",
        "url": "/news-center/news-briefs/boards-approve-2018-budgets"
    },
    {
        "title": "Johnson elected to Touchstone Energy board",
        "publishDate": "December 15",
        "summary": "“I will be a strong voice for cooperatives around the country, from the largest to the smallest,” said Dick Johnson, West River Electric Association CEO/general manager. ",
        "url": "/news-center/news-briefs/johnson-elected-touchstone-energy-board"
    },
    {
        "title": "Basin Electric talks tax reform on Capitol Hill",
        "publishDate": "December 12",
        "summary": "The Lignite Energy Council Legislative Conference, held Dec. 4-6, included several visits with U.S. legislators and staff representing constituents in North Dakota, South Dakota, Minnesota, and Wyoming. ",
        "url": "/news-center/news-briefs/basin-electric-talks-tax-reform-capitol-hill"
    },
    {
        "title": "Boards hold reorganization meetings in December",
        "publishDate": "December 12",
        "summary": "Officers are nominated and elected for one-year terms by the directors.",
        "url": "/news-center/news-briefs/boards-hold-reorganization-meetings-december"
    },
    {
        "title": "Drilling is underway at CarbonSAFE North Dakota site",
        "publishDate": "December 12",
        "summary": "The primary driver for the Phase II effort is to further the knowledge of the area’s carbon dioxide storage potential so a commercial-scale carbon capture and storage project can be developed in an economic and technically sound manner.  ",
        "url": "/news-center/news-briefs/drilling-underway-carbonsafe-north-dakota-site"
    },
    {
        "title": "Basin Electric completes Headquarters expansion",
        "publishDate": "December 12",
        "summary": "The 91,000-square-foot expansion features a modern work environment designed to increase collaboration and exchange of knowledge and ideas, and will help bring all Headquarters employees under one roof.",
        "url": "/news-center/news-briefs/basin-electric-completes-headquarters-expansion"
    },
    {
        "title": "Tweeten accepts position as new Laramie River Station plant manager",
        "publishDate": "December 11",
        "summary": "Tweeten previously served as operations superintendent at Dry Fork Station.",
        "url": "/news-center/news-briefs/tweeten-accepts-position-new-laramie-river-station-plant-manager"
    },
    {
        "title": "Student scholarships available in 2018",
        "publishDate": "December 5",
        "summary": "The application deadline date is March 1, 2018.",
        "url": "/news-center/news-briefs/student-scholarships-available-2018"
    },
    {
        "title": "Basin Electric energizes western North Dakota transmission project",
        "publishDate": "December 5",
        "summary": "Construction on the project began in 2014.",
        "url": "/news-center/news-briefs/basin-electric-energizes-western-north-dakota-transmission-project"
    },
    {
        "title": "Urea facility project on track",
        "publishDate": "November 21",
        "summary": "The project is on schedule for commercial operation by the end of January 2018.",
        "url": "/news-center/news-briefs/urea-facility-project-track"
    },
    {
        "title": "Basin Electric senior staff facilitate Members Strategic Direction Meeting",
        "publishDate": "November 21",
        "summary": "Basin Electric directors and senior staff held the meeting to facilitate an open dialogue with the membership concerning the strategic direction of the cooperative’s largest subsidiary, Dakota Gasification Company. ",
        "url": "/news-center/news-briefs/basin-electric-senior-staff-facilitate-members-strategic-direction-meeting"
    },
    {
        "title": "Freedom Mine employees awarded Sentinels of Safety Award",
        "publishDate": "November 21",
        "summary": "All of Freedom Mine’s production – about 14.5 million tons per year – is sold to Dakota Coal Company to fuel the nearby Antelope Valley Station, Leland Olds Station, and Dakota Gasification Company’s Great Plains Synfuels Plant.",
        "url": "/news-center/news-briefs/freedom-mine-employees-awarded-sentinels-safety-award"
    },
    {
        "title": "Basin Electric participates in North American NERC security exercise",
        "publishDate": "November 21",
        "summary": "Several of Basin Electric employees, members, and government partners were among 6,000 people from across the continent to participate in GridEx IV.",
        "url": "/news-center/news-briefs/basin-electric-participates-north-american-nerc-security-exercise"
    },
    {
        "title": "Basin Electric board approves patronage capital distribution",
        "publishDate": "November 21",
        "summary": "The distribution brings the total patronage capital returned to member patrons to almost $250 million.",
        "url": "/news-center/news-briefs/basin-electric-board-approves-patronage-capital-distribution"
    },
    {
        "title": "Fourth employee safety initiative set to roll out",
        "publishDate": "November 14",
        "summary": "The initiative focuses on the communication of leading safety indicators in a clear and consistent way across the cooperative. ",
        "url": "/news-center/news-briefs/fourth-employee-safety-initiative-set-roll-out"
    },
    {
        "title": "Dry Fork Station sets monthly generation record",
        "publishDate": "November 6",
        "summary": "The plant generated 300,804 net megawatts (MW) in the month of October, the first time in its history that it generated more than 300,000 net MW within a month.",
        "url": "/news-center/news-briefs/dry-fork-station-sets-monthly-generation-record"
    },
    {
        "title": "Basin Electric joins other utilities to talk tax reform in D.C.",
        "publishDate": "November 6",
        "summary": "The group held meetings with the U.S. Department of Energy, a special assistant to the President, and a policy adviser for U.S. Rep. David B. McKinley (R-WV), who chairs the Congressional Coal Caucus.",
        "url": "/news-center/news-briefs/basin-electric-joins-other-utilities-talk-tax-reform-dc"
    },
    {
        "title": "Empower Youth looking for cooperative interest",
        "publishDate": "October 31",
        "summary": "Participating co-ops will be asked to help reach out to students in their respective service area who might be interested in the program, and assist with program sessions, which get underway in spring 2018.",
        "url": "/news-center/news-briefs/empower-youth-looking-cooperative-interest"
    },
    {
        "title": "Freedom Mine savings passed to Basin Electric",
        "publishDate": "October 31",
        "summary": "The Coteau Properties Company, which operates the Freedom Mine, found opportunities to reduce costs in 2017.",
        "url": "/news-center/news-briefs/freedom-mine-savings-passed-basin-electric"
    },
    {
        "title": "Touchstone Energy® #WhoPowersYou contest closes Nov. 4",
        "publishDate": "October 31",
        "summary": "Winners will be selected by a panel of judges in December 2017 and receive up to $5,000.",
        "url": "/news-center/news-briefs/touchstone-energyr-whopowersyou-contest-closes-nov-4"
    },
    {
        "title": "Basin Electric Annual Meeting: What to know before you go",
        "publishDate": "October 31",
        "summary": "Unable to attend? Find a link to the event livestream and more at am.bepc.com.",
        "url": "/news-center/news-briefs/basin-electric-annual-meeting-what-know-you-go"
    },
    {
        "title": "Deer Creek Station outage focuses on combustion turbine",
        "publishDate": "October 24",
        "summary": "Deer Creek Station was brought back on-line Oct. 11. ",
        "url": "/news-center/news-briefs/deer-creek-station-outage-focuses-combustion-turbine"
    },
    {
        "title": "Lightning strike starts fire, damaging structures along Dry Fork to Tongue River line",
        "publishDate": "October 24",
        "summary": "The fire damage was approximately 34 miles southeast of the Tongue River substation, which is located north of Sheridan, WY.",
        "url": "/news-center/news-briefs/lightning-strike-starts-fire-damaging-structures-along-dry-fork-tongue"
    },
    {
        "title": "Basin Electric supports DOE’s efforts to ensure fair baseload compensation",
        "publishDate": "October 23",
        "summary": "Basin Electric CEO and General Manager Paul Sukut said Basin Electric supports the DOE’s efforts to ensure baseload power generation resources are adequately compensated for their role in providing essential electric service in the wholesale power markets.",
        "url": "/news-center/news-briefs/basin-electric-supports-does-efforts-ensure-fair-baseload-compensation"
    },
    {
        "title": "Homeland Security inspection results positive at Synfuels Plant",
        "publishDate": "October 20",
        "summary": "The Department of Homeland Security's focus was on response, personnel surety, and cybersecurity.",
        "url": "/news-center/news-briefs/homeland-security-inspection-results-positive-synfuels-plant"
    },
    {
        "title": "2018 Basin Electric Scholarship Program materials available",
        "publishDate": "October 17",
        "summary": "Basin Electric’s Human Resources Department is providing the applications to member co-ops by both email and hard copy. ",
        "url": "/news-center/news-briefs/2018-basin-electric-scholarship-program-materials-available"
    },
    {
        "title": "Commitment to Community Campaign in full force",
        "publishDate": "October 17",
        "summary": "The cooperative-wide employee fundraising campaign runs Oct. 16-20, though employees can donate as part of the campaign until Oct. 27.",
        "url": "/news-center/news-briefs/commitment-community-campaign-full-force-0"
    },
    {
        "title": "2017 annual meeting around the corner",
        "publishDate": "October 17",
        "summary": "The deadline to register for Basin Electric's annual meeting is Oct. 27.",
        "url": "/news-center/news-briefs/2017-annual-meeting-around-corner"
    },
    {
        "title": "Public open houses held Oct. 11, 12 for CarbonSAFE-North Dakota",
        "publishDate": "October 17",
        "summary": "Interested community members and stakeholders attended both open houses to learn more about the two-year feasibility project.",
        "url": "/news-center/news-briefs/public-open-houses-held-oct-11-12-carbonsafe-north-dakota"
    },
    {
        "title": "Basin Electric representatives visit ratings agencies, investors",
        "publishDate": "October 16",
        "summary": "Steve Johnson, Basin Electric senior vice president and chief financial officer, said the group focused on some key credit strengths.",
        "url": "/news-center/news-briefs/basin-electric-representatives-visit-ratings-agencies-investors"
    },
    {
        "title": "Hazen child care center celebrates co-op month",
        "publishDate": "October 13",
        "summary": "The child care center's unique business model as a membership nonprofit is guided by the seven cooperative principles and continues to find inspiration within them, said Erin Huntimer, ECCCC board president and Basin Electric project coordinations representative.",
        "url": "/news-center/news-briefs/hazen-daycare-center-celebrates-co-op-month-0"
    }
]