import * as React from "react"

export const LatestNewsListView: React.SFC<IProps> = ({ data=[] }) => (
    <div className="news-list-wrapper">
        <ul className="news-list">
            {getBody(data)}
        </ul>
    </div>
)
LatestNewsListView.defaultProps = {
    data: []
}
LatestNewsListView.displayName = "LatestNewsListView"


export const getBody = (data: IMLViewData[] = []) => 
    data.map(({url, title, summary, publishDate, newsClass = ""}, i) => {
        newsClass = newsClass !== "" ? `${newsClass} - ` : ""
        return (
            <li key={i}>
                <a href={url} title={title}>
                    <div className="news-card">
                        <div className="title">{title}</div>
                        <div className="summary">{summary}</div>
                        <div className="dateline">
                            <span className="no-link-color">
                                {publishDate} - {newsClass}
                            </span>
                            Read Article
                        </div>
                    </div>
                </a>
            </li>
        )
    }
)

export default LatestNewsListView

export interface IProps {
    data?: IMLViewData[]
}

export interface IMLViewData {
    title: string,
    url: string,
    publishDate: string,
    summary: string,
    newsClass?: string,
}
