import * as React from "react"
import {AxiosResponse, default as axios} from "axios"
import * as Page from "../../../../DrupalContentTypes/PageInterface"
import { Paragraph } from "../../../index"

export class RetireePagesView extends React.Component<IRetireePagesProps, IRetireePagesState> {

    constructor(props: IRetireePagesProps) {
        super(props)
        this.state = {
            pages: []
        }
    }

    render() {
        const {pages} = this.state
        const {domain} = this.props
        return (
            <div className="retiree-pages">
                {pages.map((page) => (
                    <div className="retiree-page">
                        <h2 className="retiree-article-title">{page.data.attributes.title}</h2>
                        <Paragraph
                            items={page.data.relationships.field_content.data}
                            includedRelationships={page.included || []}
                            siteDomain={domain}
                        />
                    </div>
                ))}
            </div>
        )
    }

    componentDidUpdate(prevProps: IRetireePagesProps, prevState: IRetireePagesState){
        if(typeof prevProps.data !== "undefined" && 
            typeof this.props.data !== "undefined" && 
            prevProps.data.length !== this.props.data.length){
            this.getPagesContent()
        }
    }


    async getPagesContent() {
        const {data} = this.props
        if(typeof data === "undefined"){
            return
        }
        const responses = await Promise.all(
            data.map(async ({uuid}) => {
                return await axios.get("//api.bepc.com/api/cms/page/basinretiree/" + uuid)
            })
        )
        const pages = responses.map((response) => response.data)
        this.setState({
            pages
        })
    }
}

export default RetireePagesView

export interface IRetireePagesProps {
    data?: {uuid: string}[],
    domain: string
}

export interface IRetireePagesState {
    pages: IRetireeBasicPage[]
}

export interface IMultiplePagesResponse extends AxiosResponse {
    data: IRetireeBasicPage
}

export interface IRetireeBasicPage extends Page.IPage {
    data: Page.IPageData & {
        relationships: Page.IRelationships & {
            field_content: Page.IFieldContent 
        }
    }
}
