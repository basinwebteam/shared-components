// tslint:disable:max-line-length

export const pageData = {
    "data": {
        "type": "node--retirees_basic_page",
        "id": "4e512966-e1af-45d2-a865-665605a27141",
        "attributes": {
            "nid": 5521,
            "uuid": "4e512966-e1af-45d2-a865-665605a27141",
            "vid": 21526,
            "langcode": "en",
            "status": true,
            "title": "News",
            "created": 1512145381,
            "changed": 1518705744,
            "promote": false,
            "sticky": false,
            "revision_timestamp": 1518705744,
            "revision_log": null,
            "revision_translation_affected": true,
            "default_langcode": true,
            "path": {
                "alias": "/news",
                "pid": 4636
            },
            "body": null,
            "field_end_date_time": null,
            "field_published_date": null,
            "field_start_date_time": null,
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c815c6f9-2312-45f7-a509-673ebd695a8e"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/revision_uid"
                }
            },
            "menu_link": {
                "data": null
            },
            "moderation_state": {
                "data": null
            },
            "field_aside_content": {
                "data": [
                    {
                        "type": "paragraph--title",
                        "id": "f166486a-dbc5-4717-9880-5e5e1f46057e",
                        "meta": {
                            "target_revision_id": "67146"
                        }
                    },
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "0bbdac09-c4d5-4485-8c9b-6319c9759541",
                        "meta": {
                            "target_revision_id": "67151"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/relationships/field_aside_content",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/field_aside_content"
                }
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--data_view",
                        "id": "dbd67dfc-3849-4da8-91c3-a81ea687e8b3",
                        "meta": {
                            "target_revision_id": "67156"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/field_content"
                }
            },
            "field_image": {
                "data": null
            },
            "field_page_type": {
                "data": {
                    "type": "taxonomy_term--retiree_page_types",
                    "id": "1c8e5014-b26e-4bd5-ad15-f9d092ed70c3"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/relationships/field_page_type",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/field_page_type"
                }
            },
            "field_web_layout": {
                "data": {
                    "type": "taxonomy_term--web_layouts",
                    "id": "40f93ba8-666a-45d6-82f0-0c0e4503c363"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/relationships/field_web_layout",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141/field_web_layout"
                }
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/4e512966-e1af-45d2-a865-665605a27141?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_content.field_board%2Cfield_content.field_board.field_content%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--title",
            "id": "f166486a-dbc5-4717-9880-5e5e1f46057e",
            "attributes": {
                "id": 13846,
                "uuid": "f166486a-dbc5-4717-9880-5e5e1f46057e",
                "revision_id": 67146,
                "langcode": "en",
                "status": true,
                "created": 1518015081,
                "parent_id": "5521",
                "parent_type": "node",
                "parent_field_name": "field_aside_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": "Publications"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1e5a7347-50e5-4f3a-af40-6724fa1bf928"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/title/f166486a-dbc5-4717-9880-5e5e1f46057e/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/title/f166486a-dbc5-4717-9880-5e5e1f46057e/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/title/f166486a-dbc5-4717-9880-5e5e1f46057e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/title/f166486a-dbc5-4717-9880-5e5e1f46057e/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/title/f166486a-dbc5-4717-9880-5e5e1f46057e/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/title/f166486a-dbc5-4717-9880-5e5e1f46057e/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/title/f166486a-dbc5-4717-9880-5e5e1f46057e"
            }
        },
        {
            "type": "paragraph--basic_paragraph",
            "id": "0bbdac09-c4d5-4485-8c9b-6319c9759541",
            "attributes": {
                "id": 13786,
                "uuid": "0bbdac09-c4d5-4485-8c9b-6319c9759541",
                "revision_id": 67151,
                "langcode": "en",
                "status": true,
                "created": 1517932594,
                "parent_id": "5521",
                "parent_type": "node",
                "parent_field_name": "field_aside_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_body": {
                    "value": "<ul>\r\n\t<li><a href=\"https://basinelectric.com/News-Center/Publications/Annual-Report/\" target=\"_blank\">Annual Report</a></li>\r\n\t<li><a href=\"http://basinelectric.com/News-Center/Publications/Basin-Today/index.html\" target=\"_blank\">Basin Today</a></li>\r\n\t<li><a href=\"http://cms.bepc.com/sites/CMS/files/Publications/adviser.pdf\" target=\"_blank\">Independent Adviser</a></li>\r\n\t<li><a href=\"http://cms.bepc.com/sites/CMS/files/Publications/The_Bulletin.pdf\" target=\"_blank\">The Bulletin</a></li>\r\n\t<li><a href=\"https://vimeopro.com/user65352350/boardpaq\" target=\"_blank\">Board Videos</a></li>\r\n\t<li><a href=\"http://cms.bepc.com/sites/CMS/files/Publications/WattsNews.pdf\" target=\"_blank\">Watts News</a></li>\r\n</ul>\r\n",
                    "format": "rich_text"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/0bbdac09-c4d5-4485-8c9b-6319c9759541/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/0bbdac09-c4d5-4485-8c9b-6319c9759541/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/0bbdac09-c4d5-4485-8c9b-6319c9759541/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/0bbdac09-c4d5-4485-8c9b-6319c9759541/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/0bbdac09-c4d5-4485-8c9b-6319c9759541/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/0bbdac09-c4d5-4485-8c9b-6319c9759541/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/0bbdac09-c4d5-4485-8c9b-6319c9759541"
            }
        },
        {
            "type": "paragraph--data_view",
            "id": "dbd67dfc-3849-4da8-91c3-a81ea687e8b3",
            "attributes": {
                "id": 12751,
                "uuid": "dbd67dfc-3849-4da8-91c3-a81ea687e8b3",
                "revision_id": 67156,
                "langcode": "en",
                "status": true,
                "created": 1512145429,
                "parent_id": "5521",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "0b02c669-1e41-447a-bf90-b2fa878b58cf"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/dbd67dfc-3849-4da8-91c3-a81ea687e8b3/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/dbd67dfc-3849-4da8-91c3-a81ea687e8b3/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/dbd67dfc-3849-4da8-91c3-a81ea687e8b3/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/dbd67dfc-3849-4da8-91c3-a81ea687e8b3/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/dbd67dfc-3849-4da8-91c3-a81ea687e8b3/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/dbd67dfc-3849-4da8-91c3-a81ea687e8b3/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_view": {
                    "data": {
                        "type": "view--view",
                        "id": "ca3898e4-b6b0-43f0-8bb6-0f95e4cb92b2"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/dbd67dfc-3849-4da8-91c3-a81ea687e8b3/relationships/field_view",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/dbd67dfc-3849-4da8-91c3-a81ea687e8b3/field_view"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/dbd67dfc-3849-4da8-91c3-a81ea687e8b3"
            }
        },
        {
            "type": "taxonomy_term--web_layouts",
            "id": "40f93ba8-666a-45d6-82f0-0c0e4503c363",
            "attributes": {
                "tid": 611,
                "uuid": "40f93ba8-666a-45d6-82f0-0c0e4503c363",
                "langcode": "en",
                "name": "Page with aside Content",
                "description": null,
                "weight": 0,
                "changed": 1502982037,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "9481b046-bd2c-4ef8-ba1d-acde28597ffc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/40f93ba8-666a-45d6-82f0-0c0e4503c363/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/40f93ba8-666a-45d6-82f0-0c0e4503c363/vid"
                    }
                },
                "parent": {
                    "data": []
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/40f93ba8-666a-45d6-82f0-0c0e4503c363"
            }
        },
        {
            "type": "view--view",
            "id": "ca3898e4-b6b0-43f0-8bb6-0f95e4cb92b2",
            "attributes": {
                "uuid": "ca3898e4-b6b0-43f0-8bb6-0f95e4cb92b2",
                "langcode": "en",
                "status": true,
                "dependencies": {
                    "config": [
                        "field.storage.node.field_news_thumb",
                        "field.storage.node.field_published_date",
                        "field.storage.node.field_summary",
                        "node.type.news_page",
                        "node.type.retirees_basic_page",
                        "taxonomy.vocabulary.news_class",
                        "taxonomy.vocabulary.news_shared_domains",
                        "taxonomy.vocabulary.retiree_page_types"
                    ],
                    "content": [
                        "taxonomy_term:news_class:7945bfdd-7c7c-402f-8249-b3e493a64863",
                        "taxonomy_term:news_shared_domains:213e34f1-0b4a-4770-92f3-959fc1cf2def",
                        "taxonomy_term:retiree_page_types:dc0572eb-06be-4ae2-8de4-373a122871dd"
                    ],
                    "module": [
                        "datetime",
                        "image",
                        "node",
                        "rest",
                        "serialization",
                        "taxonomy",
                        "user"
                    ]
                },
                "id": "retirees_news_list",
                "label": "Retirees News List",
                "module": "views",
                "description": "Lists retiree news types",
                "tag": "",
                "base_table": "node_field_data",
                "base_field": "nid",
                "core": "8.x",
                "display": {
                    "default": {
                        "display_plugin": "default",
                        "id": "default",
                        "display_title": "Master",
                        "position": 0,
                        "display_options": {
                            "access": {
                                "type": "perm",
                                "options": {
                                    "perm": "access content"
                                }
                            },
                            "cache": {
                                "type": "tag",
                                "options": []
                            },
                            "query": {
                                "type": "views_query",
                                "options": {
                                    "disable_sql_rewrite": false,
                                    "distinct": false,
                                    "replica": false,
                                    "query_comment": "",
                                    "query_tags": []
                                }
                            },
                            "exposed_form": {
                                "type": "basic",
                                "options": {
                                    "submit_button": "Apply",
                                    "reset_button": false,
                                    "reset_button_label": "Reset",
                                    "exposed_sorts_label": "Sort by",
                                    "expose_sort_order": true,
                                    "sort_asc_label": "Asc",
                                    "sort_desc_label": "Desc"
                                }
                            },
                            "pager": {
                                "type": "mini",
                                "options": {
                                    "items_per_page": 15,
                                    "offset": 0,
                                    "id": 0,
                                    "total_pages": null,
                                    "expose": {
                                        "items_per_page": false,
                                        "items_per_page_label": "Items per page",
                                        "items_per_page_options": "5, 10, 25, 50",
                                        "items_per_page_options_all": false,
                                        "items_per_page_options_all_label": "- All -",
                                        "offset": false,
                                        "offset_label": "Offset"
                                    },
                                    "tags": {
                                        "previous": "‹‹",
                                        "next": "››"
                                    }
                                }
                            },
                            "style": {
                                "type": "grid",
                                "options": {
                                    "grouping": [],
                                    "columns": 3,
                                    "automatic_width": true,
                                    "alignment": "horizontal",
                                    "col_class_default": true,
                                    "col_class_custom": "",
                                    "row_class_default": true,
                                    "row_class_custom": ""
                                }
                            },
                            "row": {
                                "type": "fields"
                            },
                            "fields": {
                                "field_news_thumb": {
                                    "id": "field_news_thumb",
                                    "table": "node__field_news_thumb",
                                    "field": "field_news_thumb",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image",
                                    "settings": {
                                        "image_style": "",
                                        "image_link": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_summary": {
                                    "id": "field_summary",
                                    "table": "node__field_summary",
                                    "field": "field_summary",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 100,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": true,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "basic_string",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "m/d/y"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "nothing": {
                                    "id": "nothing",
                                    "table": "views",
                                    "field": "nothing",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": true,
                                        "text": "{{ field_news_thumb }}<br />\n<h2>{{ title }}</h2>\n<p>{{ field_summary }}<br/>\n<span style=\"color:#ccc;\"> {{ field_news_class }} - {{ field_published_date }}</span></p>",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": true,
                                    "empty_zero": false,
                                    "hide_alter_empty": false,
                                    "plugin_id": "custom"
                                }
                            },
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "value": {
                                        "news_page": "news_page"
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                },
                                "field_publish_to_sites_target_id": {
                                    "id": "field_publish_to_sites_target_id",
                                    "table": "node__field_publish_to_sites",
                                    "field": "field_publish_to_sites_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "336": 336
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_shared_domains",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_news_class_target_id": {
                                    "id": "field_news_class_target_id",
                                    "table": "node__field_news_class",
                                    "field": "field_news_class_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "356": 356
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_class",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "plugin_id": "datetime"
                                }
                            },
                            "sorts": {
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "order": "DESC",
                                    "exposed": false,
                                    "expose": {
                                        "label": ""
                                    },
                                    "granularity": "day",
                                    "plugin_id": "datetime"
                                }
                            },
                            "title": "News",
                            "header": [],
                            "footer": [],
                            "empty": [],
                            "relationships": [],
                            "arguments": {
                                "field_published_date_value_full_date": {
                                    "id": "field_published_date_value_full_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value_full_date",
                                    "plugin_id": "datetime_full_date"
                                }
                            },
                            "display_extenders": []
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url",
                                "url.query_args",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    },
                    "rest_export_1": {
                        "display_plugin": "rest_export",
                        "id": "rest_export_1",
                        "display_title": "news",
                        "position": 2,
                        "display_options": {
                            "display_extenders": [],
                            "style": {
                                "type": "serializer",
                                "options": {
                                    "formats": {
                                        "json": "json"
                                    }
                                }
                            },
                            "defaults": {
                                "style": false,
                                "row": false,
                                "pager": false,
                                "fields": false,
                                "filters": false,
                                "filter_groups": false
                            },
                            "row": {
                                "type": "data_field",
                                "options": {
                                    "field_options": {
                                        "title": {
                                            "alias": "",
                                            "raw_output": true
                                        },
                                        "path": {
                                            "alias": "url",
                                            "raw_output": false
                                        },
                                        "promote": {
                                            "alias": "",
                                            "raw_output": false
                                        },
                                        "sticky": {
                                            "alias": "",
                                            "raw_output": false
                                        },
                                        "field_published_date": {
                                            "alias": "publishDate",
                                            "raw_output": false
                                        },
                                        "field_image": {
                                            "alias": "image",
                                            "raw_output": false
                                        },
                                        "field_content": {
                                            "alias": "content",
                                            "raw_output": false
                                        }
                                    }
                                }
                            },
                            "pager": {
                                "type": "some",
                                "options": {
                                    "items_per_page": 25,
                                    "offset": 0
                                }
                            },
                            "path": "rest-retirees-news",
                            "display_description": "Sends list of latest News articles",
                            "fields": {
                                "uuid": {
                                    "id": "uuid",
                                    "table": "node",
                                    "field": "uuid",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": false
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "entity_type": "node",
                                    "entity_field": "uuid",
                                    "plugin_id": "field"
                                }
                            },
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "in",
                                    "value": {
                                        "retirees_basic_page": "retirees_basic_page"
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false,
                                        "argument": null
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                },
                                "field_page_type_target_id": {
                                    "id": "field_page_type_target_id",
                                    "table": "node__field_page_type",
                                    "field": "field_page_type_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": [
                                        681
                                    ],
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "textfield",
                                    "limit": true,
                                    "vid": "retiree_page_types",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "<=",
                                    "value": {
                                        "min": "",
                                        "max": "",
                                        "value": "-120",
                                        "type": "offset"
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        }
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "plugin_id": "datetime"
                                }
                            },
                            "filter_groups": {
                                "operator": "AND",
                                "groups": {
                                    "1": "AND"
                                }
                            }
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "request_format",
                                "url",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": []
                        }
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/view/view/ca3898e4-b6b0-43f0-8bb6-0f95e4cb92b2"
            }
        }
    ]
}