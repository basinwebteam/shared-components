import * as React from "react"

export const BasinTodayListView: React.SFC<IProps> = ({data=[], domain = ""}) => (
    <div className="basin-today">
        {getFeature(data, domain)}
        <div className="btoday row">
            <ul className="medium-block-grid-2 large-block-grid-3">
                {getThumbnails(data, domain)}
            </ul>
        </div>
    </div>
)

BasinTodayListView.displayName = "BasinTodayListView"

export const getFeature = (data: IMLViewData[] = [], domain="") => {
    let foundPromoted = false
    return data
    .filter((news) => {
        if (foundPromoted === false && news.sticky === "True") {
            foundPromoted = true
            return true
        }
        return false
    })
    .map((news, i) => {
        const image = getFeaturedImage(news.field_news_top_image, domain)
        return (
            <div className="btoday-feature-card" key={i}>
                <a href={news.url} title={news.title}>
                    <div className="btoday-feature-img">
                        <img src={`${image}`} />
                    </div>
                    <div className="btoday-feature-title">{news.title}</div>
                    <div className="btoday-feature-summary">{news.summary}</div>
                    <div className="btoday-feature-dateline">{news.byline} - {news.publishDate}</div>
                </a>
            </div>
        )
    })
}

export const getThumbnails = (data: IMLViewData[] = [], domain = "") => {
    let foundPromoted = false
    return data
        .filter((news) => {
            if (foundPromoted === false && news.sticky === "True") {
                foundPromoted = true
                return false
            }
            return true
        })
        .map((news, i) => {
            const image = getImageThumbnail(news.btThumb, domain)
            return (
                <li key={i}>
                    <a href={news.url} title={news.title}>
                        <div className="btoday-card">
                            <div className="btoday-thumb" dangerouslySetInnerHTML={{__html: image}} />
                            <div className="btoday-title">{news.title}</div>
                            <div className="btoday-summary">{news.summary}</div>
                            <div className="btoday-dateline">{news.byline} - {news.publishDate}</div>
                        </div>
                    </a>
                </li>
            )
    })
}

// Useful for images that aren't hosted on the local site
export const getImageThumbnail = (image: string = "", domain = "") => domain !== "" ? image.replace('src="/', `src="${domain}/`) : image

// Useful for images that aren't hosted on the local site
export const getFeaturedImage = (image: string = "", domain = "") => domain !== "" ?domain + image : image


export default BasinTodayListView


export interface IProps {
    data?: IMLViewData[],
    domain?: string, // Use when images aren't hosted on local site
}
export interface IMLViewData {
    title: string,
    url: string,
    publishDate: string,
    summary: string,
    byline: string,
    btThumb: string,
    thumbImg: string,
    field_news_top_image: string,
    sticky: BooleanString | string,
    promote?: BooleanString | string,
}

export type BooleanString = "False" | "True"
