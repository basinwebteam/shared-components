// tslint:disable:max-line-length
export const pageData = {
    "data": {
        "type": "node--basin_electric_basic_page",
        "id": "f3deb14f-7a49-4654-b386-93c5db542416",
        "attributes": {
            "nid": 4481,
            "uuid": "f3deb14f-7a49-4654-b386-93c5db542416",
            "vid": 11421,
            "langcode": "en",
            "status": true,
            "title": "Basin Today",
            "created": 1502314241,
            "changed": 1504625248,
            "promote": false,
            "sticky": false,
            "revision_timestamp": 1504625248,
            "revision_log": null,
            "revision_translation_affected": true,
            "default_langcode": true,
            "path": {
                "alias": "/news-center/publications/basin-today",
                "pid": 3541
            },
            "field_meta_tags": "a:0:{}",
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c1953c9b-859c-42d1-a2ae-a74572456c0c"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/revision_uid"
                }
            },
            "menu_link": {
                "data": null
            },
            "moderation_state": {
                "data": null
            },
            "field_aside_content": {
                "data": []
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--data_view",
                        "id": "e232bbc8-1cb3-4d71-99cd-70864906c706",
                        "meta": {
                            "target_revision_id": "33641"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/field_content"
                }
            },
            "field_related_videos": {
                "data": null
            },
            "field_web_layout": {
                "data": {
                    "type": "taxonomy_term--web_layouts",
                    "id": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/relationships/field_web_layout",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416/field_web_layout"
                }
            },
            "scheduled_update": {
                "data": []
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/f3deb14f-7a49-4654-b386-93c5db542416?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_content.field_board%2Cfield_content.field_board.field_content%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--data_view",
            "id": "e232bbc8-1cb3-4d71-99cd-70864906c706",
            "attributes": {
                "id": 8381,
                "uuid": "e232bbc8-1cb3-4d71-99cd-70864906c706",
                "revision_id": 33641,
                "langcode": "en",
                "status": true,
                "created": 1502314249,
                "parent_id": "4481",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "0b02c669-1e41-447a-bf90-b2fa878b58cf"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/e232bbc8-1cb3-4d71-99cd-70864906c706/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/e232bbc8-1cb3-4d71-99cd-70864906c706/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/e232bbc8-1cb3-4d71-99cd-70864906c706/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/e232bbc8-1cb3-4d71-99cd-70864906c706/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/e232bbc8-1cb3-4d71-99cd-70864906c706/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/e232bbc8-1cb3-4d71-99cd-70864906c706/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_view": {
                    "data": {
                        "type": "view--view",
                        "id": "e3f56a2f-32bd-492f-9313-5477795c4a1c"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/e232bbc8-1cb3-4d71-99cd-70864906c706/relationships/field_view",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/e232bbc8-1cb3-4d71-99cd-70864906c706/field_view"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/e232bbc8-1cb3-4d71-99cd-70864906c706"
            }
        },
        {
            "type": "taxonomy_term--web_layouts",
            "id": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb",
            "attributes": {
                "tid": 606,
                "uuid": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb",
                "langcode": "en",
                "name": "Full Page",
                "description": null,
                "weight": 0,
                "changed": 1502982012,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "9481b046-bd2c-4ef8-ba1d-acde28597ffc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb/vid"
                    }
                },
                "parent": {
                    "data": []
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb"
            }
        },
        {
            "type": "view--view",
            "id": "e3f56a2f-32bd-492f-9313-5477795c4a1c",
            "attributes": {
                "uuid": "e3f56a2f-32bd-492f-9313-5477795c4a1c",
                "langcode": "en",
                "status": true,
                "dependencies": {
                    "config": [
                        "field.storage.node.field_basin_today_thumb",
                        "field.storage.node.field_byline",
                        "field.storage.node.field_news_thumb",
                        "field.storage.node.field_news_top_image",
                        "field.storage.node.field_published_date",
                        "field.storage.node.field_summary",
                        "node.type.news_page",
                        "taxonomy.vocabulary.news_class",
                        "taxonomy.vocabulary.news_shared_domains"
                    ],
                    "content": [
                        "taxonomy_term:news_class:7945bfdd-7c7c-402f-8249-b3e493a64863",
                        "taxonomy_term:news_shared_domains:213e34f1-0b4a-4770-92f3-959fc1cf2def"
                    ],
                    "module": [
                        "datetime",
                        "image",
                        "node",
                        "rest",
                        "serialization",
                        "taxonomy",
                        "user"
                    ]
                },
                "id": "news_basin_today",
                "label": "News Basin Today",
                "module": "views",
                "description": "Lists all news types in order by published date",
                "tag": "",
                "base_table": "node_field_data",
                "base_field": "nid",
                "core": "8.x",
                "display": {
                    "default": {
                        "display_plugin": "default",
                        "id": "default",
                        "display_title": "Master",
                        "position": 0,
                        "display_options": {
                            "access": {
                                "type": "perm",
                                "options": {
                                    "perm": "access content"
                                }
                            },
                            "cache": {
                                "type": "tag",
                                "options": []
                            },
                            "query": {
                                "type": "views_query",
                                "options": {
                                    "disable_sql_rewrite": false,
                                    "distinct": false,
                                    "replica": false,
                                    "query_comment": "",
                                    "query_tags": []
                                }
                            },
                            "exposed_form": {
                                "type": "basic",
                                "options": {
                                    "submit_button": "Apply",
                                    "reset_button": false,
                                    "reset_button_label": "Reset",
                                    "exposed_sorts_label": "Sort by",
                                    "expose_sort_order": true,
                                    "sort_asc_label": "Asc",
                                    "sort_desc_label": "Desc"
                                }
                            },
                            "pager": {
                                "type": "mini",
                                "options": {
                                    "items_per_page": 15,
                                    "offset": 0,
                                    "id": 0,
                                    "total_pages": null,
                                    "expose": {
                                        "items_per_page": false,
                                        "items_per_page_label": "Items per page",
                                        "items_per_page_options": "5, 10, 25, 50",
                                        "items_per_page_options_all": false,
                                        "items_per_page_options_all_label": "- All -",
                                        "offset": false,
                                        "offset_label": "Offset"
                                    },
                                    "tags": {
                                        "previous": "‹‹",
                                        "next": "››"
                                    }
                                }
                            },
                            "style": {
                                "type": "grid",
                                "options": {
                                    "grouping": [],
                                    "columns": 3,
                                    "automatic_width": true,
                                    "alignment": "horizontal",
                                    "col_class_default": true,
                                    "col_class_custom": "",
                                    "row_class_default": true,
                                    "row_class_custom": ""
                                }
                            },
                            "row": {
                                "type": "fields"
                            },
                            "fields": {
                                "field_news_thumb": {
                                    "id": "field_news_thumb",
                                    "table": "node__field_news_thumb",
                                    "field": "field_news_thumb",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image",
                                    "settings": {
                                        "image_style": "",
                                        "image_link": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_summary": {
                                    "id": "field_summary",
                                    "table": "node__field_summary",
                                    "field": "field_summary",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 100,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": true,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "basic_string",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "m/d/y"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "nothing": {
                                    "id": "nothing",
                                    "table": "views",
                                    "field": "nothing",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": true,
                                        "text": "{{ field_news_thumb }}<br />\n<h2>{{ title }}</h2>\n<p>{{ field_summary }}<br/>\n<span style=\"color:#ccc;\"> {{ field_news_class }} - {{ field_published_date }}</span></p>",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": true,
                                    "empty_zero": false,
                                    "hide_alter_empty": false,
                                    "plugin_id": "custom"
                                }
                            },
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "value": {
                                        "news_page": "news_page"
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                },
                                "field_publish_to_sites_target_id": {
                                    "id": "field_publish_to_sites_target_id",
                                    "table": "node__field_publish_to_sites",
                                    "field": "field_publish_to_sites_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "336": 336
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_shared_domains",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_news_class_target_id": {
                                    "id": "field_news_class_target_id",
                                    "table": "node__field_news_class",
                                    "field": "field_news_class_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "356": 356
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_class",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "plugin_id": "datetime"
                                }
                            },
                            "sorts": {
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "order": "DESC",
                                    "exposed": false,
                                    "expose": {
                                        "label": ""
                                    },
                                    "granularity": "day",
                                    "plugin_id": "datetime"
                                }
                            },
                            "title": "",
                            "header": [],
                            "footer": [],
                            "empty": [],
                            "relationships": [],
                            "arguments": {
                                "field_published_date_value_full_date": {
                                    "id": "field_published_date_value_full_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value_full_date",
                                    "plugin_id": "datetime_full_date"
                                }
                            },
                            "display_extenders": []
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url",
                                "url.query_args",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    },
                    "block_1": {
                        "display_plugin": "block",
                        "id": "block_1",
                        "display_title": "Block-Basin Today",
                        "position": 2,
                        "display_options": {
                            "display_extenders": [],
                            "style": {
                                "type": "grid",
                                "options": {
                                    "grouping": [],
                                    "columns": 3,
                                    "automatic_width": true,
                                    "alignment": "horizontal",
                                    "col_class_default": true,
                                    "col_class_custom": "",
                                    "row_class_default": true,
                                    "row_class_custom": ""
                                }
                            },
                            "defaults": {
                                "style": false,
                                "row": false,
                                "pager": false,
                                "fields": false
                            },
                            "row": {
                                "type": "fields"
                            },
                            "pager": {
                                "type": "some",
                                "options": {
                                    "items_per_page": 10,
                                    "offset": 0
                                }
                            },
                            "fields": {
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "m/d/y"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_summary": {
                                    "id": "field_summary",
                                    "table": "node__field_summary",
                                    "field": "field_summary",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 192,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": true,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "basic_string",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_news_thumb": {
                                    "id": "field_news_thumb",
                                    "table": "node__field_news_thumb",
                                    "field": "field_news_thumb",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image",
                                    "settings": {
                                        "image_style": "",
                                        "image_link": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "nothing": {
                                    "id": "nothing",
                                    "table": "views",
                                    "field": "nothing",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": true,
                                        "text": "{{ field_news_thumb }}<br/>\n<h2>{{ title }}</h2>\n<p> {{ field_summary }} </p>\n<p> {{ field_news_class }} - {{ field_published_date }}</p>",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": false,
                                    "plugin_id": "custom"
                                }
                            },
                            "block_description": "Basin Today",
                            "allow": {
                                "items_per_page": true
                            },
                            "display_description": ""
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    },
                    "page_1": {
                        "display_plugin": "page",
                        "id": "page_1",
                        "display_title": "Page-Basin Today",
                        "position": 1,
                        "display_options": {
                            "display_extenders": [],
                            "path": "basin-today",
                            "defaults": {
                                "filters": true,
                                "filter_groups": true
                            },
                            "exposed_block": true,
                            "display_description": ""
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url",
                                "url.query_args",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    },
                    "rest_export_1": {
                        "display_plugin": "rest_export",
                        "id": "rest_export_1",
                        "display_title": "Rest-Basin Today",
                        "position": 2,
                        "display_options": {
                            "display_extenders": [],
                            "style": {
                                "type": "serializer",
                                "options": {
                                    "formats": {
                                        "json": "json"
                                    }
                                }
                            },
                            "defaults": {
                                "style": false,
                                "row": false,
                                "pager": false,
                                "fields": false,
                                "filters": false,
                                "filter_groups": false
                            },
                            "row": {
                                "type": "data_field",
                                "options": {
                                    "field_options": {
                                        "title": {
                                            "alias": "",
                                            "raw_output": true
                                        },
                                        "field_summary": {
                                            "alias": "summary",
                                            "raw_output": true
                                        },
                                        "field_published_date": {
                                            "alias": "publishDate",
                                            "raw_output": false
                                        },
                                        "path": {
                                            "alias": "url",
                                            "raw_output": false
                                        },
                                        "field_news_thumb": {
                                            "alias": "thumbImg",
                                            "raw_output": false
                                        },
                                        "promote": {
                                            "alias": "",
                                            "raw_output": false
                                        },
                                        "field_news_top_image": {
                                            "alias": "",
                                            "raw_output": false
                                        },
                                        "field_basin_today_thumb": {
                                            "alias": "btThumb",
                                            "raw_output": false
                                        },
                                        "field_byline": {
                                            "alias": "byline",
                                            "raw_output": true
                                        }
                                    }
                                }
                            },
                            "pager": {
                                "type": "some",
                                "options": {
                                    "items_per_page": 22,
                                    "offset": 0
                                }
                            },
                            "path": "rest-basin-today",
                            "display_description": "Sends list of latest news articles",
                            "fields": {
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_summary": {
                                    "id": "field_summary",
                                    "table": "node__field_summary",
                                    "field": "field_summary",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 100,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": true,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "basic_string",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "F j"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "path": {
                                    "id": "path",
                                    "table": "node",
                                    "field": "path",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "absolute": false,
                                    "entity_type": "node",
                                    "plugin_id": "node_path"
                                },
                                "field_news_thumb": {
                                    "id": "field_news_thumb",
                                    "table": "node__field_news_thumb",
                                    "field": "field_news_thumb",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image",
                                    "settings": {
                                        "image_style": "",
                                        "image_link": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "promote": {
                                    "id": "promote",
                                    "table": "node_field_data",
                                    "field": "promote",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": false,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "boolean",
                                    "settings": {
                                        "format": "true-false",
                                        "format_custom_true": "",
                                        "format_custom_false": ""
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "entity_type": "node",
                                    "entity_field": "promote",
                                    "plugin_id": "field"
                                },
                                "field_news_top_image": {
                                    "id": "field_news_top_image",
                                    "table": "node__field_news_top_image",
                                    "field": "field_news_top_image",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image_url",
                                    "settings": {
                                        "image_style": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_basin_today_thumb": {
                                    "id": "field_basin_today_thumb",
                                    "table": "node__field_basin_today_thumb",
                                    "field": "field_basin_today_thumb",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image",
                                    "settings": {
                                        "image_style": "",
                                        "image_link": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_byline": {
                                    "id": "field_byline",
                                    "table": "node__field_byline",
                                    "field": "field_byline",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": false
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "sticky": {
                                    "id": "sticky",
                                    "table": "node_field_data",
                                    "field": "sticky",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "boolean",
                                    "settings": {
                                        "format": "true-false",
                                        "format_custom_true": "",
                                        "format_custom_false": ""
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "entity_type": "node",
                                    "entity_field": "sticky",
                                    "plugin_id": "field"
                                }
                            },
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "value": {
                                        "news_page": "news_page"
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                },
                                "field_publish_to_sites_target_id": {
                                    "id": "field_publish_to_sites_target_id",
                                    "table": "node__field_publish_to_sites",
                                    "field": "field_publish_to_sites_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "336": 336
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_shared_domains",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_news_class_target_id": {
                                    "id": "field_news_class_target_id",
                                    "table": "node__field_news_class",
                                    "field": "field_news_class_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "356": 356
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_class",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": ">",
                                    "value": {
                                        "min": "",
                                        "max": "",
                                        "value": "-300 days",
                                        "type": "offset"
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        }
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "plugin_id": "datetime"
                                }
                            },
                            "filter_groups": {
                                "operator": "AND",
                                "groups": {
                                    "1": "AND"
                                }
                            }
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "request_format",
                                "url",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_basin_today_thumb",
                                "config:field.storage.node.field_byline",
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_news_top_image",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/view/view/e3f56a2f-32bd-492f-9313-5477795c4a1c"
            }
        }
    ]
}


export const viewData = [
    {
        "title": "A look into the development of the financial forecast",
        "summary": "This year\u2019s Basin Electric financial forecast, approved by directors at their August meeting, was put together a little differently than in the past.",
        "publishDate": "October 31",
        "url": "\/news-center\/publications\/basin-today\/look-development-financial-forecast",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0910-financial-forecast.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0910-financial-forecast.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022line graphs\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tracie Bettenhausen",
        "sticky": "False"
    },
    {
        "title": "Meet me in Montana",
        "summary": "These members are situated in central and south-central Montana. Meet some of the people who call Basin Electric\u0027s newest co-ops home.",
        "publishDate": "October 31",
        "url": "\/news-center\/publications\/basin-today\/meet-me-montana",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0910-mt-members.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0910-mt-members.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022John Hamilton\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tracie Bettenhausen",
        "sticky": "False"
    },
    {
        "title": "Mountain West Transmission Group seeks membership into SPP RTO",
        "summary": "Various studies conducted during the past four years show that collectively there\u2019s an economic benefit of an RTO for all participants, according to Mike Risan, senior vice president of Transmission.",
        "publishDate": "October 31",
        "url": "\/news-center\/publications\/basin-today\/mountain-west-transmission-group-seeks-membership-spp-rto",
        "thumbImg": "",
        "promote": "True",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0910-mwtg.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0910-mwtg.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Transmission line\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tammy Langerud",
        "sticky": "True"
    },
    {
        "title": "Carbon solutions going underground",
        "summary": "Basin Electric is currently a partner in one of only three Phase II CarbonSAFE feasibility projects underway in the U.S.",
        "publishDate": "October 31",
        "url": "\/news-center\/publications\/basin-today\/carbon-solutions-going-underground",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0910-carbonsafe.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022CarbonSAFE logo treatment\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Chris Gessele",
        "sticky": "False"
    },
    {
        "title": "A day in the life of Senior Contract Administrator Mike Seefeld",
        "summary": "Mike Seefeld and his team have written more than 100 material and contracted services contracts, as well as all the purchase orders for every nut, bolt, and steel beam that makes up the buildings. ",
        "publishDate": "October 31",
        "url": "\/news-center\/publications\/basin-today\/day-life-senior-contract-administrator-mike-seefeld",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0910-day-in-the-life-mike-seefeld.jpg\u0022 width=\u0022350\u0022 height=\u0022180\u0022 alt=\u0022Mike Seefeld\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Angela Magstadt",
        "sticky": "False"
    },
    {
        "title": "Industry leaders meet with EPA Administrator to discuss regulations",
        "summary": "The meeting provided an opportunity for utility leaders to share their thoughts about the current regulatory environment, and while Pruitt never specifically mentioned the Clean Power Plan, that was the main topic of discussion.",
        "publishDate": "August 1",
        "url": "\/news-center\/news-briefs\/industry-leaders-meet-epa-administrator-discuss-regulations",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0708-industry-leaders-meet-with-epa-administrator-to-discuss-regulations.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0708-industry-leaders-meet-with-epa-administrator-to-discuss-regulations.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022gears\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Angela Magstadt",
        "sticky": "False"
    },
    {
        "title": "Construction zone",
        "summary": "In 2015, Basin Electric\u2019s board of directors approved an addition to the Headquarters building in Bismarck to support the future of the cooperative workforce.",
        "publishDate": "August 1",
        "url": "\/news-center\/publications\/basin-today\/construction-zone",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0708-Construction-zone.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0708-construction-zone.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Basin Electric Headquarters\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tammy Langerud",
        "sticky": "False"
    },
    {
        "title": "Taking care of the land",
        "summary": "Glenharold Mine has been habitat for bees since 2004.",
        "publishDate": "August 1",
        "url": "\/News-Center\/Publications\/Basin-Today\/taking-care-land",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0708-taking-care-of-the-land.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0708-taking-care-of-the-land.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022bees\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tracie Bettenhausen",
        "sticky": "False"
    },
    {
        "title": "Giving the gift of life",
        "summary": "Kayla McCloud says she would tell anyone to donate. ",
        "publishDate": "August 1",
        "url": "\/News-Center\/Publications\/Basin-Today\/giving-gift-life",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0708-giving-the-gift-of-life.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0708-giving-the-gift-of-life.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Kayla McCloud\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Andrea Blowers",
        "sticky": "False"
    },
    {
        "title": "A day in the life of Performance Engineer Josh Raynes",
        "summary": "\u201cThe goal is to produce power at a reduced cost, which is then passed down to the member-owners at the end of the line,\u201d says Josh Raynes.",
        "publishDate": "August 1",
        "url": "\/News-Center\/Publications\/Basin-Today\/day-life-performance-engineer-josh-raynes",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0708-day-in-the-life-performance-engineer-Josh-Raynes.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0708-day-in-the-life-performance-engineer-Josh-Raynes.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Josh Raynes\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Chris Gessele",
        "sticky": "False"
    },
    {
        "title": "Power to keep the electric bill low",
        "summary": "Turning off lights when no one is in the room, or turning off the TV when no one\u2019s watching it helps reduce the amount of energy a household uses. The time of day energy is used also impacts the electricity bill.",
        "publishDate": "July 3",
        "url": "\/News-Center\/Publications\/Basin-Today\/power-keep-electric-bill-low",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/Demand-%20web%20page%201122x400.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0506-demand-management.jpg\u0022 width=\u0022350\u0022 height=\u0022201\u0022 alt=\u0022local cooperative\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tammy Langerud",
        "sticky": "False"
    },
    {
        "title": "Survey results show improving safety culture at Basin Electric",
        "summary": "Safety has always been a part of the discussion at Basin Electric, and the Our Power, My Safety (OPMS) process has rocketed the co-op\u2019s safety focus to new heights.",
        "publishDate": "June 23",
        "url": "\/News-Center\/Publications\/Basin-Today\/survey-results-show-improving-safety-culture-basin-electric",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0506-OPMS-Survey.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0506-opms-survey.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Employees\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Chris Gessele",
        "sticky": "False"
    },
    {
        "title": "Employees in service",
        "summary": "It\u2019s Basin Electric\u2019s commitment that encourages service beyond providing electricity.",
        "publishDate": "June 23",
        "url": "\/News-Center\/Publications\/Basin-Today\/employees-service",
        "thumbImg": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/news-thumbs\/2017-0506-employees-in-service.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Great American Bike Race\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "promote": "False",
        "field_news_top_image": "",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0506-employees-in-service_0.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Great American Bike Race\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tammy Langerud",
        "sticky": "False"
    },
    {
        "title": "A day in the life of Project Coordinations Representative Kristie Ching",
        "summary": "Kristie Ching is a project coordinations representative, tasked with rolling out Basin Electric\u2019s Empower Youth Program, a youth leadership program originally developed and exclusively run by Basin Electric Class C member Sioux Valley Energy.",
        "publishDate": "June 23",
        "url": "\/News-Center\/Publications\/Basin-Today\/day-life-project-coordinations-representative-kristie-ching",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0506-Kristie-Ching-Day-In-the-Life.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0506-kristie-ching-day-in-the-life.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Kristie Ching\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Angela Magstadt",
        "sticky": "False"
    },
    {
        "title": "Sakakawea Medical Center: A modern facility made possible with co-op support",
        "summary": "More than 300 individual donors contributed to the $3 million capital campaign to get the project going.",
        "publishDate": "June 23",
        "url": "\/News-Center\/Publications\/Basin-Today\/sakakawea-medical-center-modern-facility-made-possible-co-op",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0506-Sakakawea-Medical-Center.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0506-sakakawea-medical-center.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Sakakawea Medical Center\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tracie Bettenhausen",
        "sticky": "False"
    },
    {
        "title": "Fostering growth and creating connections",
        "summary": "Basin Electric\u0027s retirement projections are presenting opportunities for the next generation.",
        "publishDate": "April 19",
        "url": "\/News-Center\/Publications\/Basin-Today\/fostering-growth-and-creating-connections",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0304-Building-Co-op-Connections.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0304-building-co-op-connections.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Building Co-op Connections\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tammy Langerud",
        "sticky": "False"
    },
    {
        "title": "A day in the life of Director Troy Presser",
        "summary": "Troy Presser has wasted no time making a positive impact since being elected to the Basin Electric board in 2015.",
        "publishDate": "April 19",
        "url": "\/News-Center\/Publications\/Basin-Today\/day-life-director-troy-presser",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0304-Day-in-the-Life-Troy-Presser.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0304-day-in-the-life-troy-presser.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Troy Presser\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Chris Gessele",
        "sticky": "False"
    },
    {
        "title": "Hearts, hope, and hair",
        "summary": "Brave the Shave 2017 events and fundraising.",
        "publishDate": "April 19",
        "url": "\/News-Center\/Publications\/Basin-Today\/hearts-hope-and-hair",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0304-Brave-the-Shave_0.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0304-brave-the-shave.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Brave the Shave\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Angela Magstadt",
        "sticky": "False"
    },
    {
        "title": "Be the light March-April 2017",
        "summary": "How Basin Electric employees shine in their work.",
        "publishDate": "April 19",
        "url": "\/news-center\/publications\/basin-today\/be-light-march-april-2017",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0304-Be-the-Light.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0304-be-the-light.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022Joe Neumiller\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tracie Bettenhausen",
        "sticky": "False"
    },
    {
        "title": "Electricity and cooperatives",
        "summary": "How the electric cooperative movement began and where it\u0027s going.",
        "publishDate": "April 19",
        "url": "\/News-Center\/Publications\/Basin-Today\/electricity-and-cooperatives",
        "thumbImg": "",
        "promote": "False",
        "field_news_top_image": "\/sites\/CMS\/files\/images\/news-features\/2017-0304-electricing-and-co-ops.jpg",
        "btThumb": "  \u003Cimg src=\u0022\/sites\/CMS\/files\/images\/basin-today\/2017-0304-electricity-and-co-ops.jpg\u0022 width=\u0022350\u0022 height=\u0022200\u0022 alt=\u0022George Anderson\u0022 typeof=\u0022foaf:Image\u0022 \/\u003E\n\n",
        "byline": "Tammy Langerud",
        "sticky": "False"
    }
]