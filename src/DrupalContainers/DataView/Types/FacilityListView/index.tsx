import * as React from "react"
import { Table } from "antd"

export const FacilityListView: React.SFC<IProps> = ({ data = [] }) => (
    <div id="facilities_list_wrapper" className="facility-list-view dataTables_wrapper no-footer">
        {data.length > 0 &&
            <Table
                dataSource={getData(data)}
                columns={columns}
                pagination={false}
            />
        }
    </div>
)

FacilityListView.displayName = "FacilityListView"

export const columns: any = [
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: (a: any, b: any) => a.name.localeCompare(b.name),
        onFilter: (value: any, record: any) => record.name.indexOf(value) === 0,
        render: (text: string, record: IMLViewData) => {
            return (
                <a href={record.path}>{text}</a>
            )
        }
    },
    {
        title: 'State',
        dataIndex: 'state',
        key: 'state',
        sorter: (a: any, b: any) => {
            const first = a.state || ""
            const second = b.state || ""
            return first.localeCompare(second)
        },
        onFilter: (value: any, record: any) => record.state.indexOf(value) === 0,
    },
    {
        title: 'Type',
        dataIndex: 'type',
        key: 'type',
        sorter: (a: any, b: any) => a.type.localeCompare(b.type),
        onFilter: (value: any, record: any) => record.type.indexOf(value) === 0,
    },
    {
        title: 'Fuel',
        dataIndex: 'fuel',
        key: 'fuel',
        sorter: (a: any, b: any) => a.fuel.localeCompare(b.fuel),
        onFilter: (value: any, record: any) => record.fuel.indexOf(value) === 0,
    },
    {
        title: 'Capacity',
        dataIndex: 'capacity',
        key: 'capacity',
        sorter: (a: any, b: any) => a.capacity.localeCompare(b.capacity),
        onFilter: (value: any, record: any) => record.capacity.indexOf(value) === 0,
    },
]

export const getData = (data: IMLViewData[]) => {
    const x = data
        .map((d, i) => ({
            key: i,
            name: d.name,
            state: d.state,
            type: d.class,
            fuel: d.type,
            capacity: d.capacity,
            path: d.path
        }))
    return x
}

export default FacilityListView

export interface IProps {
    data?: IMLViewData[]
}

export interface IMLViewData {
    key?: number,
    name: string,
    state: string,
    class: string,
    type: string,
    capacity: string,
    path: string,
}