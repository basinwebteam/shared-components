// tslint:disable:max-line-length

export const pageData = {
    "data": {
        "type": "node--basin_electric_basic_page",
        "id": "73c4d43d-cb8a-470f-9680-74a59dc02ae1",
        "attributes": {
            "nid": 221,
            "uuid": "73c4d43d-cb8a-470f-9680-74a59dc02ae1",
            "vid": 5966,
            "langcode": "en",
            "status": true,
            "title": "Facilities",
            "created": 1499454764,
            "changed": 1501872712,
            "promote": false,
            "sticky": false,
            "revision_timestamp": 1501872712,
            "revision_log": null,
            "revision_translation_affected": true,
            "default_langcode": true,
            "path": {
                "alias": "/facilities",
                "pid": 181
            },
            "field_meta_tags": "a:0:{}",
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c1953c9b-859c-42d1-a2ae-a74572456c0c"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1/revision_uid"
                }
            },
            "menu_link": {
                "data": null
            },
            "moderation_state": {
                "data": null
            },
            "field_aside_content": {
                "data": []
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--image_banner",
                        "id": "d2a304bc-8677-4cbf-ac61-898b702224a2",
                        "meta": {
                            "target_revision_id": "11151"
                        }
                    },
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "ea411d8b-a49a-41e4-b576-3a066e42dd87",
                        "meta": {
                            "target_revision_id": "11156"
                        }
                    },
                    {
                        "type": "paragraph--data_view",
                        "id": "6f7cf0f1-be0e-4e60-9938-2f833bfec23c",
                        "meta": {
                            "target_revision_id": "11161"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1/field_content"
                }
            },
            "field_related_videos": {
                "data": null
            },
            "field_web_layout": {
                "data": null
            },
            "scheduled_update": {
                "data": []
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/73c4d43d-cb8a-470f-9680-74a59dc02ae1?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_content.field_board%2Cfield_content.field_board.field_content%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--image_banner",
            "id": "d2a304bc-8677-4cbf-ac61-898b702224a2",
            "attributes": {
                "id": 876,
                "uuid": "d2a304bc-8677-4cbf-ac61-898b702224a2",
                "revision_id": 11151,
                "langcode": "en",
                "status": true,
                "created": 1499454773,
                "parent_id": "221",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_caption": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "185ce104-3bbe-4c90-bda0-81866f8e437d"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/d2a304bc-8677-4cbf-ac61-898b702224a2/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_banner/d2a304bc-8677-4cbf-ac61-898b702224a2/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/d2a304bc-8677-4cbf-ac61-898b702224a2/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_banner/d2a304bc-8677-4cbf-ac61-898b702224a2/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/d2a304bc-8677-4cbf-ac61-898b702224a2/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_banner/d2a304bc-8677-4cbf-ac61-898b702224a2/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "199f4ace-58de-4ca7-8c1a-bf6d18a95b4c",
                        "meta": {
                            "alt": "Facilities Image",
                            "title": "",
                            "width": "1122",
                            "height": "225"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/d2a304bc-8677-4cbf-ac61-898b702224a2/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_banner/d2a304bc-8677-4cbf-ac61-898b702224a2/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/d2a304bc-8677-4cbf-ac61-898b702224a2"
            }
        },
        {
            "type": "paragraph--basic_paragraph",
            "id": "ea411d8b-a49a-41e4-b576-3a066e42dd87",
            "attributes": {
                "id": 881,
                "uuid": "ea411d8b-a49a-41e4-b576-3a066e42dd87",
                "revision_id": 11156,
                "langcode": "en",
                "status": true,
                "created": 1499454795,
                "parent_id": "221",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_body": {
                    "value": "<p>Whether we're running on coal, wind, natural gas, hydro, nuclear or oil, or ramping up our generation to fulfill peak demand, you can be sure we've got the power for our members.</p>\r\n",
                    "format": "rich_text"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/ea411d8b-a49a-41e4-b576-3a066e42dd87/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/ea411d8b-a49a-41e4-b576-3a066e42dd87/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/ea411d8b-a49a-41e4-b576-3a066e42dd87/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/ea411d8b-a49a-41e4-b576-3a066e42dd87/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/ea411d8b-a49a-41e4-b576-3a066e42dd87/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/ea411d8b-a49a-41e4-b576-3a066e42dd87/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/ea411d8b-a49a-41e4-b576-3a066e42dd87"
            }
        },
        {
            "type": "paragraph--data_view",
            "id": "6f7cf0f1-be0e-4e60-9938-2f833bfec23c",
            "attributes": {
                "id": 5871,
                "uuid": "6f7cf0f1-be0e-4e60-9938-2f833bfec23c",
                "revision_id": 11161,
                "langcode": "en",
                "status": true,
                "created": 1501872684,
                "parent_id": "221",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": "Facility List"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "0b02c669-1e41-447a-bf90-b2fa878b58cf"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/6f7cf0f1-be0e-4e60-9938-2f833bfec23c/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/6f7cf0f1-be0e-4e60-9938-2f833bfec23c/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/6f7cf0f1-be0e-4e60-9938-2f833bfec23c/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/6f7cf0f1-be0e-4e60-9938-2f833bfec23c/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/6f7cf0f1-be0e-4e60-9938-2f833bfec23c/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/6f7cf0f1-be0e-4e60-9938-2f833bfec23c/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_view": {
                    "data": {
                        "type": "view--view",
                        "id": "b1d64664-c044-49cb-91e2-baf7aa795e7e"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/6f7cf0f1-be0e-4e60-9938-2f833bfec23c/relationships/field_view",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/6f7cf0f1-be0e-4e60-9938-2f833bfec23c/field_view"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/6f7cf0f1-be0e-4e60-9938-2f833bfec23c"
            }
        },
        {
            "type": "file--file",
            "id": "199f4ace-58de-4ca7-8c1a-bf6d18a95b4c",
            "attributes": {
                "fid": 371,
                "uuid": "199f4ace-58de-4ca7-8c1a-bf6d18a95b4c",
                "langcode": "en",
                "filename": "DFS-sky-1122px-Impact.jpg",
                "uri": "public://images/DFS-sky-1122px-Impact.jpg",
                "filemime": "image/jpeg",
                "filesize": 25948,
                "status": true,
                "created": 1499454780,
                "changed": 1499455038,
                "url": "/sites/CMS/files/images/DFS-sky-1122px-Impact.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/199f4ace-58de-4ca7-8c1a-bf6d18a95b4c/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/199f4ace-58de-4ca7-8c1a-bf6d18a95b4c/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/199f4ace-58de-4ca7-8c1a-bf6d18a95b4c"
            }
        },
        {
            "type": "view--view",
            "id": "b1d64664-c044-49cb-91e2-baf7aa795e7e",
            "attributes": {
                "uuid": "b1d64664-c044-49cb-91e2-baf7aa795e7e",
                "langcode": "en",
                "status": true,
                "dependencies": {
                    "config": [
                        "field.storage.node.field_facility_capacity",
                        "field.storage.node.field_facility_class",
                        "field.storage.node.field_fuel_type",
                        "field.storage.node.field_physical_location_city",
                        "field.storage.node.field_physical_location_state",
                        "node.type.facility"
                    ],
                    "module": [
                        "node",
                        "options",
                        "rest",
                        "serialization",
                        "user"
                    ]
                },
                "id": "facility_list",
                "label": "Facility List",
                "module": "views",
                "description": "Listing of the facility assets and their key information",
                "tag": "",
                "base_table": "node_field_data",
                "base_field": "nid",
                "core": "8.x",
                "display": {
                    "default": {
                        "display_plugin": "default",
                        "id": "default",
                        "display_title": "Master",
                        "position": 0,
                        "display_options": {
                            "access": {
                                "type": "perm",
                                "options": {
                                    "perm": "access content"
                                }
                            },
                            "cache": {
                                "type": "tag",
                                "options": []
                            },
                            "query": {
                                "type": "views_query",
                                "options": {
                                    "disable_sql_rewrite": false,
                                    "distinct": false,
                                    "replica": false,
                                    "query_comment": "",
                                    "query_tags": []
                                }
                            },
                            "exposed_form": {
                                "type": "basic",
                                "options": {
                                    "submit_button": "Apply",
                                    "reset_button": false,
                                    "reset_button_label": "Reset",
                                    "exposed_sorts_label": "Sort by",
                                    "expose_sort_order": true,
                                    "sort_asc_label": "Asc",
                                    "sort_desc_label": "Desc"
                                }
                            },
                            "pager": {
                                "type": "some",
                                "options": {
                                    "items_per_page": 25,
                                    "offset": 0
                                }
                            },
                            "style": {
                                "type": "table",
                                "options": {
                                    "grouping": [],
                                    "row_class": "",
                                    "default_row_class": true,
                                    "override": true,
                                    "sticky": false,
                                    "caption": "List of Facilities",
                                    "summary": "",
                                    "description": "",
                                    "columns": {
                                        "title": "title",
                                        "field_facility_class": "field_facility_class",
                                        "field_fuel_type": "field_fuel_type",
                                        "field_facility_output": "field_facility_output",
                                        "field_physical_location_state": "field_physical_location_state",
                                        "field_physical_location_city": "field_physical_location_city"
                                    },
                                    "info": {
                                        "title": {
                                            "sortable": false,
                                            "default_sort_order": "asc",
                                            "align": "",
                                            "separator": "",
                                            "empty_column": false,
                                            "responsive": ""
                                        },
                                        "field_facility_class": {
                                            "sortable": true,
                                            "default_sort_order": "asc",
                                            "align": "",
                                            "separator": "",
                                            "empty_column": false,
                                            "responsive": ""
                                        },
                                        "field_fuel_type": {
                                            "sortable": true,
                                            "default_sort_order": "asc",
                                            "align": "",
                                            "separator": "",
                                            "empty_column": false,
                                            "responsive": ""
                                        },
                                        "field_facility_output": {
                                            "sortable": true,
                                            "default_sort_order": "asc",
                                            "align": "",
                                            "separator": "",
                                            "empty_column": false,
                                            "responsive": ""
                                        },
                                        "field_physical_location_state": {
                                            "sortable": true,
                                            "default_sort_order": "asc",
                                            "align": "",
                                            "separator": "",
                                            "empty_column": false,
                                            "responsive": ""
                                        },
                                        "field_physical_location_city": {
                                            "sortable": true,
                                            "default_sort_order": "asc",
                                            "align": "",
                                            "separator": "",
                                            "empty_column": false,
                                            "responsive": ""
                                        }
                                    },
                                    "default": "field_facility_output",
                                    "empty_table": false
                                }
                            },
                            "row": {
                                "type": "fields"
                            },
                            "fields": {
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "Name",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": false,
                                        "ellipsis": false,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "entity_type": "node",
                                    "entity_field": "title",
                                    "plugin_id": "field"
                                },
                                "field_facility_class": {
                                    "id": "field_facility_class",
                                    "table": "node__field_facility_class",
                                    "field": "field_facility_class",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "Type",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "entity_reference_label",
                                    "settings": {
                                        "link": false
                                    },
                                    "group_column": "target_id",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_fuel_type": {
                                    "id": "field_fuel_type",
                                    "table": "node__field_fuel_type",
                                    "field": "field_fuel_type",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "Fuel",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "entity_reference_label",
                                    "settings": {
                                        "link": false
                                    },
                                    "group_column": "target_id",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_facility_capacity": {
                                    "id": "field_facility_capacity",
                                    "table": "node__field_facility_capacity",
                                    "field": "field_facility_capacity",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "Capacity",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "number_decimal",
                                    "settings": {
                                        "thousand_separator": ",",
                                        "prefix_suffix": true,
                                        "decimal_separator": ".",
                                        "scale": 0
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_physical_location_state": {
                                    "id": "field_physical_location_state",
                                    "table": "node__field_physical_location_state",
                                    "field": "field_physical_location_state",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "State",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "list_default",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_physical_location_city": {
                                    "id": "field_physical_location_city",
                                    "table": "node__field_physical_location_city",
                                    "field": "field_physical_location_city",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "City",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": false
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                }
                            },
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "value": {
                                        "facility": "facility"
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                }
                            },
                            "sorts": {
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "order": "ASC",
                                    "entity_type": "node",
                                    "entity_field": "title",
                                    "plugin_id": "standard",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "exposed": false,
                                    "expose": {
                                        "label": ""
                                    }
                                }
                            },
                            "title": "Block-Facility List",
                            "header": [],
                            "footer": [],
                            "empty": [],
                            "relationships": [],
                            "arguments": [],
                            "display_extenders": []
                        },
                        "cache_metadata": {
                            "max-age": 0,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url.query_args",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_facility_capacity",
                                "config:field.storage.node.field_facility_class",
                                "config:field.storage.node.field_fuel_type",
                                "config:field.storage.node.field_physical_location_city",
                                "config:field.storage.node.field_physical_location_state"
                            ]
                        }
                    },
                    "block_1": {
                        "display_plugin": "block",
                        "id": "block_1",
                        "display_title": "Block-Facility List",
                        "position": 1,
                        "display_options": {
                            "display_extenders": [],
                            "display_description": ""
                        },
                        "cache_metadata": {
                            "max-age": 0,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url.query_args",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_facility_capacity",
                                "config:field.storage.node.field_facility_class",
                                "config:field.storage.node.field_fuel_type",
                                "config:field.storage.node.field_physical_location_city",
                                "config:field.storage.node.field_physical_location_state"
                            ]
                        }
                    },
                    "rest_export_1": {
                        "display_plugin": "rest_export",
                        "id": "rest_export_1",
                        "display_title": "REST-Facility List",
                        "position": 2,
                        "display_options": {
                            "display_extenders": [],
                            "path": "rest-facility-list",
                            "style": {
                                "type": "serializer",
                                "options": {
                                    "formats": {
                                        "json": "json"
                                    }
                                }
                            },
                            "defaults": {
                                "style": false,
                                "row": false,
                                "fields": false
                            },
                            "row": {
                                "type": "data_field",
                                "options": {
                                    "field_options": {
                                        "title": {
                                            "alias": "name",
                                            "raw_output": true
                                        },
                                        "path": {
                                            "alias": "",
                                            "raw_output": false
                                        },
                                        "field_facility_class": {
                                            "alias": "class",
                                            "raw_output": false
                                        },
                                        "field_fuel_type": {
                                            "alias": "type",
                                            "raw_output": false
                                        },
                                        "field_facility_capacity": {
                                            "alias": "capacity",
                                            "raw_output": true
                                        },
                                        "field_physical_location_state": {
                                            "alias": "state",
                                            "raw_output": true
                                        },
                                        "field_physical_location_city": {
                                            "alias": "city",
                                            "raw_output": true
                                        }
                                    }
                                }
                            },
                            "pager": {
                                "type": "some",
                                "options": {
                                    "items_per_page": 0,
                                    "offset": 0
                                }
                            },
                            "display_description": "",
                            "fields": {
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "Title",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": false,
                                        "ellipsis": false,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": true,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "entity_type": "node",
                                    "entity_field": "title",
                                    "plugin_id": "field"
                                },
                                "path": {
                                    "id": "path",
                                    "table": "node",
                                    "field": "path",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "absolute": false,
                                    "entity_type": "node",
                                    "plugin_id": "node_path"
                                },
                                "field_facility_class": {
                                    "id": "field_facility_class",
                                    "table": "node__field_facility_class",
                                    "field": "field_facility_class",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "entity_reference_label",
                                    "settings": {
                                        "link": false
                                    },
                                    "group_column": "target_id",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_fuel_type": {
                                    "id": "field_fuel_type",
                                    "table": "node__field_fuel_type",
                                    "field": "field_fuel_type",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "entity_reference_label",
                                    "settings": {
                                        "link": false
                                    },
                                    "group_column": "target_id",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_facility_capacity": {
                                    "id": "field_facility_capacity",
                                    "table": "node__field_facility_capacity",
                                    "field": "field_facility_capacity",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "number_decimal",
                                    "settings": {
                                        "thousand_separator": ",",
                                        "prefix_suffix": false,
                                        "decimal_separator": ".",
                                        "scale": 0
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_physical_location_state": {
                                    "id": "field_physical_location_state",
                                    "table": "node__field_physical_location_state",
                                    "field": "field_physical_location_state",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "list_default",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_physical_location_city": {
                                    "id": "field_physical_location_city",
                                    "table": "node__field_physical_location_city",
                                    "field": "field_physical_location_city",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": false
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                }
                            }
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "request_format",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_facility_capacity",
                                "config:field.storage.node.field_facility_class",
                                "config:field.storage.node.field_fuel_type",
                                "config:field.storage.node.field_physical_location_city",
                                "config:field.storage.node.field_physical_location_state"
                            ]
                        }
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/view/view/b1d64664-c044-49cb-91e2-baf7aa795e7e"
            }
        }
    ]
}

export const viewData = [
    {
        "name": "Antelope Valley Station",
        "path": "/Facilities/Antelope-Valley",
        "class": "Baseload",
        "type": "Coal",
        "capacity": "900",
        "state": "ND",
        "city": "Beulah"
    },
    {
        "name": "Crow Lake Wind",
        "path": "/Facilities/Crow-Lake-Wind",
        "class": "Renewable",
        "type": "Wind",
        "capacity": "162",
        "state": "SD",
        "city": "Chamberlain"
    },
    {
        "name": "Culbertson Station",
        "path": "/Facilities/Culbertson",
        "class": "Peaking",
        "type": "Natural Gas",
        "capacity": "95",
        "state": "MT",
        "city": "Culbertson"
    },
    {
        "name": "Deer Creek Station",
        "path": "/Facilities/Deer-Creek",
        "class": "Intermediate",
        "type": "Natural Gas",
        "capacity": "300",
        "state": "SD",
        "city": "Elkton"
    },
    {
        "name": "Dry Fork Station",
        "path": "/Facilities/Dry-Fork",
        "class": "Baseload",
        "type": "Coal",
        "capacity": "385",
        "state": "WY",
        "city": "Gillette"
    },
    {
        "name": "Groton Generation Station",
        "path": "/Facilities/Groton",
        "class": "Peaking",
        "type": "Natural Gas",
        "capacity": "196",
        "state": "SD",
        "city": "Groton"
    },
    {
        "name": "Laramie River Station",
        "path": "/Facilities/Laramie-River",
        "class": "Baseload",
        "type": "Coal",
        "capacity": "1710",
        "state": "WY",
        "city": "Wheatland"
    },
    {
        "name": "Leland Olds Station ",
        "path": "/Facilities/Leland-Olds",
        "class": "Baseload",
        "type": "Coal",
        "capacity": "669",
        "state": "ND",
        "city": "Stanton"
    },
    {
        "name": "Lonesome Creek Station",
        "path": "/Facilities/Lonesome-Creek",
        "class": "Peaking",
        "type": "Natural Gas",
        "capacity": "225",
        "state": "ND",
        "city": "Watford City"
    },
    {
        "name": "Pioneer Generation Station",
        "path": "/Facilities/Pioneer",
        "class": "Peaking",
        "type": "Natural Gas",
        "capacity": "247",
        "state": "ND",
        "city": "Williston"
    },
    {
        "name": "PrairieWinds 1 and Minot Wind",
        "path": "/Facilities/PrairieWinds-1",
        "class": "Renewable",
        "type": "Wind",
        "capacity": "122",
        "state": "ND",
        "city": "Minot"
    },
    {
        "name": "Recovered Energy Generation",
        "path": "/Facilities/Recovered-Energy",
        "class": "Renewable",
        "type": "Waste Heat",
        "capacity": "44",
        "state": false,
        "city": "Multiple Locations"
    },
    {
        "name": "Spirit Mound Station",
        "path": "/Facilities/Spirit-Mound",
        "class": "Peaking",
        "type": "Oil",
        "capacity": "120",
        "state": "SD",
        "city": "Vermillion"
    },
    {
        "name": "Wisdom Generating Station",
        "path": "/Facilities/Wisdom",
        "class": "Peaking",
        "type": "Natural Gas",
        "capacity": "80",
        "state": "IA",
        "city": "Spencer"
    },
    {
        "name": "Wyoming Distributed Generation",
        "path": "/Facilities/WY-Distributed-Gen",
        "class": "Peaking",
        "type": "Natural Gas",
        "capacity": "45",
        "state": "WY",
        "city": "Buffalo, Wright, Gillette, Arvada"
    }
]