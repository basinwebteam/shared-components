import axios from "axios"
import * as moxios from "moxios"
import * as React from "react"
import { shallow, mount } from 'enzyme'
import toJson from "enzyme-to-json"
import * as Page from "../../../../../DrupalContentTypes/PageInterface"

import DataView from "../../../../DataView"
import BasinTodayListView from "../"
import DataViewWrapper from "../../../Wrapper"
import {pageData, viewData} from "./mocks"

describe("DataView - Basin Today List View", () => {
    const SITE_DOMAIN = "https://basinelectric.com"

    // beforeEach(() => {
    //     moxios.install()
    // })

    // afterEach(() => {
    //     moxios.uninstall()
    // })

    it("should render empty Basin Today List View without data sent", () => {
        const element = mount(
            <DataView
                paragraphId="e232bbc8-1cb3-4d71-99cd-70864906c706"
                siteDomain={SITE_DOMAIN}
                includedRelationships={pageData.included}
            />
        )
        const BasinTodayListViewComponent = element.childAt(0).childAt(0).childAt(0).childAt(0)
        
        expect(BasinTodayListViewComponent.name()).toEqual("BasinTodayListView")
        expect(BasinTodayListViewComponent.prop("data").length).toEqual(0)
        expect(BasinTodayListViewComponent.text()).toEqual("")
    })

    it("should render Basin Today List View with props sent", () => {
        const element = shallow(
            <BasinTodayListView data={viewData} />
        )
        expect(element.find(".btoday-card").length).toBeGreaterThan(10)
        expect(toJson(element)).toMatchSnapshot()
    })

})
