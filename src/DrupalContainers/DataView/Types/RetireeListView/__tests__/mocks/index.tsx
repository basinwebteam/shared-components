// tslint:disable:max-line-length
export const pageData = {
    "data": {
        "type": "node--retirees_basic_page",
        "id": "9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8",
        "attributes": {
            "nid": 5516,
            "uuid": "9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8",
            "vid": 19466,
            "langcode": "en",
            "status": true,
            "title": "Throwback Thursday",
            "created": 1512145222,
            "changed": 1513872972,
            "promote": false,
            "sticky": false,
            "revision_timestamp": 1513872972,
            "revision_log": null,
            "revision_translation_affected": true,
            "default_langcode": true,
            "path": {
                "alias": "/throwback-thursday",
                "pid": 4631
            },
            "body": null,
            "field_end_date_time": null,
            "field_published_date": null,
            "field_start_date_time": null,
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c815c6f9-2312-45f7-a509-673ebd695a8e"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/revision_uid"
                }
            },
            "menu_link": {
                "data": null
            },
            "moderation_state": {
                "data": null
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--data_view",
                        "id": "95c92bf9-892a-465e-879a-2012a674d281",
                        "meta": {
                            "target_revision_id": "61951"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/field_content"
                }
            },
            "field_image": {
                "data": null
            },
            "field_page_type": {
                "data": {
                    "type": "taxonomy_term--retiree_page_types",
                    "id": "1c8e5014-b26e-4bd5-ad15-f9d092ed70c3"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/relationships/field_page_type",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/field_page_type"
                }
            },
            "field_web_layout": {
                "data": {
                    "type": "taxonomy_term--web_layouts",
                    "id": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/relationships/field_web_layout",
                    "related": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8/field_web_layout"
                }
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/retirees_basic_page/9c6ace44-9dc1-4ec4-8dae-cfa79814b7a8?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_content.field_board%2Cfield_content.field_board.field_content%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--data_view",
            "id": "95c92bf9-892a-465e-879a-2012a674d281",
            "attributes": {
                "id": 12746,
                "uuid": "95c92bf9-892a-465e-879a-2012a674d281",
                "revision_id": 61951,
                "langcode": "en",
                "status": true,
                "created": 1512145252,
                "parent_id": "5516",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "0b02c669-1e41-447a-bf90-b2fa878b58cf"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/95c92bf9-892a-465e-879a-2012a674d281/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/95c92bf9-892a-465e-879a-2012a674d281/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/95c92bf9-892a-465e-879a-2012a674d281/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/95c92bf9-892a-465e-879a-2012a674d281/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/95c92bf9-892a-465e-879a-2012a674d281/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/95c92bf9-892a-465e-879a-2012a674d281/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_view": {
                    "data": {
                        "type": "view--view",
                        "id": "3b77f0a1-b7eb-4bf0-a766-da6757db4d6c"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/95c92bf9-892a-465e-879a-2012a674d281/relationships/field_view",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/data_view/95c92bf9-892a-465e-879a-2012a674d281/field_view"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/data_view/95c92bf9-892a-465e-879a-2012a674d281"
            }
        },
        {
            "type": "taxonomy_term--web_layouts",
            "id": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb",
            "attributes": {
                "tid": 606,
                "uuid": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb",
                "langcode": "en",
                "name": "Full Page",
                "description": null,
                "weight": 0,
                "changed": 1502982012,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "9481b046-bd2c-4ef8-ba1d-acde28597ffc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb/vid"
                    }
                },
                "parent": {
                    "data": []
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb"
            }
        },
        {
            "type": "view--view",
            "id": "3b77f0a1-b7eb-4bf0-a766-da6757db4d6c",
            "attributes": {
                "uuid": "3b77f0a1-b7eb-4bf0-a766-da6757db4d6c",
                "langcode": "en",
                "status": true,
                "dependencies": {
                    "config": [
                        "field.storage.node.field_news_thumb",
                        "field.storage.node.field_published_date",
                        "field.storage.node.field_summary",
                        "node.type.news_page",
                        "node.type.retirees_basic_page",
                        "taxonomy.vocabulary.news_class",
                        "taxonomy.vocabulary.news_shared_domains",
                        "taxonomy.vocabulary.retiree_page_types"
                    ],
                    "content": [
                        "taxonomy_term:news_class:7945bfdd-7c7c-402f-8249-b3e493a64863",
                        "taxonomy_term:news_shared_domains:213e34f1-0b4a-4770-92f3-959fc1cf2def",
                        "taxonomy_term:retiree_page_types:dafab72b-b4f1-4c97-9e2b-b9f004a19ba3"
                    ],
                    "module": [
                        "datetime",
                        "image",
                        "node",
                        "rest",
                        "serialization",
                        "taxonomy",
                        "user"
                    ]
                },
                "id": "retirees_throwback_list",
                "label": "Retirees Throwback List",
                "module": "views",
                "description": "Lists retirements",
                "tag": "",
                "base_table": "node_field_data",
                "base_field": "nid",
                "core": "8.x",
                "display": {
                    "default": {
                        "display_plugin": "default",
                        "id": "default",
                        "display_title": "Master",
                        "position": 0,
                        "display_options": {
                            "access": {
                                "type": "perm",
                                "options": {
                                    "perm": "access content"
                                }
                            },
                            "cache": {
                                "type": "tag",
                                "options": []
                            },
                            "query": {
                                "type": "views_query",
                                "options": {
                                    "disable_sql_rewrite": false,
                                    "distinct": false,
                                    "replica": false,
                                    "query_comment": "",
                                    "query_tags": []
                                }
                            },
                            "exposed_form": {
                                "type": "basic",
                                "options": {
                                    "submit_button": "Apply",
                                    "reset_button": false,
                                    "reset_button_label": "Reset",
                                    "exposed_sorts_label": "Sort by",
                                    "expose_sort_order": true,
                                    "sort_asc_label": "Asc",
                                    "sort_desc_label": "Desc"
                                }
                            },
                            "pager": {
                                "type": "mini",
                                "options": {
                                    "items_per_page": 15,
                                    "offset": 0,
                                    "id": 0,
                                    "total_pages": null,
                                    "expose": {
                                        "items_per_page": false,
                                        "items_per_page_label": "Items per page",
                                        "items_per_page_options": "5, 10, 25, 50",
                                        "items_per_page_options_all": false,
                                        "items_per_page_options_all_label": "- All -",
                                        "offset": false,
                                        "offset_label": "Offset"
                                    },
                                    "tags": {
                                        "previous": "‹‹",
                                        "next": "››"
                                    }
                                }
                            },
                            "style": {
                                "type": "grid",
                                "options": {
                                    "grouping": [],
                                    "columns": 3,
                                    "automatic_width": true,
                                    "alignment": "horizontal",
                                    "col_class_default": true,
                                    "col_class_custom": "",
                                    "row_class_default": true,
                                    "row_class_custom": ""
                                }
                            },
                            "row": {
                                "type": "fields"
                            },
                            "fields": {
                                "field_news_thumb": {
                                    "id": "field_news_thumb",
                                    "table": "node__field_news_thumb",
                                    "field": "field_news_thumb",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image",
                                    "settings": {
                                        "image_style": "",
                                        "image_link": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_summary": {
                                    "id": "field_summary",
                                    "table": "node__field_summary",
                                    "field": "field_summary",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 100,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": true,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "basic_string",
                                    "settings": [],
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": true,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_custom",
                                    "settings": {
                                        "timezone_override": "",
                                        "date_format": "m/d/y"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "nothing": {
                                    "id": "nothing",
                                    "table": "views",
                                    "field": "nothing",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": true,
                                        "text": "{{ field_news_thumb }}<br />\n<h2>{{ title }}</h2>\n<p>{{ field_summary }}<br/>\n<span style=\"color:#ccc;\"> {{ field_news_class }} - {{ field_published_date }}</span></p>",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": true,
                                    "empty_zero": false,
                                    "hide_alter_empty": false,
                                    "plugin_id": "custom"
                                }
                            },
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "value": {
                                        "news_page": "news_page"
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                },
                                "field_publish_to_sites_target_id": {
                                    "id": "field_publish_to_sites_target_id",
                                    "table": "node__field_publish_to_sites",
                                    "field": "field_publish_to_sites_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "336": 336
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_shared_domains",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_news_class_target_id": {
                                    "id": "field_news_class_target_id",
                                    "table": "node__field_news_class",
                                    "field": "field_news_class_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": {
                                        "356": 356
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "select",
                                    "limit": true,
                                    "vid": "news_class",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "plugin_id": "datetime"
                                }
                            },
                            "sorts": {
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "order": "DESC",
                                    "exposed": false,
                                    "expose": {
                                        "label": ""
                                    },
                                    "granularity": "day",
                                    "plugin_id": "datetime"
                                }
                            },
                            "title": "Throwbacks",
                            "header": [],
                            "footer": [],
                            "empty": [],
                            "relationships": [],
                            "arguments": {
                                "field_published_date_value_full_date": {
                                    "id": "field_published_date_value_full_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value_full_date",
                                    "plugin_id": "datetime_full_date"
                                }
                            },
                            "display_extenders": []
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "url",
                                "url.query_args",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_published_date",
                                "config:field.storage.node.field_summary"
                            ]
                        }
                    },
                    "rest_export_1": {
                        "display_plugin": "rest_export",
                        "id": "rest_export_1",
                        "display_title": "throwbacks",
                        "position": 2,
                        "display_options": {
                            "display_extenders": [],
                            "style": {
                                "type": "serializer",
                                "options": {
                                    "formats": {
                                        "json": "json"
                                    }
                                }
                            },
                            "defaults": {
                                "style": false,
                                "row": false,
                                "pager": false,
                                "fields": false,
                                "filters": false,
                                "filter_groups": false
                            },
                            "row": {
                                "type": "data_field",
                                "options": {
                                    "field_options": {
                                        "title": {
                                            "alias": "",
                                            "raw_output": true
                                        },
                                        "path": {
                                            "alias": "url",
                                            "raw_output": false
                                        },
                                        "field_news_thumb": {
                                            "alias": "thumbImg",
                                            "raw_output": false
                                        },
                                        "promote": {
                                            "alias": "",
                                            "raw_output": false
                                        },
                                        "sticky": {
                                            "alias": "",
                                            "raw_output": false
                                        },
                                        "field_published_date": {
                                            "alias": "publishDate",
                                            "raw_output": false
                                        }
                                    }
                                }
                            },
                            "pager": {
                                "type": "some",
                                "options": {
                                    "items_per_page": 25,
                                    "offset": 0
                                }
                            },
                            "path": "rest-retirees-throwbacks",
                            "display_description": "Sends list of latest Throwback Thrursday articles",
                            "fields": {
                                "title": {
                                    "id": "title",
                                    "table": "node_field_data",
                                    "field": "title",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "string",
                                    "settings": {
                                        "link_to_entity": true
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "path": {
                                    "id": "path",
                                    "table": "node",
                                    "field": "path",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "absolute": false,
                                    "entity_type": "node",
                                    "plugin_id": "node_path"
                                },
                                "field_news_thumb": {
                                    "id": "field_news_thumb",
                                    "table": "node__field_news_thumb",
                                    "field": "field_news_thumb",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "target_id",
                                    "type": "image",
                                    "settings": {
                                        "image_style": "",
                                        "image_link": ""
                                    },
                                    "group_column": "",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                },
                                "promote": {
                                    "id": "promote",
                                    "table": "node_field_data",
                                    "field": "promote",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": false,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "boolean",
                                    "settings": {
                                        "format": "true-false",
                                        "format_custom_true": "",
                                        "format_custom_false": ""
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "entity_type": "node",
                                    "entity_field": "promote",
                                    "plugin_id": "field"
                                },
                                "sticky": {
                                    "id": "sticky",
                                    "table": "node_field_data",
                                    "field": "sticky",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "boolean",
                                    "settings": {
                                        "format": "true-false",
                                        "format_custom_true": "",
                                        "format_custom_false": ""
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "entity_type": "node",
                                    "entity_field": "sticky",
                                    "plugin_id": "field"
                                },
                                "field_published_date": {
                                    "id": "field_published_date",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "label": "",
                                    "exclude": false,
                                    "alter": {
                                        "alter_text": false,
                                        "text": "",
                                        "make_link": false,
                                        "path": "",
                                        "absolute": false,
                                        "external": false,
                                        "replace_spaces": false,
                                        "path_case": "none",
                                        "trim_whitespace": false,
                                        "alt": "",
                                        "rel": "",
                                        "link_class": "",
                                        "prefix": "",
                                        "suffix": "",
                                        "target": "",
                                        "nl2br": false,
                                        "max_length": 0,
                                        "word_boundary": true,
                                        "ellipsis": true,
                                        "more_link": false,
                                        "more_link_text": "",
                                        "more_link_path": "",
                                        "strip_tags": false,
                                        "trim": false,
                                        "preserve_tags": "",
                                        "html": false
                                    },
                                    "element_type": "",
                                    "element_class": "",
                                    "element_label_type": "",
                                    "element_label_class": "",
                                    "element_label_colon": false,
                                    "element_wrapper_type": "",
                                    "element_wrapper_class": "",
                                    "element_default_classes": true,
                                    "empty": "",
                                    "hide_empty": false,
                                    "empty_zero": false,
                                    "hide_alter_empty": true,
                                    "click_sort_column": "value",
                                    "type": "datetime_default",
                                    "settings": {
                                        "timezone_override": "America/Chicago",
                                        "format_type": "medium"
                                    },
                                    "group_column": "value",
                                    "group_columns": [],
                                    "group_rows": true,
                                    "delta_limit": 0,
                                    "delta_offset": 0,
                                    "delta_reversed": false,
                                    "delta_first_last": false,
                                    "multi_type": "separator",
                                    "separator": ", ",
                                    "field_api_classes": false,
                                    "plugin_id": "field"
                                }
                            },
                            "filters": {
                                "status": {
                                    "value": "1",
                                    "table": "node_field_data",
                                    "field": "status",
                                    "plugin_id": "boolean",
                                    "entity_type": "node",
                                    "entity_field": "status",
                                    "id": "status",
                                    "expose": {
                                        "operator": ""
                                    },
                                    "group": 1
                                },
                                "type": {
                                    "id": "type",
                                    "table": "node_field_data",
                                    "field": "type",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "in",
                                    "value": {
                                        "retirees_basic_page": "retirees_basic_page"
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false,
                                        "argument": null
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "entity_type": "node",
                                    "entity_field": "type",
                                    "plugin_id": "bundle"
                                },
                                "field_page_type_target_id": {
                                    "id": "field_page_type_target_id",
                                    "table": "node__field_page_type",
                                    "field": "field_page_type_target_id",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "or",
                                    "value": [
                                        686
                                    ],
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        },
                                        "reduce": false
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "reduce_duplicates": false,
                                    "type": "textfield",
                                    "limit": true,
                                    "vid": "retiree_page_types",
                                    "hierarchy": false,
                                    "error_message": true,
                                    "plugin_id": "taxonomy_index_tid"
                                },
                                "field_published_date_value": {
                                    "id": "field_published_date_value",
                                    "table": "node__field_published_date",
                                    "field": "field_published_date_value",
                                    "relationship": "none",
                                    "group_type": "group",
                                    "admin_label": "",
                                    "operator": "<=",
                                    "value": {
                                        "min": "",
                                        "max": "",
                                        "value": "-120",
                                        "type": "offset"
                                    },
                                    "group": 1,
                                    "exposed": false,
                                    "expose": {
                                        "operator_id": "",
                                        "label": "",
                                        "description": "",
                                        "use_operator": false,
                                        "operator": "",
                                        "identifier": "",
                                        "required": false,
                                        "remember": false,
                                        "multiple": false,
                                        "remember_roles": {
                                            "authenticated": "authenticated"
                                        }
                                    },
                                    "is_grouped": false,
                                    "group_info": {
                                        "label": "",
                                        "description": "",
                                        "identifier": "",
                                        "optional": true,
                                        "widget": "select",
                                        "multiple": false,
                                        "remember": false,
                                        "default_group": "All",
                                        "default_group_multiple": [],
                                        "group_items": []
                                    },
                                    "plugin_id": "datetime"
                                }
                            },
                            "filter_groups": {
                                "operator": "AND",
                                "groups": {
                                    "1": "AND"
                                }
                            }
                        },
                        "cache_metadata": {
                            "max-age": -1,
                            "contexts": [
                                "languages:language_content",
                                "languages:language_interface",
                                "request_format",
                                "url",
                                "user",
                                "user.node_grants:view",
                                "user.permissions"
                            ],
                            "tags": [
                                "config:field.storage.node.field_news_thumb",
                                "config:field.storage.node.field_published_date"
                            ]
                        }
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/view/view/3b77f0a1-b7eb-4bf0-a766-da6757db4d6c"
            }
        }
    ]
}


export const viewData = [
    {
        "title": "Throwback Thursday, December 21",
        "url": "/throwback-thursday-december-21",
        "thumbImg": "",
        "promote": "True",
        "sticky": "False",
        "publishDate": "<time datetime=\"2017-12-22T12:00:00Z\" class=\"datetime\">Fri, 12/22/2017 - 06:00</time>\n"
    },
    {
        "title": "Throwback Thursday, November 16",
        "url": "/throwback-thursday-november-16",
        "thumbImg": "",
        "promote": "True",
        "sticky": "True",
        "publishDate": "<time datetime=\"2017-11-16T12:00:00Z\" class=\"datetime\">Thu, 11/16/2017 - 06:00</time>\n"
    },
    {
        "title": "Throwback Thursday, October 26",
        "url": "/throwback-thursday-october-26",
        "thumbImg": "",
        "promote": "True",
        "sticky": "False",
        "publishDate": "<time datetime=\"2017-10-26T12:00:00Z\" class=\"datetime\">Thu, 10/26/2017 - 07:00</time>\n"
    },
    {
        "title": "Throwback Thursday, October 12",
        "url": "/throwback-thursday-october-12",
        "thumbImg": "",
        "promote": "True",
        "sticky": "False",
        "publishDate": "<time datetime=\"2017-10-12T12:00:00Z\" class=\"datetime\">Thu, 10/12/2017 - 07:00</time>\n"
    },
    {
        "title": "Throwback Thursday, September 28",
        "url": "/throwback-thursday/throwback-thursday-september-28",
        "thumbImg": "",
        "promote": "True",
        "sticky": "False",
        "publishDate": "<time datetime=\"2017-09-28T12:00:00Z\" class=\"datetime\">Thu, 09/28/2017 - 07:00</time>\n"
    },
    {
        "title": "Throw Back Thursday, September 21",
        "url": "/throwback-thursday/throw-back-thursday-september-21",
        "thumbImg": "",
        "promote": "True",
        "sticky": "False",
        "publishDate": "<time datetime=\"2017-09-21T12:00:00Z\" class=\"datetime\">Thu, 09/21/2017 - 07:00</time>\n"
    },
    {
        "title": "Throw Back Thursday, September 14",
        "url": "/throwback-thursday/throw-back-thursday-september-14",
        "thumbImg": "",
        "promote": "True",
        "sticky": "False",
        "publishDate": "<time datetime=\"2017-09-14T12:00:00Z\" class=\"datetime\">Thu, 09/14/2017 - 07:00</time>\n"
    },
    {
        "title": "Throw Back Thursday, September 7",
        "url": "/throw-back-thursday-september-7",
        "thumbImg": "",
        "promote": "True",
        "sticky": "False",
        "publishDate": "<time datetime=\"2017-09-07T12:00:00Z\" class=\"datetime\">Thu, 09/07/2017 - 07:00</time>\n"
    }
]