import * as React from "react"

export const RetireeListView: React.SFC<IProps> = ({data=[], domain = ""}) => (
    <div className="retiree-dataview">
            {data
                .sort(orderByPromoted)
                .map((news, i) => {
                    const image = getImageThumbnail(news.image, domain)
                    return (
                        <div className="retiree-article" key={i}>
                            <h2 className="retiree-article-title">{news.title}</h2>
                            <div className="retiree-article-dateline" dangerouslySetInnerHTML={{__html: news.publishDate}} />
                            <div className="retiree-article-body" dangerouslySetInnerHTML={{__html: news.content}} />
                            {image!=="" && <div className="retiree-article-img" dangerouslySetInnerHTML={{__html: image}} />}
                        </div>
                    )
                })
            }
    </div>
)

RetireeListView.displayName = "RetireeListView"

export const orderByPromoted = (a: IMLViewData, b: IMLViewData) => {
    if(a.promote === "True"){
        return -1
    }
    return 1
}

// Useful for images that aren't hosted on the local site
export const getImageThumbnail = (image: string="", domain = "") => domain !== "" ? image.replace('src="/', `src="${domain}/`) : image

export default RetireeListView

export interface IProps {
    data?: IMLViewData[],
    domain?: string, // Use when images aren't hosted on local site
}
export interface IMLViewData {
    title: string,
    url: string,
    publishDate: string,
    image: string,
    content:string,
    sticky: BooleanString | string,
    promote?: BooleanString | string,
}

export type BooleanString = "False" | "True"
