import * as React from "react"
import CalendarFeed from "../../../../components/CalendarFeed"
import {IEventProps} from "../../../../components/CalendarFeed/Event"

export const RetireeEventView: React.SFC<IProps> = ({data=[]}) => (
    <CalendarFeed events={data} className="retiree-events" />
)

export default RetireeEventView

export interface IProps {
    data?: IEventProps[],
}
