import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import DataView from "../DataView"
import "./style.scss"
// import "antd/lib/table/style/index.css"
import { pageData as AgendaListPageData } from "./Types/AgendaListView/__tests__/mocks"
import { pageData as BasinTodayListData } from "./Types/BasinTodayListView/__tests__/mocks"
import { pageData as FacilityListData } from "./Types/FacilityListView/__tests__/mocks"
import { pageData as LatestNewsBepcData } from "./Types/LatestNewsListView/__tests__/mocks"
import { pageData as NewsBriefsBepcData } from "./Types/LatestNewsListView/__tests__/mocks/newsBriefBepc"
import { pageData as MemberListData } from "./Types/MemberListView/__tests__/mocks"
import { pageData as RetireeListData } from "./Types/RetireeListView/__tests__/mocks"
import { pageData as RetireeEventsData } from "./Types/RetireeEventView/__tests__/mocks"
import { pageData as RetireePagesData } from "./Types/RetireePagesView/__tests__/mocks"

const description = `Displays a Data View from Drupal.`
const siteDomain = '//cms.bepc.com'

storiesOf("Data Views", module)
    .add("Agenda List View",
        withInfo(`${description} Lists an agenda for events like Annual Meeting.`)(() => {
            return (
                <DataView
                    siteDomain={siteDomain}
                    includedRelationships={AgendaListPageData.included}
                    paragraphId="26d23f6d-757e-4b75-9136-418bb73a3a18"
                />
            )
        })
    )
    .add("Basin Today List View",
        withInfo(`${description} Lists Basin Today articles in a grid.`)(() => {
            return (
                <DataView
                    siteDomain={siteDomain}
                    includedRelationships={BasinTodayListData.included}
                    paragraphId="e232bbc8-1cb3-4d71-99cd-70864906c706"
                    dev={true} // used to show images in dev
                />
            )
        })
    )
    .add("Facilities List View",
        withInfo(`${description} Lists Facilities articles in a grid.`)(() => {
            return (
                <DataView
                    siteDomain={siteDomain}
                    includedRelationships={FacilityListData.included}
                    paragraphId="6f7cf0f1-be0e-4e60-9938-2f833bfec23c"
                    dev={true} // used to show images in dev
                />
            )
        })
    )
    .add("Latest News BEPC List View",
        withInfo(`${description} Lists news articles in a list.`)(() => {
            return (
                <DataView
                    siteDomain={siteDomain}
                    includedRelationships={LatestNewsBepcData.included}
                    paragraphId="87a4c9bf-b981-4758-8c9a-dbb9bb0f2170"
                    dev={true} // used to show images in dev
                />
            )
        })
    )
    .add("News Briefs BEPC List View",
        withInfo(`${description} Lists News Brief articles in a list.`)(() => {
            return (
                <DataView
                    siteDomain={siteDomain}
                    includedRelationships={NewsBriefsBepcData.included}
                    paragraphId="402a4bd0-613c-492d-b7bc-16c96ba8ce5d"
                    dev={true} // used to show images in dev
                />
            )
        })
    )
    .add("Member List View",
        withInfo(`${description} Lists Members in a table.`)(() => {
            return (
                <DataView
                    siteDomain={siteDomain}
                    includedRelationships={MemberListData.included}
                    paragraphId="887a78f3-cd5c-4240-9246-bd9974637c5c"
                    dev={true} // used to show images in dev
                />
            )
        })
    )
    .add("Retiree List View",
        withInfo(`${description} Lists Retiree news.`)(() => {
            return (
                <DataView
                    siteDomain={siteDomain}
                    includedRelationships={RetireeListData.included}
                    paragraphId="95c92bf9-892a-465e-879a-2012a674d281"
                    dev={true} // used to show images in dev
                />
            )
        })
    )
    .add("Retiree Events View",
        withInfo(`${description} Lists Events in a Calendar Event View.`)(() => {
            return (
                <DataView
                    siteDomain={siteDomain}
                    includedRelationships={RetireeEventsData.included}
                    paragraphId="d84f3ce1-61a8-4414-9f80-99768f5c9c90"
                />
            )
        })
    )
    .add("Retiree Pages View",
        withInfo(`${description} Lists Pages in a Calendar Event View.`)(() => {
            return (
                <DataView
                    siteDomain={siteDomain}
                    includedRelationships={RetireePagesData.included}
                    paragraphId="dbd67dfc-3849-4da8-91c3-a81ea687e8b3"
                />
            )
        })
    )
