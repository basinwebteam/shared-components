import axios from "axios"
import * as moxios from "moxios"
import * as React from "react"
import { shallow, mount } from 'enzyme'
import toJson from "enzyme-to-json"
import * as Page from "../../../DrupalContentTypes/PageInterface"

import DataView from "../../DataView"
import DataViewWrapper from "../Wrapper"
import AgendaListView from "../Types/AgendaListView"
import {pageData} from "../Types/AgendaListView/__tests__/mocks"

describe("DataView - Agenda List", () => {
    const SITE_DOMAIN = "https://basinelectric.com"

    beforeEach(() => {
        moxios.install()
    })

    afterEach(() => {
        moxios.uninstall()
    })

    it("should render empty Agenda List View without data sent", () => {
        const element = mount(
            <DataView
                paragraphId="26d23f6d-757e-4b75-9136-418bb73a3a18"
                siteDomain={SITE_DOMAIN}
                includedRelationships={pageData.included}
            />
        )
        const agendaListViewComponent = element.childAt(0).childAt(0).childAt(0).childAt(0)

        expect(agendaListViewComponent.name()).toEqual("AgendaListView")
        expect(agendaListViewComponent.prop("data").length).toEqual(0)
        expect(agendaListViewComponent.text()).toEqual("")
    })
})
