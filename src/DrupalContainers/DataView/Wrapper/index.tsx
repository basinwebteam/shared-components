import { AxiosResponse, default as axios } from "axios"
import * as React from "react"

export class DataViewWrapper extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props)
        this.state = {
            data: new Array<any>(),
        }
    }

    public render() {
        const data = this.state.data
        const childrenWithProps = React.Children.map(
            this.props.children, (child) =>
                React.cloneElement(child as React.ReactElement<any>, {data})
        )

        return (
            <div>
                {childrenWithProps}
            </div>
        )
    }

    public componentDidMount() {
        this.updateComponent()
    }

    public componentDidUpdate(prevProps: IProps, prevState: IState) {
        if (this.props.url !== prevProps.url) {
            this.updateComponent()
        }
    }

    public async updateComponent() {
        const url = this.props.url.startsWith("rest-") ?
            `//api.bepc.com/api/cms/rest/${this.props.url.substr(5)}` :
            `${this.props.siteDomain}/${this.props.url}`

        const response = await axios.get
            (url) as IAxiosResponseView
        this.setState({
            data: response.data,
        })
    }

}

export default DataViewWrapper

export interface IProps {
    url: string,
    siteDomain: string,
}

export interface IState {
    data: any[],
}

export interface IAxiosResponseView extends AxiosResponse {
    data: any
}
