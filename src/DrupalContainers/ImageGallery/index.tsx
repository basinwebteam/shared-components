import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import ImageContainer from "../Image"

export class ImageGalleryContainer extends React.Component<IImageGalleryProps, IImageGalleryState> {
    constructor(props: IImageGalleryProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    public render() {
        if (this.state.images.length > 0) {
            return (
                <div className="row photo-gallery">
                    {this.state.images.map((image, i) => (
                        <div className="column small-6 medium-4 large-3 photo-gallery-image" key={image.id + i}>
                            <ImageContainer
                                paragraphId={image.id}
                                includedRelationships={this.props.includedRelationships}
                                siteDomain={this.props.siteDomain}
                            />
                            <div className="photo-gallery-image-description" style={{ minHeight: "5rem" }} dangerouslySetInnerHTML={{ __html: image.description }} />
                        </div>
                    ))}
                </div>
            )
        } else {
            return (<div className="row photo-gallery" />)
        }
    }

    public componentDidMount() {
        if (this.state.images.length === 0) {
            this.updateComponent()
        }
    }

    public componentDidUpdate(prevProps: IImageGalleryProps, prevState: IImageGalleryState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateComponent()
        }
    }

    public updateComponent() {
        const images = this.getImages()
        this.setState(images)
    }

    private getImageDescription(id: string) {
        const image = this.props.includedRelationships.find((paragraph) => id === paragraph.id) as IImageParagraph
        return (typeof image !== "undefined") ? this.getFieldCaption(image.attributes.field_caption) : ""
    }

    private getFieldCaption(caption: Page.IWYSIWYG) {
        if (caption === null) {
            return ""
        }
        if (typeof caption === "string") {
            return caption
        }
        if (caption.hasOwnProperty("value")) {
            return caption.value
        }
        return ""
    }

    private getImages() {
        const ImageParagraph = this.props.includedRelationships.find((paragraph) => paragraph.id === this.props.paragraphId) as IImageGalleryParagraph
        const images = ImageParagraph.relationships.field_gallery_image.data.map((image) => {
            return {
                id: image.id,
                description: this.getImageDescription(image.id),
            } as IImage
        })
        return {
            images,
        } as IImageGalleryState
    }

    private getDefaultState() {
        try {
            return this.getImages()
        } catch (e) {
            return {
                images: new Array(),
            } as IImageGalleryState
        }

    }
}

export default ImageGalleryContainer

export interface IImageGalleryProps {
    includedRelationships: Array<Page.IPageData | any>
    paragraphId: string
    siteDomain: string
}

export interface IImageGalleryState {
    images: IImage[]
}

export interface IImage {
    id: string,
    description: string
}

export interface IImageGalleryParagraph extends Page.IPageData {
    relationships: IImageGalleryRelationships
}

export interface IImageGalleryRelationships extends Page.IRelationships {
    field_gallery_image: {
        data: Page.IDatum[],
    }
}

export interface IImageParagraph extends Page.IPageData {
    attributes: IImageAttributes
}

export interface IImageAttributes extends Page.IAttributes {
    field_caption: Page.IWYSIWYG
}
