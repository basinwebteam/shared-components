import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import pageData from "./__tests__/mocks"
import AwardContainer from "./index"

const siteDomain = "//cms.bepc.com"

storiesOf("Award", module)
    .add("Default List of Award Winners",
        withInfo(`Lists award winners for multiple types of awards`)(() => {
            return (
                <div>
                    <AwardContainer
                        includedRelationships={pageData.included}
                        paragraphId={"596a8350-e486-476c-a14d-3bad7b0e7b51"}
                        siteDomain={siteDomain}
                    />
                </div>
            )
        })
    )
