// tslint:disable:max-line-length

export default {
    "data": {
        "type": "node--basin_electric_basic_page",
        "id": "5ff9aa8e-0939-4501-8583-c33a11a6343e",
        "attributes": {
            "nid": 101,
            "uuid": "5ff9aa8e-0939-4501-8583-c33a11a6343e",
            "vid": 26446,
            "langcode": "en",
            "revision_timestamp": 1531256144,
            "revision_log": null,
            "status": true,
            "title": "Hall of Fame",
            "created": 1499354910,
            "changed": 1531256144,
            "promote": false,
            "sticky": false,
            "default_langcode": true,
            "revision_translation_affected": true,
            "metatag": null,
            "path": {
                "alias": "/about-us/organization/history/hall-fame",
                "pid": 56,
                "langcode": "en"
            },
            "field_meta_tags": "a:0:{}",
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c1953c9b-859c-42d1-a2ae-a74572456c0c"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/type"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/revision_uid"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/uid"
                }
            },
            "menu_link": {
                "data": null,
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/relationships/menu_link",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/menu_link"
                }
            },
            "field_aside_content": {
                "data": [
                    {
                        "type": "paragraph--reusable_block",
                        "id": "7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f",
                        "meta": {
                            "target_revision_id": "87871"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/relationships/field_aside_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/field_aside_content"
                }
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "34df3bdd-7d70-41e0-b247-8003cdaa23ff",
                        "meta": {
                            "target_revision_id": "87876"
                        }
                    },
                    {
                        "type": "paragraph--image_gallery",
                        "id": "a520290a-105b-4acf-aefd-ebd4047da76d",
                        "meta": {
                            "target_revision_id": "87946"
                        }
                    },
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "3ae698d3-69c5-4578-b402-9f074c47fc89",
                        "meta": {
                            "target_revision_id": "87951"
                        }
                    },
                    {
                        "type": "paragraph--image_gallery",
                        "id": "734afd22-5988-4c75-aff7-2beacf23d8b3",
                        "meta": {
                            "target_revision_id": "88021"
                        }
                    },
                    {
                        "type": "paragraph--award",
                        "id": "596a8350-e486-476c-a14d-3bad7b0e7b51",
                        "meta": {
                            "target_revision_id": "88096"
                        }
                    },
                    {
                        "type": "paragraph--award",
                        "id": "81579e3f-c454-479e-a1c2-ca32c05f6103",
                        "meta": {
                            "target_revision_id": "88166"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/field_content"
                }
            },
            "field_related_videos": {
                "data": null,
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/relationships/field_related_videos",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/field_related_videos"
                }
            },
            "field_web_layout": {
                "data": null,
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/relationships/field_web_layout",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/field_web_layout"
                }
            },
            "scheduled_update": {
                "data": [],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/relationships/scheduled_update",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e/scheduled_update"
                }
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e"
        }
    },
    "jsonapi": {
        "version": "1.0",
        "meta": {
            "links": {
                "self": "http://jsonapi.org/format/1.0/"
            }
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/5ff9aa8e-0939-4501-8583-c33a11a6343e?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_block%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_content.field_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_content.field_calendar%2Cfield_content.field_winners%2Cfield_content.field_winners.field_image%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--reusable_block",
            "id": "7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f",
            "attributes": {
                "id": 8731,
                "uuid": "7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f",
                "revision_id": 87871,
                "langcode": "en",
                "status": true,
                "created": 1502374239,
                "parent_id": "101",
                "parent_type": "node",
                "parent_field_name": "field_aside_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "2f24beb8-4347-4ba8-8762-4348bf464846"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f/revision_uid"
                    }
                },
                "field_block": {
                    "data": {
                        "type": "block_content--basic",
                        "id": "42bbff65-5754-4a37-a43f-6b0355be5ce4"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f/relationships/field_block",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f/field_block"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/7a64ce36-ac20-42eb-aaf5-4cffe8a1b80f"
            }
        },
        {
            "type": "paragraph--basic_paragraph",
            "id": "34df3bdd-7d70-41e0-b247-8003cdaa23ff",
            "attributes": {
                "id": 351,
                "uuid": "34df3bdd-7d70-41e0-b247-8003cdaa23ff",
                "revision_id": 87876,
                "langcode": "en",
                "status": true,
                "created": 1499354917,
                "parent_id": "101",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_body": {
                    "value": "<p>Basin Electric Power Cooperative has a proud history of serving its membership through innovative energy solutions that bolster and sustain local and regional economies and enhance community life in rural towns and cities. To honor those who have made significant contributions to rural electrics and communities, Basin Electric periodically bestows its Cooperative Spirit Award and Cornerstone Award on individuals who have made outstanding contributions to rural electric cooperatives.</p>\r\n\r\n<h2>Cornerstone Award</h2>\r\n\r\n<p>The Cornerstone Award is Basin Electric's highest award. It honors an individual whose leadership and outstanding service to electric cooperatives has been a \"cornerstone\" for Basin Electric, its member systems, and the rural Americans they serve.</p>\r\n\r\n<p>The award will be given only to individuals truly deserving significant recognition and, as such, the award may not be presented every year.</p>\r\n\r\n<p>Award recipients will be current or past leaders in, or supporters of, the cooperative movement.</p>\r\n\r\n<p>The recipient's leadership, personal commitment, vision and business endeavors will be both distinguished and extraordinary and will have made a difference in the lives of the rural Americans, Basin Electric and its member systems served.</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Basin Electric Power Cooperative has a proud history of serving its membership through innovative energy solutions that bolster and sustain local and regional economies and enhance community life in rural towns and cities. To honor those who have made significant contributions to rural electrics and communities, Basin Electric periodically bestows its Cooperative Spirit Award and Cornerstone Award on individuals who have made outstanding contributions to rural electric cooperatives.</p>\n<h2>Cornerstone Award</h2>\n<p>The Cornerstone Award is Basin Electric's highest award. It honors an individual whose leadership and outstanding service to electric cooperatives has been a \"cornerstone\" for Basin Electric, its member systems, and the rural Americans they serve.</p>\n<p>The award will be given only to individuals truly deserving significant recognition and, as such, the award may not be presented every year.</p>\n<p>Award recipients will be current or past leaders in, or supporters of, the cooperative movement.</p>\n<p>The recipient's leadership, personal commitment, vision and business endeavors will be both distinguished and extraordinary and will have made a difference in the lives of the rural Americans, Basin Electric and its member systems served.</p>\n"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/34df3bdd-7d70-41e0-b247-8003cdaa23ff/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/34df3bdd-7d70-41e0-b247-8003cdaa23ff/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/34df3bdd-7d70-41e0-b247-8003cdaa23ff/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/34df3bdd-7d70-41e0-b247-8003cdaa23ff/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/34df3bdd-7d70-41e0-b247-8003cdaa23ff/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/34df3bdd-7d70-41e0-b247-8003cdaa23ff/revision_uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/34df3bdd-7d70-41e0-b247-8003cdaa23ff"
            }
        },
        {
            "type": "paragraph--image_gallery",
            "id": "a520290a-105b-4acf-aefd-ebd4047da76d",
            "attributes": {
                "id": 421,
                "uuid": "a520290a-105b-4acf-aefd-ebd4047da76d",
                "revision_id": 87946,
                "langcode": "en",
                "status": true,
                "created": 1499354966,
                "parent_id": "101",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "7fef1721-5aff-4ea0-8c13-93b1f7197c6e"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/a520290a-105b-4acf-aefd-ebd4047da76d/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/a520290a-105b-4acf-aefd-ebd4047da76d/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/a520290a-105b-4acf-aefd-ebd4047da76d/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/a520290a-105b-4acf-aefd-ebd4047da76d/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/a520290a-105b-4acf-aefd-ebd4047da76d/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/a520290a-105b-4acf-aefd-ebd4047da76d/revision_uid"
                    }
                },
                "field_gallery_image": {
                    "data": [
                        {
                            "type": "paragraph--image",
                            "id": "eb01df7e-3ff6-4de6-aa66-1fddffe2983b",
                            "meta": {
                                "target_revision_id": "87881"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "ae85c351-38d4-473b-8a01-7a184702ab86",
                            "meta": {
                                "target_revision_id": "87886"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "cbfde1ff-1e57-4788-97c4-665bd3b9b726",
                            "meta": {
                                "target_revision_id": "87891"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "01f5252c-6ffc-43d7-becc-39f72519640e",
                            "meta": {
                                "target_revision_id": "87896"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "b91d9556-a2f3-454d-9ba4-ac3b36f87669",
                            "meta": {
                                "target_revision_id": "87901"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "0c67cdf9-4350-486b-97f9-965aaab193a8",
                            "meta": {
                                "target_revision_id": "87906"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "74947390-a285-4584-80eb-53e2baa20d80",
                            "meta": {
                                "target_revision_id": "87911"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "5e7d8745-ea52-4973-b89e-d6259b508ca1",
                            "meta": {
                                "target_revision_id": "87916"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "3c26a109-231f-4ea6-8014-5596d94edd69",
                            "meta": {
                                "target_revision_id": "87921"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "bb0ed643-77df-4c12-ab43-32fedd970501",
                            "meta": {
                                "target_revision_id": "87926"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "b4ec47ac-2df8-445a-b925-b17771acefd0",
                            "meta": {
                                "target_revision_id": "87931"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "9c72d3d8-8711-4d8b-b309-4afa477c5701",
                            "meta": {
                                "target_revision_id": "87936"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "8802b428-7697-4069-a38d-d4f7d0806096",
                            "meta": {
                                "target_revision_id": "87941"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/a520290a-105b-4acf-aefd-ebd4047da76d/relationships/field_gallery_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/a520290a-105b-4acf-aefd-ebd4047da76d/field_gallery_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/a520290a-105b-4acf-aefd-ebd4047da76d"
            }
        },
        {
            "type": "paragraph--basic_paragraph",
            "id": "3ae698d3-69c5-4578-b402-9f074c47fc89",
            "attributes": {
                "id": 426,
                "uuid": "3ae698d3-69c5-4578-b402-9f074c47fc89",
                "revision_id": 87951,
                "langcode": "en",
                "status": true,
                "created": 1499356219,
                "parent_id": "101",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_body": {
                    "value": "<h2>Cooperative Spirit Award</h2>\r\n\r\n<p>This award is given for significant services performed by the recipient to Basin Electric and its membership. It is envisioned that honorees will be recognized for having been diligent in all aspects of their electric cooperative-related careers and created value and success for the cooperatives they work with/for through consistent good work. This award typically focuses on the recipient's years of excellent performance, commitment and service to Basin Electric and its member systems.</p>\r\n\r\n<p>The Cooperative Spirit Award may be given to more than one person annually. The recipient may be a recent retiree, manager or director from Basin Electric, a member system, or another related organization. This award recognizes the recipient's outstanding performance and support for innovative and effective cooperative programs. Leadership, effectiveness and extent of impact shall be considered important factors in judging this award.</p>\r\n",
                    "format": "rich_text",
                    "processed": "<h2>Cooperative Spirit Award</h2>\n<p>This award is given for significant services performed by the recipient to Basin Electric and its membership. It is envisioned that honorees will be recognized for having been diligent in all aspects of their electric cooperative-related careers and created value and success for the cooperatives they work with/for through consistent good work. This award typically focuses on the recipient's years of excellent performance, commitment and service to Basin Electric and its member systems.</p>\n<p>The Cooperative Spirit Award may be given to more than one person annually. The recipient may be a recent retiree, manager or director from Basin Electric, a member system, or another related organization. This award recognizes the recipient's outstanding performance and support for innovative and effective cooperative programs. Leadership, effectiveness and extent of impact shall be considered important factors in judging this award.</p>\n"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3ae698d3-69c5-4578-b402-9f074c47fc89/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3ae698d3-69c5-4578-b402-9f074c47fc89/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3ae698d3-69c5-4578-b402-9f074c47fc89/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3ae698d3-69c5-4578-b402-9f074c47fc89/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3ae698d3-69c5-4578-b402-9f074c47fc89/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3ae698d3-69c5-4578-b402-9f074c47fc89/revision_uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/3ae698d3-69c5-4578-b402-9f074c47fc89"
            }
        },
        {
            "type": "paragraph--image_gallery",
            "id": "734afd22-5988-4c75-aff7-2beacf23d8b3",
            "attributes": {
                "id": 461,
                "uuid": "734afd22-5988-4c75-aff7-2beacf23d8b3",
                "revision_id": 88021,
                "langcode": "en",
                "status": true,
                "created": 1499356386,
                "parent_id": "101",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "7fef1721-5aff-4ea0-8c13-93b1f7197c6e"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/734afd22-5988-4c75-aff7-2beacf23d8b3/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/734afd22-5988-4c75-aff7-2beacf23d8b3/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/734afd22-5988-4c75-aff7-2beacf23d8b3/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/734afd22-5988-4c75-aff7-2beacf23d8b3/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/734afd22-5988-4c75-aff7-2beacf23d8b3/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/734afd22-5988-4c75-aff7-2beacf23d8b3/revision_uid"
                    }
                },
                "field_gallery_image": {
                    "data": [
                        {
                            "type": "paragraph--image",
                            "id": "244a99d8-321c-49b7-af6b-57a48b871178",
                            "meta": {
                                "target_revision_id": "87956"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "e8be2f99-89b8-4601-937a-a186da16015d",
                            "meta": {
                                "target_revision_id": "87961"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "5804cb04-9810-4945-97ef-ae31513bfb15",
                            "meta": {
                                "target_revision_id": "87966"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "d51985eb-99ac-4e8e-ae12-756e6c4f38c8",
                            "meta": {
                                "target_revision_id": "87971"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "e99bbdf2-ca35-4749-b85c-4b9cef31c5a6",
                            "meta": {
                                "target_revision_id": "87976"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "49b0206c-8bbe-46b0-b5cd-c215eb961acc",
                            "meta": {
                                "target_revision_id": "87981"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "003d8aa2-f89e-4f00-af44-3556b5302dc8",
                            "meta": {
                                "target_revision_id": "87986"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "93a67e57-5098-4192-8625-33f60e13b4bb",
                            "meta": {
                                "target_revision_id": "87991"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "843948ea-49b8-4464-ac56-d92b8dfae695",
                            "meta": {
                                "target_revision_id": "87996"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "9139b904-2d40-4caf-bcda-0598d53b54ea",
                            "meta": {
                                "target_revision_id": "88001"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "ffd043de-2abf-4c1b-89b7-bfa11c870053",
                            "meta": {
                                "target_revision_id": "88006"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "f04a8e92-c1c9-415b-aaea-a8db47b2f924",
                            "meta": {
                                "target_revision_id": "88011"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "4c5f9580-f5d4-4d55-97cc-c9d80705e5fc",
                            "meta": {
                                "target_revision_id": "88016"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/734afd22-5988-4c75-aff7-2beacf23d8b3/relationships/field_gallery_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/734afd22-5988-4c75-aff7-2beacf23d8b3/field_gallery_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image_gallery/734afd22-5988-4c75-aff7-2beacf23d8b3"
            }
        },
        {
            "type": "paragraph--award",
            "id": "596a8350-e486-476c-a14d-3bad7b0e7b51",
            "attributes": {
                "id": 16056,
                "uuid": "596a8350-e486-476c-a14d-3bad7b0e7b51",
                "revision_id": 88096,
                "langcode": "en",
                "status": true,
                "created": 1531251223,
                "parent_id": "101",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_title": "Cornerstone Award"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "39d6717f-f693-4072-bf09-4761a18df81a"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award/596a8350-e486-476c-a14d-3bad7b0e7b51/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award/596a8350-e486-476c-a14d-3bad7b0e7b51/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award/596a8350-e486-476c-a14d-3bad7b0e7b51/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award/596a8350-e486-476c-a14d-3bad7b0e7b51/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award/596a8350-e486-476c-a14d-3bad7b0e7b51/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award/596a8350-e486-476c-a14d-3bad7b0e7b51/revision_uid"
                    }
                },
                "field_winners": {
                    "data": [
                        {
                            "type": "paragraph--award_winner",
                            "id": "db7ab17b-9bc7-4a99-b390-15f633ebbde7",
                            "meta": {
                                "target_revision_id": "88026"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "177b977c-208d-42b1-8a4a-f0648c84e57f",
                            "meta": {
                                "target_revision_id": "88031"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "d0d6ed63-0c56-47c9-93e8-b0a27ca5793b",
                            "meta": {
                                "target_revision_id": "88036"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "88ef46fd-0698-4529-9316-fdd92b0e3449",
                            "meta": {
                                "target_revision_id": "88041"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "d07519c1-c8eb-4040-8a4f-d9576bfe2340",
                            "meta": {
                                "target_revision_id": "88046"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "3f778603-24c7-4c8a-9e3a-90602ad67ea5",
                            "meta": {
                                "target_revision_id": "88051"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "11ffd6f9-573e-489a-8beb-cb700d88eeb5",
                            "meta": {
                                "target_revision_id": "88056"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "7c7d6429-9b46-42d2-882a-74bba8bd8af7",
                            "meta": {
                                "target_revision_id": "88061"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "753bae4f-ac51-4e81-906c-4d8b91e4460f",
                            "meta": {
                                "target_revision_id": "88066"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "3f85365f-a06d-4d7d-ae90-210bf8241c7e",
                            "meta": {
                                "target_revision_id": "88071"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2",
                            "meta": {
                                "target_revision_id": "88076"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "9543b22d-9007-4245-8ad9-11dee272fc99",
                            "meta": {
                                "target_revision_id": "88081"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "f316f08b-5996-415f-8fc4-7800512c02c2",
                            "meta": {
                                "target_revision_id": "88086"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "56214377-fcc5-40d0-8e3b-5c1c37e0369e",
                            "meta": {
                                "target_revision_id": "88091"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award/596a8350-e486-476c-a14d-3bad7b0e7b51/relationships/field_winners",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award/596a8350-e486-476c-a14d-3bad7b0e7b51/field_winners"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award/596a8350-e486-476c-a14d-3bad7b0e7b51"
            }
        },
        {
            "type": "paragraph--award",
            "id": "81579e3f-c454-479e-a1c2-ca32c05f6103",
            "attributes": {
                "id": 16121,
                "uuid": "81579e3f-c454-479e-a1c2-ca32c05f6103",
                "revision_id": 88166,
                "langcode": "en",
                "status": true,
                "created": 1531255116,
                "parent_id": "101",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_title": "Cooperative Spirit Award"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "39d6717f-f693-4072-bf09-4761a18df81a"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award/81579e3f-c454-479e-a1c2-ca32c05f6103/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award/81579e3f-c454-479e-a1c2-ca32c05f6103/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award/81579e3f-c454-479e-a1c2-ca32c05f6103/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award/81579e3f-c454-479e-a1c2-ca32c05f6103/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award/81579e3f-c454-479e-a1c2-ca32c05f6103/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award/81579e3f-c454-479e-a1c2-ca32c05f6103/revision_uid"
                    }
                },
                "field_winners": {
                    "data": [
                        {
                            "type": "paragraph--award_winner",
                            "id": "1ae0c91a-d4fe-4557-8086-59c20dbc59cd",
                            "meta": {
                                "target_revision_id": "88101"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "9584f89e-1634-4810-94a4-3b3fa45f977c",
                            "meta": {
                                "target_revision_id": "88106"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "83e5304e-2d73-4f2b-874e-c6b30e076526",
                            "meta": {
                                "target_revision_id": "88111"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "401437e2-2c26-4433-8746-777da27d3dda",
                            "meta": {
                                "target_revision_id": "88116"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "7193098b-23cc-41db-b57c-9e5efb50aff7",
                            "meta": {
                                "target_revision_id": "88121"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "0e1fafcd-7ed3-454c-aa7b-ca5629dd5344",
                            "meta": {
                                "target_revision_id": "88126"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "8b6d505e-df69-416b-b3b4-16ee3e34ab0f",
                            "meta": {
                                "target_revision_id": "88131"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "e62b6065-ceee-4f85-bc6d-81c970c470b5",
                            "meta": {
                                "target_revision_id": "88136"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "2af61a62-fc57-4e7c-85e8-46d5c1e83ff6",
                            "meta": {
                                "target_revision_id": "88141"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "fdf9d738-5888-4c83-9b03-fe612d3cca1e",
                            "meta": {
                                "target_revision_id": "88146"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "7e0d5293-878e-4728-b9e8-d4a5ba9294af",
                            "meta": {
                                "target_revision_id": "88151"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "80c33607-47c1-4a9a-a99b-acd0766dc4fb",
                            "meta": {
                                "target_revision_id": "88156"
                            }
                        },
                        {
                            "type": "paragraph--award_winner",
                            "id": "6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b",
                            "meta": {
                                "target_revision_id": "88161"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award/81579e3f-c454-479e-a1c2-ca32c05f6103/relationships/field_winners",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award/81579e3f-c454-479e-a1c2-ca32c05f6103/field_winners"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award/81579e3f-c454-479e-a1c2-ca32c05f6103"
            }
        },
        {
            "type": "paragraph--image",
            "id": "eb01df7e-3ff6-4de6-aa66-1fddffe2983b",
            "attributes": {
                "id": 356,
                "uuid": "eb01df7e-3ff6-4de6-aa66-1fddffe2983b",
                "revision_id": 87881,
                "langcode": "en",
                "status": true,
                "created": 1499354966,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Gerald C. Wegner<br />\r\n2002</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Gerald C. Wegner<br />\n2002</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/eb01df7e-3ff6-4de6-aa66-1fddffe2983b/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/eb01df7e-3ff6-4de6-aa66-1fddffe2983b/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/eb01df7e-3ff6-4de6-aa66-1fddffe2983b/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/eb01df7e-3ff6-4de6-aa66-1fddffe2983b/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/eb01df7e-3ff6-4de6-aa66-1fddffe2983b/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/eb01df7e-3ff6-4de6-aa66-1fddffe2983b/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "fd656252-f76b-453f-900f-d1905411ca81",
                        "meta": {
                            "alt": "Gerald C. Wegner  2002",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/eb01df7e-3ff6-4de6-aa66-1fddffe2983b/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/eb01df7e-3ff6-4de6-aa66-1fddffe2983b/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/eb01df7e-3ff6-4de6-aa66-1fddffe2983b"
            }
        },
        {
            "type": "paragraph--image",
            "id": "ae85c351-38d4-473b-8a01-7a184702ab86",
            "attributes": {
                "id": 361,
                "uuid": "ae85c351-38d4-473b-8a01-7a184702ab86",
                "revision_id": 87886,
                "langcode": "en",
                "status": true,
                "created": 1499355816,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Blaine Stockton<br />\r\n2003</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Blaine Stockton<br />\n2003</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/ae85c351-38d4-473b-8a01-7a184702ab86/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/ae85c351-38d4-473b-8a01-7a184702ab86/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/ae85c351-38d4-473b-8a01-7a184702ab86/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/ae85c351-38d4-473b-8a01-7a184702ab86/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/ae85c351-38d4-473b-8a01-7a184702ab86/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/ae85c351-38d4-473b-8a01-7a184702ab86/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "204abcb1-debb-4f18-a38b-a9cbc911f796",
                        "meta": {
                            "alt": "Blaine Stockton  2003",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/ae85c351-38d4-473b-8a01-7a184702ab86/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/ae85c351-38d4-473b-8a01-7a184702ab86/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/ae85c351-38d4-473b-8a01-7a184702ab86"
            }
        },
        {
            "type": "paragraph--image",
            "id": "cbfde1ff-1e57-4788-97c4-665bd3b9b726",
            "attributes": {
                "id": 366,
                "uuid": "cbfde1ff-1e57-4788-97c4-665bd3b9b726",
                "revision_id": 87891,
                "langcode": "en",
                "status": true,
                "created": 1499355836,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Robert L. McPhail<br />\r\n2005</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Robert L. McPhail<br />\n2005</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/cbfde1ff-1e57-4788-97c4-665bd3b9b726/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/cbfde1ff-1e57-4788-97c4-665bd3b9b726/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/cbfde1ff-1e57-4788-97c4-665bd3b9b726/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/cbfde1ff-1e57-4788-97c4-665bd3b9b726/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/cbfde1ff-1e57-4788-97c4-665bd3b9b726/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/cbfde1ff-1e57-4788-97c4-665bd3b9b726/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "c1138c8d-45ad-4d4a-b871-486d2300731e",
                        "meta": {
                            "alt": "Robert L. McPhail  2005",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/cbfde1ff-1e57-4788-97c4-665bd3b9b726/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/cbfde1ff-1e57-4788-97c4-665bd3b9b726/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/cbfde1ff-1e57-4788-97c4-665bd3b9b726"
            }
        },
        {
            "type": "paragraph--image",
            "id": "01f5252c-6ffc-43d7-becc-39f72519640e",
            "attributes": {
                "id": 371,
                "uuid": "01f5252c-6ffc-43d7-becc-39f72519640e",
                "revision_id": 87896,
                "langcode": "en",
                "status": true,
                "created": 1499355868,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>William L. Guy<br />\r\n2006</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>William L. Guy<br />\n2006</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/01f5252c-6ffc-43d7-becc-39f72519640e/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/01f5252c-6ffc-43d7-becc-39f72519640e/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/01f5252c-6ffc-43d7-becc-39f72519640e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/01f5252c-6ffc-43d7-becc-39f72519640e/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/01f5252c-6ffc-43d7-becc-39f72519640e/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/01f5252c-6ffc-43d7-becc-39f72519640e/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "611812bd-82bf-4fa8-9b29-e045c330f979",
                        "meta": {
                            "alt": "William L. Guy  2006",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/01f5252c-6ffc-43d7-becc-39f72519640e/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/01f5252c-6ffc-43d7-becc-39f72519640e/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/01f5252c-6ffc-43d7-becc-39f72519640e"
            }
        },
        {
            "type": "paragraph--image",
            "id": "b91d9556-a2f3-454d-9ba4-ac3b36f87669",
            "attributes": {
                "id": 376,
                "uuid": "b91d9556-a2f3-454d-9ba4-ac3b36f87669",
                "revision_id": 87901,
                "langcode": "en",
                "status": true,
                "created": 1499355966,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Michael Hacskylo<br />\r\n2007&nbsp;</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Michael Hacskylo<br />\n2007 </p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/b91d9556-a2f3-454d-9ba4-ac3b36f87669/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/b91d9556-a2f3-454d-9ba4-ac3b36f87669/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/b91d9556-a2f3-454d-9ba4-ac3b36f87669/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/b91d9556-a2f3-454d-9ba4-ac3b36f87669/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/b91d9556-a2f3-454d-9ba4-ac3b36f87669/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/b91d9556-a2f3-454d-9ba4-ac3b36f87669/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "a688de88-5553-4307-9587-7413c5d3d81e",
                        "meta": {
                            "alt": "Michael Hacskylo  2007 ",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/b91d9556-a2f3-454d-9ba4-ac3b36f87669/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/b91d9556-a2f3-454d-9ba4-ac3b36f87669/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/b91d9556-a2f3-454d-9ba4-ac3b36f87669"
            }
        },
        {
            "type": "paragraph--image",
            "id": "0c67cdf9-4350-486b-97f9-965aaab193a8",
            "attributes": {
                "id": 381,
                "uuid": "0c67cdf9-4350-486b-97f9-965aaab193a8",
                "revision_id": 87906,
                "langcode": "en",
                "status": true,
                "created": 1499355989,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Robert Feragen<br />\r\n2008</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Robert Feragen<br />\n2008</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/0c67cdf9-4350-486b-97f9-965aaab193a8/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/0c67cdf9-4350-486b-97f9-965aaab193a8/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/0c67cdf9-4350-486b-97f9-965aaab193a8/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/0c67cdf9-4350-486b-97f9-965aaab193a8/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/0c67cdf9-4350-486b-97f9-965aaab193a8/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/0c67cdf9-4350-486b-97f9-965aaab193a8/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "66b87045-f949-4443-9814-ba2b6053466a",
                        "meta": {
                            "alt": "Robert Feragen  2008",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/0c67cdf9-4350-486b-97f9-965aaab193a8/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/0c67cdf9-4350-486b-97f9-965aaab193a8/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/0c67cdf9-4350-486b-97f9-965aaab193a8"
            }
        },
        {
            "type": "paragraph--image",
            "id": "74947390-a285-4584-80eb-53e2baa20d80",
            "attributes": {
                "id": 386,
                "uuid": "74947390-a285-4584-80eb-53e2baa20d80",
                "revision_id": 87911,
                "langcode": "en",
                "status": true,
                "created": 1499356018,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Byron Dorgan<br />\r\n2010</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Byron Dorgan<br />\n2010</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/74947390-a285-4584-80eb-53e2baa20d80/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/74947390-a285-4584-80eb-53e2baa20d80/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/74947390-a285-4584-80eb-53e2baa20d80/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/74947390-a285-4584-80eb-53e2baa20d80/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/74947390-a285-4584-80eb-53e2baa20d80/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/74947390-a285-4584-80eb-53e2baa20d80/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "d82da2cf-225b-4f14-99de-77bbb01b53b8",
                        "meta": {
                            "alt": "Byron Dorgan  2010",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/74947390-a285-4584-80eb-53e2baa20d80/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/74947390-a285-4584-80eb-53e2baa20d80/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/74947390-a285-4584-80eb-53e2baa20d80"
            }
        },
        {
            "type": "paragraph--image",
            "id": "5e7d8745-ea52-4973-b89e-d6259b508ca1",
            "attributes": {
                "id": 391,
                "uuid": "5e7d8745-ea52-4973-b89e-d6259b508ca1",
                "revision_id": 87916,
                "langcode": "en",
                "status": true,
                "created": 1499356041,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Hub Thompson<br />\r\n2010</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Hub Thompson<br />\n2010</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/5e7d8745-ea52-4973-b89e-d6259b508ca1/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/5e7d8745-ea52-4973-b89e-d6259b508ca1/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/5e7d8745-ea52-4973-b89e-d6259b508ca1/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/5e7d8745-ea52-4973-b89e-d6259b508ca1/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/5e7d8745-ea52-4973-b89e-d6259b508ca1/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/5e7d8745-ea52-4973-b89e-d6259b508ca1/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "26842908-545f-4108-a3bd-cae1b50caf9c",
                        "meta": {
                            "alt": "Hub Thompson  2010",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/5e7d8745-ea52-4973-b89e-d6259b508ca1/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/5e7d8745-ea52-4973-b89e-d6259b508ca1/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/5e7d8745-ea52-4973-b89e-d6259b508ca1"
            }
        },
        {
            "type": "paragraph--image",
            "id": "3c26a109-231f-4ea6-8014-5596d94edd69",
            "attributes": {
                "id": 396,
                "uuid": "3c26a109-231f-4ea6-8014-5596d94edd69",
                "revision_id": 87921,
                "langcode": "en",
                "status": true,
                "created": 1499356061,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Dave Freudenthal<br />\r\n2011</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Dave Freudenthal<br />\n2011</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/3c26a109-231f-4ea6-8014-5596d94edd69/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/3c26a109-231f-4ea6-8014-5596d94edd69/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/3c26a109-231f-4ea6-8014-5596d94edd69/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/3c26a109-231f-4ea6-8014-5596d94edd69/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/3c26a109-231f-4ea6-8014-5596d94edd69/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/3c26a109-231f-4ea6-8014-5596d94edd69/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "1e7e60ee-542c-4994-bd83-57e1a8636fea",
                        "meta": {
                            "alt": "Dave Freudenthal   2011",
                            "title": "",
                            "width": "113",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/3c26a109-231f-4ea6-8014-5596d94edd69/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/3c26a109-231f-4ea6-8014-5596d94edd69/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/3c26a109-231f-4ea6-8014-5596d94edd69"
            }
        },
        {
            "type": "paragraph--image",
            "id": "bb0ed643-77df-4c12-ab43-32fedd970501",
            "attributes": {
                "id": 401,
                "uuid": "bb0ed643-77df-4c12-ab43-32fedd970501",
                "revision_id": 87926,
                "langcode": "en",
                "status": true,
                "created": 1499356091,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Wally Beyer<br />\r\n2011</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Wally Beyer<br />\n2011</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/bb0ed643-77df-4c12-ab43-32fedd970501/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/bb0ed643-77df-4c12-ab43-32fedd970501/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/bb0ed643-77df-4c12-ab43-32fedd970501/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/bb0ed643-77df-4c12-ab43-32fedd970501/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/bb0ed643-77df-4c12-ab43-32fedd970501/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/bb0ed643-77df-4c12-ab43-32fedd970501/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "232d6116-4a6d-4d2f-b143-23b2ec9e967a",
                        "meta": {
                            "alt": "Wally Beyer  2011",
                            "title": "",
                            "width": "117",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/bb0ed643-77df-4c12-ab43-32fedd970501/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/bb0ed643-77df-4c12-ab43-32fedd970501/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/bb0ed643-77df-4c12-ab43-32fedd970501"
            }
        },
        {
            "type": "paragraph--image",
            "id": "b4ec47ac-2df8-445a-b925-b17771acefd0",
            "attributes": {
                "id": 406,
                "uuid": "b4ec47ac-2df8-445a-b925-b17771acefd0",
                "revision_id": 87931,
                "langcode": "en",
                "status": true,
                "created": 1499356112,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Kent Conrad<br />\r\n2012</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Kent Conrad<br />\n2012</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/b4ec47ac-2df8-445a-b925-b17771acefd0/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/b4ec47ac-2df8-445a-b925-b17771acefd0/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/b4ec47ac-2df8-445a-b925-b17771acefd0/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/b4ec47ac-2df8-445a-b925-b17771acefd0/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/b4ec47ac-2df8-445a-b925-b17771acefd0/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/b4ec47ac-2df8-445a-b925-b17771acefd0/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "920a3c4c-5a97-4c0d-b638-ea1a349150b5",
                        "meta": {
                            "alt": "Kent Conrad 2012",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/b4ec47ac-2df8-445a-b925-b17771acefd0/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/b4ec47ac-2df8-445a-b925-b17771acefd0/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/b4ec47ac-2df8-445a-b925-b17771acefd0"
            }
        },
        {
            "type": "paragraph--image",
            "id": "9c72d3d8-8711-4d8b-b309-4afa477c5701",
            "attributes": {
                "id": 411,
                "uuid": "9c72d3d8-8711-4d8b-b309-4afa477c5701",
                "revision_id": 87936,
                "langcode": "en",
                "status": true,
                "created": 1499356140,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Wayne Child<br />\r\n2014</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Wayne Child<br />\n2014</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/9c72d3d8-8711-4d8b-b309-4afa477c5701/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/9c72d3d8-8711-4d8b-b309-4afa477c5701/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/9c72d3d8-8711-4d8b-b309-4afa477c5701/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/9c72d3d8-8711-4d8b-b309-4afa477c5701/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/9c72d3d8-8711-4d8b-b309-4afa477c5701/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/9c72d3d8-8711-4d8b-b309-4afa477c5701/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "1166f9da-4204-4aee-8887-3a8478e7bc0e",
                        "meta": {
                            "alt": "Wayne Child 2014",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/9c72d3d8-8711-4d8b-b309-4afa477c5701/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/9c72d3d8-8711-4d8b-b309-4afa477c5701/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/9c72d3d8-8711-4d8b-b309-4afa477c5701"
            }
        },
        {
            "type": "paragraph--image",
            "id": "8802b428-7697-4069-a38d-d4f7d0806096",
            "attributes": {
                "id": 416,
                "uuid": "8802b428-7697-4069-a38d-d4f7d0806096",
                "revision_id": 87941,
                "langcode": "en",
                "status": true,
                "created": 1499356164,
                "parent_id": "421",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Bob Engel<br />\r\n2016</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Bob Engel<br />\n2016</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/8802b428-7697-4069-a38d-d4f7d0806096/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/8802b428-7697-4069-a38d-d4f7d0806096/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/8802b428-7697-4069-a38d-d4f7d0806096/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/8802b428-7697-4069-a38d-d4f7d0806096/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/8802b428-7697-4069-a38d-d4f7d0806096/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/8802b428-7697-4069-a38d-d4f7d0806096/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "a85c13d0-3d03-461c-8e62-dc83f80f0bf4",
                        "meta": {
                            "alt": "Bob Engel 2016",
                            "title": "",
                            "width": "117",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/8802b428-7697-4069-a38d-d4f7d0806096/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/8802b428-7697-4069-a38d-d4f7d0806096/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/8802b428-7697-4069-a38d-d4f7d0806096"
            }
        },
        {
            "type": "file--file",
            "id": "fd656252-f76b-453f-900f-d1905411ca81",
            "attributes": {
                "fid": 66,
                "uuid": "fd656252-f76b-453f-900f-d1905411ca81",
                "langcode": "en",
                "filename": "Gerald-C-Wegner-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Gerald-C-Wegner-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Gerald-C-Wegner-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 20519,
                "status": true,
                "created": 1499355781,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Gerald-C-Wegner-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/fd656252-f76b-453f-900f-d1905411ca81/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/fd656252-f76b-453f-900f-d1905411ca81/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/fd656252-f76b-453f-900f-d1905411ca81"
            }
        },
        {
            "type": "file--file",
            "id": "204abcb1-debb-4f18-a38b-a9cbc911f796",
            "attributes": {
                "fid": 71,
                "uuid": "204abcb1-debb-4f18-a38b-a9cbc911f796",
                "langcode": "en",
                "filename": "Blaine-Stockton-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Blaine-Stockton-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Blaine-Stockton-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 21150,
                "status": true,
                "created": 1499355822,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Blaine-Stockton-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/204abcb1-debb-4f18-a38b-a9cbc911f796/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/204abcb1-debb-4f18-a38b-a9cbc911f796/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/204abcb1-debb-4f18-a38b-a9cbc911f796"
            }
        },
        {
            "type": "file--file",
            "id": "c1138c8d-45ad-4d4a-b871-486d2300731e",
            "attributes": {
                "fid": 76,
                "uuid": "c1138c8d-45ad-4d4a-b871-486d2300731e",
                "langcode": "en",
                "filename": "Bob-McPhail-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Bob-McPhail-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Bob-McPhail-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 22157,
                "status": true,
                "created": 1499355850,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Bob-McPhail-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/c1138c8d-45ad-4d4a-b871-486d2300731e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/c1138c8d-45ad-4d4a-b871-486d2300731e/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/c1138c8d-45ad-4d4a-b871-486d2300731e"
            }
        },
        {
            "type": "file--file",
            "id": "611812bd-82bf-4fa8-9b29-e045c330f979",
            "attributes": {
                "fid": 81,
                "uuid": "611812bd-82bf-4fa8-9b29-e045c330f979",
                "langcode": "en",
                "filename": "Bill-Guy-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Bill-Guy-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Bill-Guy-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 24397,
                "status": true,
                "created": 1499355946,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Bill-Guy-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/611812bd-82bf-4fa8-9b29-e045c330f979/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/611812bd-82bf-4fa8-9b29-e045c330f979/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/611812bd-82bf-4fa8-9b29-e045c330f979"
            }
        },
        {
            "type": "file--file",
            "id": "a688de88-5553-4307-9587-7413c5d3d81e",
            "attributes": {
                "fid": 86,
                "uuid": "a688de88-5553-4307-9587-7413c5d3d81e",
                "langcode": "en",
                "filename": "Michael-Hacskaylo-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Michael-Hacskaylo-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Michael-Hacskaylo-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 18440,
                "status": true,
                "created": 1499355974,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Michael-Hacskaylo-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/a688de88-5553-4307-9587-7413c5d3d81e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/a688de88-5553-4307-9587-7413c5d3d81e/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/a688de88-5553-4307-9587-7413c5d3d81e"
            }
        },
        {
            "type": "file--file",
            "id": "66b87045-f949-4443-9814-ba2b6053466a",
            "attributes": {
                "fid": 91,
                "uuid": "66b87045-f949-4443-9814-ba2b6053466a",
                "langcode": "en",
                "filename": "Robert-Feragen-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Robert-Feragen-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Robert-Feragen-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 17160,
                "status": true,
                "created": 1499355997,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Robert-Feragen-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/66b87045-f949-4443-9814-ba2b6053466a/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/66b87045-f949-4443-9814-ba2b6053466a/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/66b87045-f949-4443-9814-ba2b6053466a"
            }
        },
        {
            "type": "file--file",
            "id": "d82da2cf-225b-4f14-99de-77bbb01b53b8",
            "attributes": {
                "fid": 96,
                "uuid": "d82da2cf-225b-4f14-99de-77bbb01b53b8",
                "langcode": "en",
                "filename": "Byron_Dorgan-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Byron_Dorgan-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Byron_Dorgan-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 4872,
                "status": true,
                "created": 1499356026,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Byron_Dorgan-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/d82da2cf-225b-4f14-99de-77bbb01b53b8/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/d82da2cf-225b-4f14-99de-77bbb01b53b8/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/d82da2cf-225b-4f14-99de-77bbb01b53b8"
            }
        },
        {
            "type": "file--file",
            "id": "26842908-545f-4108-a3bd-cae1b50caf9c",
            "attributes": {
                "fid": 101,
                "uuid": "26842908-545f-4108-a3bd-cae1b50caf9c",
                "langcode": "en",
                "filename": "Hub_Thompson-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Hub_Thompson-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Hub_Thompson-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 27735,
                "status": true,
                "created": 1499356047,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Hub_Thompson-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/26842908-545f-4108-a3bd-cae1b50caf9c/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/26842908-545f-4108-a3bd-cae1b50caf9c/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/26842908-545f-4108-a3bd-cae1b50caf9c"
            }
        },
        {
            "type": "file--file",
            "id": "1e7e60ee-542c-4994-bd83-57e1a8636fea",
            "attributes": {
                "fid": 106,
                "uuid": "1e7e60ee-542c-4994-bd83-57e1a8636fea",
                "langcode": "en",
                "filename": "Dave-Freudenthal-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Dave-Freudenthal-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Dave-Freudenthal-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 22298,
                "status": true,
                "created": 1499356070,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Dave-Freudenthal-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/1e7e60ee-542c-4994-bd83-57e1a8636fea/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/1e7e60ee-542c-4994-bd83-57e1a8636fea/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/1e7e60ee-542c-4994-bd83-57e1a8636fea"
            }
        },
        {
            "type": "file--file",
            "id": "232d6116-4a6d-4d2f-b143-23b2ec9e967a",
            "attributes": {
                "fid": 111,
                "uuid": "232d6116-4a6d-4d2f-b143-23b2ec9e967a",
                "langcode": "en",
                "filename": "Wally-Beyer-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Wally-Beyer-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Wally-Beyer-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 23217,
                "status": true,
                "created": 1499356100,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Wally-Beyer-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/232d6116-4a6d-4d2f-b143-23b2ec9e967a/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/232d6116-4a6d-4d2f-b143-23b2ec9e967a/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/232d6116-4a6d-4d2f-b143-23b2ec9e967a"
            }
        },
        {
            "type": "file--file",
            "id": "920a3c4c-5a97-4c0d-b638-ea1a349150b5",
            "attributes": {
                "fid": 116,
                "uuid": "920a3c4c-5a97-4c0d-b638-ea1a349150b5",
                "langcode": "en",
                "filename": "Kent-Conrad-BW-118w-2006.jpg",
                "uri": {
                    "value": "public://images/Kent-Conrad-BW-118w-2006.jpg",
                    "url": "/sites/CMS/files/images/Kent-Conrad-BW-118w-2006.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 12698,
                "status": true,
                "created": 1499356121,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Kent-Conrad-BW-118w-2006.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/920a3c4c-5a97-4c0d-b638-ea1a349150b5/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/920a3c4c-5a97-4c0d-b638-ea1a349150b5/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/920a3c4c-5a97-4c0d-b638-ea1a349150b5"
            }
        },
        {
            "type": "file--file",
            "id": "1166f9da-4204-4aee-8887-3a8478e7bc0e",
            "attributes": {
                "fid": 121,
                "uuid": "1166f9da-4204-4aee-8887-3a8478e7bc0e",
                "langcode": "en",
                "filename": "Wayne-Child-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Wayne-Child-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Wayne-Child-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 11272,
                "status": true,
                "created": 1499356150,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/Wayne-Child-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/1166f9da-4204-4aee-8887-3a8478e7bc0e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/1166f9da-4204-4aee-8887-3a8478e7bc0e/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/1166f9da-4204-4aee-8887-3a8478e7bc0e"
            }
        },
        {
            "type": "file--file",
            "id": "a85c13d0-3d03-461c-8e62-dc83f80f0bf4",
            "attributes": {
                "fid": 126,
                "uuid": "a85c13d0-3d03-461c-8e62-dc83f80f0bf4",
                "langcode": "en",
                "filename": "bob-engel-cornerstone-award.jpg",
                "uri": {
                    "value": "public://images/bob-engel-cornerstone-award.jpg",
                    "url": "/sites/CMS/files/images/bob-engel-cornerstone-award.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 4786,
                "status": true,
                "created": 1499356172,
                "changed": 1499356282,
                "url": "/sites/CMS/files/images/bob-engel-cornerstone-award.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/a85c13d0-3d03-461c-8e62-dc83f80f0bf4/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/a85c13d0-3d03-461c-8e62-dc83f80f0bf4/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/a85c13d0-3d03-461c-8e62-dc83f80f0bf4"
            }
        },
        {
            "type": "paragraph--image",
            "id": "244a99d8-321c-49b7-af6b-57a48b871178",
            "attributes": {
                "id": 431,
                "uuid": "244a99d8-321c-49b7-af6b-57a48b871178",
                "revision_id": 87956,
                "langcode": "en",
                "status": true,
                "created": 1499356386,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Orville Fossland<br />\r\n2002</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Orville Fossland<br />\n2002</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/244a99d8-321c-49b7-af6b-57a48b871178/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/244a99d8-321c-49b7-af6b-57a48b871178/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/244a99d8-321c-49b7-af6b-57a48b871178/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/244a99d8-321c-49b7-af6b-57a48b871178/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/244a99d8-321c-49b7-af6b-57a48b871178/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/244a99d8-321c-49b7-af6b-57a48b871178/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "4c6e0ea8-50cd-4bf4-8b19-270309225654",
                        "meta": {
                            "alt": "Orville Fossland  2002",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/244a99d8-321c-49b7-af6b-57a48b871178/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/244a99d8-321c-49b7-af6b-57a48b871178/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/244a99d8-321c-49b7-af6b-57a48b871178"
            }
        },
        {
            "type": "paragraph--image",
            "id": "e8be2f99-89b8-4601-937a-a186da16015d",
            "attributes": {
                "id": 436,
                "uuid": "e8be2f99-89b8-4601-937a-a186da16015d",
                "revision_id": 87961,
                "langcode": "en",
                "status": true,
                "created": 1499356416,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Ray Kruckenberg<br />\r\n2003</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Ray Kruckenberg<br />\n2003</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/e8be2f99-89b8-4601-937a-a186da16015d/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/e8be2f99-89b8-4601-937a-a186da16015d/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/e8be2f99-89b8-4601-937a-a186da16015d/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/e8be2f99-89b8-4601-937a-a186da16015d/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/e8be2f99-89b8-4601-937a-a186da16015d/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/e8be2f99-89b8-4601-937a-a186da16015d/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "c54f9bd8-94dd-4a76-844d-616bc89e99d2",
                        "meta": {
                            "alt": "Ray Kruckenberg  2003",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/e8be2f99-89b8-4601-937a-a186da16015d/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/e8be2f99-89b8-4601-937a-a186da16015d/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/e8be2f99-89b8-4601-937a-a186da16015d"
            }
        },
        {
            "type": "paragraph--image",
            "id": "5804cb04-9810-4945-97ef-ae31513bfb15",
            "attributes": {
                "id": 441,
                "uuid": "5804cb04-9810-4945-97ef-ae31513bfb15",
                "revision_id": 87966,
                "langcode": "en",
                "status": true,
                "created": 1499356434,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Frank Knutson<br />\r\n2003</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Frank Knutson<br />\n2003</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/5804cb04-9810-4945-97ef-ae31513bfb15/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/5804cb04-9810-4945-97ef-ae31513bfb15/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/5804cb04-9810-4945-97ef-ae31513bfb15/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/5804cb04-9810-4945-97ef-ae31513bfb15/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/5804cb04-9810-4945-97ef-ae31513bfb15/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/5804cb04-9810-4945-97ef-ae31513bfb15/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "634c3c4e-e242-437d-8291-c79cc6b58a07",
                        "meta": {
                            "alt": "Frank Knutson  2003",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/5804cb04-9810-4945-97ef-ae31513bfb15/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/5804cb04-9810-4945-97ef-ae31513bfb15/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/5804cb04-9810-4945-97ef-ae31513bfb15"
            }
        },
        {
            "type": "paragraph--image",
            "id": "d51985eb-99ac-4e8e-ae12-756e6c4f38c8",
            "attributes": {
                "id": 446,
                "uuid": "d51985eb-99ac-4e8e-ae12-756e6c4f38c8",
                "revision_id": 87971,
                "langcode": "en",
                "status": true,
                "created": 1499356456,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>William Keller<br />\r\n2004</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>William Keller<br />\n2004</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/d51985eb-99ac-4e8e-ae12-756e6c4f38c8/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/d51985eb-99ac-4e8e-ae12-756e6c4f38c8/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/d51985eb-99ac-4e8e-ae12-756e6c4f38c8/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/d51985eb-99ac-4e8e-ae12-756e6c4f38c8/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/d51985eb-99ac-4e8e-ae12-756e6c4f38c8/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/d51985eb-99ac-4e8e-ae12-756e6c4f38c8/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "32382e51-83a1-44c1-b4af-43dd1daa9c05",
                        "meta": {
                            "alt": "William Keller  2004",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/d51985eb-99ac-4e8e-ae12-756e6c4f38c8/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/d51985eb-99ac-4e8e-ae12-756e6c4f38c8/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/d51985eb-99ac-4e8e-ae12-756e6c4f38c8"
            }
        },
        {
            "type": "paragraph--image",
            "id": "e99bbdf2-ca35-4749-b85c-4b9cef31c5a6",
            "attributes": {
                "id": 451,
                "uuid": "e99bbdf2-ca35-4749-b85c-4b9cef31c5a6",
                "revision_id": 87976,
                "langcode": "en",
                "status": true,
                "created": 1499356473,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Gary Williamson<br />\r\n2005</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Gary Williamson<br />\n2005</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/e99bbdf2-ca35-4749-b85c-4b9cef31c5a6/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/e99bbdf2-ca35-4749-b85c-4b9cef31c5a6/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/e99bbdf2-ca35-4749-b85c-4b9cef31c5a6/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/e99bbdf2-ca35-4749-b85c-4b9cef31c5a6/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/e99bbdf2-ca35-4749-b85c-4b9cef31c5a6/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/e99bbdf2-ca35-4749-b85c-4b9cef31c5a6/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "13fbe596-ecfa-4e68-97c6-259a93d37544",
                        "meta": {
                            "alt": "Gary Williamson  2005",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/e99bbdf2-ca35-4749-b85c-4b9cef31c5a6/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/e99bbdf2-ca35-4749-b85c-4b9cef31c5a6/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/e99bbdf2-ca35-4749-b85c-4b9cef31c5a6"
            }
        },
        {
            "type": "paragraph--image",
            "id": "49b0206c-8bbe-46b0-b5cd-c215eb961acc",
            "attributes": {
                "id": 456,
                "uuid": "49b0206c-8bbe-46b0-b5cd-c215eb961acc",
                "revision_id": 87981,
                "langcode": "en",
                "status": true,
                "created": 1499356493,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Eugene Appeldorn<br />\r\n2008</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Eugene Appeldorn<br />\n2008</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/49b0206c-8bbe-46b0-b5cd-c215eb961acc/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/49b0206c-8bbe-46b0-b5cd-c215eb961acc/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/49b0206c-8bbe-46b0-b5cd-c215eb961acc/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/49b0206c-8bbe-46b0-b5cd-c215eb961acc/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/49b0206c-8bbe-46b0-b5cd-c215eb961acc/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/49b0206c-8bbe-46b0-b5cd-c215eb961acc/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "a9777de0-7f90-4585-82aa-2f679a064a86",
                        "meta": {
                            "alt": "Eugene Appeldorn  2008",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/49b0206c-8bbe-46b0-b5cd-c215eb961acc/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/49b0206c-8bbe-46b0-b5cd-c215eb961acc/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/49b0206c-8bbe-46b0-b5cd-c215eb961acc"
            }
        },
        {
            "type": "paragraph--image",
            "id": "003d8aa2-f89e-4f00-af44-3556b5302dc8",
            "attributes": {
                "id": 466,
                "uuid": "003d8aa2-f89e-4f00-af44-3556b5302dc8",
                "revision_id": 87986,
                "langcode": "en",
                "status": true,
                "created": 1499365543,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Kent Janssen<br />\r\n2008</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Kent Janssen<br />\n2008</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/003d8aa2-f89e-4f00-af44-3556b5302dc8/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/003d8aa2-f89e-4f00-af44-3556b5302dc8/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/003d8aa2-f89e-4f00-af44-3556b5302dc8/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/003d8aa2-f89e-4f00-af44-3556b5302dc8/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/003d8aa2-f89e-4f00-af44-3556b5302dc8/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/003d8aa2-f89e-4f00-af44-3556b5302dc8/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "af9c2a4a-689b-42e3-8850-114fe435afa3",
                        "meta": {
                            "alt": "Kent Janssen  2008",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/003d8aa2-f89e-4f00-af44-3556b5302dc8/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/003d8aa2-f89e-4f00-af44-3556b5302dc8/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/003d8aa2-f89e-4f00-af44-3556b5302dc8"
            }
        },
        {
            "type": "paragraph--image",
            "id": "93a67e57-5098-4192-8625-33f60e13b4bb",
            "attributes": {
                "id": 471,
                "uuid": "93a67e57-5098-4192-8625-33f60e13b4bb",
                "revision_id": 87991,
                "langcode": "en",
                "status": true,
                "created": 1499365569,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Howard Easton<br />\r\n2009</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Howard Easton<br />\n2009</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/93a67e57-5098-4192-8625-33f60e13b4bb/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/93a67e57-5098-4192-8625-33f60e13b4bb/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/93a67e57-5098-4192-8625-33f60e13b4bb/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/93a67e57-5098-4192-8625-33f60e13b4bb/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/93a67e57-5098-4192-8625-33f60e13b4bb/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/93a67e57-5098-4192-8625-33f60e13b4bb/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "6f735d41-3627-48ea-bde9-6809749c9f5a",
                        "meta": {
                            "alt": "Howard Easton  2009",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/93a67e57-5098-4192-8625-33f60e13b4bb/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/93a67e57-5098-4192-8625-33f60e13b4bb/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/93a67e57-5098-4192-8625-33f60e13b4bb"
            }
        },
        {
            "type": "paragraph--image",
            "id": "843948ea-49b8-4464-ac56-d92b8dfae695",
            "attributes": {
                "id": 476,
                "uuid": "843948ea-49b8-4464-ac56-d92b8dfae695",
                "revision_id": 87996,
                "langcode": "en",
                "status": true,
                "created": 1499365593,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Richard Fockler<br />\r\n2009</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Richard Fockler<br />\n2009</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/843948ea-49b8-4464-ac56-d92b8dfae695/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/843948ea-49b8-4464-ac56-d92b8dfae695/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/843948ea-49b8-4464-ac56-d92b8dfae695/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/843948ea-49b8-4464-ac56-d92b8dfae695/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/843948ea-49b8-4464-ac56-d92b8dfae695/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/843948ea-49b8-4464-ac56-d92b8dfae695/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "3296d9b7-afab-4935-b7a7-ee690efce870",
                        "meta": {
                            "alt": "Richard Fockler  2009",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/843948ea-49b8-4464-ac56-d92b8dfae695/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/843948ea-49b8-4464-ac56-d92b8dfae695/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/843948ea-49b8-4464-ac56-d92b8dfae695"
            }
        },
        {
            "type": "paragraph--image",
            "id": "9139b904-2d40-4caf-bcda-0598d53b54ea",
            "attributes": {
                "id": 481,
                "uuid": "9139b904-2d40-4caf-bcda-0598d53b54ea",
                "revision_id": 88001,
                "langcode": "en",
                "status": true,
                "created": 1499365613,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Ken Ziegler<br />\r\n2009</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Ken Ziegler<br />\n2009</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/9139b904-2d40-4caf-bcda-0598d53b54ea/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/9139b904-2d40-4caf-bcda-0598d53b54ea/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/9139b904-2d40-4caf-bcda-0598d53b54ea/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/9139b904-2d40-4caf-bcda-0598d53b54ea/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/9139b904-2d40-4caf-bcda-0598d53b54ea/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/9139b904-2d40-4caf-bcda-0598d53b54ea/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "2960163d-abb8-4735-9d31-60f4ea302cc1",
                        "meta": {
                            "alt": "Ken Ziegler  2009",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/9139b904-2d40-4caf-bcda-0598d53b54ea/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/9139b904-2d40-4caf-bcda-0598d53b54ea/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/9139b904-2d40-4caf-bcda-0598d53b54ea"
            }
        },
        {
            "type": "paragraph--image",
            "id": "ffd043de-2abf-4c1b-89b7-bfa11c870053",
            "attributes": {
                "id": 486,
                "uuid": "ffd043de-2abf-4c1b-89b7-bfa11c870053",
                "revision_id": 88006,
                "langcode": "en",
                "status": true,
                "created": 1499365635,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Jim Headley<br />\r\n2011</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Jim Headley<br />\n2011</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/ffd043de-2abf-4c1b-89b7-bfa11c870053/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/ffd043de-2abf-4c1b-89b7-bfa11c870053/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/ffd043de-2abf-4c1b-89b7-bfa11c870053/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/ffd043de-2abf-4c1b-89b7-bfa11c870053/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/ffd043de-2abf-4c1b-89b7-bfa11c870053/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/ffd043de-2abf-4c1b-89b7-bfa11c870053/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "8baaa240-483b-4c86-880d-306161308013",
                        "meta": {
                            "alt": "Jim Headley  2011",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/ffd043de-2abf-4c1b-89b7-bfa11c870053/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/ffd043de-2abf-4c1b-89b7-bfa11c870053/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/ffd043de-2abf-4c1b-89b7-bfa11c870053"
            }
        },
        {
            "type": "paragraph--image",
            "id": "f04a8e92-c1c9-415b-aaea-a8db47b2f924",
            "attributes": {
                "id": 491,
                "uuid": "f04a8e92-c1c9-415b-aaea-a8db47b2f924",
                "revision_id": 88011,
                "langcode": "en",
                "status": true,
                "created": 1499365657,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Clifton \"Buzz\" Hudgins<br />\r\n2014</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Clifton \"Buzz\" Hudgins<br />\n2014</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/f04a8e92-c1c9-415b-aaea-a8db47b2f924/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/f04a8e92-c1c9-415b-aaea-a8db47b2f924/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/f04a8e92-c1c9-415b-aaea-a8db47b2f924/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/f04a8e92-c1c9-415b-aaea-a8db47b2f924/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/f04a8e92-c1c9-415b-aaea-a8db47b2f924/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/f04a8e92-c1c9-415b-aaea-a8db47b2f924/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "d71ef3d2-eba9-4b1f-afef-ce965128758c",
                        "meta": {
                            "alt": "Clifton \"Buzz\" Hudgins 2014",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/f04a8e92-c1c9-415b-aaea-a8db47b2f924/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/f04a8e92-c1c9-415b-aaea-a8db47b2f924/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/f04a8e92-c1c9-415b-aaea-a8db47b2f924"
            }
        },
        {
            "type": "paragraph--image",
            "id": "4c5f9580-f5d4-4d55-97cc-c9d80705e5fc",
            "attributes": {
                "id": 496,
                "uuid": "4c5f9580-f5d4-4d55-97cc-c9d80705e5fc",
                "revision_id": 88016,
                "langcode": "en",
                "status": true,
                "created": 1499365680,
                "parent_id": "461",
                "parent_type": "paragraph",
                "parent_field_name": "field_gallery_image",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_caption": {
                    "value": "<p>Daryl Hill&nbsp;<br />\r\n2014</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Daryl Hill <br />\n2014</p>\n"
                },
                "field_website_link": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "87ac238d-a191-4617-bb34-6a19f8436979"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/4c5f9580-f5d4-4d55-97cc-c9d80705e5fc/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/4c5f9580-f5d4-4d55-97cc-c9d80705e5fc/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/4c5f9580-f5d4-4d55-97cc-c9d80705e5fc/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/4c5f9580-f5d4-4d55-97cc-c9d80705e5fc/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/4c5f9580-f5d4-4d55-97cc-c9d80705e5fc/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/4c5f9580-f5d4-4d55-97cc-c9d80705e5fc/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "1374ad2c-97b6-438b-8e3e-339f204bd8f8",
                        "meta": {
                            "alt": "Daryl Hill  ​​​​​​​2014",
                            "title": "",
                            "width": "118",
                            "height": "151"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image/4c5f9580-f5d4-4d55-97cc-c9d80705e5fc/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image/4c5f9580-f5d4-4d55-97cc-c9d80705e5fc/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image/4c5f9580-f5d4-4d55-97cc-c9d80705e5fc"
            }
        },
        {
            "type": "file--file",
            "id": "4c6e0ea8-50cd-4bf4-8b19-270309225654",
            "attributes": {
                "fid": 131,
                "uuid": "4c6e0ea8-50cd-4bf4-8b19-270309225654",
                "langcode": "en",
                "filename": "Orville-Fossland-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Orville-Fossland-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Orville-Fossland-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 23494,
                "status": true,
                "created": 1499356399,
                "changed": 1499356546,
                "url": "/sites/CMS/files/images/Orville-Fossland-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/4c6e0ea8-50cd-4bf4-8b19-270309225654/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/4c6e0ea8-50cd-4bf4-8b19-270309225654/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/4c6e0ea8-50cd-4bf4-8b19-270309225654"
            }
        },
        {
            "type": "file--file",
            "id": "c54f9bd8-94dd-4a76-844d-616bc89e99d2",
            "attributes": {
                "fid": 136,
                "uuid": "c54f9bd8-94dd-4a76-844d-616bc89e99d2",
                "langcode": "en",
                "filename": "Ray-Kruckenberg-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Ray-Kruckenberg-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Ray-Kruckenberg-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 23324,
                "status": true,
                "created": 1499356425,
                "changed": 1499356546,
                "url": "/sites/CMS/files/images/Ray-Kruckenberg-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/c54f9bd8-94dd-4a76-844d-616bc89e99d2/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/c54f9bd8-94dd-4a76-844d-616bc89e99d2/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/c54f9bd8-94dd-4a76-844d-616bc89e99d2"
            }
        },
        {
            "type": "file--file",
            "id": "634c3c4e-e242-437d-8291-c79cc6b58a07",
            "attributes": {
                "fid": 141,
                "uuid": "634c3c4e-e242-437d-8291-c79cc6b58a07",
                "langcode": "en",
                "filename": "Frank-Knutson-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Frank-Knutson-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Frank-Knutson-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 22506,
                "status": true,
                "created": 1499356447,
                "changed": 1499356546,
                "url": "/sites/CMS/files/images/Frank-Knutson-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/634c3c4e-e242-437d-8291-c79cc6b58a07/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/634c3c4e-e242-437d-8291-c79cc6b58a07/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/634c3c4e-e242-437d-8291-c79cc6b58a07"
            }
        },
        {
            "type": "file--file",
            "id": "32382e51-83a1-44c1-b4af-43dd1daa9c05",
            "attributes": {
                "fid": 146,
                "uuid": "32382e51-83a1-44c1-b4af-43dd1daa9c05",
                "langcode": "en",
                "filename": "Bill-Keller-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Bill-Keller-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Bill-Keller-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 22385,
                "status": true,
                "created": 1499356466,
                "changed": 1499356546,
                "url": "/sites/CMS/files/images/Bill-Keller-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/32382e51-83a1-44c1-b4af-43dd1daa9c05/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/32382e51-83a1-44c1-b4af-43dd1daa9c05/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/32382e51-83a1-44c1-b4af-43dd1daa9c05"
            }
        },
        {
            "type": "file--file",
            "id": "13fbe596-ecfa-4e68-97c6-259a93d37544",
            "attributes": {
                "fid": 151,
                "uuid": "13fbe596-ecfa-4e68-97c6-259a93d37544",
                "langcode": "en",
                "filename": "Gary-Williamson-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Gary-Williamson-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Gary-Williamson-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 22903,
                "status": true,
                "created": 1499356484,
                "changed": 1499356546,
                "url": "/sites/CMS/files/images/Gary-Williamson-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/13fbe596-ecfa-4e68-97c6-259a93d37544/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/13fbe596-ecfa-4e68-97c6-259a93d37544/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/13fbe596-ecfa-4e68-97c6-259a93d37544"
            }
        },
        {
            "type": "file--file",
            "id": "a9777de0-7f90-4585-82aa-2f679a064a86",
            "attributes": {
                "fid": 156,
                "uuid": "a9777de0-7f90-4585-82aa-2f679a064a86",
                "langcode": "en",
                "filename": "Eugene-Appeldorn-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Eugene-Appeldorn-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Eugene-Appeldorn-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 18427,
                "status": true,
                "created": 1499356535,
                "changed": 1499356546,
                "url": "/sites/CMS/files/images/Eugene-Appeldorn-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/a9777de0-7f90-4585-82aa-2f679a064a86/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/a9777de0-7f90-4585-82aa-2f679a064a86/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/a9777de0-7f90-4585-82aa-2f679a064a86"
            }
        },
        {
            "type": "file--file",
            "id": "af9c2a4a-689b-42e3-8850-114fe435afa3",
            "attributes": {
                "fid": 161,
                "uuid": "af9c2a4a-689b-42e3-8850-114fe435afa3",
                "langcode": "en",
                "filename": "Kent-Janssen-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Kent-Janssen-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Kent-Janssen-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 19608,
                "status": true,
                "created": 1499365556,
                "changed": 1499365785,
                "url": "/sites/CMS/files/images/Kent-Janssen-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/af9c2a4a-689b-42e3-8850-114fe435afa3/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/af9c2a4a-689b-42e3-8850-114fe435afa3/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/af9c2a4a-689b-42e3-8850-114fe435afa3"
            }
        },
        {
            "type": "file--file",
            "id": "6f735d41-3627-48ea-bde9-6809749c9f5a",
            "attributes": {
                "fid": 166,
                "uuid": "6f735d41-3627-48ea-bde9-6809749c9f5a",
                "langcode": "en",
                "filename": "Howard-Easton-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Howard-Easton-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Howard-Easton-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 19008,
                "status": true,
                "created": 1499365582,
                "changed": 1499365785,
                "url": "/sites/CMS/files/images/Howard-Easton-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/6f735d41-3627-48ea-bde9-6809749c9f5a/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/6f735d41-3627-48ea-bde9-6809749c9f5a/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/6f735d41-3627-48ea-bde9-6809749c9f5a"
            }
        },
        {
            "type": "file--file",
            "id": "3296d9b7-afab-4935-b7a7-ee690efce870",
            "attributes": {
                "fid": 171,
                "uuid": "3296d9b7-afab-4935-b7a7-ee690efce870",
                "langcode": "en",
                "filename": "Rich-Fockler-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Rich-Fockler-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Rich-Fockler-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 19072,
                "status": true,
                "created": 1499365606,
                "changed": 1499365785,
                "url": "/sites/CMS/files/images/Rich-Fockler-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/3296d9b7-afab-4935-b7a7-ee690efce870/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/3296d9b7-afab-4935-b7a7-ee690efce870/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/3296d9b7-afab-4935-b7a7-ee690efce870"
            }
        },
        {
            "type": "file--file",
            "id": "2960163d-abb8-4735-9d31-60f4ea302cc1",
            "attributes": {
                "fid": 176,
                "uuid": "2960163d-abb8-4735-9d31-60f4ea302cc1",
                "langcode": "en",
                "filename": "Ken-Ziegler-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Ken-Ziegler-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Ken-Ziegler-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 19034,
                "status": true,
                "created": 1499365627,
                "changed": 1499365785,
                "url": "/sites/CMS/files/images/Ken-Ziegler-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/2960163d-abb8-4735-9d31-60f4ea302cc1/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/2960163d-abb8-4735-9d31-60f4ea302cc1/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/2960163d-abb8-4735-9d31-60f4ea302cc1"
            }
        },
        {
            "type": "file--file",
            "id": "8baaa240-483b-4c86-880d-306161308013",
            "attributes": {
                "fid": 181,
                "uuid": "8baaa240-483b-4c86-880d-306161308013",
                "langcode": "en",
                "filename": "Jim-Headley-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Jim-Headley-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Jim-Headley-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 33115,
                "status": true,
                "created": 1499365650,
                "changed": 1499365785,
                "url": "/sites/CMS/files/images/Jim-Headley-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/8baaa240-483b-4c86-880d-306161308013/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/8baaa240-483b-4c86-880d-306161308013/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/8baaa240-483b-4c86-880d-306161308013"
            }
        },
        {
            "type": "file--file",
            "id": "d71ef3d2-eba9-4b1f-afef-ce965128758c",
            "attributes": {
                "fid": 186,
                "uuid": "d71ef3d2-eba9-4b1f-afef-ce965128758c",
                "langcode": "en",
                "filename": "Clifton-Buzz-Hudgins-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Clifton-Buzz-Hudgins-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Clifton-Buzz-Hudgins-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 10749,
                "status": true,
                "created": 1499365673,
                "changed": 1499365785,
                "url": "/sites/CMS/files/images/Clifton-Buzz-Hudgins-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/d71ef3d2-eba9-4b1f-afef-ce965128758c/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/d71ef3d2-eba9-4b1f-afef-ce965128758c/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/d71ef3d2-eba9-4b1f-afef-ce965128758c"
            }
        },
        {
            "type": "file--file",
            "id": "1374ad2c-97b6-438b-8e3e-339f204bd8f8",
            "attributes": {
                "fid": 191,
                "uuid": "1374ad2c-97b6-438b-8e3e-339f204bd8f8",
                "langcode": "en",
                "filename": "Daryl-Hill-BW-118w.jpg",
                "uri": {
                    "value": "public://images/Daryl-Hill-BW-118w.jpg",
                    "url": "/sites/CMS/files/images/Daryl-Hill-BW-118w.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 11833,
                "status": true,
                "created": 1499365693,
                "changed": 1499365785,
                "url": "/sites/CMS/files/images/Daryl-Hill-BW-118w.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/1374ad2c-97b6-438b-8e3e-339f204bd8f8/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/1374ad2c-97b6-438b-8e3e-339f204bd8f8/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/1374ad2c-97b6-438b-8e3e-339f204bd8f8"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "db7ab17b-9bc7-4a99-b390-15f633ebbde7",
            "attributes": {
                "id": 16046,
                "uuid": "db7ab17b-9bc7-4a99-b390-15f633ebbde7",
                "revision_id": 88026,
                "langcode": "en",
                "status": true,
                "created": 1531251223,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2002-01-01",
                "field_title": "Gerald C. Wegner",
                "field_year": 2002
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/db7ab17b-9bc7-4a99-b390-15f633ebbde7/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/db7ab17b-9bc7-4a99-b390-15f633ebbde7/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/db7ab17b-9bc7-4a99-b390-15f633ebbde7/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/db7ab17b-9bc7-4a99-b390-15f633ebbde7/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/db7ab17b-9bc7-4a99-b390-15f633ebbde7/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/db7ab17b-9bc7-4a99-b390-15f633ebbde7/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "5ee7dde7-ad8b-44b1-9778-4ae8da1d2748",
                        "meta": {
                            "alt": "Gerald C. Wegner",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/db7ab17b-9bc7-4a99-b390-15f633ebbde7/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/db7ab17b-9bc7-4a99-b390-15f633ebbde7/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/db7ab17b-9bc7-4a99-b390-15f633ebbde7"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "177b977c-208d-42b1-8a4a-f0648c84e57f",
            "attributes": {
                "id": 16051,
                "uuid": "177b977c-208d-42b1-8a4a-f0648c84e57f",
                "revision_id": 88031,
                "langcode": "en",
                "status": true,
                "created": 1531251402,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2003-01-31",
                "field_title": "Blaine Stockton",
                "field_year": 2003
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/177b977c-208d-42b1-8a4a-f0648c84e57f/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/177b977c-208d-42b1-8a4a-f0648c84e57f/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/177b977c-208d-42b1-8a4a-f0648c84e57f/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/177b977c-208d-42b1-8a4a-f0648c84e57f/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/177b977c-208d-42b1-8a4a-f0648c84e57f/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/177b977c-208d-42b1-8a4a-f0648c84e57f/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "9bc3c6e3-ac6d-44f5-a0ce-fae6a9008823",
                        "meta": {
                            "alt": "Blaine Stockton ",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/177b977c-208d-42b1-8a4a-f0648c84e57f/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/177b977c-208d-42b1-8a4a-f0648c84e57f/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/177b977c-208d-42b1-8a4a-f0648c84e57f"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "d0d6ed63-0c56-47c9-93e8-b0a27ca5793b",
            "attributes": {
                "id": 16061,
                "uuid": "d0d6ed63-0c56-47c9-93e8-b0a27ca5793b",
                "revision_id": 88036,
                "langcode": "en",
                "status": true,
                "created": 1531254707,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Robert L. McPhail",
                "field_year": 2005
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d0d6ed63-0c56-47c9-93e8-b0a27ca5793b/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d0d6ed63-0c56-47c9-93e8-b0a27ca5793b/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d0d6ed63-0c56-47c9-93e8-b0a27ca5793b/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d0d6ed63-0c56-47c9-93e8-b0a27ca5793b/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d0d6ed63-0c56-47c9-93e8-b0a27ca5793b/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d0d6ed63-0c56-47c9-93e8-b0a27ca5793b/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "2120dc4c-924f-4622-a546-8a052c47911e",
                        "meta": {
                            "alt": "Robert L. McPhail",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d0d6ed63-0c56-47c9-93e8-b0a27ca5793b/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d0d6ed63-0c56-47c9-93e8-b0a27ca5793b/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d0d6ed63-0c56-47c9-93e8-b0a27ca5793b"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "88ef46fd-0698-4529-9316-fdd92b0e3449",
            "attributes": {
                "id": 16066,
                "uuid": "88ef46fd-0698-4529-9316-fdd92b0e3449",
                "revision_id": 88041,
                "langcode": "en",
                "status": true,
                "created": 1531254814,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "William L. Guy",
                "field_year": 2006
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/88ef46fd-0698-4529-9316-fdd92b0e3449/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/88ef46fd-0698-4529-9316-fdd92b0e3449/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/88ef46fd-0698-4529-9316-fdd92b0e3449/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/88ef46fd-0698-4529-9316-fdd92b0e3449/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/88ef46fd-0698-4529-9316-fdd92b0e3449/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/88ef46fd-0698-4529-9316-fdd92b0e3449/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "1e618fac-b9e8-429f-a33c-e28f1cb37b0c",
                        "meta": {
                            "alt": "William L. Guy",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/88ef46fd-0698-4529-9316-fdd92b0e3449/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/88ef46fd-0698-4529-9316-fdd92b0e3449/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/88ef46fd-0698-4529-9316-fdd92b0e3449"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "d07519c1-c8eb-4040-8a4f-d9576bfe2340",
            "attributes": {
                "id": 16071,
                "uuid": "d07519c1-c8eb-4040-8a4f-d9576bfe2340",
                "revision_id": 88046,
                "langcode": "en",
                "status": true,
                "created": 1531254845,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Michael Hacskylo",
                "field_year": 2007
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d07519c1-c8eb-4040-8a4f-d9576bfe2340/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d07519c1-c8eb-4040-8a4f-d9576bfe2340/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d07519c1-c8eb-4040-8a4f-d9576bfe2340/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d07519c1-c8eb-4040-8a4f-d9576bfe2340/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d07519c1-c8eb-4040-8a4f-d9576bfe2340/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d07519c1-c8eb-4040-8a4f-d9576bfe2340/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "49ddc524-6f80-4904-8224-c057c01f4289",
                        "meta": {
                            "alt": "Michael Hacskylo",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d07519c1-c8eb-4040-8a4f-d9576bfe2340/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d07519c1-c8eb-4040-8a4f-d9576bfe2340/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/d07519c1-c8eb-4040-8a4f-d9576bfe2340"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "3f778603-24c7-4c8a-9e3a-90602ad67ea5",
            "attributes": {
                "id": 16076,
                "uuid": "3f778603-24c7-4c8a-9e3a-90602ad67ea5",
                "revision_id": 88051,
                "langcode": "en",
                "status": true,
                "created": 1531254872,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Robert Feragen",
                "field_year": 2008
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f778603-24c7-4c8a-9e3a-90602ad67ea5/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f778603-24c7-4c8a-9e3a-90602ad67ea5/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f778603-24c7-4c8a-9e3a-90602ad67ea5/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f778603-24c7-4c8a-9e3a-90602ad67ea5/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f778603-24c7-4c8a-9e3a-90602ad67ea5/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f778603-24c7-4c8a-9e3a-90602ad67ea5/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "4c877a0f-19f9-4db5-9cc6-6ba9d958c4cf",
                        "meta": {
                            "alt": "Robert Feragen",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f778603-24c7-4c8a-9e3a-90602ad67ea5/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f778603-24c7-4c8a-9e3a-90602ad67ea5/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f778603-24c7-4c8a-9e3a-90602ad67ea5"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "11ffd6f9-573e-489a-8beb-cb700d88eeb5",
            "attributes": {
                "id": 16081,
                "uuid": "11ffd6f9-573e-489a-8beb-cb700d88eeb5",
                "revision_id": 88056,
                "langcode": "en",
                "status": true,
                "created": 1531254899,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Byron Dorgan",
                "field_year": 2010
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/11ffd6f9-573e-489a-8beb-cb700d88eeb5/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/11ffd6f9-573e-489a-8beb-cb700d88eeb5/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/11ffd6f9-573e-489a-8beb-cb700d88eeb5/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/11ffd6f9-573e-489a-8beb-cb700d88eeb5/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/11ffd6f9-573e-489a-8beb-cb700d88eeb5/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/11ffd6f9-573e-489a-8beb-cb700d88eeb5/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "851d0da8-112e-4375-9d65-553bf9d109da",
                        "meta": {
                            "alt": "Byron Dorgan",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/11ffd6f9-573e-489a-8beb-cb700d88eeb5/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/11ffd6f9-573e-489a-8beb-cb700d88eeb5/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/11ffd6f9-573e-489a-8beb-cb700d88eeb5"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "7c7d6429-9b46-42d2-882a-74bba8bd8af7",
            "attributes": {
                "id": 16086,
                "uuid": "7c7d6429-9b46-42d2-882a-74bba8bd8af7",
                "revision_id": 88061,
                "langcode": "en",
                "status": true,
                "created": 1531254922,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Hub Thompson",
                "field_year": 2010
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7c7d6429-9b46-42d2-882a-74bba8bd8af7/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7c7d6429-9b46-42d2-882a-74bba8bd8af7/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7c7d6429-9b46-42d2-882a-74bba8bd8af7/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7c7d6429-9b46-42d2-882a-74bba8bd8af7/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7c7d6429-9b46-42d2-882a-74bba8bd8af7/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7c7d6429-9b46-42d2-882a-74bba8bd8af7/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "59a27fc1-ed05-4002-b5e3-608b5e8978c8",
                        "meta": {
                            "alt": "Hub Thompson",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7c7d6429-9b46-42d2-882a-74bba8bd8af7/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7c7d6429-9b46-42d2-882a-74bba8bd8af7/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7c7d6429-9b46-42d2-882a-74bba8bd8af7"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "753bae4f-ac51-4e81-906c-4d8b91e4460f",
            "attributes": {
                "id": 16091,
                "uuid": "753bae4f-ac51-4e81-906c-4d8b91e4460f",
                "revision_id": 88066,
                "langcode": "en",
                "status": true,
                "created": 1531254956,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Dave Freudenthal",
                "field_year": 2011
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/753bae4f-ac51-4e81-906c-4d8b91e4460f/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/753bae4f-ac51-4e81-906c-4d8b91e4460f/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/753bae4f-ac51-4e81-906c-4d8b91e4460f/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/753bae4f-ac51-4e81-906c-4d8b91e4460f/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/753bae4f-ac51-4e81-906c-4d8b91e4460f/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/753bae4f-ac51-4e81-906c-4d8b91e4460f/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "826a090d-ade8-4a00-a548-33de18fd3c09",
                        "meta": {
                            "alt": "Dave Freudenthal",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/753bae4f-ac51-4e81-906c-4d8b91e4460f/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/753bae4f-ac51-4e81-906c-4d8b91e4460f/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/753bae4f-ac51-4e81-906c-4d8b91e4460f"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "3f85365f-a06d-4d7d-ae90-210bf8241c7e",
            "attributes": {
                "id": 16096,
                "uuid": "3f85365f-a06d-4d7d-ae90-210bf8241c7e",
                "revision_id": 88071,
                "langcode": "en",
                "status": true,
                "created": 1531254982,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Wally Beyer",
                "field_year": 2011
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f85365f-a06d-4d7d-ae90-210bf8241c7e/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f85365f-a06d-4d7d-ae90-210bf8241c7e/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f85365f-a06d-4d7d-ae90-210bf8241c7e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f85365f-a06d-4d7d-ae90-210bf8241c7e/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f85365f-a06d-4d7d-ae90-210bf8241c7e/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f85365f-a06d-4d7d-ae90-210bf8241c7e/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "ad8333fc-2cb0-41a3-a42a-1a1b52fa5f14",
                        "meta": {
                            "alt": "Wally Beyer",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f85365f-a06d-4d7d-ae90-210bf8241c7e/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f85365f-a06d-4d7d-ae90-210bf8241c7e/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/3f85365f-a06d-4d7d-ae90-210bf8241c7e"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2",
            "attributes": {
                "id": 16101,
                "uuid": "2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2",
                "revision_id": 88076,
                "langcode": "en",
                "status": true,
                "created": 1531255001,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Kent Conrad",
                "field_year": 2012
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "ddc047be-6f4e-44bd-b137-ccb24d12ea86",
                        "meta": {
                            "alt": "Kent Conrad",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2d75c7bd-a77b-4e7b-ab5c-7cee7b9172a2"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "9543b22d-9007-4245-8ad9-11dee272fc99",
            "attributes": {
                "id": 16106,
                "uuid": "9543b22d-9007-4245-8ad9-11dee272fc99",
                "revision_id": 88081,
                "langcode": "en",
                "status": true,
                "created": 1531255030,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Wayne Child",
                "field_year": 2014
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9543b22d-9007-4245-8ad9-11dee272fc99/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9543b22d-9007-4245-8ad9-11dee272fc99/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9543b22d-9007-4245-8ad9-11dee272fc99/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9543b22d-9007-4245-8ad9-11dee272fc99/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9543b22d-9007-4245-8ad9-11dee272fc99/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9543b22d-9007-4245-8ad9-11dee272fc99/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "65f62b0c-0a44-4f5d-9de3-e1dc4a138894",
                        "meta": {
                            "alt": "Wayne Child",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9543b22d-9007-4245-8ad9-11dee272fc99/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9543b22d-9007-4245-8ad9-11dee272fc99/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9543b22d-9007-4245-8ad9-11dee272fc99"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "f316f08b-5996-415f-8fc4-7800512c02c2",
            "attributes": {
                "id": 16111,
                "uuid": "f316f08b-5996-415f-8fc4-7800512c02c2",
                "revision_id": 88086,
                "langcode": "en",
                "status": true,
                "created": 1531255063,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Bob Engel",
                "field_year": 2016
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/f316f08b-5996-415f-8fc4-7800512c02c2/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/f316f08b-5996-415f-8fc4-7800512c02c2/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/f316f08b-5996-415f-8fc4-7800512c02c2/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/f316f08b-5996-415f-8fc4-7800512c02c2/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/f316f08b-5996-415f-8fc4-7800512c02c2/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/f316f08b-5996-415f-8fc4-7800512c02c2/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "c6da9856-12f7-42f9-97f1-69e4d5409273",
                        "meta": {
                            "alt": "Bob Engel",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/f316f08b-5996-415f-8fc4-7800512c02c2/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/f316f08b-5996-415f-8fc4-7800512c02c2/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/f316f08b-5996-415f-8fc4-7800512c02c2"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "56214377-fcc5-40d0-8e3b-5c1c37e0369e",
            "attributes": {
                "id": 16116,
                "uuid": "56214377-fcc5-40d0-8e3b-5c1c37e0369e",
                "revision_id": 88091,
                "langcode": "en",
                "status": true,
                "created": 1531255089,
                "parent_id": "16056",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_date": "2018-07-10",
                "field_title": "Bob Harris",
                "field_year": 2017
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/56214377-fcc5-40d0-8e3b-5c1c37e0369e/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/56214377-fcc5-40d0-8e3b-5c1c37e0369e/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/56214377-fcc5-40d0-8e3b-5c1c37e0369e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/56214377-fcc5-40d0-8e3b-5c1c37e0369e/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/56214377-fcc5-40d0-8e3b-5c1c37e0369e/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/56214377-fcc5-40d0-8e3b-5c1c37e0369e/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "dc04bd55-6339-4e84-ba88-e6fe6c2b391b",
                        "meta": {
                            "alt": "Bob Harris",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/56214377-fcc5-40d0-8e3b-5c1c37e0369e/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/56214377-fcc5-40d0-8e3b-5c1c37e0369e/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/56214377-fcc5-40d0-8e3b-5c1c37e0369e"
            }
        },
        {
            "type": "file--file",
            "id": "5ee7dde7-ad8b-44b1-9778-4ae8da1d2748",
            "attributes": {
                "fid": 10506,
                "uuid": "5ee7dde7-ad8b-44b1-9778-4ae8da1d2748",
                "langcode": "en",
                "filename": "Wegner-Gerald-258px-Portrait.jpg",
                "uri": {
                    "value": "public://award-winners/Wegner-Gerald-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/award-winners/Wegner-Gerald-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 17366,
                "status": true,
                "created": 1531251349,
                "changed": 1531251607,
                "url": "/sites/CMS/files/award-winners/Wegner-Gerald-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/5ee7dde7-ad8b-44b1-9778-4ae8da1d2748/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/5ee7dde7-ad8b-44b1-9778-4ae8da1d2748/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/5ee7dde7-ad8b-44b1-9778-4ae8da1d2748"
            }
        },
        {
            "type": "file--file",
            "id": "9bc3c6e3-ac6d-44f5-a0ce-fae6a9008823",
            "attributes": {
                "fid": 10511,
                "uuid": "9bc3c6e3-ac6d-44f5-a0ce-fae6a9008823",
                "langcode": "en",
                "filename": "Stockton-Blaine-258px-Portrait.jpg",
                "uri": {
                    "value": "public://award-winners/Stockton-Blaine-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/award-winners/Stockton-Blaine-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 17294,
                "status": true,
                "created": 1531251431,
                "changed": 1531251607,
                "url": "/sites/CMS/files/award-winners/Stockton-Blaine-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/9bc3c6e3-ac6d-44f5-a0ce-fae6a9008823/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/9bc3c6e3-ac6d-44f5-a0ce-fae6a9008823/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/9bc3c6e3-ac6d-44f5-a0ce-fae6a9008823"
            }
        },
        {
            "type": "file--file",
            "id": "2120dc4c-924f-4622-a546-8a052c47911e",
            "attributes": {
                "fid": 10696,
                "uuid": "2120dc4c-924f-4622-a546-8a052c47911e",
                "langcode": "en",
                "filename": "McPhail-Robert-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/McPhail-Robert-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/McPhail-Robert-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 17481,
                "status": true,
                "created": 1531253202,
                "changed": 1531253202,
                "url": "/sites/CMS/files/images/people/McPhail-Robert-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/2120dc4c-924f-4622-a546-8a052c47911e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/2120dc4c-924f-4622-a546-8a052c47911e/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/2120dc4c-924f-4622-a546-8a052c47911e"
            }
        },
        {
            "type": "file--file",
            "id": "1e618fac-b9e8-429f-a33c-e28f1cb37b0c",
            "attributes": {
                "fid": 10646,
                "uuid": "1e618fac-b9e8-429f-a33c-e28f1cb37b0c",
                "langcode": "en",
                "filename": "Guy-William-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Guy-William-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Guy-William-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 17151,
                "status": true,
                "created": 1531253199,
                "changed": 1531253199,
                "url": "/sites/CMS/files/images/people/Guy-William-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/1e618fac-b9e8-429f-a33c-e28f1cb37b0c/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/1e618fac-b9e8-429f-a33c-e28f1cb37b0c/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/1e618fac-b9e8-429f-a33c-e28f1cb37b0c"
            }
        },
        {
            "type": "file--file",
            "id": "49ddc524-6f80-4904-8224-c057c01f4289",
            "attributes": {
                "fid": 10651,
                "uuid": "49ddc524-6f80-4904-8224-c057c01f4289",
                "langcode": "en",
                "filename": "Hacskylo-Michael-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Hacskylo-Michael-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Hacskylo-Michael-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 13513,
                "status": true,
                "created": 1531253199,
                "changed": 1531253199,
                "url": "/sites/CMS/files/images/people/Hacskylo-Michael-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/49ddc524-6f80-4904-8224-c057c01f4289/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/49ddc524-6f80-4904-8224-c057c01f4289/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/49ddc524-6f80-4904-8224-c057c01f4289"
            }
        },
        {
            "type": "file--file",
            "id": "4c877a0f-19f9-4db5-9cc6-6ba9d958c4cf",
            "attributes": {
                "fid": 10626,
                "uuid": "4c877a0f-19f9-4db5-9cc6-6ba9d958c4cf",
                "langcode": "en",
                "filename": "Feragen-Robert-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Feragen-Robert-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Feragen-Robert-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 18097,
                "status": true,
                "created": 1531253197,
                "changed": 1531253197,
                "url": "/sites/CMS/files/images/people/Feragen-Robert-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/4c877a0f-19f9-4db5-9cc6-6ba9d958c4cf/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/4c877a0f-19f9-4db5-9cc6-6ba9d958c4cf/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/4c877a0f-19f9-4db5-9cc6-6ba9d958c4cf"
            }
        },
        {
            "type": "file--file",
            "id": "851d0da8-112e-4375-9d65-553bf9d109da",
            "attributes": {
                "fid": 10611,
                "uuid": "851d0da8-112e-4375-9d65-553bf9d109da",
                "langcode": "en",
                "filename": "Dorgan-Byron-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Dorgan-Byron-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Dorgan-Byron-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 21357,
                "status": true,
                "created": 1531253196,
                "changed": 1531253196,
                "url": "/sites/CMS/files/images/people/Dorgan-Byron-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/851d0da8-112e-4375-9d65-553bf9d109da/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/851d0da8-112e-4375-9d65-553bf9d109da/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/851d0da8-112e-4375-9d65-553bf9d109da"
            }
        },
        {
            "type": "file--file",
            "id": "59a27fc1-ed05-4002-b5e3-608b5e8978c8",
            "attributes": {
                "fid": 10706,
                "uuid": "59a27fc1-ed05-4002-b5e3-608b5e8978c8",
                "langcode": "en",
                "filename": "Thompson-Hub-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Thompson-Hub-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Thompson-Hub-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 15579,
                "status": true,
                "created": 1531253203,
                "changed": 1531253203,
                "url": "/sites/CMS/files/images/people/Thompson-Hub-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/59a27fc1-ed05-4002-b5e3-608b5e8978c8/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/59a27fc1-ed05-4002-b5e3-608b5e8978c8/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/59a27fc1-ed05-4002-b5e3-608b5e8978c8"
            }
        },
        {
            "type": "file--file",
            "id": "826a090d-ade8-4a00-a548-33de18fd3c09",
            "attributes": {
                "fid": 10641,
                "uuid": "826a090d-ade8-4a00-a548-33de18fd3c09",
                "langcode": "en",
                "filename": "Freudenthal-Dave-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Freudenthal-Dave-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Freudenthal-Dave-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 20955,
                "status": true,
                "created": 1531253198,
                "changed": 1531253198,
                "url": "/sites/CMS/files/images/people/Freudenthal-Dave-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/826a090d-ade8-4a00-a548-33de18fd3c09/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/826a090d-ade8-4a00-a548-33de18fd3c09/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/826a090d-ade8-4a00-a548-33de18fd3c09"
            }
        },
        {
            "type": "file--file",
            "id": "ad8333fc-2cb0-41a3-a42a-1a1b52fa5f14",
            "attributes": {
                "fid": 10596,
                "uuid": "ad8333fc-2cb0-41a3-a42a-1a1b52fa5f14",
                "langcode": "en",
                "filename": "Beyer-Wally-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Beyer-Wally-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Beyer-Wally-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 13775,
                "status": true,
                "created": 1531253196,
                "changed": 1531253196,
                "url": "/sites/CMS/files/images/people/Beyer-Wally-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/ad8333fc-2cb0-41a3-a42a-1a1b52fa5f14/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/ad8333fc-2cb0-41a3-a42a-1a1b52fa5f14/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/ad8333fc-2cb0-41a3-a42a-1a1b52fa5f14"
            }
        },
        {
            "type": "file--file",
            "id": "ddc047be-6f4e-44bd-b137-ccb24d12ea86",
            "attributes": {
                "fid": 10606,
                "uuid": "ddc047be-6f4e-44bd-b137-ccb24d12ea86",
                "langcode": "en",
                "filename": "Conrad-Kent-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Conrad-Kent-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Conrad-Kent-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 19169,
                "status": true,
                "created": 1531253196,
                "changed": 1531253196,
                "url": "/sites/CMS/files/images/people/Conrad-Kent-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/ddc047be-6f4e-44bd-b137-ccb24d12ea86/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/ddc047be-6f4e-44bd-b137-ccb24d12ea86/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/ddc047be-6f4e-44bd-b137-ccb24d12ea86"
            }
        },
        {
            "type": "file--file",
            "id": "65f62b0c-0a44-4f5d-9de3-e1dc4a138894",
            "attributes": {
                "fid": 10601,
                "uuid": "65f62b0c-0a44-4f5d-9de3-e1dc4a138894",
                "langcode": "en",
                "filename": "Child-Wayne-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Child-Wayne-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Child-Wayne-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 18911,
                "status": true,
                "created": 1531253196,
                "changed": 1531253196,
                "url": "/sites/CMS/files/images/people/Child-Wayne-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/65f62b0c-0a44-4f5d-9de3-e1dc4a138894/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/65f62b0c-0a44-4f5d-9de3-e1dc4a138894/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/65f62b0c-0a44-4f5d-9de3-e1dc4a138894"
            }
        },
        {
            "type": "file--file",
            "id": "c6da9856-12f7-42f9-97f1-69e4d5409273",
            "attributes": {
                "fid": 10621,
                "uuid": "c6da9856-12f7-42f9-97f1-69e4d5409273",
                "langcode": "en",
                "filename": "Engel-Bob-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Engel-Bob-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Engel-Bob-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 14172,
                "status": true,
                "created": 1531253197,
                "changed": 1531253197,
                "url": "/sites/CMS/files/images/people/Engel-Bob-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/c6da9856-12f7-42f9-97f1-69e4d5409273/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/c6da9856-12f7-42f9-97f1-69e4d5409273/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/c6da9856-12f7-42f9-97f1-69e4d5409273"
            }
        },
        {
            "type": "file--file",
            "id": "dc04bd55-6339-4e84-ba88-e6fe6c2b391b",
            "attributes": {
                "fid": 10656,
                "uuid": "dc04bd55-6339-4e84-ba88-e6fe6c2b391b",
                "langcode": "en",
                "filename": "Harris-Bob-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Harris-Bob-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Harris-Bob-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 13333,
                "status": true,
                "created": 1531253199,
                "changed": 1531253199,
                "url": "/sites/CMS/files/images/people/Harris-Bob-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/dc04bd55-6339-4e84-ba88-e6fe6c2b391b/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/dc04bd55-6339-4e84-ba88-e6fe6c2b391b/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/dc04bd55-6339-4e84-ba88-e6fe6c2b391b"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "1ae0c91a-d4fe-4557-8086-59c20dbc59cd",
            "attributes": {
                "id": 16126,
                "uuid": "1ae0c91a-d4fe-4557-8086-59c20dbc59cd",
                "revision_id": 88101,
                "langcode": "en",
                "status": true,
                "created": 1531255516,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Orville Fossland",
                "field_year": 2002
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/1ae0c91a-d4fe-4557-8086-59c20dbc59cd/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/1ae0c91a-d4fe-4557-8086-59c20dbc59cd/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/1ae0c91a-d4fe-4557-8086-59c20dbc59cd/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/1ae0c91a-d4fe-4557-8086-59c20dbc59cd/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/1ae0c91a-d4fe-4557-8086-59c20dbc59cd/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/1ae0c91a-d4fe-4557-8086-59c20dbc59cd/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "522fbfd4-72a1-42f9-86dc-7d71f8938a18",
                        "meta": {
                            "alt": "Orville Fossland",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/1ae0c91a-d4fe-4557-8086-59c20dbc59cd/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/1ae0c91a-d4fe-4557-8086-59c20dbc59cd/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/1ae0c91a-d4fe-4557-8086-59c20dbc59cd"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "9584f89e-1634-4810-94a4-3b3fa45f977c",
            "attributes": {
                "id": 16131,
                "uuid": "9584f89e-1634-4810-94a4-3b3fa45f977c",
                "revision_id": 88106,
                "langcode": "en",
                "status": true,
                "created": 1531255547,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Ray Kruckenberg",
                "field_year": 2003
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9584f89e-1634-4810-94a4-3b3fa45f977c/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9584f89e-1634-4810-94a4-3b3fa45f977c/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9584f89e-1634-4810-94a4-3b3fa45f977c/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9584f89e-1634-4810-94a4-3b3fa45f977c/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9584f89e-1634-4810-94a4-3b3fa45f977c/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9584f89e-1634-4810-94a4-3b3fa45f977c/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "d3464cf8-fb0b-48de-bb4d-977a59533f64",
                        "meta": {
                            "alt": "Ray Kruckenberg",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9584f89e-1634-4810-94a4-3b3fa45f977c/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9584f89e-1634-4810-94a4-3b3fa45f977c/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/9584f89e-1634-4810-94a4-3b3fa45f977c"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "83e5304e-2d73-4f2b-874e-c6b30e076526",
            "attributes": {
                "id": 16136,
                "uuid": "83e5304e-2d73-4f2b-874e-c6b30e076526",
                "revision_id": 88111,
                "langcode": "en",
                "status": true,
                "created": 1531255692,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Frank Knutson",
                "field_year": 2003
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/83e5304e-2d73-4f2b-874e-c6b30e076526/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/83e5304e-2d73-4f2b-874e-c6b30e076526/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/83e5304e-2d73-4f2b-874e-c6b30e076526/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/83e5304e-2d73-4f2b-874e-c6b30e076526/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/83e5304e-2d73-4f2b-874e-c6b30e076526/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/83e5304e-2d73-4f2b-874e-c6b30e076526/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "232e16fb-b561-4ed5-ad8c-38b38dfd1982",
                        "meta": {
                            "alt": "Frank Knutson",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/83e5304e-2d73-4f2b-874e-c6b30e076526/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/83e5304e-2d73-4f2b-874e-c6b30e076526/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/83e5304e-2d73-4f2b-874e-c6b30e076526"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "401437e2-2c26-4433-8746-777da27d3dda",
            "attributes": {
                "id": 16141,
                "uuid": "401437e2-2c26-4433-8746-777da27d3dda",
                "revision_id": 88116,
                "langcode": "en",
                "status": true,
                "created": 1531255719,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "William Keller",
                "field_year": 2004
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/401437e2-2c26-4433-8746-777da27d3dda/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/401437e2-2c26-4433-8746-777da27d3dda/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/401437e2-2c26-4433-8746-777da27d3dda/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/401437e2-2c26-4433-8746-777da27d3dda/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/401437e2-2c26-4433-8746-777da27d3dda/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/401437e2-2c26-4433-8746-777da27d3dda/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "a3260e09-28e0-42bc-a592-5a94df703f89",
                        "meta": {
                            "alt": "William Keller",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/401437e2-2c26-4433-8746-777da27d3dda/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/401437e2-2c26-4433-8746-777da27d3dda/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/401437e2-2c26-4433-8746-777da27d3dda"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "7193098b-23cc-41db-b57c-9e5efb50aff7",
            "attributes": {
                "id": 16146,
                "uuid": "7193098b-23cc-41db-b57c-9e5efb50aff7",
                "revision_id": 88121,
                "langcode": "en",
                "status": true,
                "created": 1531255791,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Gary Williamson",
                "field_year": 2005
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7193098b-23cc-41db-b57c-9e5efb50aff7/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7193098b-23cc-41db-b57c-9e5efb50aff7/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7193098b-23cc-41db-b57c-9e5efb50aff7/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7193098b-23cc-41db-b57c-9e5efb50aff7/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7193098b-23cc-41db-b57c-9e5efb50aff7/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7193098b-23cc-41db-b57c-9e5efb50aff7/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "c1acf39c-26c2-4571-871c-359a725a5313",
                        "meta": {
                            "alt": "Gary Williamson",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7193098b-23cc-41db-b57c-9e5efb50aff7/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7193098b-23cc-41db-b57c-9e5efb50aff7/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7193098b-23cc-41db-b57c-9e5efb50aff7"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "0e1fafcd-7ed3-454c-aa7b-ca5629dd5344",
            "attributes": {
                "id": 16151,
                "uuid": "0e1fafcd-7ed3-454c-aa7b-ca5629dd5344",
                "revision_id": 88126,
                "langcode": "en",
                "status": true,
                "created": 1531255829,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Eugene Appeldorn",
                "field_year": 2008
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/0e1fafcd-7ed3-454c-aa7b-ca5629dd5344/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/0e1fafcd-7ed3-454c-aa7b-ca5629dd5344/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/0e1fafcd-7ed3-454c-aa7b-ca5629dd5344/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/0e1fafcd-7ed3-454c-aa7b-ca5629dd5344/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/0e1fafcd-7ed3-454c-aa7b-ca5629dd5344/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/0e1fafcd-7ed3-454c-aa7b-ca5629dd5344/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "718c006d-5cd2-4dd2-af47-f84be3931115",
                        "meta": {
                            "alt": "Eugene Appeldorn",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/0e1fafcd-7ed3-454c-aa7b-ca5629dd5344/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/0e1fafcd-7ed3-454c-aa7b-ca5629dd5344/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/0e1fafcd-7ed3-454c-aa7b-ca5629dd5344"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "8b6d505e-df69-416b-b3b4-16ee3e34ab0f",
            "attributes": {
                "id": 16156,
                "uuid": "8b6d505e-df69-416b-b3b4-16ee3e34ab0f",
                "revision_id": 88131,
                "langcode": "en",
                "status": true,
                "created": 1531255854,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Kent Janssen",
                "field_year": 2008
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/8b6d505e-df69-416b-b3b4-16ee3e34ab0f/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/8b6d505e-df69-416b-b3b4-16ee3e34ab0f/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/8b6d505e-df69-416b-b3b4-16ee3e34ab0f/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/8b6d505e-df69-416b-b3b4-16ee3e34ab0f/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/8b6d505e-df69-416b-b3b4-16ee3e34ab0f/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/8b6d505e-df69-416b-b3b4-16ee3e34ab0f/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "44ebb7b4-1925-4278-bf87-b412a9d75071",
                        "meta": {
                            "alt": "Kent Janssen",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/8b6d505e-df69-416b-b3b4-16ee3e34ab0f/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/8b6d505e-df69-416b-b3b4-16ee3e34ab0f/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/8b6d505e-df69-416b-b3b4-16ee3e34ab0f"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "e62b6065-ceee-4f85-bc6d-81c970c470b5",
            "attributes": {
                "id": 16161,
                "uuid": "e62b6065-ceee-4f85-bc6d-81c970c470b5",
                "revision_id": 88136,
                "langcode": "en",
                "status": true,
                "created": 1531255893,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Howard Easton",
                "field_year": 2009
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/e62b6065-ceee-4f85-bc6d-81c970c470b5/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/e62b6065-ceee-4f85-bc6d-81c970c470b5/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/e62b6065-ceee-4f85-bc6d-81c970c470b5/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/e62b6065-ceee-4f85-bc6d-81c970c470b5/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/e62b6065-ceee-4f85-bc6d-81c970c470b5/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/e62b6065-ceee-4f85-bc6d-81c970c470b5/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "7860a1c7-3154-46e3-a43a-a5604bc1d08b",
                        "meta": {
                            "alt": "Howard Easton",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/e62b6065-ceee-4f85-bc6d-81c970c470b5/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/e62b6065-ceee-4f85-bc6d-81c970c470b5/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/e62b6065-ceee-4f85-bc6d-81c970c470b5"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "2af61a62-fc57-4e7c-85e8-46d5c1e83ff6",
            "attributes": {
                "id": 16166,
                "uuid": "2af61a62-fc57-4e7c-85e8-46d5c1e83ff6",
                "revision_id": 88141,
                "langcode": "en",
                "status": true,
                "created": 1531255946,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Richard Fockler",
                "field_year": 2009
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2af61a62-fc57-4e7c-85e8-46d5c1e83ff6/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2af61a62-fc57-4e7c-85e8-46d5c1e83ff6/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2af61a62-fc57-4e7c-85e8-46d5c1e83ff6/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2af61a62-fc57-4e7c-85e8-46d5c1e83ff6/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2af61a62-fc57-4e7c-85e8-46d5c1e83ff6/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2af61a62-fc57-4e7c-85e8-46d5c1e83ff6/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "db8e8b0b-11af-40f1-aac8-25fd72fcd9de",
                        "meta": {
                            "alt": "Richard Fockler",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2af61a62-fc57-4e7c-85e8-46d5c1e83ff6/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2af61a62-fc57-4e7c-85e8-46d5c1e83ff6/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/2af61a62-fc57-4e7c-85e8-46d5c1e83ff6"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "fdf9d738-5888-4c83-9b03-fe612d3cca1e",
            "attributes": {
                "id": 16171,
                "uuid": "fdf9d738-5888-4c83-9b03-fe612d3cca1e",
                "revision_id": 88146,
                "langcode": "en",
                "status": true,
                "created": 1531255976,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Ken Ziegler",
                "field_year": 2009
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/fdf9d738-5888-4c83-9b03-fe612d3cca1e/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/fdf9d738-5888-4c83-9b03-fe612d3cca1e/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/fdf9d738-5888-4c83-9b03-fe612d3cca1e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/fdf9d738-5888-4c83-9b03-fe612d3cca1e/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/fdf9d738-5888-4c83-9b03-fe612d3cca1e/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/fdf9d738-5888-4c83-9b03-fe612d3cca1e/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "a82cf579-2999-40ac-a2ec-5edbb49565fc",
                        "meta": {
                            "alt": "Ken Ziegler",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/fdf9d738-5888-4c83-9b03-fe612d3cca1e/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/fdf9d738-5888-4c83-9b03-fe612d3cca1e/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/fdf9d738-5888-4c83-9b03-fe612d3cca1e"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "7e0d5293-878e-4728-b9e8-d4a5ba9294af",
            "attributes": {
                "id": 16176,
                "uuid": "7e0d5293-878e-4728-b9e8-d4a5ba9294af",
                "revision_id": 88151,
                "langcode": "en",
                "status": true,
                "created": 1531256003,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Jim Headley",
                "field_year": 2011
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7e0d5293-878e-4728-b9e8-d4a5ba9294af/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7e0d5293-878e-4728-b9e8-d4a5ba9294af/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7e0d5293-878e-4728-b9e8-d4a5ba9294af/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7e0d5293-878e-4728-b9e8-d4a5ba9294af/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7e0d5293-878e-4728-b9e8-d4a5ba9294af/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7e0d5293-878e-4728-b9e8-d4a5ba9294af/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "b733bbf1-4591-4e16-8511-a5df88acad47",
                        "meta": {
                            "alt": "Jim Headley",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7e0d5293-878e-4728-b9e8-d4a5ba9294af/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7e0d5293-878e-4728-b9e8-d4a5ba9294af/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/7e0d5293-878e-4728-b9e8-d4a5ba9294af"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "80c33607-47c1-4a9a-a99b-acd0766dc4fb",
            "attributes": {
                "id": 16181,
                "uuid": "80c33607-47c1-4a9a-a99b-acd0766dc4fb",
                "revision_id": 88156,
                "langcode": "en",
                "status": true,
                "created": 1531256032,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Clifton \"Buzz\" Hudgins",
                "field_year": 2014
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/80c33607-47c1-4a9a-a99b-acd0766dc4fb/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/80c33607-47c1-4a9a-a99b-acd0766dc4fb/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/80c33607-47c1-4a9a-a99b-acd0766dc4fb/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/80c33607-47c1-4a9a-a99b-acd0766dc4fb/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/80c33607-47c1-4a9a-a99b-acd0766dc4fb/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/80c33607-47c1-4a9a-a99b-acd0766dc4fb/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "7cfa4f78-f4ca-402f-8871-db4f9ccca23b",
                        "meta": {
                            "alt": "Clifton \"Buzz\" Hudgins",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/80c33607-47c1-4a9a-a99b-acd0766dc4fb/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/80c33607-47c1-4a9a-a99b-acd0766dc4fb/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/80c33607-47c1-4a9a-a99b-acd0766dc4fb"
            }
        },
        {
            "type": "paragraph--award_winner",
            "id": "6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b",
            "attributes": {
                "id": 16186,
                "uuid": "6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b",
                "revision_id": 88161,
                "langcode": "en",
                "status": true,
                "created": 1531256069,
                "parent_id": "16121",
                "parent_type": "paragraph",
                "parent_field_name": "field_winners",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_date": "2018-07-10",
                "field_title": "Daryl Hill",
                "field_year": 2014
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "b32f2e71-c927-4880-b5ae-c06450eec9a3"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "231d1091-4253-40e5-8d2b-8d9d484cc84d",
                        "meta": {
                            "alt": "Daryl Hill",
                            "title": "",
                            "width": "258",
                            "height": "355"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/award_winner/6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/award_winner/6db1c3c1-cb81-4cd1-87fb-5a78cd22a46b"
            }
        },
        {
            "type": "file--file",
            "id": "522fbfd4-72a1-42f9-86dc-7d71f8938a18",
            "attributes": {
                "fid": 10636,
                "uuid": "522fbfd4-72a1-42f9-86dc-7d71f8938a18",
                "langcode": "en",
                "filename": "Fossland-Orville-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Fossland-Orville-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Fossland-Orville-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 17358,
                "status": true,
                "created": 1531253198,
                "changed": 1531253198,
                "url": "/sites/CMS/files/images/people/Fossland-Orville-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/522fbfd4-72a1-42f9-86dc-7d71f8938a18/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/522fbfd4-72a1-42f9-86dc-7d71f8938a18/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/522fbfd4-72a1-42f9-86dc-7d71f8938a18"
            }
        },
        {
            "type": "file--file",
            "id": "d3464cf8-fb0b-48de-bb4d-977a59533f64",
            "attributes": {
                "fid": 10691,
                "uuid": "d3464cf8-fb0b-48de-bb4d-977a59533f64",
                "langcode": "en",
                "filename": "Kruckenberg-Ray-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Kruckenberg-Ray-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Kruckenberg-Ray-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 18962,
                "status": true,
                "created": 1531253201,
                "changed": 1531253201,
                "url": "/sites/CMS/files/images/people/Kruckenberg-Ray-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/d3464cf8-fb0b-48de-bb4d-977a59533f64/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/d3464cf8-fb0b-48de-bb4d-977a59533f64/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/d3464cf8-fb0b-48de-bb4d-977a59533f64"
            }
        },
        {
            "type": "file--file",
            "id": "232e16fb-b561-4ed5-ad8c-38b38dfd1982",
            "attributes": {
                "fid": 10686,
                "uuid": "232e16fb-b561-4ed5-ad8c-38b38dfd1982",
                "langcode": "en",
                "filename": "Knutson-Frank-258px-portrait.jpg",
                "uri": {
                    "value": "public://images/people/Knutson-Frank-258px-portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Knutson-Frank-258px-portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 19367,
                "status": true,
                "created": 1531253201,
                "changed": 1531253201,
                "url": "/sites/CMS/files/images/people/Knutson-Frank-258px-portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/232e16fb-b561-4ed5-ad8c-38b38dfd1982/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/232e16fb-b561-4ed5-ad8c-38b38dfd1982/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/232e16fb-b561-4ed5-ad8c-38b38dfd1982"
            }
        },
        {
            "type": "file--file",
            "id": "a3260e09-28e0-42bc-a592-5a94df703f89",
            "attributes": {
                "fid": 10681,
                "uuid": "a3260e09-28e0-42bc-a592-5a94df703f89",
                "langcode": "en",
                "filename": "Keller-Bill-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Keller-Bill-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Keller-Bill-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 13732,
                "status": true,
                "created": 1531253201,
                "changed": 1531253201,
                "url": "/sites/CMS/files/images/people/Keller-Bill-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/a3260e09-28e0-42bc-a592-5a94df703f89/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/a3260e09-28e0-42bc-a592-5a94df703f89/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/a3260e09-28e0-42bc-a592-5a94df703f89"
            }
        },
        {
            "type": "file--file",
            "id": "c1acf39c-26c2-4571-871c-359a725a5313",
            "attributes": {
                "fid": 10716,
                "uuid": "c1acf39c-26c2-4571-871c-359a725a5313",
                "langcode": "en",
                "filename": "Williamson-Gary-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Williamson-Gary-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Williamson-Gary-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 18889,
                "status": true,
                "created": 1531253203,
                "changed": 1531253203,
                "url": "/sites/CMS/files/images/people/Williamson-Gary-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/c1acf39c-26c2-4571-871c-359a725a5313/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/c1acf39c-26c2-4571-871c-359a725a5313/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/c1acf39c-26c2-4571-871c-359a725a5313"
            }
        },
        {
            "type": "file--file",
            "id": "718c006d-5cd2-4dd2-af47-f84be3931115",
            "attributes": {
                "fid": 10591,
                "uuid": "718c006d-5cd2-4dd2-af47-f84be3931115",
                "langcode": "en",
                "filename": "Appeldorn-Eugene-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Appeldorn-Eugene-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Appeldorn-Eugene-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 18333,
                "status": true,
                "created": 1531253195,
                "changed": 1531253195,
                "url": "/sites/CMS/files/images/people/Appeldorn-Eugene-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/718c006d-5cd2-4dd2-af47-f84be3931115/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/718c006d-5cd2-4dd2-af47-f84be3931115/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/718c006d-5cd2-4dd2-af47-f84be3931115"
            }
        },
        {
            "type": "file--file",
            "id": "44ebb7b4-1925-4278-bf87-b412a9d75071",
            "attributes": {
                "fid": 10676,
                "uuid": "44ebb7b4-1925-4278-bf87-b412a9d75071",
                "langcode": "en",
                "filename": "Janssen-Kent-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Janssen-Kent-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Janssen-Kent-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 16306,
                "status": true,
                "created": 1531253201,
                "changed": 1531253201,
                "url": "/sites/CMS/files/images/people/Janssen-Kent-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/44ebb7b4-1925-4278-bf87-b412a9d75071/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/44ebb7b4-1925-4278-bf87-b412a9d75071/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/44ebb7b4-1925-4278-bf87-b412a9d75071"
            }
        },
        {
            "type": "file--file",
            "id": "7860a1c7-3154-46e3-a43a-a5604bc1d08b",
            "attributes": {
                "fid": 10616,
                "uuid": "7860a1c7-3154-46e3-a43a-a5604bc1d08b",
                "langcode": "en",
                "filename": "Easton-Howard-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Easton-Howard-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Easton-Howard-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 17753,
                "status": true,
                "created": 1531253197,
                "changed": 1531253197,
                "url": "/sites/CMS/files/images/people/Easton-Howard-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/7860a1c7-3154-46e3-a43a-a5604bc1d08b/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/7860a1c7-3154-46e3-a43a-a5604bc1d08b/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/7860a1c7-3154-46e3-a43a-a5604bc1d08b"
            }
        },
        {
            "type": "file--file",
            "id": "db8e8b0b-11af-40f1-aac8-25fd72fcd9de",
            "attributes": {
                "fid": 10631,
                "uuid": "db8e8b0b-11af-40f1-aac8-25fd72fcd9de",
                "langcode": "en",
                "filename": "Fockler-Richard-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Fockler-Richard-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Fockler-Richard-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 15357,
                "status": true,
                "created": 1531253197,
                "changed": 1531253197,
                "url": "/sites/CMS/files/images/people/Fockler-Richard-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/db8e8b0b-11af-40f1-aac8-25fd72fcd9de/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/db8e8b0b-11af-40f1-aac8-25fd72fcd9de/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/db8e8b0b-11af-40f1-aac8-25fd72fcd9de"
            }
        },
        {
            "type": "file--file",
            "id": "a82cf579-2999-40ac-a2ec-5edbb49565fc",
            "attributes": {
                "fid": 10721,
                "uuid": "a82cf579-2999-40ac-a2ec-5edbb49565fc",
                "langcode": "en",
                "filename": "Ziegler-Ken-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Ziegler-Ken-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Ziegler-Ken-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 14992,
                "status": true,
                "created": 1531253204,
                "changed": 1531253204,
                "url": "/sites/CMS/files/images/people/Ziegler-Ken-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/a82cf579-2999-40ac-a2ec-5edbb49565fc/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/a82cf579-2999-40ac-a2ec-5edbb49565fc/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/a82cf579-2999-40ac-a2ec-5edbb49565fc"
            }
        },
        {
            "type": "file--file",
            "id": "b733bbf1-4591-4e16-8511-a5df88acad47",
            "attributes": {
                "fid": 10661,
                "uuid": "b733bbf1-4591-4e16-8511-a5df88acad47",
                "langcode": "en",
                "filename": "Headley-Jim-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Headley-Jim-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Headley-Jim-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 16488,
                "status": true,
                "created": 1531253200,
                "changed": 1531253200,
                "url": "/sites/CMS/files/images/people/Headley-Jim-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/b733bbf1-4591-4e16-8511-a5df88acad47/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/b733bbf1-4591-4e16-8511-a5df88acad47/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/b733bbf1-4591-4e16-8511-a5df88acad47"
            }
        },
        {
            "type": "file--file",
            "id": "7cfa4f78-f4ca-402f-8871-db4f9ccca23b",
            "attributes": {
                "fid": 10671,
                "uuid": "7cfa4f78-f4ca-402f-8871-db4f9ccca23b",
                "langcode": "en",
                "filename": "Hudgins-Clifton-Buzz-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Hudgins-Clifton-Buzz-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Hudgins-Clifton-Buzz-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 14022,
                "status": true,
                "created": 1531253200,
                "changed": 1531253200,
                "url": "/sites/CMS/files/images/people/Hudgins-Clifton-Buzz-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/7cfa4f78-f4ca-402f-8871-db4f9ccca23b/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/7cfa4f78-f4ca-402f-8871-db4f9ccca23b/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/7cfa4f78-f4ca-402f-8871-db4f9ccca23b"
            }
        },
        {
            "type": "file--file",
            "id": "231d1091-4253-40e5-8d2b-8d9d484cc84d",
            "attributes": {
                "fid": 10666,
                "uuid": "231d1091-4253-40e5-8d2b-8d9d484cc84d",
                "langcode": "en",
                "filename": "Hill-Daryl-258px-Portrait.jpg",
                "uri": {
                    "value": "public://images/people/Hill-Daryl-258px-Portrait.jpg",
                    "url": "/sites/CMS/files/images/people/Hill-Daryl-258px-Portrait.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 19961,
                "status": true,
                "created": 1531253200,
                "changed": 1531253200,
                "url": "/sites/CMS/files/images/people/Hill-Daryl-258px-Portrait.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "1939d6dd-53c8-485e-9da1-0cc55eb37902"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/231d1091-4253-40e5-8d2b-8d9d484cc84d/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/231d1091-4253-40e5-8d2b-8d9d484cc84d/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/231d1091-4253-40e5-8d2b-8d9d484cc84d"
            }
        }
    ]
}