import axios from "axios"
import * as IAxios from "axios/index"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"

export const AwardContainer: React.SFC<AwardProps> = ({ includedRelationships, paragraphId, siteDomain }) => {
    const AwardParagraph = includedRelationships.find((p) => p.id === paragraphId) as AwardPageData
    const winners = GetAwardContent(AwardParagraph, includedRelationships, siteDomain)
    if (AwardParagraph) {
        return (
            <ul className="small-block-grid-2 large-block-grid-3">
                {winners.map(({ name, year, image }: IAwardWinner, i) => (
                    <li key={i}>
                        <img src={image} />
                        <div>{name}</div>
                        <div>{year}</div>
                    </li>
                ))}
            </ul>
        )
    } else {
        return <span className="award-paragraph-empty" />
    }
}

// Expects content to be in includedRelationships. 
// Doesn't query content if it's missing
export function GetAwardContent(AwardParagraph: AwardPageData, includedRelationships: Array<AwardIncludedRelationships>, siteDomain: string): IAwardWinner[] {
    return AwardParagraph.relationships.field_winners.data
        .map((data) => GetWinnerData(data, includedRelationships, siteDomain))
        .filter(awards => awards !== undefined) as IAwardWinner[]
}

export function GetWinnerData(winnerData: Page.IDatum, includedRelationships: Array<AwardIncludedRelationships>, siteDomain: string): IAwardWinner | undefined {
    const winner = includedRelationships.find((p) => p.id === winnerData.id) as FieldWinnerData

    if (winner) {
        return {
            name: winner.attributes.field_title,
            year: winner.attributes.field_year,
            image: GetWinnerPhoto(winner, includedRelationships, siteDomain)
        } as IAwardWinner
    }

    return undefined
}

export function GetWinnerPhoto(winner: FieldWinnerData, includedRelationships: Array<AwardIncludedRelationships>, siteDomain: string): string {
    const image = includedRelationships.find(p => p.id === winner.relationships.field_image.data.id) as Page.IField_File

    if (image) {
        return siteDomain + image.attributes.url
    }
    return ""
}

export default AwardContainer

export interface AwardProps {
    includedRelationships: Array<Page.IPageData | AwardPageData | any>
    paragraphId: string
    siteDomain: string
}

export interface AwardPageData extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_title: string
    }
    relationships: Page.IRelationships & {
        field_winners: {
            data: Page.IDatum[]
        }
    }
}

export interface FieldWinnerData extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_date: string //DON'T USE
        field_title: string
        field_year: number
    }
    relationships: Page.IRelationships & {
        field_image: Page.IField_Image
    }
}

export interface IAwardWinner {
    name: string
    image: string
    year: number
}

export type AwardIncludedRelationships = Page.IPageData | AwardPageData | FieldWinnerData | Page.IField_File | any