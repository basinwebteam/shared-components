import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import TopicBoxContainer from "../TopicBox"

export const TopicBoxRowContainer: React.SFC<ITopicBoxRowProps> = (props: ITopicBoxRowProps) => {

    const getTopicBoxes = () => {
        const {includedRelationships, paragraphId, siteDomain} = props
        
        const rowData = includedRelationships.find((paragraph) => paragraph.id === paragraphId) as ITopicBoxRowData
        
        return rowData.relationships.field_topic_row.data.map((topicBox, i) => {
            if(typeof topicBox !== "undefined") {
                return (
                    <TopicBoxContainer
                        key={topicBox.id}
                        paragraphId={topicBox.id}
                        includedRelationships={includedRelationships}
                        siteDomain={siteDomain}
                    />
                )
            }
            return ""
        })
    }
    
    return (
        <div className="row topic-box-row">
            {getTopicBoxes()}
        </div>
    )
}

TopicBoxRowContainer.displayName = "DrupalTopicBoxContainer"

export default TopicBoxRowContainer

export interface ITopicBoxRowProps {
    includedRelationships: Page.IPageData[],
    paragraphId: string,
    siteDomain: string,
}

export interface ITopicBoxRowData extends Page.IPageData {
    relationships: ITopicBoxDataRelationships
}

export interface ITopicBoxDataRelationships extends Page.IRelationships {
    field_topic_row: Page.IFieldContent
}
