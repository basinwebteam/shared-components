import { AxiosResponse, default as axios } from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import { default as PieChart, GenerationData, GenerationDataPlants } from "../../components/EnergyPortfolioPieChart"
import { EnergyPortfolioResourceDetails, IEPResourceDetailsProps } from "../../components/EnergyPortfolioResourceDetails"
import EnergyPortfolioResourceTable from "../../components/EnergyPortfolioResourceTable"
import { IVideoGalleryItem, IYouTubeCollection, VideoGallery } from "../VideoGallery"
import Image from "../../components/Image"
import { RelatedVideos } from "../RelatedVideo"
import YouTube from "../../components/YouTube"
import { BasicParagraph, IBasicParagraphProps, IBasicParagraphPageData } from "../BasicParagraph"

export class EnergyPortfolioPieChartContainer extends React.Component<IPropsEnergyPortfolio, IStateEnergyPortfolio> {
    constructor(props: IPropsEnergyPortfolio) {
        super(props)
        this.state = {
            generationData: [],
            generationRenewableData: [],
            details: {
                name: "Resource Details",
                photo: "",
                plants: [],
                videos: "",
                briefInfo: "",
            },
            explanationParagraph: {
                paragraphId: "",
                siteDomain: this.props.siteDomain,
                data: undefined
            },
            operatedGenSummer: "",
            operatedGenWinter: "",
            ownedGenSummer: "",
            ownedGenWinter: "",
            purchasedGenSummer: "",
            purchasedGenWinter: "",
        }
        this.linkClickHandler = this.linkClickHandler.bind(this)
    }

    render() {
        if (this.state.generationData.length > 0) {
            return (
                <div className="row">
                    <div className="small-12 medium-9 column">
                        <PieChart
                            size={[600, 450]}
                            generationData={this.state.generationData}
                            generationRenewableData={this.state.generationRenewableData}
                            clickHandler={this.linkClickHandler}
                        />
                        <BasicParagraph
                            siteDomain={this.props.siteDomain}
                            paragraphId={this.state.explanationParagraph.paragraphId}
                            data={this.state.explanationParagraph.data}
                        />
                        <EnergyPortfolioResourceTable
                            data={this.state.generationData}
                            operatedGenSummer={this.state.operatedGenSummer}
                            operatedGenWinter={this.state.operatedGenWinter}
                            ownedGenSummer={this.state.ownedGenSummer}
                            ownedGenWinter={this.state.ownedGenWinter}
                            purchasedGenSummer={this.state.purchasedGenSummer}
                            purchasedGenWinter={this.state.purchasedGenWinter}
                        />
                    </div>
                    <div className="small-12 medium-3 column">
                        <EnergyPortfolioResourceDetails
                            name={this.state.details.name}
                            photo={this.state.details.photo}
                            plants={this.state.details.plants}
                            videos={this.state.details.videos}
                            briefInfo={this.state.details.briefInfo}
                        />
                    </div>
                </div>
            )
        }
        return <div className="pie-chart-loading" />
    }

    componentDidMount() {
        this.getData(this.props.id)
    }

    async getData(id: string) {
        const includedFields = [
            "field_energy_type",
            "field_energy_type.field_image",
            // "field_energy_type.field_body",
            "field_energy_type.field_plant",
            // "field_energy_type.field_video_youtube",
            "field_data_explanation"
        ]

        const energyPortfolioData = await axios.get(
            `//api.bepc.com/api/cms/json/block_content/energy_portfolio_interactive_pie/${id}/?include=${includedFields.join(",")}`
        ) as IAxiosResponseEnergyPortfolioData

        // const tags = await axios.get(`//api.bepc.com/api/cms/relatedvideotags`) as IAxiosResponseRelatedVideoTags

        const generationData = this.modelGenerationData(energyPortfolioData.data)
        const generationRenewableData = this.modelGenerationData(energyPortfolioData.data, true)

        const explanationParagraphDataField = energyPortfolioData.data.data.relationships.field_data_explanation.data

        const explanationParagraphId = Array.isArray(explanationParagraphDataField) && explanationParagraphDataField.length > 0 ? explanationParagraphDataField[0].id : ""

        const explanationParagraphData = energyPortfolioData.data.included.find(p => p.id === explanationParagraphId) as IBasicParagraphPageData

        this.setState({
            generationData,
            generationRenewableData,
            explanationParagraph: {
                paragraphId: explanationParagraphId,
                siteDomain: this.props.siteDomain,
                data: explanationParagraphData
            },
            operatedGenSummer: energyPortfolioData.data.data.attributes.field_operated_generation_summer,
            operatedGenWinter: energyPortfolioData.data.data.attributes.field_operated_generation_winter,
            ownedGenSummer: energyPortfolioData.data.data.attributes.field_owned_generation_summer,
            ownedGenWinter: energyPortfolioData.data.data.attributes.field_owned_generation_winter,
            purchasedGenSummer: energyPortfolioData.data.data.attributes.field_purchased_gen_summer,
            purchasedGenWinter: energyPortfolioData.data.data.attributes.field_purchased_gen_winter,
        })
    }

    modelGenerationData(data: IEnergyPortfolio, renewables: boolean = false) {
        const genData = data.data.relationships.field_energy_type.data.map(
            (p) => {
                const energy = data.included.find((portfolio) => portfolio.id == p.id) as IEnergyPortfolioParagraph
                if (energy !== undefined) {
                    return {
                        name: energy.attributes.field_type_name_short,
                        mw: parseFloat(energy.attributes.field_megawatts),
                        mwSummer: parseFloat(energy.attributes.field_megawatts_summer),
                        longName: energy.attributes.field_type_name_long,
                        photo: this.getPhoto(energy.relationships.field_image as Page.IField_Image, data.included),
                        plants: this.getPlants(energy.relationships.field_plant, data.included),
                        videos: this.getVideos(energy.attributes.field_video_youtube || []),
                        briefInfo: this.getBriefInfo(energy)
                    } as GenerationData
                }
                throw new Error(`Unable to find Portfolio id ${p.id} for Energy Portfolio Pie Chart.`)
            }
        )
        if (renewables) {
            return genData.filter((d) => d.name === "Renewables")
        } else {
            return genData.filter((d) => d.name !== "Renewables")
        }
    }

    getBriefInfo(body: IEnergyPortfolioParagraph): any {
        if (body.attributes.field_body === null) {
            return ""
        }
        return (
            <BasicParagraph
                siteDomain={this.props.siteDomain}
                paragraphId={body.id}
                data={body as IBasicParagraphPageData}
            />
        )
    }

    getPhoto(paragraph: Page.IField_Image, includedData: Page.IPageData[]) {
        if (paragraph.data == null) { return "" }
        const url = includedData.find((data) => data.id === paragraph.data.id) as Page.IField_File
        if (url === undefined) { return "" }
        return (
            <Image
                altText={paragraph.data.meta.alt}
                url={this.props.siteDomain + url.attributes.url}
            />
        )
    }
    getPlants(plants: Page.ITypeMultipleData, includedData: Page.IPageData[]) {
        if (plants.data.length === 0) { return [] }
        return plants.data.map((plant) => {
            const plantInfo = includedData.find((d) => plant.id === d.id) as IFacilityData
            if (plantInfo === undefined) {
                throw new Error(`Plant info undefined for plant ${plant.id}`)
            }
            return {
                name: plantInfo.attributes.field_facility_name,
                url: plantInfo.attributes.path.alias
            } as GenerationDataPlants
        })
    }

    getVideos(videos: string[]) {
        return (
            <div>
                {/* <RelatedVideos
                    videos={videos}
                /> */}
                {
                    videos.map((v, i) => (
                        <p key={i}>
                            <YouTube
                                link={v}
                            />
                        </p>
                    ))
                }
            </div>
        )
    }

    linkClickHandler(name: string) {
        let resource = this.state.generationData.find(r => r.name === name)
        if (resource === undefined) {
            resource = this.state.generationRenewableData.find(r => r.name === name)
        }
        if (resource !== undefined) {
            this.setState({
                details: {
                    name: resource.longName,
                    photo: resource.photo || "",
                    plants: resource.plants || [],
                    videos: resource.videos || "",
                    briefInfo: resource.briefInfo || ""
                }
            })
        }
    }


}

export default EnergyPortfolioPieChartContainer

export interface IPropsEnergyPortfolio {
    id: string,
    siteDomain: string
}
export interface IStateEnergyPortfolio {
    generationData: GenerationData[],
    generationRenewableData: GenerationData[],
    details: IEPResourceDetailsProps
    explanationParagraph: IBasicParagraphProps
    operatedGenSummer: string,
    operatedGenWinter: string,
    ownedGenSummer: string,
    ownedGenWinter: string,
    purchasedGenSummer: string,
    purchasedGenWinter: string
}
export interface IAxiosResponseEnergyPortfolioData {
    data: IEnergyPortfolio
}

export interface IEnergyPortfolio {
    data: IEnergyPortfolioData,
    included: Array<Page.IPageData | IEnergyPortfolioParagraph | IYouTubeCollection | IVideoGalleryItem | Page.IParagraphFile>
}
export interface IEnergyPortfolioData extends Page.IPageData {
    attributes: Page.IAttributes & {
        info: string,
        field_operated_generation_summer: string,
        field_operated_generation_winter: string,
        field_owned_generation_summer: string,
        field_owned_generation_winter: string,
        field_purchased_gen_summer: string,
        field_purchased_gen_winter: string,
    },
    relationships: Page.IRelationships & {
        field_energy_type: Page.ITypeMultipleData
        field_data_explanation: Page.ITypeMultipleData
    }
}
export interface IEnergyPortfolioParagraph extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_body: Page.IWYSIWYG,
        field_megawatts: string,
        field_megawatts_summer: string,
        field_type_name_long: string,
        field_type_name_short: string,
        field_video_youtube: string[]
    }
    relationships: Page.IRelationships & {
        field_image: Page.IType,
        field_plant: Page.ITypeMultipleData,
    }
}
export interface IAxiosResponseRelatedVideoTags {
    data: IRelatedVideoTags
}
export interface IRelatedVideoTags {
    data: IRelatedVideoTagData[]
    included: Page.IPageData & {
        attributes: Page.IAttributes & {
            name: string
        }
    }
}
export interface IRelatedVideoTagData extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_video_limit: number
    }
    relationships: Page.IRelationships & {
        field_tags: Page.ITypeMultipleData
    }
}
export interface IFacilityData extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_city: string,
        field_facility_capacity: string,
        field_facility_name: string,
        field_facility_phone: string,
        field_physical_location_city: string,
        field_physical_location_state: string,
        field_state: string,
        field_street_address: string,
        field_zip_code: string,
        path: {
            alias: string
        }
    }
}