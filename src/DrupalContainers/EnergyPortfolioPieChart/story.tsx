import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'
import EnergyPortfolioPieChartContainer from "../EnergyPortfolioPieChart"

const description = `Displays an Energy Portfolio Pie Chart `

storiesOf("EnergyPortfolioPieChart", module)
    .addDecorator(withKnobs)
    .add("Pie Chart Container with Drupal Data", 
        withInfo(`${description}.`)(() => {
            const SITE_DOMAIN = "https://basinelectric.com"

            return (
                <EnergyPortfolioPieChartContainer
                    siteDomain={SITE_DOMAIN}
                    id={'c47b0f03-c797-47a4-bf83-f470f049f996'}
                />
            )
        })
    )