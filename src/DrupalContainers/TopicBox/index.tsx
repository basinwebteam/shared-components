import {AxiosResponse, default as axios} from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import * as Type from "../../DrupalContentTypes/ParagraphTypes"
import {BasicParagraph, IBasicParagraphPageData} from "../BasicParagraph"
import {GetSiteMap} from "../../DrupalAPI/sitemap"
import Image from "../Image"
import TopicBox from "../../components/TopicBox"

export class TopicBoxContainer extends React.Component<ITopicBoxProps, ITopicBoxState> {
    constructor(props: ITopicBoxProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    public displayName = "DrupalTopicBoxContainer"

    public render() {
        const className = this.getColumnClassWidth()
        return (
            <TopicBox
                title={this.state.title}
                className={className}
                image={this.getImage()}
                paragraph={this.getBasicParagraph()}
                color={this.state.titleColor.toLowerCase()}
                url={this.state.url}
            />
        )
    }

    public getColumnClassWidth() {
        switch (this.state.width) {
            case(0):
            default:
                return "medium-4"
            case(1):
                return "medium-8"
            case(2):
                return "small-12"
        }
    }

    public getBasicParagraph() {
        if (this.state.basicParagraphId !== null) {
            const data = this.state.includedRelationships.find((paragraph) => paragraph.id === this.state.basicParagraphId) as IBasicParagraphPageData
            
            return (
                <BasicParagraph
                    paragraphId={this.state.basicParagraphId}
                    data={data}
                    siteDomain={this.props.siteDomain}
                />
            )
        }
        return ""
    }

    public getImage() {
        if (this.state.imageId !== null) {
            return (
                <Image
                    className="image-topic-box"
                    paragraphId={this.state.imageId}
                    siteDomain={this.props.siteDomain}
                    includedRelationships={this.state.includedRelationships}
                    imageParagraphName="image_topic_box"
                />
            )
        }
        return ""
    }

    public componentDidMount() {
        this.updateComponent(true)
    }

    public componentDidUpdate(prevProps: ITopicBoxProps, prevState: ITopicBoxState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateComponent()
        }
    }

    public async updateComponent(queryIncludedData = false) {
        const topicBox = this.getTopicBoxData()

        if (typeof topicBox === "undefined") {
            this.queryTopicBoxData()
        } else {
            if (queryIncludedData) {
                if (this.state.imageId === null || this.state.basicParagraphId === null) {
                    this.queryTopicBoxData()
                }
            } else {
                if (this.checkUrlStringForEntity(this.state.url)) {
                    const url = await this.getUrl({uri: this.state.url})
                    if (url !== null) {
                        this.setState({url})
                    }
                }
            }
        }

    }

    private async queryTopicBoxData() {
        const response = await axios.get(
            `//api.bepc.com/api/cms/topicbox/${this.props.paragraphId}`) as IAxiosResponseTopicBox

        const topicBox = response.data.data

        const includedRelationships = this.state.includedRelationships

        if (typeof response.data.included !== "undefined") {
            includedRelationships.push(...response.data.included)
        }

        const {imageData, basicParagraphData} = this.getImageAndBasicParagraphData(topicBox)

        let titleColor: TitleColor
        if (topicBox.relationships.field_title_color.data !== null) {
            const titleColorData = this.props.includedRelationships.find((p) => p.id === topicBox.relationships.field_title_color.data.id) as ITaxonomyData

            titleColor = (typeof titleColorData !== "undefined") ? titleColorData.attributes.name : this.state.titleColor
        } else {
            titleColor = "Blue"
        }
        const url = await this.getUrl(topicBox.attributes.field_website_link)

        this.setState({
            title: topicBox.attributes.field_title,
            titleColor,
            width: topicBox.attributes.field_topic_box_width,
            url: url || "",
            imageId: (typeof imageData !== "undefined") ? imageData.id : this.state.imageId,
            basicParagraphId: (typeof basicParagraphData !== "undefined") ? basicParagraphData.id : this.state.basicParagraphId,
            includedRelationships,
        })
    }

    private getDefaultState() {
        try {
            const topicBox = this.getTopicBoxData()
            const {imageData, basicParagraphData} = this.getImageAndBasicParagraphData(topicBox)
            const titleColorData = this.props.includedRelationships.find((p) => p.id === topicBox.relationships.field_title_color.data.id) as ITaxonomyData
            const titleColor = (typeof titleColorData !== "undefined") ? titleColorData.attributes.name : null
            return {
                title: topicBox.attributes.field_title,
                width: topicBox.attributes.field_topic_box_width,
                titleColor,
                url: (topicBox.attributes.field_website_link !== null) ? topicBox.attributes.field_website_link.uri : null,
                imageId: (typeof imageData !== "undefined") ? imageData.id : null,
                basicParagraphId: (typeof basicParagraphData !== "undefined") ? basicParagraphData.id : null,
                includedRelationships: this.props.includedRelationships,
            } as ITopicBoxState
        } catch (e) {
            return {
                title: "",
                width: 1,
                titleColor: "Blue",
                url: "",
                imageId: "",
                basicParagraphId: "",
                includedRelationships: this.props.includedRelationships,
            } as ITopicBoxState
        }
    }

    private checkUrlForEntity(link: ITopicBoxWebsiteLink) {
        if (link !== null && link.uri !== null) {
            if (link.uri.substr(0, 11) === "entity:node") {
                return true
            }
        }
        return false
    }

    private checkUrlStringForEntity(link: string) {
        if (link.substr(0, 11) === "entity:node") {
            return true
        }
        return false
    }

    private async getUrl(link: ITopicBoxWebsiteLink) {
        const map = this.props.siteDomain
            .replace("https://", "")
            .replace("//", "")
            .replace(".com", "")
        if (this.checkUrlForEntity(link)) {
            const sitemap = await GetSiteMap(map)
            const matchedPage = sitemap.find((page) =>
                page.attributes.nid.toString() === link.uri.substr(12))
            if (typeof matchedPage !== "undefined") {
                return matchedPage.attributes.path.alias
            }

            const newsSitemap = await GetSiteMap("news")
            const matchedNewsPage = newsSitemap.find((page) =>
                page.attributes.nid.toString() === link.uri.substr(12))
            if (typeof matchedNewsPage !== "undefined") {
                return matchedNewsPage.attributes.path.alias
            }

        }
        return (link !== null && link.uri !== null) ? link.uri : null
    }

    private getTopicBoxData() {
        return this.props.includedRelationships.find((p) => p.id === this.props.paragraphId) as ITopicBoxData
    }

    private getImageAndBasicParagraphData(topicBox: ITopicBoxData) {
        const imageData = topicBox.relationships.field_content.data.find(
            (data) => data.type === Type.IMAGE_TOPIC_BOX,
        )
        const basicParagraphData = topicBox.relationships.field_content.data.find(
            (data) => data.type === Type.BASIC_PARAGRAPH,
        )
        return {imageData, basicParagraphData}
    }

}

export default TopicBoxContainer

export interface ITopicBoxProps {
    includedRelationships: Page.IPageData[],
    paragraphId: string,
    siteDomain: string,
}

export interface ITopicBoxState {
    title: string,
    titleColor: TitleColor,
    width: number,
    url: string,
    imageId: string,
    basicParagraphId: string,
    includedRelationships: Page.IPageData[],
}

export interface ITopicBoxData extends Page.IPageData {
    attributes: ITopicBoxAttributes,
    relationships: ITopicBoxRelationships,
}

export interface ITopicBoxAttributes extends Page.IAttributes {
    field_title: string,
    field_topic_box_width: number,
    field_website_link: ITopicBoxWebsiteLink,
}

export interface ITopicBoxWebsiteLink {
    uri: string,
    title?: string,
}

export interface ITaxonomyData extends Page.IPageData {
    attributes: ITaxonomyAttributes
}

export interface ITaxonomyAttributes extends Page.IAttributes {
    name: TitleColor
}

export interface ITopicBoxRelationships extends Page.IRelationships {
    field_content: {
        data: Page.IData[],
    }
    field_title_color: {
        data: Page.IData,
    }
}

export interface IAxiosResponseTopicBox extends AxiosResponse {
    data: {
        data: ITopicBoxData,
        included: Page.IPageData[],
    }
}

export type TitleColor = "Red" | "Green" | "Orange" | "Blue" | "Gray"
