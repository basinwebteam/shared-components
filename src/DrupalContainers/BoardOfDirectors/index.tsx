import axios from "axios"
import * as IAxios from "axios/index"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import { BoardOfDirectors, IBoard, IPeople } from "../../components/BoardOfDirectors"

export class BoardOfDirectorsContainer extends React.Component<IBoardDirContainerProps, IBoardDirContainerState> {
    constructor(props: IBoardDirContainerProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    public displayName = "DrupalBoardOfDirectorsContainer"

    public render() {
        return (
            <BoardOfDirectors boards={this.state.boards} />
        )
    }

    async componentDidMount() {
        await this.getData()
    }

    async componentDidUpdate(prevProps: IBoardDirContainerProps, prevState: IBoardDirContainerState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            await this.getData()
        }
    }

    async getData() {
        const response = await axios.get(
            "//api.bepc.com/api/cms/json/paragraph/board_of_directors_list/" +
            this.props.paragraphId +
            "?include=" +
            "field_board," +
            "field_board.field_content," +
            "field_board.field_content.field_biography_reference," +
            "field_board.field_content.field_biography_reference.field_image") as IBoardOfDirectorsListResponse

        const boardIds = response.data.data.relationships.field_board.data.map((board) => board.id) as string[]

        const boards = boardIds.map((id) => {
            const board = response.data.included.find((paragraph) => paragraph.id === id) as IParagraphBoardOfDirectors
            const members = board.relationships.field_content.data.map((member) =>
                this.getBoardMemberData(member.id, response.data.included as IParagraphBoardMember[])
            )
            return {
                name: board.attributes.title,
                members,
            }
        })

        this.setState({
            boards,
        })
    }

    getBoardMemberData(memberId: string, includedData: Array<IncludedParagraphTypes>) {
        const memberData = includedData.find((paragraph) => paragraph.id === memberId) as IParagraphBoardMember
        const { name, description } = this.getBoardMemberNameAndDescription(memberData.relationships.field_biography_reference.data.id, includedData)

        return {
            name,
            description,
            image: this.getBoardMemberImage(memberData.relationships.field_biography_reference.data.id, includedData),
            title: memberData.attributes.field_title,
            district: memberData.attributes.field_co_op_district.toString(),
        } as IPeople
    }

    getBoardMemberNameAndDescription(id: string, includedData: Array<IncludedParagraphTypes>) {
        const memberBioReference = includedData.find((paragraph) => paragraph.id === id) as IParagraphBiographyRef

        const description = typeof memberBioReference.attributes.field_biography === "string" ? memberBioReference.attributes.field_biography :
            memberBioReference.attributes.field_biography ?
                memberBioReference.attributes.field_biography.value :
                ""

        return {
            name: memberBioReference.attributes.field_first_name + " " + memberBioReference.attributes.field_last_name,
            description: description
        }
    }

    getBoardMemberImage(id: string, includedData: Array<IncludedParagraphTypes>) {
        const memberBioReference = includedData.find((paragraph) => paragraph.id === id) as IParagraphBiographyRef
        const imageId = memberBioReference.relationships.field_image.data.id
        const imageData = includedData.find((paragraph) => paragraph.id === imageId) as Page.IParagraphFile
        return this.props.siteDomain + imageData.attributes.url
    }

    getDefaultState() {
        return {
            boards: [
                {
                    name: "",
                    members: [
                        {
                            name: "",
                            image: "",
                            title: "",
                            district: "",
                            description: "",
                        },
                    ],
                },
            ],
        }
    }
}

export default BoardOfDirectorsContainer

export interface IBoardDirContainerProps {
    paragraphId: string,
    siteDomain: string,
}

export interface IBoardDirContainerState {
    boards: IBoard[]
}

export interface IBoardOfDirectorsListResponse extends IAxios.AxiosResponse {
    data: Page.IPage & {
        data: Page.IPageData & {
            attributes: Page.IAttributes & {
                field_include_show_all_link: boolean
            },
            relationships: Page.IRelationships & {
                field_board: Page.ITypeMultipleData
            }
        },
        included: Array<Page.IPageData | IParagraphBoardMember>
    }
}

export interface IParagraphBoardMember extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_title: string,
        field_co_op_district: number,
    },
    relationships: Page.IRelationships & {
        field_biography_reference: Page.IType
    }
}

export interface IParagraphBoardOfDirectors extends Page.IPageData {
    attributes: Page.IAttributes & {
        title: string
    },
    relationships: Page.IRelationships & {
        field_content: Page.ITypeMultipleData
    }
}

export interface IParagraphBiographyRef extends Page.IPageData {
    attributes: Page.IAttributes & {
        title: string,
        field_biography: Page.IWYSIWYG,
        field_employee_e_mail: string,
        field_employee_phone_number: string,
        field_employee_title: string,
        field_first_name: string,
        field_last_name: string,
    },
    relationships: Page.IRelationships & {
        field_image: Page.IField_Image
    }
}

export type IncludedParagraphTypes = IParagraphBoardMember | IParagraphBoardOfDirectors | IParagraphBiographyRef | Page.IParagraphFile
