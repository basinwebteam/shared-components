// tslint:disable:max-line-length

export default {
    "data": {
        "type": "paragraph--board_of_directors_list",
            "id": "0f3dea01-c972-4b8e-a050-fcf190234e89",
                "attributes": {
            "id": 12866,
                "uuid": "0f3dea01-c972-4b8e-a050-fcf190234e89",
                    "revision_id": 61596,
                        "langcode": "en",
                            "status": true,
                                "created": 1512682255,
                                    "parent_id": "5611",
                                        "parent_type": "node",
                                            "parent_field_name": "field_content",
                                                "behavior_settings": "a:0:{}",
                                                    "default_langcode": true,
                                                        "field_include_show_all_link": true
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "paragraphs_type--paragraphs_type",
                        "id": "580dd9d6-f100-4744-89d3-679006b98565"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89/revision_uid"
                }
            },
            "moderation_state": {
                "data": null
            },
            "field_board": {
                "data": [
                    {
                        "type": "node--board_of_directors",
                        "id": "b60b8d59-8e3f-43c1-a723-62ead2b97cff"
                    },
                    {
                        "type": "node--board_of_directors",
                        "id": "aaa7bcdc-0be9-462a-9151-b3d1406fb5bf"
                    }
                ],
                    "links": {
                    "self": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89/relationships/field_board",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89/field_board"
                }
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/paragraph/board_of_directors_list/0f3dea01-c972-4b8e-a050-fcf190234e89?include=field_board%2Cfield_board.field_content%2Cfield_board.field_content.field_biography_reference%2Cfield_board.field_content.field_biography_reference.field_image"
    },
    "included": [
        {
            "type": "node--board_of_directors",
            "id": "b60b8d59-8e3f-43c1-a723-62ead2b97cff",
            "attributes": {
                "nid": 5606,
                "uuid": "b60b8d59-8e3f-43c1-a723-62ead2b97cff",
                "vid": 19296,
                "langcode": "en",
                "status": true,
                "title": "Basin Electric",
                "created": 1512671796,
                "changed": 1513700335,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513700335,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "4fc1114c-fa13-4b89-89c4-45682feca961"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/b60b8d59-8e3f-43c1-a723-62ead2b97cff/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/board_of_directors/b60b8d59-8e3f-43c1-a723-62ead2b97cff/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/b60b8d59-8e3f-43c1-a723-62ead2b97cff/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/board_of_directors/b60b8d59-8e3f-43c1-a723-62ead2b97cff/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/b60b8d59-8e3f-43c1-a723-62ead2b97cff/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/board_of_directors/b60b8d59-8e3f-43c1-a723-62ead2b97cff/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_content": {
                    "data": [
                        {
                            "type": "paragraph--board_member",
                            "id": "dc88abce-d0ba-4398-a49f-721a1affbd66",
                            "meta": {
                                "target_revision_id": "61536"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "ab7d373d-33d2-48c3-aa06-46ec7b9b81d8",
                            "meta": {
                                "target_revision_id": "61541"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "edf95a79-bbb5-48d6-bafd-8cb2401ce854",
                            "meta": {
                                "target_revision_id": "61546"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d",
                            "meta": {
                                "target_revision_id": "61551"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "1291502f-a1a6-4391-9f84-c3c5a9ddff13",
                            "meta": {
                                "target_revision_id": "61556"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "3e1dae09-3c37-4b29-8fdb-ffc4a3b69946",
                            "meta": {
                                "target_revision_id": "61561"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "250ce772-12f3-47ce-b766-1069ac7c76ab",
                            "meta": {
                                "target_revision_id": "61566"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "789af649-23f5-4237-8ae2-d58fcfbb8493",
                            "meta": {
                                "target_revision_id": "61571"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "66cb892f-e2ba-444f-a147-c6044007af80",
                            "meta": {
                                "target_revision_id": "61576"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "bdfbb377-8c72-400d-8fb0-cb819328e3c3",
                            "meta": {
                                "target_revision_id": "61581"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "ec5e2fa5-f029-4aaa-a5fe-70ef9542da01",
                            "meta": {
                                "target_revision_id": "61586"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/b60b8d59-8e3f-43c1-a723-62ead2b97cff/relationships/field_content",
                        "related": "http://cms.bepc.com/jsonapi/node/board_of_directors/b60b8d59-8e3f-43c1-a723-62ead2b97cff/field_content"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/b60b8d59-8e3f-43c1-a723-62ead2b97cff"
            }
        },
        {
            "type": "node--board_of_directors",
            "id": "aaa7bcdc-0be9-462a-9151-b3d1406fb5bf",
            "attributes": {
                "nid": 5616,
                "uuid": "aaa7bcdc-0be9-462a-9151-b3d1406fb5bf",
                "vid": 19381,
                "langcode": "en",
                "status": true,
                "title": "Dakota Gas board",
                "created": 1512746262,
                "changed": 1513723141,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513723141,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "4fc1114c-fa13-4b89-89c4-45682feca961"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/aaa7bcdc-0be9-462a-9151-b3d1406fb5bf/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/board_of_directors/aaa7bcdc-0be9-462a-9151-b3d1406fb5bf/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/aaa7bcdc-0be9-462a-9151-b3d1406fb5bf/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/board_of_directors/aaa7bcdc-0be9-462a-9151-b3d1406fb5bf/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/aaa7bcdc-0be9-462a-9151-b3d1406fb5bf/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/board_of_directors/aaa7bcdc-0be9-462a-9151-b3d1406fb5bf/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_content": {
                    "data": [
                        {
                            "type": "paragraph--board_member",
                            "id": "51941656-9d32-4af0-9b39-5cff0078b965",
                            "meta": {
                                "target_revision_id": "61736"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "309617a4-8f58-43ce-88e2-c5d4f1640109",
                            "meta": {
                                "target_revision_id": "61741"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "617083db-3ad2-4d36-ad22-00887649aeb4",
                            "meta": {
                                "target_revision_id": "61746"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "ccbc67da-afab-4013-8b84-d4f46eb0657e",
                            "meta": {
                                "target_revision_id": "61751"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "e44b8ee7-a069-4916-90d7-654c1778a607",
                            "meta": {
                                "target_revision_id": "61756"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "4af2d19e-084f-42a2-a94d-3fd259b4f477",
                            "meta": {
                                "target_revision_id": "61761"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "f077c10f-f45b-4ddc-bbea-0de3f5ff9b21",
                            "meta": {
                                "target_revision_id": "61766"
                            }
                        },
                        {
                            "type": "paragraph--board_member",
                            "id": "d6ef933f-377f-4481-93ad-4cd5034ff71a",
                            "meta": {
                                "target_revision_id": "61771"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/aaa7bcdc-0be9-462a-9151-b3d1406fb5bf/relationships/field_content",
                        "related": "http://cms.bepc.com/jsonapi/node/board_of_directors/aaa7bcdc-0be9-462a-9151-b3d1406fb5bf/field_content"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/board_of_directors/aaa7bcdc-0be9-462a-9151-b3d1406fb5bf"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "dc88abce-d0ba-4398-a49f-721a1affbd66",
            "attributes": {
                "id": 12846,
                "uuid": "dc88abce-d0ba-4398-a49f-721a1affbd66",
                "revision_id": 61536,
                "langcode": "en",
                "status": true,
                "created": 1512671854,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 9,
                "field_title": "President of the Basin Electric board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/dc88abce-d0ba-4398-a49f-721a1affbd66/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/dc88abce-d0ba-4398-a49f-721a1affbd66/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/dc88abce-d0ba-4398-a49f-721a1affbd66/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/dc88abce-d0ba-4398-a49f-721a1affbd66/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/dc88abce-d0ba-4398-a49f-721a1affbd66/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/dc88abce-d0ba-4398-a49f-721a1affbd66/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "c2971698-ff3e-43ba-b712-6241ccc1a018"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/dc88abce-d0ba-4398-a49f-721a1affbd66/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/dc88abce-d0ba-4398-a49f-721a1affbd66/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/dc88abce-d0ba-4398-a49f-721a1affbd66"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "ab7d373d-33d2-48c3-aa06-46ec7b9b81d8",
            "attributes": {
                "id": 12851,
                "uuid": "ab7d373d-33d2-48c3-aa06-46ec7b9b81d8",
                "revision_id": 61541,
                "langcode": "en",
                "status": true,
                "created": 1512671854,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 1,
                "field_title": "Vice president of the Basin Electric board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ab7d373d-33d2-48c3-aa06-46ec7b9b81d8/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ab7d373d-33d2-48c3-aa06-46ec7b9b81d8/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ab7d373d-33d2-48c3-aa06-46ec7b9b81d8/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ab7d373d-33d2-48c3-aa06-46ec7b9b81d8/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ab7d373d-33d2-48c3-aa06-46ec7b9b81d8/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ab7d373d-33d2-48c3-aa06-46ec7b9b81d8/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "37fbc92b-5dbd-48a1-9160-dd2333a12833"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ab7d373d-33d2-48c3-aa06-46ec7b9b81d8/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ab7d373d-33d2-48c3-aa06-46ec7b9b81d8/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ab7d373d-33d2-48c3-aa06-46ec7b9b81d8"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "edf95a79-bbb5-48d6-bafd-8cb2401ce854",
            "attributes": {
                "id": 12996,
                "uuid": "edf95a79-bbb5-48d6-bafd-8cb2401ce854",
                "revision_id": 61546,
                "langcode": "en",
                "status": true,
                "created": 1513700024,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 8,
                "field_title": "Chairman of the Dakota Gasification Company board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/edf95a79-bbb5-48d6-bafd-8cb2401ce854/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/edf95a79-bbb5-48d6-bafd-8cb2401ce854/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/edf95a79-bbb5-48d6-bafd-8cb2401ce854/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/edf95a79-bbb5-48d6-bafd-8cb2401ce854/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/edf95a79-bbb5-48d6-bafd-8cb2401ce854/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/edf95a79-bbb5-48d6-bafd-8cb2401ce854/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "380a800d-e100-41aa-8885-25c0dcc5b7de"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/edf95a79-bbb5-48d6-bafd-8cb2401ce854/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/edf95a79-bbb5-48d6-bafd-8cb2401ce854/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/edf95a79-bbb5-48d6-bafd-8cb2401ce854"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d",
            "attributes": {
                "id": 13001,
                "uuid": "ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d",
                "revision_id": 61551,
                "langcode": "en",
                "status": true,
                "created": 1513700056,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 11,
                "field_title": "Vice chairman of the Dakota Gasification Company board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "468b06e1-327c-4fa7-a86d-b3da63eb7635"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ecc64e4c-a8d5-4745-be2e-5a4bdddeff2d"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "1291502f-a1a6-4391-9f84-c3c5a9ddff13",
            "attributes": {
                "id": 13006,
                "uuid": "1291502f-a1a6-4391-9f84-c3c5a9ddff13",
                "revision_id": 61556,
                "langcode": "en",
                "status": true,
                "created": 1513700135,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 10,
                "field_title": "Vice chairman of the Dakota Coal Company board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/1291502f-a1a6-4391-9f84-c3c5a9ddff13/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/1291502f-a1a6-4391-9f84-c3c5a9ddff13/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/1291502f-a1a6-4391-9f84-c3c5a9ddff13/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/1291502f-a1a6-4391-9f84-c3c5a9ddff13/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/1291502f-a1a6-4391-9f84-c3c5a9ddff13/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/1291502f-a1a6-4391-9f84-c3c5a9ddff13/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "057efa43-2937-4e86-afc0-a513d131e61a"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/1291502f-a1a6-4391-9f84-c3c5a9ddff13/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/1291502f-a1a6-4391-9f84-c3c5a9ddff13/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/1291502f-a1a6-4391-9f84-c3c5a9ddff13"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "3e1dae09-3c37-4b29-8fdb-ffc4a3b69946",
            "attributes": {
                "id": 13011,
                "uuid": "3e1dae09-3c37-4b29-8fdb-ffc4a3b69946",
                "revision_id": 61561,
                "langcode": "en",
                "status": true,
                "created": 1513700097,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 3,
                "field_title": "Chairman of the Dakota Coal board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/3e1dae09-3c37-4b29-8fdb-ffc4a3b69946/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/3e1dae09-3c37-4b29-8fdb-ffc4a3b69946/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/3e1dae09-3c37-4b29-8fdb-ffc4a3b69946/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/3e1dae09-3c37-4b29-8fdb-ffc4a3b69946/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/3e1dae09-3c37-4b29-8fdb-ffc4a3b69946/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/3e1dae09-3c37-4b29-8fdb-ffc4a3b69946/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "936741d0-5dee-4c66-9529-ace8b1402479"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/3e1dae09-3c37-4b29-8fdb-ffc4a3b69946/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/3e1dae09-3c37-4b29-8fdb-ffc4a3b69946/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/3e1dae09-3c37-4b29-8fdb-ffc4a3b69946"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "250ce772-12f3-47ce-b766-1069ac7c76ab",
            "attributes": {
                "id": 13016,
                "uuid": "250ce772-12f3-47ce-b766-1069ac7c76ab",
                "revision_id": 61566,
                "langcode": "en",
                "status": true,
                "created": 1513700164,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 5,
                "field_title": "Assistant Secretary of the Basin Electric board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/250ce772-12f3-47ce-b766-1069ac7c76ab/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/250ce772-12f3-47ce-b766-1069ac7c76ab/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/250ce772-12f3-47ce-b766-1069ac7c76ab/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/250ce772-12f3-47ce-b766-1069ac7c76ab/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/250ce772-12f3-47ce-b766-1069ac7c76ab/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/250ce772-12f3-47ce-b766-1069ac7c76ab/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "f8bcc486-cff0-4f5a-9917-1404147f62bc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/250ce772-12f3-47ce-b766-1069ac7c76ab/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/250ce772-12f3-47ce-b766-1069ac7c76ab/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/250ce772-12f3-47ce-b766-1069ac7c76ab"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "789af649-23f5-4237-8ae2-d58fcfbb8493",
            "attributes": {
                "id": 13021,
                "uuid": "789af649-23f5-4237-8ae2-d58fcfbb8493",
                "revision_id": 61571,
                "langcode": "en",
                "status": true,
                "created": 1513700203,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 7,
                "field_title": "Director on the Basin Electric board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/789af649-23f5-4237-8ae2-d58fcfbb8493/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/789af649-23f5-4237-8ae2-d58fcfbb8493/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/789af649-23f5-4237-8ae2-d58fcfbb8493/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/789af649-23f5-4237-8ae2-d58fcfbb8493/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/789af649-23f5-4237-8ae2-d58fcfbb8493/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/789af649-23f5-4237-8ae2-d58fcfbb8493/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "05f97e27-f739-4063-9278-fe65c2423209"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/789af649-23f5-4237-8ae2-d58fcfbb8493/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/789af649-23f5-4237-8ae2-d58fcfbb8493/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/789af649-23f5-4237-8ae2-d58fcfbb8493"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "66cb892f-e2ba-444f-a147-c6044007af80",
            "attributes": {
                "id": 12856,
                "uuid": "66cb892f-e2ba-444f-a147-c6044007af80",
                "revision_id": 61576,
                "langcode": "en",
                "status": true,
                "created": 1512671871,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 2,
                "field_title": "Director on the Basin Electric board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/66cb892f-e2ba-444f-a147-c6044007af80/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/66cb892f-e2ba-444f-a147-c6044007af80/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/66cb892f-e2ba-444f-a147-c6044007af80/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/66cb892f-e2ba-444f-a147-c6044007af80/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/66cb892f-e2ba-444f-a147-c6044007af80/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/66cb892f-e2ba-444f-a147-c6044007af80/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "2725759f-a11d-43c9-9df8-a941fa489ca4"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/66cb892f-e2ba-444f-a147-c6044007af80/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/66cb892f-e2ba-444f-a147-c6044007af80/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/66cb892f-e2ba-444f-a147-c6044007af80"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "bdfbb377-8c72-400d-8fb0-cb819328e3c3",
            "attributes": {
                "id": 12861,
                "uuid": "bdfbb377-8c72-400d-8fb0-cb819328e3c3",
                "revision_id": 61581,
                "langcode": "en",
                "status": true,
                "created": 1512671884,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 4,
                "field_title": "Treasurer on the Dakota Coal board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/bdfbb377-8c72-400d-8fb0-cb819328e3c3/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/bdfbb377-8c72-400d-8fb0-cb819328e3c3/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/bdfbb377-8c72-400d-8fb0-cb819328e3c3/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/bdfbb377-8c72-400d-8fb0-cb819328e3c3/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/bdfbb377-8c72-400d-8fb0-cb819328e3c3/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/bdfbb377-8c72-400d-8fb0-cb819328e3c3/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "53d3426e-85a7-4421-b7d8-f0543d0fe5e2"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/bdfbb377-8c72-400d-8fb0-cb819328e3c3/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/bdfbb377-8c72-400d-8fb0-cb819328e3c3/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/bdfbb377-8c72-400d-8fb0-cb819328e3c3"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "ec5e2fa5-f029-4aaa-a5fe-70ef9542da01",
            "attributes": {
                "id": 13026,
                "uuid": "ec5e2fa5-f029-4aaa-a5fe-70ef9542da01",
                "revision_id": 61586,
                "langcode": "en",
                "status": true,
                "created": 1513700235,
                "parent_id": "5606",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 6,
                "field_title": "Director on the Basin Electric board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ec5e2fa5-f029-4aaa-a5fe-70ef9542da01/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ec5e2fa5-f029-4aaa-a5fe-70ef9542da01/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ec5e2fa5-f029-4aaa-a5fe-70ef9542da01/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ec5e2fa5-f029-4aaa-a5fe-70ef9542da01/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ec5e2fa5-f029-4aaa-a5fe-70ef9542da01/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ec5e2fa5-f029-4aaa-a5fe-70ef9542da01/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "1dfabd63-c469-47d4-b82c-5fe696653d2b"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ec5e2fa5-f029-4aaa-a5fe-70ef9542da01/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ec5e2fa5-f029-4aaa-a5fe-70ef9542da01/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ec5e2fa5-f029-4aaa-a5fe-70ef9542da01"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "c2971698-ff3e-43ba-b712-6241ccc1a018",
            "attributes": {
                "nid": 376,
                "uuid": "c2971698-ff3e-43ba-b712-6241ccc1a018",
                "vid": 18946,
                "langcode": "en",
                "status": true,
                "title": "President of the Basin Electric board - Wayne Peltier",
                "created": 1500041041,
                "changed": 1513034175,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034175,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Also serves on the Dakota Coal Company and Montana Limestone Company boards; Basin Electric director since 2008 representing District 9; electric cooperative board member since 1999; Willmar Junior College (MN); farmer and co-owner of P&amp;K Fabricating, Cottonwood, MN.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "President of the Basin Electric board",
                "field_first_name": "Wayne",
                "field_last_name": "Peltier",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/c2971698-ff3e-43ba-b712-6241ccc1a018/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/c2971698-ff3e-43ba-b712-6241ccc1a018/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "480de710-5353-495c-becd-042972822e98"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/c2971698-ff3e-43ba-b712-6241ccc1a018/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/c2971698-ff3e-43ba-b712-6241ccc1a018/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/c2971698-ff3e-43ba-b712-6241ccc1a018/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/c2971698-ff3e-43ba-b712-6241ccc1a018/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "77b5a76c-30e2-4b5d-aa58-46f931faf440",
                        "meta": {
                            "alt": "Wayne Peltier",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/c2971698-ff3e-43ba-b712-6241ccc1a018/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/c2971698-ff3e-43ba-b712-6241ccc1a018/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/c2971698-ff3e-43ba-b712-6241ccc1a018"
            }
        },
        {
            "type": "file--file",
            "id": "77b5a76c-30e2-4b5d-aa58-46f931faf440",
            "attributes": {
                "fid": 646,
                "uuid": "77b5a76c-30e2-4b5d-aa58-46f931faf440",
                "langcode": "en",
                "filename": "Peltier-Wayne-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Peltier-Wayne-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 8766,
                "status": true,
                "created": 1500041059,
                "changed": 1500041288,
                "url": "/sites/CMS/files/images/bios/Peltier-Wayne-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "480de710-5353-495c-becd-042972822e98"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/77b5a76c-30e2-4b5d-aa58-46f931faf440/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/77b5a76c-30e2-4b5d-aa58-46f931faf440/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/77b5a76c-30e2-4b5d-aa58-46f931faf440"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "37fbc92b-5dbd-48a1-9160-dd2333a12833",
            "attributes": {
                "nid": 386,
                "uuid": "37fbc92b-5dbd-48a1-9160-dd2333a12833",
                "vid": 18951,
                "langcode": "en",
                "status": true,
                "title": "Vice president of the Basin Electric board - Kermit Pearson",
                "created": 1500043098,
                "changed": 1513034202,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034202,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Also serves as Treasurer&nbsp;on the Dakota Gasification Company; Basin Electric director since 1997 representing District 1, East River Electric Cooperative; electric cooperative board member since 1981; B.S. South Dakota State University; farmer/rancher raising small grains and purebred Gelbvieh cattle.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Vice president of the Basin Electric board",
                "field_first_name": "Kermit",
                "field_last_name": "Pearson",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/37fbc92b-5dbd-48a1-9160-dd2333a12833/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/37fbc92b-5dbd-48a1-9160-dd2333a12833/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/37fbc92b-5dbd-48a1-9160-dd2333a12833/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/37fbc92b-5dbd-48a1-9160-dd2333a12833/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/37fbc92b-5dbd-48a1-9160-dd2333a12833/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/37fbc92b-5dbd-48a1-9160-dd2333a12833/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "148bc7dd-415b-4258-989d-a35218689d1c",
                        "meta": {
                            "alt": "Kermit Pearson",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/37fbc92b-5dbd-48a1-9160-dd2333a12833/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/37fbc92b-5dbd-48a1-9160-dd2333a12833/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/37fbc92b-5dbd-48a1-9160-dd2333a12833"
            }
        },
        {
            "type": "file--file",
            "id": "148bc7dd-415b-4258-989d-a35218689d1c",
            "attributes": {
                "fid": 651,
                "uuid": "148bc7dd-415b-4258-989d-a35218689d1c",
                "langcode": "en",
                "filename": "Pearson-Kermit-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Pearson-Kermit-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 8631,
                "status": true,
                "created": 1500043140,
                "changed": 1500043155,
                "url": "/sites/CMS/files/images/bios/Pearson-Kermit-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/148bc7dd-415b-4258-989d-a35218689d1c/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/148bc7dd-415b-4258-989d-a35218689d1c/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/148bc7dd-415b-4258-989d-a35218689d1c"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "380a800d-e100-41aa-8885-25c0dcc5b7de",
            "attributes": {
                "nid": 416,
                "uuid": "380a800d-e100-41aa-8885-25c0dcc5b7de",
                "vid": 18961,
                "langcode": "en",
                "status": true,
                "title": "Chairman of the Dakota Gasification Company board - Allen Thiessen",
                "created": 1500043947,
                "changed": 1513034392,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034392,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Basin Electric director since 2012 representing District 8, Upper Missouri G&amp;T; resides in Lambert, MT; director at his home cooperative at Lower Yellowstone REA, Sidney, MT.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Chairman of the Dakota Gasification Company board",
                "field_first_name": "Allen",
                "field_last_name": "Thiessen",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/380a800d-e100-41aa-8885-25c0dcc5b7de/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/380a800d-e100-41aa-8885-25c0dcc5b7de/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/380a800d-e100-41aa-8885-25c0dcc5b7de/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/380a800d-e100-41aa-8885-25c0dcc5b7de/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/380a800d-e100-41aa-8885-25c0dcc5b7de/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/380a800d-e100-41aa-8885-25c0dcc5b7de/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "275217fd-8af4-42ef-8cdb-3cd83c74de09",
                        "meta": {
                            "alt": "Allen Thiessen",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/380a800d-e100-41aa-8885-25c0dcc5b7de/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/380a800d-e100-41aa-8885-25c0dcc5b7de/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/380a800d-e100-41aa-8885-25c0dcc5b7de"
            }
        },
        {
            "type": "file--file",
            "id": "275217fd-8af4-42ef-8cdb-3cd83c74de09",
            "attributes": {
                "fid": 681,
                "uuid": "275217fd-8af4-42ef-8cdb-3cd83c74de09",
                "langcode": "en",
                "filename": "Thiessen-Allen-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Thiessen-Allen-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 8717,
                "status": true,
                "created": 1500043965,
                "changed": 1500043975,
                "url": "/sites/CMS/files/images/bios/Thiessen-Allen-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/275217fd-8af4-42ef-8cdb-3cd83c74de09/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/275217fd-8af4-42ef-8cdb-3cd83c74de09/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/275217fd-8af4-42ef-8cdb-3cd83c74de09"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "468b06e1-327c-4fa7-a86d-b3da63eb7635",
            "attributes": {
                "nid": 411,
                "uuid": "468b06e1-327c-4fa7-a86d-b3da63eb7635",
                "vid": 18921,
                "langcode": "en",
                "status": true,
                "title": "Vice chairman of the Dakota Gasification Company board - Charlie Gilbert",
                "created": 1500043905,
                "changed": 1513033770,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513033770,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Also serves as Secretary/Treasurer of the Basin Electric board; Basin Electric director since 2009 representing Corn Belt Power Cooperative, District 11; Midland Power Cooperative board director since 1998; Corn Belt Power Cooperative board director since 2000; Iowa State University; corn and soybean farmer.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Vice chairman of the Dakota Gasification Company board",
                "field_first_name": "Charlie",
                "field_last_name": "Gilbert",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/468b06e1-327c-4fa7-a86d-b3da63eb7635/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/468b06e1-327c-4fa7-a86d-b3da63eb7635/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/468b06e1-327c-4fa7-a86d-b3da63eb7635/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/468b06e1-327c-4fa7-a86d-b3da63eb7635/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/468b06e1-327c-4fa7-a86d-b3da63eb7635/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/468b06e1-327c-4fa7-a86d-b3da63eb7635/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "b4ce703c-201c-43c1-be77-634caf1df934",
                        "meta": {
                            "alt": "Charlie Gilbert",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/468b06e1-327c-4fa7-a86d-b3da63eb7635/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/468b06e1-327c-4fa7-a86d-b3da63eb7635/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/468b06e1-327c-4fa7-a86d-b3da63eb7635"
            }
        },
        {
            "type": "file--file",
            "id": "b4ce703c-201c-43c1-be77-634caf1df934",
            "attributes": {
                "fid": 676,
                "uuid": "b4ce703c-201c-43c1-be77-634caf1df934",
                "langcode": "en",
                "filename": "Gilbert-Charlie-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Gilbert-Charlie-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 9589,
                "status": true,
                "created": 1500043924,
                "changed": 1500043936,
                "url": "/sites/CMS/files/images/bios/Gilbert-Charlie-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/b4ce703c-201c-43c1-be77-634caf1df934/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/b4ce703c-201c-43c1-be77-634caf1df934/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/b4ce703c-201c-43c1-be77-634caf1df934"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "057efa43-2937-4e86-afc0-a513d131e61a",
            "attributes": {
                "nid": 426,
                "uuid": "057efa43-2937-4e86-afc0-a513d131e61a",
                "vid": 18936,
                "langcode": "en",
                "status": true,
                "title": "Vice chairman of the Dakota Coal Company board - Paul Baker",
                "created": 1500044040,
                "changed": 1513034021,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034021,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Also serves as Vice Chairman of the Montana Limestone Company board; Basin Electric director since 2013 representing District 10, Members 1st Power Cooperative.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Vice chairman of the Dakota Coal Company board",
                "field_first_name": "Paul",
                "field_last_name": "Baker",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/057efa43-2937-4e86-afc0-a513d131e61a/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/057efa43-2937-4e86-afc0-a513d131e61a/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/057efa43-2937-4e86-afc0-a513d131e61a/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/057efa43-2937-4e86-afc0-a513d131e61a/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/057efa43-2937-4e86-afc0-a513d131e61a/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/057efa43-2937-4e86-afc0-a513d131e61a/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "7fbe2ca9-ffa2-4429-b937-29218b45cc7e",
                        "meta": {
                            "alt": "Paul Baker",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/057efa43-2937-4e86-afc0-a513d131e61a/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/057efa43-2937-4e86-afc0-a513d131e61a/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/057efa43-2937-4e86-afc0-a513d131e61a"
            }
        },
        {
            "type": "file--file",
            "id": "7fbe2ca9-ffa2-4429-b937-29218b45cc7e",
            "attributes": {
                "fid": 691,
                "uuid": "7fbe2ca9-ffa2-4429-b937-29218b45cc7e",
                "langcode": "en",
                "filename": "Baker-Paul-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Baker-Paul-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 9889,
                "status": true,
                "created": 1500044057,
                "changed": 1500044066,
                "url": "/sites/CMS/files/images/bios/Baker-Paul-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/7fbe2ca9-ffa2-4429-b937-29218b45cc7e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/7fbe2ca9-ffa2-4429-b937-29218b45cc7e/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/7fbe2ca9-ffa2-4429-b937-29218b45cc7e"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "936741d0-5dee-4c66-9529-ace8b1402479",
            "attributes": {
                "nid": 401,
                "uuid": "936741d0-5dee-4c66-9529-ace8b1402479",
                "vid": 18966,
                "langcode": "en",
                "status": true,
                "title": "Chairman of the Dakota Coal board - Troy Presser",
                "created": 1500043289,
                "changed": 1513034474,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034474,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Also serves as chairman of the Montana Limestone board; Basin Electric director since 2015 representing District 3, Central Power Electric; electric cooperative board member since 2007; rancher.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Chairman of the Dakota Coal board",
                "field_first_name": "Troy",
                "field_last_name": "Presser",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/936741d0-5dee-4c66-9529-ace8b1402479/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/936741d0-5dee-4c66-9529-ace8b1402479/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/936741d0-5dee-4c66-9529-ace8b1402479/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/936741d0-5dee-4c66-9529-ace8b1402479/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/936741d0-5dee-4c66-9529-ace8b1402479/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/936741d0-5dee-4c66-9529-ace8b1402479/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "642058c9-d0c7-46da-a6c8-41bca7e2b8f5",
                        "meta": {
                            "alt": "Troy Presser",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/936741d0-5dee-4c66-9529-ace8b1402479/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/936741d0-5dee-4c66-9529-ace8b1402479/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/936741d0-5dee-4c66-9529-ace8b1402479"
            }
        },
        {
            "type": "file--file",
            "id": "642058c9-d0c7-46da-a6c8-41bca7e2b8f5",
            "attributes": {
                "fid": 666,
                "uuid": "642058c9-d0c7-46da-a6c8-41bca7e2b8f5",
                "langcode": "en",
                "filename": "Presser-Troy-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Presser-Troy-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 9055,
                "status": true,
                "created": 1500043700,
                "changed": 1500043726,
                "url": "/sites/CMS/files/images/bios/Presser-Troy-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/642058c9-d0c7-46da-a6c8-41bca7e2b8f5/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/642058c9-d0c7-46da-a6c8-41bca7e2b8f5/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/642058c9-d0c7-46da-a6c8-41bca7e2b8f5"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "f8bcc486-cff0-4f5a-9917-1404147f62bc",
            "attributes": {
                "nid": 421,
                "uuid": "f8bcc486-cff0-4f5a-9917-1404147f62bc",
                "vid": 18971,
                "langcode": "en",
                "status": true,
                "title": "Assistant Secretary of the Basin Electric board - Leo Brekel",
                "created": 1500043986,
                "changed": 1513034528,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034528,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Basin Electric board member since 2014 representing District 5, Tri-State G&amp;T; electric cooperative board member since 1995; also serves on the Dakota Gasification Company board;&nbsp;retired as physical plant director at Northeastern Junior College in Sterling, CO; works with son on wheat farm near Fleming, CO.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Assistant Secretary of the Basin Electric board",
                "field_first_name": "Leo",
                "field_last_name": "Brekel",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/f8bcc486-cff0-4f5a-9917-1404147f62bc/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/f8bcc486-cff0-4f5a-9917-1404147f62bc/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/f8bcc486-cff0-4f5a-9917-1404147f62bc/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/f8bcc486-cff0-4f5a-9917-1404147f62bc/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/f8bcc486-cff0-4f5a-9917-1404147f62bc/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/f8bcc486-cff0-4f5a-9917-1404147f62bc/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "385900d2-cb57-48fd-b742-35c1b4015b50",
                        "meta": {
                            "alt": "Leo Brekel",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/f8bcc486-cff0-4f5a-9917-1404147f62bc/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/f8bcc486-cff0-4f5a-9917-1404147f62bc/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/f8bcc486-cff0-4f5a-9917-1404147f62bc"
            }
        },
        {
            "type": "file--file",
            "id": "385900d2-cb57-48fd-b742-35c1b4015b50",
            "attributes": {
                "fid": 686,
                "uuid": "385900d2-cb57-48fd-b742-35c1b4015b50",
                "langcode": "en",
                "filename": "Brekel-Leo-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Brekel-Leo-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 8720,
                "status": true,
                "created": 1500044015,
                "changed": 1500044026,
                "url": "/sites/CMS/files/images/bios/Brekel-Leo-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/385900d2-cb57-48fd-b742-35c1b4015b50/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/385900d2-cb57-48fd-b742-35c1b4015b50/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/385900d2-cb57-48fd-b742-35c1b4015b50"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "05f97e27-f739-4063-9278-fe65c2423209",
            "attributes": {
                "nid": 431,
                "uuid": "05f97e27-f739-4063-9278-fe65c2423209",
                "vid": 18941,
                "langcode": "en",
                "status": true,
                "title": "Director on the Basin Electric board - Mike McQuistion",
                "created": 1500044078,
                "changed": 1513034093,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034093,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Basin Electric director since 2013 representing District 7, Rushmore Electric; also serves on the Dakota Gasification Company board.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Director on the Basin Electric board",
                "field_first_name": "Mike",
                "field_last_name": "McQuistion",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/05f97e27-f739-4063-9278-fe65c2423209/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/05f97e27-f739-4063-9278-fe65c2423209/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/05f97e27-f739-4063-9278-fe65c2423209/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/05f97e27-f739-4063-9278-fe65c2423209/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/05f97e27-f739-4063-9278-fe65c2423209/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/05f97e27-f739-4063-9278-fe65c2423209/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "f7d8b640-5415-4f2d-a1c5-0f9943384354",
                        "meta": {
                            "alt": "Mike McQuistion",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/05f97e27-f739-4063-9278-fe65c2423209/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/05f97e27-f739-4063-9278-fe65c2423209/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/05f97e27-f739-4063-9278-fe65c2423209"
            }
        },
        {
            "type": "file--file",
            "id": "f7d8b640-5415-4f2d-a1c5-0f9943384354",
            "attributes": {
                "fid": 696,
                "uuid": "f7d8b640-5415-4f2d-a1c5-0f9943384354",
                "langcode": "en",
                "filename": "McQuiston-Mike-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/McQuiston-Mike-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 9564,
                "status": true,
                "created": 1500044094,
                "changed": 1500044107,
                "url": "/sites/CMS/files/images/bios/McQuiston-Mike-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/f7d8b640-5415-4f2d-a1c5-0f9943384354/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/f7d8b640-5415-4f2d-a1c5-0f9943384354/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/f7d8b640-5415-4f2d-a1c5-0f9943384354"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "2725759f-a11d-43c9-9df8-a941fa489ca4",
            "attributes": {
                "nid": 391,
                "uuid": "2725759f-a11d-43c9-9df8-a941fa489ca4",
                "vid": 18976,
                "langcode": "en",
                "status": true,
                "title": "Director on the Basin Electric board - David Meschke",
                "created": 1500043168,
                "changed": 1513034569,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034569,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Also serves on the&nbsp;Dakota Coal&nbsp;and Montana Limestone boards; Basin Electric director since 2017&nbsp;representing District 2, L&amp;O Power; electric cooperative board member since 2005; certified credentialed director - National Rural Electric Cooperatives Association; board leadership certificate; crop insurance adjuster; grain and livestock farm, raising corn, soybeans, cattle, and hogs.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Director on the Basin Electric board",
                "field_first_name": "David",
                "field_last_name": "Meschke",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/2725759f-a11d-43c9-9df8-a941fa489ca4/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/2725759f-a11d-43c9-9df8-a941fa489ca4/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/2725759f-a11d-43c9-9df8-a941fa489ca4/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/2725759f-a11d-43c9-9df8-a941fa489ca4/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/2725759f-a11d-43c9-9df8-a941fa489ca4/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/2725759f-a11d-43c9-9df8-a941fa489ca4/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "9ee26b92-f74a-4fed-83a4-3a660ba356b6",
                        "meta": {
                            "alt": "David Meschke",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/2725759f-a11d-43c9-9df8-a941fa489ca4/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/2725759f-a11d-43c9-9df8-a941fa489ca4/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/2725759f-a11d-43c9-9df8-a941fa489ca4"
            }
        },
        {
            "type": "file--file",
            "id": "9ee26b92-f74a-4fed-83a4-3a660ba356b6",
            "attributes": {
                "fid": 8306,
                "uuid": "9ee26b92-f74a-4fed-83a4-3a660ba356b6",
                "langcode": "en",
                "filename": "Meschke-David-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Meschke-David-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 9980,
                "status": true,
                "created": 1513032862,
                "changed": 1513032862,
                "url": "/sites/CMS/files/images/bios/Meschke-David-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/9ee26b92-f74a-4fed-83a4-3a660ba356b6/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/9ee26b92-f74a-4fed-83a4-3a660ba356b6/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/9ee26b92-f74a-4fed-83a4-3a660ba356b6"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "53d3426e-85a7-4421-b7d8-f0543d0fe5e2",
            "attributes": {
                "nid": 396,
                "uuid": "53d3426e-85a7-4421-b7d8-f0543d0fe5e2",
                "vid": 18981,
                "langcode": "en",
                "status": true,
                "title": "Treasurer on the Dakota Coal board - Tom Wagner",
                "created": 1500043238,
                "changed": 1513034643,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034643,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Also serves as Treasurer on the Montana Limestone Company board;&nbsp;Basin Electric director since 2017 representing District 4, Northwest Iowa Power Cooperative (NIPCO); electric cooperative director since 2005; O'Brien County Soil &amp; Water District Commissioner; South O'Brien School Board;&nbsp;farmer.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Treasurer on the Dakota Coal board",
                "field_first_name": "Tom",
                "field_last_name": "Wagner",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/53d3426e-85a7-4421-b7d8-f0543d0fe5e2/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/53d3426e-85a7-4421-b7d8-f0543d0fe5e2/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/53d3426e-85a7-4421-b7d8-f0543d0fe5e2/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/53d3426e-85a7-4421-b7d8-f0543d0fe5e2/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/53d3426e-85a7-4421-b7d8-f0543d0fe5e2/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/53d3426e-85a7-4421-b7d8-f0543d0fe5e2/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "46818d35-2afd-48a9-b79f-6cb3eba6313a",
                        "meta": {
                            "alt": "Tom Wagner",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/53d3426e-85a7-4421-b7d8-f0543d0fe5e2/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/53d3426e-85a7-4421-b7d8-f0543d0fe5e2/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/53d3426e-85a7-4421-b7d8-f0543d0fe5e2"
            }
        },
        {
            "type": "file--file",
            "id": "46818d35-2afd-48a9-b79f-6cb3eba6313a",
            "attributes": {
                "fid": 8311,
                "uuid": "46818d35-2afd-48a9-b79f-6cb3eba6313a",
                "langcode": "en",
                "filename": "Wagner-Tom-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Wagner-Tom-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 9236,
                "status": true,
                "created": 1513033204,
                "changed": 1513033204,
                "url": "/sites/CMS/files/images/bios/Wagner-Tom-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/46818d35-2afd-48a9-b79f-6cb3eba6313a/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/46818d35-2afd-48a9-b79f-6cb3eba6313a/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/46818d35-2afd-48a9-b79f-6cb3eba6313a"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "1dfabd63-c469-47d4-b82c-5fe696653d2b",
            "attributes": {
                "nid": 406,
                "uuid": "1dfabd63-c469-47d4-b82c-5fe696653d2b",
                "vid": 18986,
                "langcode": "en",
                "status": true,
                "title": "Director on the Basin Electric board - Dan Gliko",
                "created": 1500043845,
                "changed": 1513034677,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1513034677,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Also serves on the Dakota Coal and&nbsp;Montana Limestone Company&nbsp;boards; Basin Electric director since 2017&nbsp;representing District 6, Central Montana; electric cooperative board member since 2001; construction contractor.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "Director on the Basin Electric board",
                "field_first_name": "Dan",
                "field_last_name": "Gliko",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/1dfabd63-c469-47d4-b82c-5fe696653d2b/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/1dfabd63-c469-47d4-b82c-5fe696653d2b/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/1dfabd63-c469-47d4-b82c-5fe696653d2b/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/1dfabd63-c469-47d4-b82c-5fe696653d2b/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/1dfabd63-c469-47d4-b82c-5fe696653d2b/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/1dfabd63-c469-47d4-b82c-5fe696653d2b/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "892ddca9-aab4-4bdc-9b31-5dc3b4d7e77f",
                        "meta": {
                            "alt": "Dan Gliko",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/1dfabd63-c469-47d4-b82c-5fe696653d2b/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/1dfabd63-c469-47d4-b82c-5fe696653d2b/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/1dfabd63-c469-47d4-b82c-5fe696653d2b"
            }
        },
        {
            "type": "file--file",
            "id": "892ddca9-aab4-4bdc-9b31-5dc3b4d7e77f",
            "attributes": {
                "fid": 8316,
                "uuid": "892ddca9-aab4-4bdc-9b31-5dc3b4d7e77f",
                "langcode": "en",
                "filename": "Gliko-Dan-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Gliko-Dan-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 10702,
                "status": true,
                "created": 1513033524,
                "changed": 1513033524,
                "url": "/sites/CMS/files/images/bios/Gliko-Dan-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/892ddca9-aab4-4bdc-9b31-5dc3b4d7e77f/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/892ddca9-aab4-4bdc-9b31-5dc3b4d7e77f/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/892ddca9-aab4-4bdc-9b31-5dc3b4d7e77f"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "51941656-9d32-4af0-9b39-5cff0078b965",
            "attributes": {
                "id": 13066,
                "uuid": "51941656-9d32-4af0-9b39-5cff0078b965",
                "revision_id": 61736,
                "langcode": "en",
                "status": true,
                "created": 1513722929,
                "parent_id": "5616",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 8,
                "field_title": "Chairman of the Dakota Gas board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/51941656-9d32-4af0-9b39-5cff0078b965/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/51941656-9d32-4af0-9b39-5cff0078b965/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/51941656-9d32-4af0-9b39-5cff0078b965/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/51941656-9d32-4af0-9b39-5cff0078b965/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/51941656-9d32-4af0-9b39-5cff0078b965/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/51941656-9d32-4af0-9b39-5cff0078b965/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "380a800d-e100-41aa-8885-25c0dcc5b7de"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/51941656-9d32-4af0-9b39-5cff0078b965/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/51941656-9d32-4af0-9b39-5cff0078b965/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/51941656-9d32-4af0-9b39-5cff0078b965"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "309617a4-8f58-43ce-88e2-c5d4f1640109",
            "attributes": {
                "id": 12876,
                "uuid": "309617a4-8f58-43ce-88e2-c5d4f1640109",
                "revision_id": 61741,
                "langcode": "en",
                "status": true,
                "created": 1512746473,
                "parent_id": "5616",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 11,
                "field_title": "Vice Chairman of the Dakota Gas board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/309617a4-8f58-43ce-88e2-c5d4f1640109/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/309617a4-8f58-43ce-88e2-c5d4f1640109/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/309617a4-8f58-43ce-88e2-c5d4f1640109/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/309617a4-8f58-43ce-88e2-c5d4f1640109/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/309617a4-8f58-43ce-88e2-c5d4f1640109/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/309617a4-8f58-43ce-88e2-c5d4f1640109/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "468b06e1-327c-4fa7-a86d-b3da63eb7635"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/309617a4-8f58-43ce-88e2-c5d4f1640109/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/309617a4-8f58-43ce-88e2-c5d4f1640109/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/309617a4-8f58-43ce-88e2-c5d4f1640109"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "617083db-3ad2-4d36-ad22-00887649aeb4",
            "attributes": {
                "id": 13071,
                "uuid": "617083db-3ad2-4d36-ad22-00887649aeb4",
                "revision_id": 61746,
                "langcode": "en",
                "status": true,
                "created": 1513722902,
                "parent_id": "5616",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 1,
                "field_title": "Treasurer of the Dakota Gas board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/617083db-3ad2-4d36-ad22-00887649aeb4/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/617083db-3ad2-4d36-ad22-00887649aeb4/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/617083db-3ad2-4d36-ad22-00887649aeb4/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/617083db-3ad2-4d36-ad22-00887649aeb4/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/617083db-3ad2-4d36-ad22-00887649aeb4/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/617083db-3ad2-4d36-ad22-00887649aeb4/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "37fbc92b-5dbd-48a1-9160-dd2333a12833"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/617083db-3ad2-4d36-ad22-00887649aeb4/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/617083db-3ad2-4d36-ad22-00887649aeb4/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/617083db-3ad2-4d36-ad22-00887649aeb4"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "ccbc67da-afab-4013-8b84-d4f46eb0657e",
            "attributes": {
                "id": 12871,
                "uuid": "ccbc67da-afab-4013-8b84-d4f46eb0657e",
                "revision_id": 61751,
                "langcode": "en",
                "status": true,
                "created": 1512746473,
                "parent_id": "5616",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 4,
                "field_title": "Director on the Dakota Gas board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ccbc67da-afab-4013-8b84-d4f46eb0657e/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ccbc67da-afab-4013-8b84-d4f46eb0657e/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ccbc67da-afab-4013-8b84-d4f46eb0657e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ccbc67da-afab-4013-8b84-d4f46eb0657e/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ccbc67da-afab-4013-8b84-d4f46eb0657e/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ccbc67da-afab-4013-8b84-d4f46eb0657e/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "53d3426e-85a7-4421-b7d8-f0543d0fe5e2"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ccbc67da-afab-4013-8b84-d4f46eb0657e/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/ccbc67da-afab-4013-8b84-d4f46eb0657e/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/ccbc67da-afab-4013-8b84-d4f46eb0657e"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "e44b8ee7-a069-4916-90d7-654c1778a607",
            "attributes": {
                "id": 13076,
                "uuid": "e44b8ee7-a069-4916-90d7-654c1778a607",
                "revision_id": 61756,
                "langcode": "en",
                "status": true,
                "created": 1513722971,
                "parent_id": "5616",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 5,
                "field_title": "Director on the Dakota Gas board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/e44b8ee7-a069-4916-90d7-654c1778a607/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/e44b8ee7-a069-4916-90d7-654c1778a607/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/e44b8ee7-a069-4916-90d7-654c1778a607/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/e44b8ee7-a069-4916-90d7-654c1778a607/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/e44b8ee7-a069-4916-90d7-654c1778a607/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/e44b8ee7-a069-4916-90d7-654c1778a607/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "f8bcc486-cff0-4f5a-9917-1404147f62bc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/e44b8ee7-a069-4916-90d7-654c1778a607/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/e44b8ee7-a069-4916-90d7-654c1778a607/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/e44b8ee7-a069-4916-90d7-654c1778a607"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "4af2d19e-084f-42a2-a94d-3fd259b4f477",
            "attributes": {
                "id": 13081,
                "uuid": "4af2d19e-084f-42a2-a94d-3fd259b4f477",
                "revision_id": 61761,
                "langcode": "en",
                "status": true,
                "created": 1513723077,
                "parent_id": "5616",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 7,
                "field_title": "Director on the Dakota Gas board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/4af2d19e-084f-42a2-a94d-3fd259b4f477/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/4af2d19e-084f-42a2-a94d-3fd259b4f477/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/4af2d19e-084f-42a2-a94d-3fd259b4f477/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/4af2d19e-084f-42a2-a94d-3fd259b4f477/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/4af2d19e-084f-42a2-a94d-3fd259b4f477/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/4af2d19e-084f-42a2-a94d-3fd259b4f477/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "05f97e27-f739-4063-9278-fe65c2423209"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/4af2d19e-084f-42a2-a94d-3fd259b4f477/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/4af2d19e-084f-42a2-a94d-3fd259b4f477/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/4af2d19e-084f-42a2-a94d-3fd259b4f477"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "f077c10f-f45b-4ddc-bbea-0de3f5ff9b21",
            "attributes": {
                "id": 13086,
                "uuid": "f077c10f-f45b-4ddc-bbea-0de3f5ff9b21",
                "revision_id": 61766,
                "langcode": "en",
                "status": true,
                "created": 1513723108,
                "parent_id": "5616",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 1,
                "field_title": "External director on the Dakota Gas board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/f077c10f-f45b-4ddc-bbea-0de3f5ff9b21/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/f077c10f-f45b-4ddc-bbea-0de3f5ff9b21/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/f077c10f-f45b-4ddc-bbea-0de3f5ff9b21/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/f077c10f-f45b-4ddc-bbea-0de3f5ff9b21/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/f077c10f-f45b-4ddc-bbea-0de3f5ff9b21/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/f077c10f-f45b-4ddc-bbea-0de3f5ff9b21/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/f077c10f-f45b-4ddc-bbea-0de3f5ff9b21/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/f077c10f-f45b-4ddc-bbea-0de3f5ff9b21/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/f077c10f-f45b-4ddc-bbea-0de3f5ff9b21"
            }
        },
        {
            "type": "paragraph--board_member",
            "id": "d6ef933f-377f-4481-93ad-4cd5034ff71a",
            "attributes": {
                "id": 13091,
                "uuid": "d6ef933f-377f-4481-93ad-4cd5034ff71a",
                "revision_id": 61771,
                "langcode": "en",
                "status": true,
                "created": 1513723111,
                "parent_id": "5616",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_co_op_district": 1,
                "field_title": "External director on the Dakota Gas board"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1359d3e3-6e6f-4b20-be9e-630f350a1647"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/d6ef933f-377f-4481-93ad-4cd5034ff71a/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/d6ef933f-377f-4481-93ad-4cd5034ff71a/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/d6ef933f-377f-4481-93ad-4cd5034ff71a/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/d6ef933f-377f-4481-93ad-4cd5034ff71a/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/d6ef933f-377f-4481-93ad-4cd5034ff71a/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/d6ef933f-377f-4481-93ad-4cd5034ff71a/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_biography_reference": {
                    "data": {
                        "type": "node--basin_contacts",
                        "id": "0927493f-02a4-42c4-b980-e2cf117189a6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/d6ef933f-377f-4481-93ad-4cd5034ff71a/relationships/field_biography_reference",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/board_member/d6ef933f-377f-4481-93ad-4cd5034ff71a/field_biography_reference"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/board_member/d6ef933f-377f-4481-93ad-4cd5034ff71a"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a",
            "attributes": {
                "nid": 436,
                "uuid": "e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a",
                "vid": 1831,
                "langcode": "en",
                "status": true,
                "title": "External director of the Dakota Gasification Company board - Jim Geringer",
                "created": 1500044118,
                "changed": 1500666339,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1500666339,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Dakota Gasification Company director since May 2012; former governor of the State of Wyoming (1995-2003); served in the Wyoming Legislature; former chairman of the Western Governor's Association, and the Education Commission of the states; currently serving as director of policy at the Environmental Systems Research Institute (Esri); B.S. mechanical engineering, 1967, Kansas State University, Manhattan.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "External director of the Dakota Gasification Company board",
                "field_first_name": "Jim",
                "field_last_name": "Geringer",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "a0c08c9a-ac59-45a1-9e7a-409531c6285f",
                        "meta": {
                            "alt": "Jim Geringer",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/e10f37d6-b2df-4f5c-a1fa-6ce5c729da8a"
            }
        },
        {
            "type": "file--file",
            "id": "a0c08c9a-ac59-45a1-9e7a-409531c6285f",
            "attributes": {
                "fid": 701,
                "uuid": "a0c08c9a-ac59-45a1-9e7a-409531c6285f",
                "langcode": "en",
                "filename": "Geringer-Jim-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Geringer-Jim-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 9251,
                "status": true,
                "created": 1500044180,
                "changed": 1500044233,
                "url": "/sites/CMS/files/images/bios/Geringer-Jim-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/a0c08c9a-ac59-45a1-9e7a-409531c6285f/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/a0c08c9a-ac59-45a1-9e7a-409531c6285f/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/a0c08c9a-ac59-45a1-9e7a-409531c6285f"
            }
        },
        {
            "type": "node--basin_contacts",
            "id": "0927493f-02a4-42c4-b980-e2cf117189a6",
            "attributes": {
                "nid": 441,
                "uuid": "0927493f-02a4-42c4-b980-e2cf117189a6",
                "vid": 1836,
                "langcode": "en",
                "status": true,
                "title": "External director of the Dakota Gasification Company board - Alan Klein",
                "created": 1500044243,
                "changed": 1500666360,
                "promote": false,
                "sticky": false,
                "revision_timestamp": 1500666360,
                "revision_log": null,
                "revision_translation_affected": true,
                "default_langcode": true,
                "path": null,
                "field_biography": {
                    "value": "<p>Dakota Gas director since 2013; Certified Public Accountant; Eide Bailly LLP retired partner, former partner-in-charge of Bismarck, ND, office; master of science in accounting, University of North Dakota, Grand Forks; bachelor's of science in business administration in accounting, University of North Dakota; serves on United Way Board of Trustees; originally from Wing, ND.</p>\r\n",
                    "format": "rich_text"
                },
                "field_employee_e_mail": null,
                "field_employee_phone_number": null,
                "field_employee_title": "External director of the Dakota Gasification Company board",
                "field_first_name": "Alan",
                "field_last_name": "Klein",
                "field_link_to_online_profile": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "node_type--node_type",
                        "id": "2eb57805-d460-436b-abe3-f34e15931ad6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/0927493f-02a4-42c4-b980-e2cf117189a6/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/0927493f-02a4-42c4-b980-e2cf117189a6/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/0927493f-02a4-42c4-b980-e2cf117189a6/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/0927493f-02a4-42c4-b980-e2cf117189a6/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/0927493f-02a4-42c4-b980-e2cf117189a6/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/0927493f-02a4-42c4-b980-e2cf117189a6/revision_uid"
                    }
                },
                "menu_link": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "f6252718-e357-4b08-873e-2879bbe5c2d0",
                        "meta": {
                            "alt": "Alan Klein",
                            "title": "",
                            "width": "160",
                            "height": "220"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/0927493f-02a4-42c4-b980-e2cf117189a6/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/node/basin_contacts/0927493f-02a4-42c4-b980-e2cf117189a6/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/node/basin_contacts/0927493f-02a4-42c4-b980-e2cf117189a6"
            }
        },
        {
            "type": "file--file",
            "id": "f6252718-e357-4b08-873e-2879bbe5c2d0",
            "attributes": {
                "fid": 706,
                "uuid": "f6252718-e357-4b08-873e-2879bbe5c2d0",
                "langcode": "en",
                "filename": "Klein-Alan-160px-Portrait-2017.jpg",
                "uri": "public://images/bios/Klein-Alan-160px-Portrait-2017.jpg",
                "filemime": "image/jpeg",
                "filesize": 8434,
                "status": true,
                "created": 1500044262,
                "changed": 1500044271,
                "url": "/sites/CMS/files/images/bios/Klein-Alan-160px-Portrait-2017.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/f6252718-e357-4b08-873e-2879bbe5c2d0/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/f6252718-e357-4b08-873e-2879bbe5c2d0/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/f6252718-e357-4b08-873e-2879bbe5c2d0"
            }
        }
    ]
}