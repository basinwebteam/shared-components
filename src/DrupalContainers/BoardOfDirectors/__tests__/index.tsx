import axios from "axios"
import * as moxios from "moxios"
import * as React from "react"
import { shallow, mount, render } from 'enzyme'
import Adapter from 'enzyme-adapter-react-15'
import toJson from "enzyme-to-json"
import * as Page from "../../../DrupalContentTypes/PageInterface"
import dataXHRRequest from "./mocks/boardOfDirectorListAPI"
import BoardOfDirectorsContainer from "../../BoardOfDirectors"

describe("Board of Directors", () => {
    const SITE_DOMAIN = "https://basinelectric.com"
    const boardOfDirectorsListId = "0f3dea01-c972-4b8e-a050-fcf190234e89"
    const testUrl = SITE_DOMAIN +
        "/jsonapi/paragraph/board_of_directors_list/" +
        boardOfDirectorsListId +
        "?include=" +
        "field_board," +
        "field_board.field_content," +
        "field_board.field_content.field_biography_reference," +
        "field_board.field_content.field_biography_reference.field_image"
    
    beforeEach(() => {
        moxios.install()
    })

    afterEach(() => {
        moxios.uninstall()
    })

    it("should not have changed output on initial render", () => {
        const element = shallow(
            <BoardOfDirectorsContainer
                paragraphId={boardOfDirectorsListId}
                siteDomain={SITE_DOMAIN}
            />
        )
        expect(toJson(element)).toMatchSnapshot()
        
    })


    it("Updates states on mount with XHR data and hasn't changed snapshot", async (done) => {
        
        const element = (
            <BoardOfDirectorsContainer
                paragraphId={boardOfDirectorsListId}
                siteDomain={SITE_DOMAIN}
            />
        )
        const mounted = mount(element)
        expect(mounted.state("boards").length).toEqual(1)
        moxios.wait(() => {
            let request = moxios.requests.mostRecent()
            request.respondWith({
                status: 200,
                response: dataXHRRequest
            }).then(() => {
                expect(mounted.state("boards").length).toEqual(2)
                expect(mounted.html()).toMatchSnapshot()  
                done()
            })
        })
    })
})