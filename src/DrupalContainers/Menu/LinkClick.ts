declare let window: IWindow

export const linkClick = (e: Event) => {
    switch (e.type) {
        case "touchstart":
        default:
            return false
        case "touchend":
        case "click":
            const target = e.target as HTMLElement

            const isChildLink = (linkIsChildLink(target))
            if (isChildLink) {
                resetMenu()
                return true // Navigate to the link that was clicked
            } else {
                showDesktopMenuDropDown(target)
                return false
            }
    }
}

const linkIsChildLink = (element: HTMLElement) => {
    const childClasses = [
        "menu-overview",
        "level2",
        "level3",
        "level4",
    ]
    let classMatch = false
    childClasses.forEach((className) => {
        classMatch = (element.parentElement && element.parentElement.classList.contains(className)) ? true : classMatch
    })
    return classMatch
}

const isDesktopMode = () => !isMobileMode()

const isMobileMode = () => ((document.querySelector(".mobile-menu") as HTMLElement).clientWidth > 0)

function showDesktopMenuDropDown(menuItem: HTMLElement): void | boolean {
    const menuItemParent = menuItem.parentElement
    const subMenu = (menuItemParent !== null) ? menuItemParent.querySelector("ul") : null
    if (menuItemParent && subMenu && isDesktopMode()) {
        if (closeMenuOnSecondClick(menuItem)) {
            return false
        }
        refreshMenu()
        menuItem.classList.add("clicked")
        // Animate Height
        // Get Last sub nav's height
        const menuHeightOldElement = document.querySelector(".mega-menu li.level1 ul.visible")
        const menuHeightOld = (menuHeightOldElement === null) ? 0 : menuHeightOldElement.clientHeight
        // Remove visible class from last sub nav
        removeClassNameFromElements("visible")

        const menuHeightNew = subMenu.clientHeight
        subMenu.style.height = menuHeightOld.toString()
        subMenu.classList.add("visible")
        subMenu.style.height = menuHeightNew + "px"
        const visElement = document.querySelector(".mega-menu ul.visible") as HTMLElement
        visElement.style.height = "auto"
    }
}

export const showMobileMenu = () => {
    const menu = document.querySelector(".mega-menu")
    if (menu !== null) {
        menu.classList.add("display-important")
        menu.classList.add("menu-open")
        refreshMenu()
        showMe(document.querySelector(".mega-menu.menu-open .level1") as HTMLElement)
        showMe(document.querySelector(".mega-menu.menu-open .level1 a") as HTMLElement)
        hideMe(document.querySelector(".mega-menu.menu-open .level1 ul") as HTMLElement)
    }
}

const removeClassNameFromElements = (className: string) => {
    const visibleElements = document.querySelectorAll(".mega-menu ." + className)
    if (visibleElements.length > 0) {
        for (let i = 0; i < visibleElements.length; i++) {
            visibleElements[i].classList.remove(className)
        }
    }
}

const closeMenuOnSecondClick = (menuItem: HTMLElement) => {
    if (menuItem.parentElement !== null) {
        const childMenu = menuItem.parentElement.getElementsByClassName("visible")
        if (childMenu.length > 0) {
            resetMenu()
            return true
        }
    }
    return false
}

export const resetMenu = () => {

    if (typeof window !== "undefined") {

        if (typeof (window as any).Event === "function") {
            const event = new Event("reset_mobile_menu")
            if (typeof window !== "undefined") {
                window.dispatchEvent(event)
            }
        } else {
            /**
             * IE 11 Pollyfill for new Events
             * TODO: 
             * Should be moved elsewhere but
             * should be using redux instead of events here
             */

            const CustomEvent = (event: string, params: { bubbles: boolean, cancelable: boolean, detail: any } = { bubbles: false, cancelable: false, detail: undefined }) => {
                params = params || { bubbles: false, cancelable: false, detail: undefined }
                const evt = document.createEvent("CustomEvent")
                evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail)
                return evt
            }

            CustomEvent.prototype = (window as any).Event.prototype

            window.CustomEvent = CustomEvent

            const IeEvent = CustomEvent("reset_mobile_menu")
            window.dispatchEvent(IeEvent)
        }
    }
}

const refreshMenu = () => {
    addRemoveMobileDesktopClasses()
    const children = document.querySelectorAll(".mega-menu li.level1 > ul") as NodeListOf<HTMLElement>
    if (isDesktopMode()) {
        for (let i = 0; i < children.length; i++) {
            styleDesktopSubMenu(children[i])
        }
    } else {
        for (let i = 0; i < children.length; i++) {
            removeStyleDesktopSubMenu(children[i])
        }
    }
}

const addRemoveMobileDesktopClasses = () => {
    const megaMenu = document.querySelector(".mega-menu")
    const nav = document.querySelector("nav")
    if (megaMenu && nav) {
        if (isDesktopMode()) {
            // remove any mobile menu scripts
            megaMenu.classList.remove("mobile")
            nav.classList.remove("display-important")
            nav.classList.remove("menu-open")
            megaMenu.classList.add("desktop")
            megaMenu.classList.remove("mobile")
            removeClassNameFromElements("clicked")
        } else {
            // Removed touched classes, important for when desktop menu is switched to mobile during landscape/portrait flip
            megaMenu.classList.remove("touched")

            megaMenu.classList.add("mobile")
            megaMenu.classList.remove("desktop")
        }
    }
}

const styleDesktopSubMenu = (item: HTMLElement) => {
    const megaMenu = document.querySelector(".mega-menu")
    const topBarSection = document.querySelector(".top-bar-section")
    const dropdownMenuContainer = (
        item.parentElement &&
        item.parentElement.parentElement &&
        item.parentElement.parentElement.parentElement &&
        item.parentElement.parentElement.parentElement.parentElement
    ) ? item.parentElement.parentElement.parentElement.parentElement : null

    if (megaMenu && topBarSection && dropdownMenuContainer) {
        const pageWidth = topBarSection.clientWidth
        let padding = (megaMenu.clientWidth - pageWidth) / 2

        // No negative padding
        if (padding < 0) {
            padding = 0
        }
        const listLeftOffset = dropdownMenuContainer.offsetLeft + padding
        item.style.width = megaMenu.clientWidth.toString() + "px"
        item.style.paddingLeft = padding.toString() + "px"
        item.style.paddingRight = padding.toString() + "px"
        item.style.left = "-" + (listLeftOffset) + "px"
    }
}

const removeStyleDesktopSubMenu = (item: HTMLElement) => {
    // return object to style css
    item.style.width = "100%"
    item.style.left = "0px"
    item.style.padding = "0px"
}

const hideMe = (element: HTMLElement) => {
    element.classList.remove("showMe")
    element.classList.add("hideMe")
}
const showMe = (element: HTMLElement) => {
    element.classList.remove("hideMe")
    element.classList.add("showMe")
}

export default linkClick

interface IWindow extends Window {
    CustomEvent: (event: any, props: any) => CustomEvent
}
