import {Menu, ILinks} from "../../components/Menu"
import * as React from "react"

export class MenuContainer extends React.Component<IMenuContainerProps> {

    constructor(props: IMenuContainerProps) {
        super(props)
        this.linkClickHandler = this.linkClickHandler.bind(this)
    }

    public render() {
        return (<Menu links={this.props.menuLinks} widthToChange={840} linkHandler={this.linkClickHandler} />)
    }

    public linkClickHandler(url: string, key: string, e: React.MouseEvent<HTMLElement>) {
        if (typeof window !== "undefined") {
            window.location.href = (e.target as HTMLAnchorElement).href
        }
    }

}

export default MenuContainer

export interface IMenuContainerProps {
    menuLinks: ILinks[]
}

/*
    <Link as={"/about-us"} href={"/page?id=81de6009-3f9e-46e6-b243-3d043d7727e6"}>
        <a>Annual Meeting</a>
    </Link>
*/
