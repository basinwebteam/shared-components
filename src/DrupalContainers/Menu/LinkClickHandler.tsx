import * as React from "react"

export const LinkClickHandler: React.StatelessComponent<IProps> = (props) => {

  const render = () => {
    const { onCustomClick, ...childProps } = props
    return <a {...childProps} onClick={handleClick} />
  }

  const handleClick = (event) => {
    if (props.onCustomClick) {
      const shouldContinue = props.onCustomClick(event)
      if (shouldContinue) {
        if (props.onClick) {
            props.onClick(event)
        }
      }
    } else {
      if (props.onClick) {
        props.onClick(event)
      }
    }

  }

  return render()

}

export default LinkClickHandler

interface IProps {
  onCustomClick?: (e: Event) => boolean,
  onClick?: (e: Event) => void,
}
