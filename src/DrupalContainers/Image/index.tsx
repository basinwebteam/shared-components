import { AxiosResponse, default as axios } from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import { getUrl } from "../../DrupalAPI/sitemap"
import Image from "../../components/Image"

/**
 * Tests TODO
 * Without default props sent
 * With default props sent
 */

export class ImageContainer extends React.Component<IImageContainerProps, IImageContainerState> {
    constructor(props: IImageContainerProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    public displayName = "DrupalImageContainer"

    public defaultProps: Partial<IImageContainerProps> = {
        imageFileFieldName: "field_image",
        imageParagraphName: "image"
    }

    public render() {
        const { url, altText, caption, link } = this.state
        if (url === "") {
            return <span className="empty-image-placeholder" />
        }
        if (link) {
            return (
                <a href={link}>
                    <Image
                        url={url}
                        altText={altText}
                        caption={caption}
                        className={(this.props.className) ? this.props.className : ""}
                    />
                </a>
            )
        }
        return (
            <Image
                url={url}
                altText={altText}
                caption={caption}
                className={(this.props.className) ? this.props.className : ""}
            />
        )
    }

    public componentDidMount() {
        this.updateImage()
    }

    public componentDidUpdate(prevProps: IImageContainerProps, prevState: IImageContainerState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateImage()
        }
    }

    public async updateImage() {
        const paragraphId = this.props.paragraphId
        const image = this.findImage()
        const link = await this.findImageLink(image)

        if (typeof image === "undefined") {
            this.queryImage(paragraphId)
        } else {
            const fileId = this.getFileIdFromImageData(image)
            const file = this.findImageFile(fileId)
            let url = ""
            if (typeof file === "undefined") {
                url = await this.queryFile(fileId)
            } else {
                url = this.props.siteDomain + file.attributes.url
            }

            this.setState({
                url,
                altText: this.getAltTextFromImageData(image),
                caption: this.getFieldCaption(image),
                link
            })
        }
    }

    public async queryImage(id: string) {
        const response = await axios.get
            (`//api.bepc.com/api/cms/json/paragraph/${this.props.imageParagraphName}/${id}/?include=${this.props.imageFileFieldName}`) as IAxiosResponseImage
        this.setState({
            url: this.props.siteDomain + response.data.included[0].attributes.url,
            altText: this.getAltTextFromImageData(response.data.data),
            caption: this.getFieldCaption(response.data.data),
        })
    }

    public getFieldCaption(image: IImageData | undefined) {
        const defaultCaption = (typeof this.state !== "undefined") ? this.state.caption : ""
        if (typeof image === "undefined") {
            return defaultCaption
        }
        if (!image.attributes.hasOwnProperty("field_caption")) {
            return defaultCaption
        }
        if (typeof image.attributes.field_caption === null) {
            return defaultCaption
        }
        if (typeof image.attributes.field_caption === "string") {
            return image.attributes.field_caption || defaultCaption
        }
        if (typeof image.attributes.field_caption === "object" && image.attributes.field_caption !== null) {
            return image.attributes.field_caption.value || defaultCaption
        }
        return defaultCaption
    }

    public async queryFile(id: string) {
        if (id === "") {
            return ""
        }
        const response = await axios.get
            (`//api.bepc.com/api/cms/file/${id}`) as IAxiosResponseImageFile
        return this.props.siteDomain + response.data.data.attributes.url
    }

    private getAltTextFromImageData(image: IImageData | undefined) {
        const defaultTxt = (typeof this.state !== "undefined") ? this.state.altText : ""
        if (typeof image === "undefined") {
            return defaultTxt
        }
        if (typeof image.relationships.field_image !== "undefined" &&
            image.relationships.field_image.data !== null) {
            return image.relationships.field_image.data.meta.alt
        }
        if (typeof image.relationships.field_image_aside !== "undefined" &&
            image.relationships.field_image_aside.data !== null) {
            return image.relationships.field_image_aside.data.meta.alt
        }
        return defaultTxt
    }

    private getFileIdFromImageData(image: IImageData | undefined) {
        if (typeof image === "undefined") {
            return ""
        }
        if (typeof image.relationships.field_image !== "undefined" &&
            image.relationships.field_image.data !== null) {
            return image.relationships.field_image.data.id
        }
        if (typeof image.relationships.field_image_aside !== "undefined" &&
            image.relationships.field_image_aside.data !== null) {
            return image.relationships.field_image_aside.data.id
        }
        return ""
    }

    private findImageFile(fileId: string) {
        if (fileId === "") {
            return undefined
        }
        return this.props.includedRelationships.find((paragraph) => paragraph.id === fileId) as IImageFileData
    }

    private findImage() {
        return this.props.includedRelationships.find((paragraph) => paragraph.id === this.props.paragraphId) as IImageData | undefined
    }

    private async findImageLink(image: IImageData | undefined) {
        const link = this.findDefaultImageLink(image)
        return link ? await getUrl(link, this.props.siteDomain) : null
    }

    private findDefaultImageLink(image: IImageData | undefined) {
        if (typeof image === "undefined") {
            return null
        }
        if (typeof image.attributes.field_website_link === "undefined") {
            return null
        }
        if (!image.attributes.field_website_link) {
            return null
        }
        return image.attributes.field_website_link.uri
    }

    private getDefaultState() {
        /*
            This tries to get the image and related file if its part of the props paragraphData value.

            If any of it is missing, this returns an empty state.
        */
        const image = this.findImage()
        const fileId = this.getFileIdFromImageData(image)
        const file = this.findImageFile(fileId)
        const caption = this.getFieldCaption(image)
        const link = this.findDefaultImageLink(image)

        if (typeof file !== "undefined") {
            return {
                altText: this.getAltTextFromImageData(image),
                url: this.props.siteDomain + file.attributes.url,
                caption,
                link,
            } as IImageContainerState
        }
        return {
            altText: "",
            url: "",
            caption: "",
            link,
        } as IImageContainerState
    }
}

export default ImageContainer

export interface IImageContainerProps {
    includedRelationships: Array<Page.IPageData | IImageFileData>,
    paragraphId: string,
    siteDomain: string,
    imageParagraphName?: string,
    imageFileFieldName?: imageFileFieldName,
    className?: string,
}
export interface IDefaultPropsImageContainer {
    imageParagraphName: string,
    imageFileFieldName: imageFileFieldName,
}
export interface IImageContainerState {
    altText: string
    url: string
    caption: string
    link: string | null
}

export interface IAxiosResponseImage extends AxiosResponse {
    data: IImageInfo
}

export interface IAxiosResponseImageFile extends AxiosResponse {
    data: IImageFileInfo,
}

export interface IImageInfo extends Page.IPage {
    data: IImageData,
    included: IImageFileData[]
}

export interface IImageData extends Page.IPageData {
    attributes: IAttributes,
    relationships: IImageDataRelationships,
}

export interface IAttributes extends Page.IAttributes {
    field_caption?: string | {
        value: string,
        format: string
    }
    field_website_link?: Page.ILinkField
}

export interface IImageDataRelationships extends Page.IRelationships {
    field_image?: Page.IField_Image,
    field_image_aside?: Page.IField_Image
}

export interface IImageFileInfo {
    data: IImageFileData
}

export interface IImageFileData extends Page.IPageData {
    attributes: IIncludedAttributes
}

export interface IIncludedAttributes extends Page.IAttributes {
    uri: string,
    url: string
}

export type PropsWithDefaults = IImageContainerProps & IDefaultPropsImageContainer

export type imageFileFieldName = "field_image" | "field_image_aside"