import * as React from "react"
import { shallow, mount, render } from 'enzyme'
import Adapter from 'enzyme-adapter-react-15'
import * as Page from "../../../DrupalContentTypes/PageInterface"
import {data as aboutUsData} from "./mocks/aboutUsPageBEPC"
import {data as ceoTalkData} from "./mocks/ceoTalkBEPC"
import {Image} from "../../../components/Image"
import {ImageContainer} from "../../Image"

const SITE_DOMAIN = "https://basinelectric.com"

describe('Image', () => {
    const includedData = ceoTalkData.included as Array<any>
    it('renders an image component without crashing', () => {
        const image = (
            <Image
                altText="test"
                url="https://cdn.auth0.com/blog/testing-react-with-jest/logo.png"
            />
        )
        expect(mount(image).find("img").length).toBe(1)
    })

    it('renders an image container without crashing', () => {
        const image = (
            <ImageContainer
                includedRelationships={includedData}
                paragraphId={"a79f7990-8d47-4634-81cd-de6ca2b71e44"}
                siteDomain={SITE_DOMAIN}
            />
        )
        expect(mount(image).find("img").length).toBe(1)
    })

    it('renders image when file exists in included relationships', () => {
        const image = (
            <ImageContainer
                includedRelationships={includedData}
                paragraphId={"a79f7990-8d47-4634-81cd-de6ca2b71e44"}
                siteDomain={SITE_DOMAIN}
            />
        )
        const i = (
            <Image
                url="https://basinelectric.com/sites/CMS/files/images/Sukut-Paul-288w-CEO-Talk-2015.jpg"
                altText="Paul Sukut"
                caption=""
                className=""
            />
        )
        expect(mount(image).contains(i)).toBeTruthy()
    })

    // Need to make axios mock first.. 
    // it('renders image when image does NOT exist in included relationships', () => {
    //     const image = (
    //         <ImageContainer
    //             includedRelationships={aboutUsData.included}
    //             paragraphId={"08490803-bd62-4175-818c-ca8828370be9"}
    //             siteDomain={SITE_DOMAIN}
    //         />
    //     )
    //     const i = (
    //         <Image
    //             url="https://basinelectric.com/sites/CMS/files/images/Sukut-Paul-288w-CEO-Talk-2015.jpg"
    //             altText="Paul Sukut"
    //             caption=""
    //             className=""
    //         />
    //     )
    //     expect(mount(image).contains(i)).toBeTruthy()
    // })
    
})