// tslint:disable:max-line-length
// https://www.basinelectric.com/news-center/publications/ceo-talk - 2017/12/01
export const data = {
    "data": {
        "type": "node--basin_electric_basic_page",
        "id": "66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9",
        "attributes": {
            "nid": 3001,
            "uuid": "66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9",
            "vid": 16896,
            "langcode": "en",
            "status": true,
            "title": "CEO Talk",
            "created": 1501852229,
            "changed": 1509475764,
            "promote": false,
            "sticky": false,
            "revision_timestamp": 1509475764,
            "revision_log": null,
            "revision_translation_affected": true,
            "default_langcode": true,
            "path": {
                "alias": "/news-center/publications/ceo-talk",
                "pid": 1686
            },
            "field_meta_tags": "a:0:{}",
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c1953c9b-859c-42d1-a2ae-a74572456c0c"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/revision_uid"
                }
            },
            "menu_link": {
                "data": null
            },
            "moderation_state": {
                "data": null
            },
            "field_aside_content": {
                "data": [
                    {
                        "type": "paragraph--unique_basic_block",
                        "id": "0d631e46-6662-4bd4-a9ec-50867b06d6be",
                        "meta": {
                            "target_revision_id": "50291"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/relationships/field_aside_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/field_aside_content"
                }
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--image_aside",
                        "id": "a79f7990-8d47-4634-81cd-de6ca2b71e44",
                        "meta": {
                            "target_revision_id": "50296"
                        }
                    },
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "981d012e-db48-405e-be55-ecd3b6613a33",
                        "meta": {
                            "target_revision_id": "50301"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9/field_content"
                }
            },
            "field_related_videos": {
                "data": null
            },
            "field_web_layout": {
                "data": null
            },
            "scheduled_update": {
                "data": []
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/66f0dd8a-faf0-4819-8dd5-b14e5e91d8c9?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--unique_basic_block",
            "id": "0d631e46-6662-4bd4-a9ec-50867b06d6be",
            "attributes": {
                "id": 5691,
                "uuid": "0d631e46-6662-4bd4-a9ec-50867b06d6be",
                "revision_id": 50291,
                "langcode": "en",
                "status": true,
                "created": 1501852544,
                "parent_id": "3001",
                "parent_type": "node",
                "parent_field_name": "field_aside_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_unique_block_title": "Past columns"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "05040298-e668-41b5-a2ee-68c3a544c9be"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/0d631e46-6662-4bd4-a9ec-50867b06d6be/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/0d631e46-6662-4bd4-a9ec-50867b06d6be/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/0d631e46-6662-4bd4-a9ec-50867b06d6be/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/0d631e46-6662-4bd4-a9ec-50867b06d6be/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/0d631e46-6662-4bd4-a9ec-50867b06d6be/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/0d631e46-6662-4bd4-a9ec-50867b06d6be/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_content": {
                    "data": [
                        {
                            "type": "paragraph--basic_paragraph",
                            "id": "3ce048f5-b0cd-473f-977a-23d14fb0ab18",
                            "meta": {
                                "target_revision_id": "50286"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/0d631e46-6662-4bd4-a9ec-50867b06d6be/relationships/field_content",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/0d631e46-6662-4bd4-a9ec-50867b06d6be/field_content"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/unique_basic_block/0d631e46-6662-4bd4-a9ec-50867b06d6be"
            }
        },
        {
            "type": "paragraph--image_aside",
            "id": "a79f7990-8d47-4634-81cd-de6ca2b71e44",
            "attributes": {
                "id": 5696,
                "uuid": "a79f7990-8d47-4634-81cd-de6ca2b71e44",
                "revision_id": 50296,
                "langcode": "en",
                "status": true,
                "created": 1501852496,
                "parent_id": "3001",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_caption": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "fb0531fe-7890-4707-83e1-3d4453e13d15"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_aside/a79f7990-8d47-4634-81cd-de6ca2b71e44/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_aside/a79f7990-8d47-4634-81cd-de6ca2b71e44/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_aside/a79f7990-8d47-4634-81cd-de6ca2b71e44/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_aside/a79f7990-8d47-4634-81cd-de6ca2b71e44/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_aside/a79f7990-8d47-4634-81cd-de6ca2b71e44/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_aside/a79f7990-8d47-4634-81cd-de6ca2b71e44/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_image_aside": {
                    "data": {
                        "type": "file--file",
                        "id": "720db6ea-b722-4e93-a17a-684e2d7b772f",
                        "meta": {
                            "alt": "Paul Sukut",
                            "title": "",
                            "width": "288",
                            "height": "514"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_aside/a79f7990-8d47-4634-81cd-de6ca2b71e44/relationships/field_image_aside",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_aside/a79f7990-8d47-4634-81cd-de6ca2b71e44/field_image_aside"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image_aside/a79f7990-8d47-4634-81cd-de6ca2b71e44"
            }
        },
        {
            "type": "paragraph--basic_paragraph",
            "id": "981d012e-db48-405e-be55-ecd3b6613a33",
            "attributes": {
                "id": 5681,
                "uuid": "981d012e-db48-405e-be55-ecd3b6613a33",
                "revision_id": 50301,
                "langcode": "en",
                "status": true,
                "created": 1501852246,
                "parent_id": "3001",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_body": {
                    "value": "<h3>Developing strategy as the seasons change</h3>\r\n\r\n<h4>by Paul Sukut</h4>\r\n\r\n<p>It seems like every year around this time, the cooling temperatures get my mind working in two different directions.&nbsp;</p>\r\n\r\n<p>Looking backward I reflect on the long dry summer we’ve had. We started with very little rain, which set back farmers and their crops throughout the Midwest.</p>\r\n\r\n<p>Where I grew up in Dickey County, ND, farmers and ranchers qualified for natural disaster assistance this year because of losses and damages caused by the drought.</p>\r\n\r\n<p>The region got a reprieve of sorts with some rain the last month or so, but all that did was halt combines and settle the harvest haze out of the air.</p>\r\n\r\n<p>Looking forward, we all know what looms on the other side of our short fall season.</p>\r\n\r\n<p>While it might be uncomfortable to look forward at what’s to come, there is always benefit in it. I’ll have my snowblower ready to go, sidewalk salt purchased, and my winter emergency survival kit stashed in my pickup before the first snowflakes fall.&nbsp;</p>\r\n\r\n<p>At Basin Electric we look forward and plan for the future in a variety of ways. With our members’ help, we develop a member load forecast to give direction to resource planning. We develop a financial forecast. And on a more long-term basis, we undergo the process of strategic planning. The last of those is what I’d like to focus on in this column.</p>\r\n\r\n<p>The <a href=\"/about-us/organization/boards-directors\">Basin Electric board</a> and <a href=\"/about-us/organization/Executives\">senior staff</a> met for our strategic planning session in May. The session focused on various possible scenarios. Each scenario includes factors or assumptions of the future of Basin Electric and its <a href=\"/about-us/organization/subsidiaries\">subsidiaries</a>.</p>\r\n\r\n<p>Those factors/assumptions were evaluated on commodity prices, such as the price of natural gas and fertilizers; member load growth, and the effect of self-generation; regulatory issues such as a tax on carbon dioxide; and Basin Electric’s margins, which are a key indicator of how the co-op is running.</p>\r\n\r\n<p>We’re developing action plans for success under any one of the scenarios that comes to fruition. These action plans are going to be specific, and as always, keep our members’ best interests at heart. Senior staff presented those potential action plans and evaluated financial outcomes to directors at a second strategic planning session in September.</p>\r\n\r\n<p>As part of the strategic planning process, directors are also considering updated mission and vision statements for the cooperative. The statements selected by directors were presented to members of the <a href=\"/news-center/news-briefs/basin-electric-resolutions-committee-proposes-two-new-resolutions\">Resolutions Committee when they met at Basin Electric Headquarters in September</a>.</p>\r\n\r\n<p>In the most literal sense, mission and vision statements are just words on paper. But in reality, they spell out, in a very straightforward way, the very backbone of Basin Electric. The co-op’s objectives, how to reach those objectives, and what a successful Basin Electric will look like in the future.</p>\r\n\r\n<p>Looking backward is valuable. You learn from your successes and your mistakes, and can take a moment to appreciate how far you’ve come.</p>\r\n\r\n<p>But in the rapidly evolving industry we’re in, we need to keep an eye on the future at all times. I feel good about our ability to do that, and to strategically position the cooperative for success. Why? Because the co-op family has some of the best employees in the business who work to serve our members, from mine to end-use meter. Because we have member co-ops who are passionate and work every single day to serve those consumers at the end of the line.&nbsp;</p>\r\n\r\n<p>By doing this and working together, we’ll fulfill our collective mission and uphold the cooperative values, no matter the season.</p>\r\n",
                    "format": "rich_text"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/981d012e-db48-405e-be55-ecd3b6613a33/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/981d012e-db48-405e-be55-ecd3b6613a33/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/981d012e-db48-405e-be55-ecd3b6613a33/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/981d012e-db48-405e-be55-ecd3b6613a33/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/981d012e-db48-405e-be55-ecd3b6613a33/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/981d012e-db48-405e-be55-ecd3b6613a33/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/981d012e-db48-405e-be55-ecd3b6613a33"
            }
        },
        {
            "type": "file--file",
            "id": "720db6ea-b722-4e93-a17a-684e2d7b772f",
            "attributes": {
                "fid": 4451,
                "uuid": "720db6ea-b722-4e93-a17a-684e2d7b772f",
                "langcode": "en",
                "filename": "Sukut-Paul-288w-CEO-Talk-2015.jpg",
                "uri": "public://images/Sukut-Paul-288w-CEO-Talk-2015.jpg",
                "filemime": "image/jpeg",
                "filesize": 32945,
                "status": true,
                "created": 1501852516,
                "changed": 1501852563,
                "url": "/sites/CMS/files/images/Sukut-Paul-288w-CEO-Talk-2015.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/720db6ea-b722-4e93-a17a-684e2d7b772f/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/720db6ea-b722-4e93-a17a-684e2d7b772f/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/720db6ea-b722-4e93-a17a-684e2d7b772f"
            }
        }
    ]
}