// tslint:disable:max-line-length
// https://basinelectric.com/about-us - 2017/12/01
export const data = {
    "data": {
        "type": "node--basin_electric_basic_page",
        "id": "81de6009-3f9e-46e6-b243-3d043d7727e6",
        "attributes": {
            "nid": 21,
            "uuid": "81de6009-3f9e-46e6-b243-3d043d7727e6",
            "vid": 11411,
            "langcode": "en",
            "status": true,
            "title": "About Us",
            "created": 1498752753,
            "changed": 1504624516,
            "promote": false,
            "sticky": false,
            "revision_timestamp": 1504624516,
            "revision_log": null,
            "revision_translation_affected": true,
            "default_langcode": true,
            "path": {
                "alias": "/about-us",
                "pid": 3306
            },
            "field_meta_tags": "a:0:{}",
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c1953c9b-859c-42d1-a2ae-a74572456c0c"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/revision_uid"
                }
            },
            "menu_link": {
                "data": null
            },
            "moderation_state": {
                "data": null
            },
            "field_aside_content": {
                "data": [
                    {
                        "type": "paragraph--reusable_block",
                        "id": "253b5fe4-3ab6-4729-8e8d-3ff8e26f6763",
                        "meta": {
                            "target_revision_id": "33611"
                        }
                    },
                    {
                        "type": "paragraph--reusable_block",
                        "id": "e392208c-5d2a-4412-8dca-25e4fe44e632",
                        "meta": {
                            "target_revision_id": "33616"
                        }
                    },
                    {
                        "type": "paragraph--reusable_block",
                        "id": "db6f9b46-8497-4c69-83e0-b9e9e8c5288f",
                        "meta": {
                            "target_revision_id": "33621"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/relationships/field_aside_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/field_aside_content"
                }
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--image_banner",
                        "id": "22b8714d-2036-4754-8363-63e87339f47c",
                        "meta": {
                            "target_revision_id": "33626"
                        }
                    },
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "aeb85743-e851-4144-8a5f-b0501625cdc9",
                        "meta": {
                            "target_revision_id": "33631"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/field_content"
                }
            },
            "field_related_videos": {
                "data": null
            },
            "field_web_layout": {
                "data": {
                    "type": "taxonomy_term--web_layouts",
                    "id": "40f93ba8-666a-45d6-82f0-0c0e4503c363"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/relationships/field_web_layout",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6/field_web_layout"
                }
            },
            "scheduled_update": {
                "data": []
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/81de6009-3f9e-46e6-b243-3d043d7727e6?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--reusable_block",
            "id": "253b5fe4-3ab6-4729-8e8d-3ff8e26f6763",
            "attributes": {
                "id": 36,
                "uuid": "253b5fe4-3ab6-4729-8e8d-3ff8e26f6763",
                "revision_id": 33611,
                "langcode": "en",
                "status": true,
                "created": 1498752948,
                "parent_id": "21",
                "parent_type": "node",
                "parent_field_name": "field_aside_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "2f24beb8-4347-4ba8-8762-4348bf464846"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/253b5fe4-3ab6-4729-8e8d-3ff8e26f6763/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/253b5fe4-3ab6-4729-8e8d-3ff8e26f6763/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/253b5fe4-3ab6-4729-8e8d-3ff8e26f6763/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/253b5fe4-3ab6-4729-8e8d-3ff8e26f6763/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/253b5fe4-3ab6-4729-8e8d-3ff8e26f6763/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/253b5fe4-3ab6-4729-8e8d-3ff8e26f6763/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_block": {
                    "data": null
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/253b5fe4-3ab6-4729-8e8d-3ff8e26f6763"
            }
        },
        {
            "type": "paragraph--reusable_block",
            "id": "e392208c-5d2a-4412-8dca-25e4fe44e632",
            "attributes": {
                "id": 56,
                "uuid": "e392208c-5d2a-4412-8dca-25e4fe44e632",
                "revision_id": 33616,
                "langcode": "en",
                "status": true,
                "created": 1498755354,
                "parent_id": "21",
                "parent_type": "node",
                "parent_field_name": "field_aside_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "2f24beb8-4347-4ba8-8762-4348bf464846"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/e392208c-5d2a-4412-8dca-25e4fe44e632/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/e392208c-5d2a-4412-8dca-25e4fe44e632/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/e392208c-5d2a-4412-8dca-25e4fe44e632/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/e392208c-5d2a-4412-8dca-25e4fe44e632/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/e392208c-5d2a-4412-8dca-25e4fe44e632/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/e392208c-5d2a-4412-8dca-25e4fe44e632/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_block": {
                    "data": {
                        "type": "block_content--basic",
                        "id": "06f29272-368d-43e2-af18-54641773ed44"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/e392208c-5d2a-4412-8dca-25e4fe44e632/relationships/field_block",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/e392208c-5d2a-4412-8dca-25e4fe44e632/field_block"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/e392208c-5d2a-4412-8dca-25e4fe44e632"
            }
        },
        {
            "type": "paragraph--reusable_block",
            "id": "db6f9b46-8497-4c69-83e0-b9e9e8c5288f",
            "attributes": {
                "id": 61,
                "uuid": "db6f9b46-8497-4c69-83e0-b9e9e8c5288f",
                "revision_id": 33621,
                "langcode": "en",
                "status": true,
                "created": 1498755361,
                "parent_id": "21",
                "parent_type": "node",
                "parent_field_name": "field_aside_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "2f24beb8-4347-4ba8-8762-4348bf464846"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/db6f9b46-8497-4c69-83e0-b9e9e8c5288f/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/db6f9b46-8497-4c69-83e0-b9e9e8c5288f/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/db6f9b46-8497-4c69-83e0-b9e9e8c5288f/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/db6f9b46-8497-4c69-83e0-b9e9e8c5288f/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/db6f9b46-8497-4c69-83e0-b9e9e8c5288f/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/db6f9b46-8497-4c69-83e0-b9e9e8c5288f/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_block": {
                    "data": {
                        "type": "block_content--basic",
                        "id": "8e567001-db27-47a1-9e12-8495fdf496cf"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/db6f9b46-8497-4c69-83e0-b9e9e8c5288f/relationships/field_block",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/db6f9b46-8497-4c69-83e0-b9e9e8c5288f/field_block"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/reusable_block/db6f9b46-8497-4c69-83e0-b9e9e8c5288f"
            }
        },
        {
            "type": "paragraph--image_banner",
            "id": "22b8714d-2036-4754-8363-63e87339f47c",
            "attributes": {
                "id": 26,
                "uuid": "22b8714d-2036-4754-8363-63e87339f47c",
                "revision_id": 33626,
                "langcode": "en",
                "status": true,
                "created": 1498752757,
                "parent_id": "21",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_caption": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "185ce104-3bbe-4c90-bda0-81866f8e437d"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/22b8714d-2036-4754-8363-63e87339f47c/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_banner/22b8714d-2036-4754-8363-63e87339f47c/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/22b8714d-2036-4754-8363-63e87339f47c/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_banner/22b8714d-2036-4754-8363-63e87339f47c/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/22b8714d-2036-4754-8363-63e87339f47c/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_banner/22b8714d-2036-4754-8363-63e87339f47c/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "552e22c6-1061-4fb7-a581-6a0378605d2e",
                        "meta": {
                            "alt": "Wilton wind farm in front of a sunset",
                            "title": "",
                            "width": "1122",
                            "height": "225"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/22b8714d-2036-4754-8363-63e87339f47c/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_banner/22b8714d-2036-4754-8363-63e87339f47c/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image_banner/22b8714d-2036-4754-8363-63e87339f47c"
            }
        },
        {
            "type": "paragraph--basic_paragraph",
            "id": "aeb85743-e851-4144-8a5f-b0501625cdc9",
            "attributes": {
                "id": 31,
                "uuid": "aeb85743-e851-4144-8a5f-b0501625cdc9",
                "revision_id": 33631,
                "langcode": "en",
                "status": true,
                "created": 1498752820,
                "parent_id": "21",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_body": {
                    "value": "<h2>Corporate profile</h2>\r\n\r\n<ul>\r\n\t<li>A not-for-profit generation and transmission cooperative incorporated in 1961 to provide supplemental power to a consortium of rural electric cooperatives</li>\r\n\t<li>Diverse energy portfolio: coal, gas, oil, nuclear, distributed, and renewable energy, including wind power</li>\r\n\t<li>Consumer owned by 141 member cooperative systems</li>\r\n\t<li>Members' service territories comprise 540,000 square miles in nine states</li>\r\n\t<li>By end of year 2016 Basin Electric will operate 5,205 megawatts (MW) of wholesale electric generating capacity and have 6,629 MW of capacity within its generation portfolio</li>\r\n\t<li>Owns 2,356 miles and maintains 2,441 miles of high-voltage transmission, and owns and maintains equipment in 81 switchyards and 195 telecommunication sites</li>\r\n\t<li>Serves 3 million electric consumers</li>\r\n</ul>\r\n\r\n<p>Statistics updated April 20, 2017, for End of Year 2017.</p>\r\n\r\n<h2>Energy profile</h2>\r\n\r\n<p>Basin Electric’s core business is generating and transmitting wholesale bulk electric power to customers, primarily to our member rural electric systems, which are located in nine states: Colorado, Iowa, Minnesota, Montana, Nebraska, New Mexico, North Dakota, South Dakota, and Wyoming. Our largest subsidiary, the for-profit&nbsp;<a href=\"http://www.dakotagas.com/\">Dakota Gasification Company</a>, owns and operates the Great Plains Synfuels Plant near Beulah, ND, which gasifies lignite coal and captures some of the carbon dioxide emissions (CO<sub>2</sub>) and sends them to depleted Canadian oil fields for geologic sequestration.</p>\r\n\r\n<p>Basin Electric will own 4,217 megawatts (MW) and operate 5,205 MW of electric generating capacity by end of year 2017 of which 987 MW is for participants of the Missouri Basin Power Project (MBPP), and 80 MW is jointly owned by Basin Electric and its Class A member, Corn Belt Power Cooperative, Humboldt, IA. Basin Electric&nbsp;owns&nbsp;generation&nbsp;in North Dakota, South Dakota, Wyoming, Montana, and Iowa, and additionally purchases power from facilities in Minnesota, Colorado, and Nebraska.&nbsp;Most of Basin Electric's baseload capacity comes from coal. Peaking facilities are oil or natural gas-based.</p>\r\n\r\n<p>Basin Electric’s total winter season generation capacity portfolio by the winter 2017/2018 season will be 6,629 MW. Of that, Basin Electric has purchased a total of 2,412 MW of electric generation capacity/energy, including 1,129 total MW of renewable energy of which 1,085 MW are wind energy, and 44 MW are waste heat energy (known as recovered energy generation - REG). Our purchased power portfolio also includes 62 MW of nuclear energy.</p>\r\n",
                    "format": "rich_text"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/aeb85743-e851-4144-8a5f-b0501625cdc9/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/aeb85743-e851-4144-8a5f-b0501625cdc9/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/aeb85743-e851-4144-8a5f-b0501625cdc9/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/aeb85743-e851-4144-8a5f-b0501625cdc9/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/aeb85743-e851-4144-8a5f-b0501625cdc9/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/aeb85743-e851-4144-8a5f-b0501625cdc9/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/aeb85743-e851-4144-8a5f-b0501625cdc9"
            }
        },
        {
            "type": "taxonomy_term--web_layouts",
            "id": "40f93ba8-666a-45d6-82f0-0c0e4503c363",
            "attributes": {
                "tid": 611,
                "uuid": "40f93ba8-666a-45d6-82f0-0c0e4503c363",
                "langcode": "en",
                "name": "Page with aside Content",
                "description": null,
                "weight": 0,
                "changed": 1502982037,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "9481b046-bd2c-4ef8-ba1d-acde28597ffc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/40f93ba8-666a-45d6-82f0-0c0e4503c363/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/40f93ba8-666a-45d6-82f0-0c0e4503c363/vid"
                    }
                },
                "parent": {
                    "data": []
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/40f93ba8-666a-45d6-82f0-0c0e4503c363"
            }
        },
        {
            "type": "block_content--basic",
            "id": "06f29272-368d-43e2-af18-54641773ed44",
            "attributes": {
                "id": 11,
                "uuid": "06f29272-368d-43e2-af18-54641773ed44",
                "revision_id": 446,
                "langcode": "en",
                "info": "Subsidiaries List w/ Links",
                "revision_log": null,
                "changed": 1508514866,
                "revision_created": 1508514866,
                "revision_translation_affected": true,
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "block_content_type--block_content_type",
                        "id": "e6f3a453-e957-476d-9e2e-1ee52e5e16e1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/block_content/basic/06f29272-368d-43e2-af18-54641773ed44/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/block_content/basic/06f29272-368d-43e2-af18-54641773ed44/type"
                    }
                },
                "revision_user": {
                    "data": {
                        "type": "user--user",
                        "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/block_content/basic/06f29272-368d-43e2-af18-54641773ed44/relationships/revision_user",
                        "related": "http://cms.bepc.com/jsonapi/block_content/basic/06f29272-368d-43e2-af18-54641773ed44/revision_user"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_content": {
                    "data": [
                        {
                            "type": "paragraph--title",
                            "id": "1389965f-e4d3-49d4-af11-7088531dac1a",
                            "meta": {
                                "target_revision_id": "47416"
                            }
                        },
                        {
                            "type": "paragraph--basic_paragraph",
                            "id": "e4b0267b-145b-449f-9286-3e6f95c21832",
                            "meta": {
                                "target_revision_id": "47421"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/block_content/basic/06f29272-368d-43e2-af18-54641773ed44/relationships/field_content",
                        "related": "http://cms.bepc.com/jsonapi/block_content/basic/06f29272-368d-43e2-af18-54641773ed44/field_content"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/block_content/basic/06f29272-368d-43e2-af18-54641773ed44"
            }
        },
        {
            "type": "block_content--basic",
            "id": "8e567001-db27-47a1-9e12-8495fdf496cf",
            "attributes": {
                "id": 16,
                "uuid": "8e567001-db27-47a1-9e12-8495fdf496cf",
                "revision_id": 16,
                "langcode": "en",
                "info": "50-Year History Book",
                "revision_log": null,
                "changed": 1498851250,
                "revision_created": 1498755071,
                "revision_translation_affected": true,
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "block_content_type--block_content_type",
                        "id": "e6f3a453-e957-476d-9e2e-1ee52e5e16e1"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/block_content/basic/8e567001-db27-47a1-9e12-8495fdf496cf/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/block_content/basic/8e567001-db27-47a1-9e12-8495fdf496cf/type"
                    }
                },
                "revision_user": {
                    "data": null
                },
                "moderation_state": {
                    "data": null
                },
                "field_content": {
                    "data": [
                        {
                            "type": "paragraph--title",
                            "id": "44e36f35-4091-4ea2-a978-1f6c8fc4ec6d",
                            "meta": {
                                "target_revision_id": "231"
                            }
                        },
                        {
                            "type": "paragraph--basic_paragraph",
                            "id": "2f480fe6-2f7e-4c8a-aef6-1d819c0636ba",
                            "meta": {
                                "target_revision_id": "66"
                            }
                        },
                        {
                            "type": "paragraph--image",
                            "id": "08490803-bd62-4175-818c-ca8828370be9",
                            "meta": {
                                "target_revision_id": "71"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/block_content/basic/8e567001-db27-47a1-9e12-8495fdf496cf/relationships/field_content",
                        "related": "http://cms.bepc.com/jsonapi/block_content/basic/8e567001-db27-47a1-9e12-8495fdf496cf/field_content"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/block_content/basic/8e567001-db27-47a1-9e12-8495fdf496cf"
            }
        },
        {
            "type": "file--file",
            "id": "552e22c6-1061-4fb7-a581-6a0378605d2e",
            "attributes": {
                "fid": 6,
                "uuid": "552e22c6-1061-4fb7-a581-6a0378605d2e",
                "langcode": "en",
                "filename": "Wilton-Wind-sunrise-1122px-Impact.jpg",
                "uri": "public://images/Wilton-Wind-sunrise-1122px-Impact.jpg",
                "filemime": "image/jpeg",
                "filesize": 32782,
                "status": true,
                "created": 1498682459,
                "changed": 1498683291,
                "url": "/sites/CMS/files/images/Wilton-Wind-sunrise-1122px-Impact.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/552e22c6-1061-4fb7-a581-6a0378605d2e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/552e22c6-1061-4fb7-a581-6a0378605d2e/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/552e22c6-1061-4fb7-a581-6a0378605d2e"
            }
        }
    ]
}
