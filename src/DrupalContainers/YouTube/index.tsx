import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import YouTubeVideo from "../../components/YouTube"

export const YouTube: React.StatelessComponent<IYouTubeProps> = (props) => (
    <YouTubeVideo link={getVideoUrl(props)} />
)

const getVideoUrl = (props: IYouTubeProps) => {
    const data = props.includedRelationships.find((item) => item.id === props.paragraphId) as IYouTubeData
    return data.attributes.field_video_youtube
}

export default YouTube

export interface IYouTubeProps {
    includedRelationships: Page.IPageData[],
    paragraphId: string,
}

export interface IYouTubeData extends Page.IPageData {
    attributes: IYouTubeAttributes,
}

export interface IYouTubeAttributes extends Page.IAttributes {
    field_video_youtube: string,
}
