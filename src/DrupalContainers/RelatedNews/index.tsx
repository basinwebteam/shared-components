import {AxiosResponse, default as axios} from "axios"
import AsideBlock from "../ReusableBlock/Types/AsideBlock"
import * as React from "react"

export class RelatedNews extends React.Component<IRelatedNewsProps, IRelatedNewsState> {
    constructor(props: IRelatedNewsProps) {
        super(props)
        this.state = {
            news: new Array(),
        }
    }

    public render() {
        return (
            <AsideBlock
                title="Related News"
                content={this.getContent()}
            />
        )
    }

    public getContent() {
        let count = 0
        return (
            <ul>
                {this.state.news.map((news, i) => {
                    if (count >= 5 || news.title === this.props.excludeTitle) {
                        return ""
                    }
                    count ++
                    return <li key={i}>
                        <a href={news.path} dangerouslySetInnerHTML={{__html: news.title}} />
                    </li>})
                }
            </ul>
        )
    }

    public componentDidMount() {
        this.updateComponent()
    }

    public componentDidUpdate(prevProps: IRelatedNewsProps, prevState: IRelatedNewsState) {
        if (this.props !== prevProps) {
            this.updateComponent()
        }
    }

    public async updateComponent() {
        const news = await this.queryData()
        this.setState({
            news,
        })
    }

    public async queryData() {
        const response: IAxiosRelatedNewsResponse =
            await axios.get(`https://api.bepc.com/api/cms/page/relatednews/${this.props.sites}/${this.props.relatedTagIds}/6`)

        return response.data
    }
}

export default RelatedNews

export interface IRelatedNewsProps {
    sites: string, // comma seperated lists
    relatedTagIds: string, // comma seperated lists
    excludeTitle: string,
}

export interface IRelatedNewsState {
    news: INewsItems[]
}

export interface INewsItems {
    title: string,
    field_publish_to_sites: string,
    field_related_news: string,
    path: string,
    uuid: string,
}

export interface IAxiosRelatedNewsResponse extends AxiosResponse {
    data: INewsItems[]
}
