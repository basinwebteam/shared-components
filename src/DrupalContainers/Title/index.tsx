import { default as axios, AxiosResponse, Canceler } from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import Title from "../../components/Title"

export class TitleContainer extends React.Component<IProps, IState> {
    private xhrCancel: Canceler | undefined

    constructor(props: IProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    public render() {
        return (
            <Title>{this.state.text}</Title>
        )
    }

    public componentDidMount() {
        this.getData()
    }

    public componentWillUnmount() {
        if (typeof this.xhrCancel !== "undefined") {
            this.xhrCancel("Component Will Unmount")
        }
    }

    public componentDidUpdate(prevProps: IProps, prevState: IState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.getData()
        }
    }

    public getData() {
        if (typeof this.props.titleParagraphData === "undefined"
            || this.props.titleParagraphData === null) {
            this.queryData(this.props.paragraphId)
        } else {
            this.setState({
                text: this.props.titleParagraphData.attributes.field_title,
            } as IState)
        }
    }

    public async queryData(id: string) {
        const response = await axios.get
            (`//api.bepc.com/api/cms/json/paragraph/title/${id}/`,
            {
                cancelToken: new axios.CancelToken((c) => { this.xhrCancel = c }),
            }) as IAxiosDataResponse
        this.setState({
            text: response.data.data.attributes.field_title,
        })
    }

    public getDefaultState() {
        /* This tries to get the block and related child data if its part of the props blockData value.

        If any of it is missing, this returns an empty state. */
        try {
            const text = this.props.titleParagraphData.attributes.field_title
            return {
                text,
            } as IState
        } catch (e) {
            return {
                text: "",
            } as IState
        }
    }
}

export default TitleContainer

export interface IProps {
    titleParagraphData: ITitleData,
    paragraphId: string,
    siteDomain: string
}

export interface IState {
    text: string
}

export interface IAxiosDataResponse extends AxiosResponse {
    data: ITitleResponse
}

export interface ITitleResponse extends Page.IType {
    data: ITitleData
}

export interface ITitleData extends Page.IPageData {
    attributes: ITitleAttributes
}

export interface ITitleAttributes extends Page.IAttributes {
    field_title: string
}
