import * as React from "react"
import * as Page from "../../../../DrupalContentTypes/PageInterface"
import AsideBlock from "../AsideBlock"
import { ITitleData, TitleContainer } from "../../../Title"
import { BasicParagraph, IBasicParagraphPageData } from "../../../BasicParagraph"
import Image from "../../../Image"

// TODO: Turn into stateful component that queries block data if props.blockData or props.includedPageData is missing

export const BasicBlock: React.StatelessComponent<IProps> = (props) => {
    if (props.useTitle && props.blockData.attributes.field_unique_block_title) {
        return (
            <AsideBlock
                title={props.blockData.attributes.field_unique_block_title}
                content={getContent(props)}
            />
        )
    }
    return <AsideBlock content={getContent(props)} />
}

const getContent = (props: IProps): JSX.Element[] => {
    const fieldContentData = (props.blockData as IBasicBlockData).relationships.field_content.data
    return fieldContentData.map(
        (data, i) => {
            if(typeof data === "undefined"){
                throw Error("The fieldContentData array in the Basic Block component has a data item that is undefined.")
            }
            switch (data.type) {
                case ("paragraph--title"):
                    return (
                        <div key={data.id} className="aside-header">
                            <TitleContainer
                                paragraphId={data.id}
                                titleParagraphData={props.includedPageData.find(p => p.id === data.id) as ITitleData}
                                siteDomain={props.siteDomain}
                            />
                        </div>
                    )
                case ("paragraph--basic_paragraph"):
                    return (
                        <div key={data.id} className="aside-body">
                            <BasicParagraph
                                paragraphId={data.id}
                                data={props.includedPageData.find(p => p.id === data.id) as IBasicParagraphPageData}
                                siteDomain={props.siteDomain}
                            />
                        </div>
                    )
                case ("paragraph--image"):
                    return (
                        <div key={data.id} className="aside-body">
                            <Image
                                includedRelationships={props.includedPageData}
                                paragraphId={data.id}
                                siteDomain={props.siteDomain}
                                className="inlineImageLarge"
                                imageFileFieldName="field_image"
                                imageParagraphName="image"
                            />
                        </div>
                    )
                default:
                    return (<div key={i} className="missing-basic-block-type" />)
            }
        }
    )
}

export default BasicBlock

export interface IProps {
    useTitle: boolean,
    blockData: IBasicBlockData,
    blockId: string,
    includedPageData: Page.IPageData[],
    siteDomain: string,
}

export interface IState {
    title: string
}

export interface IBasicBlockData extends Page.IPageData {
    relationships: IBasicBlockRelationships,
    attributes: IBasicBlockAttributes,
}

export interface IBasicBlockAttributes extends Page.IAttributes {
    field_unique_block_title: string
}

export interface IBasicBlockRelationships extends Page.IRelationships {
    field_content: Page.IFieldContent
}
