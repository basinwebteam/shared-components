export const EMPLOYEE_CONTACT_INFO = "block_content--employee_contact_info"
export const BASIC_BLOCK = "block_content--basic"
export const ENERGY_PORTFOLIO_CHART = "block_content--energy_portfolio_interactive_pie"
export const CONTACT_LIST = "block_content--contact_list"
