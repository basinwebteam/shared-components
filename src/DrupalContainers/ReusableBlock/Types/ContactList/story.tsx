import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'
import ContactList from "../ContactList"
const siteDomain = '//cms.bepc.com'

const description = `Displays an contact list with a list of anchor links to take you to a profile. `

storiesOf("Contact List Block", module)
    .addDecorator(withKnobs)
    .add("default",
        withInfo(`${description}`)(() => {
            return (
                <ContactList
                    siteDomain={siteDomain}
                    blockId="d165b9d9-8474-4229-8452-ada67bacf559"
                />
            )
        })
    )