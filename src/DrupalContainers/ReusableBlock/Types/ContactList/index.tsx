import axios from "axios"
import * as IAxios from "axios/index"
import * as React from "react"
import * as Page from "../../../../DrupalContentTypes/PageInterface"

class ContactList extends React.Component<IContactListProps, IContactListState> {
    constructor(props: IContactListProps) {
        super(props)
        this.state = {
            contacts: []
        }
    }

    public render() {
        const contacts = this.state.contacts
        return (
            <div className="contact-list-block">
                <ul>
                    {contacts.map(
                        ({ name }, i) => (
                            <li key={i}>
                                <a href={"#" + this.getAnchor(name, i)}>{name}</a>
                            </li>
                        )
                    )}
                </ul>
                {contacts.map(
                    ({ name, image, bio }, i) => (
                        <div key={i}>
                            <h2 id={this.getAnchor(name, i)}>{name}</h2>
                            <img src={this.props.siteDomain + image} />
                            <div dangerouslySetInnerHTML={{ __html: this.getBiography(bio) }} />
                        </div>
                    )
                )}
            </div>
        )
    }

    public componentDidMount() {
        this.getDataFromApi(this.props.blockId)
    }

    public componentDidUpdate(prevProps: IContactListProps, prevState: IContactListState) {
        if (this.props.blockId !== prevProps.blockId) {
            this.getDataFromApi(this.props.blockId)
        }
    }

    private getAnchor(name: string, number = 0) {
        return name.replace(/[\W_]+/g, "") + number
    }
    private async getDataFromApi(id: string) {
        const response: IAxiosResponseContactList = await axios.get(`//api.bepc.com/api/cms/json/block_content/contact_list/${id}?include=field_contact,field_contact.field_image`)
        if (response.data.data !== undefined) {
            this.updateStateWithContacts(response.data)
        }
    }
    updateStateWithContacts(data: IContactData): void {
        const contactData = data.data.relationships.field_contact.data
        const includedData = data.included
        const contacts = contactData.map(({ id }) => {
            const contact = includedData.find(data => data.id === id) as IIncludedContact
            return {
                name: `${contact.attributes.field_first_name} ${contact.attributes.field_last_name}`,
                image: this.getImageUrl(contact.relationships.field_image, includedData),
                bio: contact.attributes.field_biography
            } as IContactListContact
        })
        this.setState({
            contacts
        })
    }
    getImageUrl(image: Page.IField_Image, includedData: Array<IIncludedContact | Page.IField_File>): string {
        if (image.data === null || image.data === undefined) {
            return ""
        }
        const imageFile = includedData.find(data => data.id === image.data.id) as Page.IField_File
        if (imageFile !== undefined) {
            return imageFile.attributes.url
        }
        return ""
    }

    getBiography(bio: Page.IWYSIWYG): string {
        switch (typeof bio) {
            case ("string"):
                return bio as string
            case ("object"):
                return (bio as { value: string }).value
            default:
                return ""
        }
    }
}

export default ContactList

export interface IContactListProps {
    siteDomain: string
    blockId: string
}

export interface IContactListState {
    contacts: Array<IContactListContact>
}

export interface IContactListContact {
    name: string
    image: string
    bio: string
}

export interface IAxiosResponseContactList extends IAxios.AxiosResponse {
    data: IContactData
}

export interface IContactData extends Page.IPage {
    data: Page.IPageData & {
        relationships: Page.IRelationships & {
            field_contact: Page.ITypeMultipleData
        }
    }
    included: Array<IIncludedContact | Page.IField_File>
}

export interface IIncludedContact extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_biography: Page.IWYSIWYG,
        field_employee_e_mail: string,
        field_first_name: string,
        field_last_name: string,
        field_employee_phone_number: [string],
        field_employee_title: string
    }
    relationships: Page.IRelationships & {
        field_image: Page.IField_Image
    }
}