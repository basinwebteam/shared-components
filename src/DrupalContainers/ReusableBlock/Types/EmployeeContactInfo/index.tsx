// tslint:disable:max-line-length - Long URL in getContactInfo
import axios from "axios"
import * as IAxios from "axios/index"
import * as React from "react"
import * as Page from "../../../../DrupalContentTypes/PageInterface"

class EmployeeContactInfo extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    public render() {
        return (
            <div className="contentBlock">
                <div className="aside-block">
                    <div className="aside-header">{this.state.title}</div>
                    <div className="aside-body">
                        <p>
                            {this.state.employeeName} <br />
                            {this.state.employeeTitle} <br />
                            {this.getPhoneNumbers()}
                            {this.getBiography()}
                        </p>
                        <p><a href="/Contact/" title="Contact Page">Or use our Contact Form</a></p>
                    </div>
                </div>
            </div>
        )
    }

    public componentDidMount() {
        this.getContactInfo()
    }

    public componentDidUpdate(prevProps: IProps, prevState: IState) {
        if (this.props.blockId !== prevProps.blockId) {
            this.getContactInfo()
        }
    }

    private getPhoneNumbers() {
        if (this.state.employeePhoneNumber.length > 0) {
            return (
                <span
                    dangerouslySetInnerHTML={{ __html: this.state.employeePhoneNumber.join("<br />") }}
                />
            )
        }
        return ""
    }

    private getBiography() {
        if (this.state.biography !== null && this.state.biography.length > 0) {
            return (<div dangerouslySetInnerHTML={{ __html: `<br />${this.state.biography}` }} />)
        }
        return ""
    }

    private async getContactInfo() {
        let employee: IEmployeeContactAttribute | undefined
        if (typeof this.props.blockData === "undefined" || this.props.blockData == null) {
            employee = await this.queryBlockAndEmployeeContact()
        } else {
            const employeeContactId = (this.props.blockData as IReusableContactBlockData).relationships.field_employee_contact.data.id

            const employeeBlock = this.findEmployeeContactFromIncludedData(employeeContactId)

            if (typeof employeeBlock !== "undefined") {
                employee = employeeBlock.attributes
            } else {
                employee = await this.queryEmployeeContact(employeeContactId)
            }
        }

        if (typeof employee !== "undefined") {
            this.setEmployeeContact(employee)
        }
    }

    private findEmployeeContactFromIncludedData(employeeContactId: string) {

        return this.props.includedPageData.find((contact) =>
            (employeeContactId === contact.id)) as IEmployeeContactData
    }

    private async queryEmployeeContact(id: string) {
        const response: IAxiosResponseContactInfo = await axios.get(`//api.bepc.com/api/cms/json/node/basin_contacts/${id}?include=field_employee_contact`)
        if (typeof response.data.data !== "undefined") {
            return response.data.data.attributes as IEmployeeContactAttribute
        }
        return undefined
    }

    private async queryBlockAndEmployeeContact() {
        const employeeContact: IAxiosResponseContactInfo =
            await axios.get(`//api.bepc.com/api/cms/json/block_content/employee_contact_info/${this.props.blockId}?include=field_employee_contact`)

        if (employeeContact.data.included.length > 0) {
            return employeeContact.data.included[0].attributes
        }
        return undefined
    }

    private setEmployeeContact(employee: IEmployeeContactAttribute) {
        this.setState({
            title: employee.title,
            employeeTitle: employee.field_employee_title,
            employeeName: employee.field_employee_name,
            employeeEmail: employee.field_employee_e_mail,
            employeePhoneNumber: employee.field_employee_phone_number,
            biography: employee.field_biography,
        })
    }

    private getDefaultState() {
        /* This tries to get the block and related child data if its part of the props blockData value.

        If any of it is missing, this returns an empty state. */
        try {
            const employeeContactId = (this.props.blockData as IReusableContactBlockData).relationships.field_employee_contact.data.id

            const employeeBlock = this.findEmployeeContactFromIncludedData(employeeContactId)

            return {
                title: employeeBlock.attributes.title,
                employeeTitle: employeeBlock.attributes.field_employee_title,
                employeeName: employeeBlock.attributes.field_employee_name,
                employeeEmail: employeeBlock.attributes.field_employee_e_mail,
                employeePhoneNumber: employeeBlock.attributes.field_employee_phone_number,
                biography: employeeBlock.attributes.field_biography,
            } as IState
        } catch (e) {
            return {
                title: "",
                employeeTitle: "",
                employeeName: "",
                employeeEmail: "",
                employeePhoneNumber: [],
                biography: "",
            } as IState
        }
    }
}

export default EmployeeContactInfo

export interface IProps {
    blockData: IReusableContactBlockData | Page.IPageData | null,
    blockId: string,
    includedPageData: Page.IPageData[]
    siteDomain: string,
}

export interface IState {
    title: string
    employeeTitle: string,
    employeeName: string,
    employeeEmail: string,
    employeePhoneNumber: Array<string | undefined>,
    biography: string,
}

export interface IAxiosResponseContactInfo extends IAxios.AxiosResponse {
    data: IContactInfo,
}

export interface IContactInfo extends Page.IPage {
    included: [IEmployeeContactData]
}

export interface IContactInfoData extends Page.IPageData {
    relationships: IContactInfoRelationships,
}

export interface IContactInfoRelationships extends Page.IRelationships {
    field_employee_contact: Page.IType
}

export interface IEmployeeContactData extends Page.IPageData {
    attributes: IEmployeeContactAttribute
    relationships: Page.IRelationships
}

export interface IEmployeeContactAttribute extends Page.IAttributes {
    field_biography: string,
    field_employee_e_mail: string,
    field_employee_name: string,
    field_employee_phone_number: [string],
    field_employee_title: string,
}

export interface IReusableContactBlockData extends Page.IPageData {
    relationships: IReusableContactBlockRelationship
}

export interface IReusableContactBlockRelationship extends Page.IRelationships {
    field_employee_contact: Page.IType
}
