import * as React from "react"
import Title from "../../../../components/Title"

const AsideBlock: React.StatelessComponent<IPropsAsideBlock> = ({content, title}) => {
    return (
        <div className="contentBlock">
            {title && (<Title>{title}</Title>)}
            {content}
        </div>
    )
}
export default AsideBlock

export interface IPropsAsideBlock {
    title?: string,
    content: JSX.Element[]|JSX.Element,
}
