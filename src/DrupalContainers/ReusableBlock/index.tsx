import { AxiosResponse, default as axios } from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import { BasicBlock, IBasicBlockData } from "./Types/BasicBlock"
import EmployeeContactInfo from "./Types/EmployeeContactInfo"
import EnergyPortfolio from "../EnergyPortfolioPieChart"
import ContactList from "./Types/ContactList"
import * as Type from "./Types/reusableBlockTypes"

/**
 * ReusableBlock
 */
export class ReusableBlock extends React.Component<IReusableBlockProps, IReusableBlockState> {
    constructor(props: IReusableBlockProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    public render() {
        return (
            <div className={`reusable-block ${this.state.type}`}>
                {this.getChildComponent()}
            </div>
        )
    }

    public componentDidMount() {
        this.updateComponent()
    }

    public componentDidUpdate(prevProps: IReusableBlockProps, prevState: IReusableBlockState) {
        if (this.props !== prevProps) {
            this.updateComponent()
        }
    }

    private updateComponent() {
        const block = this.getBlock()
        if (block) {
            this.getBlockContent(block)
        }
    }

    private getChildComponent() {
        if (this.state.childBlockData === null) {
            return (<div className="reusable-block-missing-child-block-data" />)
        }
        switch (this.state.type) {
            case (Type.BASIC_BLOCK):
                return (
                    <BasicBlock
                        key={this.state.id}
                        blockId={this.state.id}
                        blockData={this.state.childBlockData as IBasicBlockData}
                        includedPageData={this.props.includedRelationships}
                        useTitle={true}
                        siteDomain={this.props.siteDomain}
                    />
                )
            case (Type.CONTACT_LIST):
                return (
                    <ContactList
                        key={this.state.id}
                        blockId={this.state.id}
                        siteDomain={this.props.siteDomain}
                    />
                )
            case (Type.EMPLOYEE_CONTACT_INFO):
                return (
                    <EmployeeContactInfo
                        key={this.state.id}
                        blockId={this.state.id}
                        blockData={this.state.childBlockData}
                        includedPageData={this.props.includedRelationships}
                        siteDomain={this.props.siteDomain}
                    />
                )
            case Type.ENERGY_PORTFOLIO_CHART:
                return (
                    <EnergyPortfolio
                        key={this.state.id}
                        id={this.state.id}
                        siteDomain={this.props.siteDomain}
                    />
                )

            default:
                return (<div className="Default-{this.state.type}" />)
        }
    }

    private getBlock() {
        if (!this.props.includedRelationships) { return null }
        const block = this.props.includedRelationships.find((item) =>
            (item.id === this.props.paragraphId)) as IReusableBlockIncludedData

        if (!block) { return null }
        return block.relationships.field_block
    }

    private async getBlockContent(block: Page.IType) {
        const content = this.props.includedRelationships.find((item) => item.id === block.data.id) as Page.IPageData
        if (typeof content === "undefined") {
            const contentRequest = await this.queryReusableBlock(this.props.paragraphId)
            const includedRelationships = this.state.includedRelationships
            includedRelationships.push(...contentRequest)
            this.setState({
                type: block.data.type,
                id: block.data.id,
                childBlockData: contentRequest.find((item) => item.id === block.data.id) as Page.IPageData,
                includedRelationships,
            })
        } else {
            this.setState({
                type: content.type,
                id: content.id,
                childBlockData: content,
                includedRelationships: this.state.includedRelationships,
            })
        }
    }

    private async queryReusableBlock(id: string) {
        const response = await axios.get
            (`//api.bepc.com/api/cms/json/paragraph/reusable_block/${id}?include=field_block,field_block.field_content`) as IAxiosResponseBlock
        return response.data.included || [] as any[]
    }

    private getDefaultState() {
        /*
            This tries to get this block and the child block's data.
            If any of it is missing, this returns an empty state.
         */

        const emptyState = {
            type: "",
            id: "",
            childBlockData: null,
            includedRelationships: this.props.includedRelationships,
        }

        try {
            const block = this.getBlock()
            if (!block) {
                return emptyState
            }
            const content = this.props.includedRelationships.find((item) => item.id === block.data.id) as Page.IPageData
            const childBlockData = this.props.includedRelationships.find((item) =>
                (item.id === content.id)) as Page.IPageData

            return {
                type: content.type,
                id: content.id,
                childBlockData,
                includedRelationships: this.props.includedRelationships,
            }
        } catch (e) {
            return emptyState
        }
    }
}

export default ReusableBlock

export interface IReusableBlockProps {
    includedRelationships: Page.IPageData[],
    paragraphId: string,
    siteDomain: string,
}

export interface IReusableBlockState {
    type: string
    id: string,
    childBlockData: Page.IPageData | null,
    includedRelationships: Page.IPageData[],
}

export interface IReusableBlockIncludedData extends Page.IPageData {
    relationships: IReusableBlockRelationships
}

export interface IReusableBlockRelationships extends Page.IRelationships {
    field_block: Page.IType
}

export interface IAxiosResponseBlock extends AxiosResponse {
    data: Page.IPage
}
