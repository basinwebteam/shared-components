import { AxiosResponse, default as axios } from "axios"
import { YouTubeVideo, getVideoId } from "../../components/YouTube"
import { IYouTubePlaylistData, IYouTubeCollectionAxiosResponse, IYouTubeAPIVideoItems } from "../../DrupalContainers/VideoGallery"
import * as React from "react"
import * as Types from "../../DrupalContentTypes/ParagraphTypes"
import * as Page from "../../DrupalContentTypes/PageInterface"

export class RelatedVideos extends React.Component<IRelatedVideosProps, IRelatedVideosState> {
    public modalVisibleStyle: React.CSSProperties = { display: "block", opacity: 1, visibility: "inherit", maxWidth: "100%", height: "100%", width: "100%", background: "rgba(0,0,0,.7)", position: "fixed", top: 0 }
    public modalHiddenStyle: React.CSSProperties = { display: "none", opacity: 1, visibility: "hidden" }

    constructor(props: IRelatedVideosProps) {
        super(props)
        this.state = {
            videos: new Array(),
            showModal: false,
            activeVideoId: "",
        }
        this.closeVideo = this.closeVideo.bind(this)
    }

    //TODO: Remove tslint disable & fix the lambda. 
    //tslint:disable:jsx-no-lambda
    public render() {
        if (this.state.videos.length > 0) {
            return (
                <div className="row" id="relatedVideos">
                    <div className="column">
                        <h2 className="related-vids-header">Related Videos</h2>
                    </div>
                    <div className="vid-list column">
                        <div className="videoPlaylist">
                            <div className="videoList" id="playlist-0">
                                <ul className="vid-gal-list-item block-grid-4">
                                    {this.state.videos.slice(0, 6).map((video) => (
                                        <li key={video.id}>
                                            <a
                                                className="vid-gal-list-link rel-vid-item"
                                                onClick={() => this.showVideo(video.id)}
                                                href="#reveal-video-wrapper"
                                            >
                                                <div className="rel-vid-icon" aria-label="Play Button" />
                                                <div className="rel-vid-thumb">
                                                    <img src={video.thumbnail} />
                                                    <div className="rel-vid-title">{video.title}</div>
                                                </div>
                                            </a>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="reveal-modal" id="reveal-video-wrapper" onClick={this.closeVideo} style={this.state.showModal ? this.modalVisibleStyle : this.modalHiddenStyle}>
                        <div className="video-player-wrapper" style={{ position: "relative", maxWidth: "72rem", margin: "0 auto" }}>
                            <div id="videoPlayer" className="flex-video" style={{ display: "block" }}>
                                {this.state.showModal && this.state.activeVideoId !== "" && <YouTubeVideo link={this.state.activeVideoId} />}
                            </div>
                            <a className="close-reveal-modal" onClick={this.closeVideo} style={{ top: "-12px", right: "-35px" }}>×</a>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (<div className="row" id="relatedVideos" />)
        }
    }

    public componentDidMount() {
        this.updateComponent()
    }

    public componentDidUpdate(prevProps: IRelatedVideosProps, prevState: IRelatedVideosState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateComponent()
        }
    }

    public showVideo(videoId: string) {
        this.setState({
            showModal: true,
            activeVideoId: videoId,
        })
    }

    public closeVideo() {
        this.setState({
            showModal: false,
        })
    }

    public async updateComponent() {
        let videos: IRelatedVideoItem[]
        let activeVideoId: string
        let videoData: IYouTubePlaylistData[] | IYouTubeAPIVideoItems[]
        if (this.props.videos !== undefined && this.props.videos.length > 0) {
            const videoIds = this.props.videos
                .map((v) => getVideoId(v))
                .join(",")
            const response = await axios.get(`//api.bepc.com/api/youtube/videosById/${videoIds}`) as IYouTubeCollectionAxiosResponse
            videoData = response.data.items
            videos = videoData.map((video) => (
                {
                    title: video.snippet.title,
                    id: video.id,
                    thumbnail: video.snippet.thumbnails.default.url,
                } as IRelatedVideoItem)
            )
        } else {
            videoData = await this.queryData()
            videos = videoData.map((video) => {
                return {
                    title: video.title,
                    id: video.id,
                    thumbnail: video.thumbnailStandard,
                } as IRelatedVideoItem
            })
        }
        activeVideoId = (videos.length > 0) ? videos[0].id : ""
        this.setState({
            videos,
            activeVideoId,
        })
    }

    public async queryData() {
        const response: IAxiosRelatedVideosResponse =
            await axios.get(this.getRelatedVideosFeed())

        return response.data
    }

    private getRelatedVideosFeed() {
        const includedRelationships = this.props.includedRelationships || []
        const paragraphId = this.props.paragraphId || []
        if (this.props.includedRelationships !== undefined && this.props.paragraphId !== undefined) {

        }
        const paragraph = includedRelationships.find((p) => p.id === paragraphId) as IRelatedVideoParagraph
        if (paragraph.type === Types.RELATED_VIDEO) {
            return `//api.bepc.com/api/youtube/videosByPlaylist/${paragraph.attributes.field_video_playlist_id}`
        } else {
            const tags = paragraph.relationships.field_tags.data.map(
                (tag) => includedRelationships.find((p) => p.id === tag.id)) as Page.ITaxonomyData[]
            return `//api.bepc.com/api/youtube/videosByTag/${tags.map((tag) => tag.attributes.name).join(",")}`
        }
    }

}

export default RelatedVideos

export interface IRelatedVideosProps {
    paragraphId?: string,
    includedRelationships?: Page.IPageData[],
    videos?: string[]
}

export interface IRelatedVideosState {
    videos: IRelatedVideoItem[],
    showModal: boolean,
    activeVideoId: string,
}

export interface IRelatedVideoItem {
    title: string,
    id: string,
    thumbnail: string
}

export interface IRelatedVideoParagraph extends Page.IPageData {
    attributes: IRelatedVideoParagraphAttributes,
    relationships: IRelatedVideoParagraphRelationships,
    type: string,
}

export interface IRelatedVideoParagraphAttributes extends Page.IAttributes {
    field_video_playlist_id: string,
}
export interface IRelatedVideoParagraphRelationships extends Page.IRelationships {
    field_tags: Page.ITypeMultipleData
}

export interface IAxiosRelatedVideosResponse extends AxiosResponse {
    data: IYouTubePlaylistData[],
}
