import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import * as Type from "../../DrupalContentTypes/ParagraphTypes"

import { BasicParagraph, IBasicParagraphPageData } from "../BasicParagraph"
import { BasicBlock, IBasicBlockData } from "../ReusableBlock/Types/BasicBlock"
import ArticlePhotoGalleryDecorators from "../SliderGallery/articlePhotoGalleryDecorators"
import Award from "../Award"
import Biography from "../Biography"
import Button from "../Button"
import BoardOfDirectors from "../BoardOfDirectors"
import Calendar from "../CalendarFeed"
import DataView from "../DataView"
import Image from "../Image"
import ImageGallery from "../ImageGallery"
import ReusableBlock from "../ReusableBlock"
import { SliderGalleryContainer } from "../SliderGallery"
import { TitleContainer, ITitleData } from "../Title"
import TopicBox from "../TopicBox"
import TopicBoxRow from "../TopicBoxRow"
import VideoGallery from "../VideoGallery"
import Vimeo from "../Vimeo"
import YouTube from "../YouTube"

export const Paragraph: React.StatelessComponent<IParagraph> = (props) => (
    <div>
        {getParagraphs(props)}
    </div>
)

export const getParagraphs = (props: IParagraph) => {
    return props.items.map((paragraph, i) => {
        if (typeof paragraph === "undefined") {
            return
        }
        const key = paragraph.id + i
        switch (paragraph.type) {
            case Type.BASIC_PARAGRAPH:
                return (
                    <BasicParagraph
                        key={key}
                        paragraphId={paragraph.id}
                        data={getParagraphData(paragraph.id, props.includedRelationships as IBasicParagraphPageData[])}
                        siteDomain={props.siteDomain}
                    />
                )

            case Type.AWARD:
                return (
                    <Award
                        key={key}
                        paragraphId={paragraph.id}
                        includedRelationships={props.includedRelationships}
                        siteDomain={props.siteDomain}
                    />
                )

            case Type.BIOGRAPHY:
                return (
                    <Biography
                        key={key}
                        paragraphId={paragraph.id}
                        includedRelationships={props.includedRelationships}
                        siteDomain={props.siteDomain}
                    />
                )

            case Type.BUTTON:
                return (
                    <Button
                        key={key}
                        paragraphId={paragraph.id}
                        includedRelationships={props.includedRelationships}
                        siteDomain={props.siteDomain}
                    />
                )

            case Type.BOARD_OF_DIRECTORS:
                return (
                    <BoardOfDirectors
                        key={key}
                        paragraphId={paragraph.id}
                        siteDomain={props.siteDomain}
                    />
                )

            case Type.DATA_VIEW:
                return (
                    <DataView
                        key={key}
                        paragraphId={paragraph.id}
                        includedRelationships={props.includedRelationships}
                        siteDomain={props.siteDomain}
                    />
                )

            case Type.IMAGE:
                return (
                    <Image
                        key={key}
                        includedRelationships={props.includedRelationships}
                        paragraphId={paragraph.id}
                        siteDomain={props.siteDomain}
                        className="inlineImageLarge"
                    />
                )

            case Type.IMAGE_ASIDE:
                return (
                    <Image
                        key={key}
                        includedRelationships={props.includedRelationships}
                        paragraphId={paragraph.id}
                        siteDomain={props.siteDomain}
                        className="inlineImage"
                        imageFileFieldName="field_image_aside"
                        imageParagraphName="image_aside"
                    />
                )

            case Type.IMAGE_BANNER:
                return (
                    <Image
                        key={key}
                        includedRelationships={props.includedRelationships}
                        paragraphId={paragraph.id}
                        siteDomain={props.siteDomain}
                        className="image-banner"
                    />
                )

            case Type.IMAGE_GALLERY:
                return (
                    <ImageGallery
                        key={key}
                        includedRelationships={props.includedRelationships}
                        siteDomain={props.siteDomain}
                        paragraphId={paragraph.id}
                    />
                )

            case Type.INCONTENT_IMAGE_GALLERY:
                return (
                    <SliderGalleryContainer
                        key={key}
                        paragraphId={paragraph.id}
                        includedRelationships={props.includedRelationships}
                        siteDomain={props.siteDomain}
                        carousel={props.carousel}
                        autoplay={false}
                    />
                )

            case Type.RESUABLE_BLOCK:
                return (
                    <ReusableBlock
                        key={key}
                        paragraphId={paragraph.id}
                        siteDomain={props.siteDomain}
                        includedRelationships={props.includedRelationships}
                    />
                )

            case Type.SHAREPOINT_CALENDAR:
                return (
                    <Calendar
                        key={key}
                        paragraphId={paragraph.id}
                        includedRelationships={props.includedRelationships}
                    />
                )

            case Type.SLIDER_GALLERY:
                if (props.carousel) {
                    return (
                        <SliderGalleryContainer
                            key={key}
                            paragraphId={paragraph.id}
                            includedRelationships={props.includedRelationships}
                            siteDomain={props.siteDomain}
                            carousel={props.carousel}
                        />
                    )
                }

            case Type.TITLE:
                return (
                    <TitleContainer
                        key={key}
                        paragraphId={paragraph.id}
                        titleParagraphData={getParagraphData(paragraph.id, props.includedRelationships as ITitleData[])}
                        siteDomain={props.siteDomain}
                    />
                )

            case Type.TOPIC_BOX_ROW:
                return (
                    <TopicBoxRow
                        key={key}
                        paragraphId={paragraph.id}
                        siteDomain={props.siteDomain}
                        includedRelationships={props.includedRelationships}
                    />
                )

            case Type.TOPIC_BOX:
                return (
                    <TopicBox
                        key={key}
                        paragraphId={paragraph.id}
                        siteDomain={props.siteDomain}
                        includedRelationships={props.includedRelationships}
                    />
                )

            case Type.UNIQUE_BASIC_BLOCK:
                return (
                    <BasicBlock
                        key={key}
                        useTitle={true}
                        blockId={paragraph.id}
                        blockData={getParagraphData(paragraph.id, props.includedRelationships as IBasicBlockData[])}
                        includedPageData={props.includedRelationships}
                        siteDomain={props.siteDomain}
                    />
                )

            case Type.VIMEO:
                return (
                    <Vimeo
                        key={key}
                        paragraphId={paragraph.id}
                        includedRelationships={props.includedRelationships}
                    />
                )

            case Type.VIDEO_GALLERY:
                return (
                    <VideoGallery
                        key={key}
                        paragraphId={paragraph.id}
                        includedRelationships={props.includedRelationships}
                        siteDomain={props.siteDomain}
                    />
                )

            case Type.YOUTUBE:
                return (
                    <YouTube
                        key={key}
                        paragraphId={paragraph.id}
                        includedRelationships={props.includedRelationships}
                    />
                )

            default:
                return (
                    <div className={paragraph.type} key={i}>{/* Default {paragraph.type} */}</div>)
        }
    })
}

export function getParagraphData<T extends Page.IPageData>(id: string, relationships: T[]): T {
    return relationships.find((paragraph) => {
        return paragraph.id === id
    }) as T
}

export default Paragraph

export interface IParagraph {
    items: Array<Page.IDatum | undefined>,
    includedRelationships: Page.IPageData[],
    siteDomain: string,
    carousel?: React.ComponentClass<any>
}
