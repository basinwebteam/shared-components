import * as React from "react"
import { MouseEvent } from "react"
import { IDecorators } from "nuka-carousel"

export const ArticleLeftButton: React.StatelessComponent<IProps> = (props) => {
  const handleClick = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault()
    props.previousSlide()
  }

  return (
    <button
      className="slick-prev"
      style={getButtonStyles(props.currentSlide === 0 && !props.wrapAround)}
      onClick={handleClick}
    >
      <span className="prev">PREV</span>
    </button>
  )
}

export const ArticleRightButton: React.StatelessComponent<IProps> = (props) => {
  const handleClick = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault()
    props.nextSlide()
  }

  return (
    <button
      className="slick-next"
      style={getButtonStyles(props.currentSlide + props.slidesToScroll >= props.slideCount && !props.wrapAround)}
      onClick={handleClick}
    >
      <span className="next">NEXT</span>
    </button>
  )
}

const getButtonStyles = (disabled: boolean) => {
  return {
    border: 0,
    background: "#FFF",
    color: "rgba(0,159,223,0.75)",
    padding: "2px 2px 30px 2px",
    outline: 0,
    opacity: disabled ? 0.3 : 1,
    cursor: "pointer",
  }
}

const ArticlePhotoGalleryDecorators = [
  {
    component: ArticleLeftButton,
    position: "BottomLeft",
  },
  {
    component: ArticleRightButton,
    position: "BottomRight",
  },
] as IDecorators[]

export default ArticlePhotoGalleryDecorators

export interface IProps {
  nextSlide: () => void,
  previousSlide: () => void,
  currentSlide: number,
  wrapAround: boolean,
  slidesToScroll: number,
  slideCount: number,
}
