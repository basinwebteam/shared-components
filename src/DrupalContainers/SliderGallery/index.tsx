import * as React from "react"
import { GetSiteMap } from "../../DrupalAPI/sitemap"
import * as Page from "../../DrupalContentTypes/PageInterface"
import DefaultDecorators from "./carouselDecorators"
import ArticlePhotoGalleryDecorators from "./articlePhotoGalleryDecorators"
import ArticlePhotoGalleryDecoratorsVertical from "./articlePhotoGalleryDecoratorsVertical"
import { ITopicBoxWebsiteLink } from "../TopicBox"

//TODO: Replace Carousel with a TypeScript component...
export class SliderGalleryContainer extends React.Component<ISliderGalContProps, ISliderGalContState> {
    constructor(props: ISliderGalContProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    displayName = "DrupalSliderGalleryContainer"

    render() {
        const Carousel = this.props.carousel
        const autoplay = this.props.autoplay === undefined ? true : this.props.autoplay
        const galleryType = this.state.galleryType
        const decorator = this.getDecorator()

        if (Carousel) {
            return (
                <div className={this.getClassName()}>
                    <Carousel decorators={decorator} wrapAround={true} autoplay={autoplay}>
                        {this.state.slides.map((slide: ISlide, i: number) => {
                            return this.props.slide || this.defaultSlide(slide, i)
                        })}
                    </Carousel>
                </div>
            )
        }
        return <div className="slider-gallery" />
    }

    defaultSlide(slide: ISlide, i: number) {
        if (slide.link === "#" || slide.link === "") {
            return (
                <span key={i}>
                    <img alt={slide.imageAlt} src={slide.imageUrl} />
                    <div className="caption" dangerouslySetInnerHTML={{ __html: slide.caption }} />
                </span>
            )
        } else {
            return (
                <a href={slide.link} key={i}>
                    <img alt={slide.imageAlt} src={slide.imageUrl} />
                    <div className="caption" dangerouslySetInnerHTML={{ __html: slide.caption }} />
                </a>
            )
        }
    }

    componentDidMount() {
        this.updateComponent()
    }

    async updateComponent(queryIncludedData = false) {
        // todo this needs more testing to make
        const slides = this.state.slides.map(async (slide, i) => {
            if (this.checkUrlStringForEntity(slide.link)) {
                const url = await this.getUrl({ uri: slide.link })
                if (url !== null) {
                    slide.link = url
                }
            }
            return slide
        })

        Promise.all(slides).then((newSlides) => {
            this.setState({ slides: newSlides })
        })
    }

    getDefaultState() {
        /* This tries to get the block and related child data if its part of the props blockData value.

        If any of it is missing, this returns an empty state. */
        try {
            const relationships = this.props.includedRelationships
            const gallery = this.props.includedRelationships.find(p => p.id === this.props.paragraphId) as ISliderGalleryPageData
            const galleryData = gallery.relationships.field_slider.data

            const slides = galleryData.map((slide) => {
                const slideData = relationships.find(p => p.id === slide.id) as ISlideData
                const imageFile = relationships.find(p => p.id === slideData.relationships.field_image.data.id) as Page.IField_File

                return {
                    imageUrl: this.props.siteDomain + imageFile.attributes.url,
                    imageAlt: slideData.relationships.field_image.data.meta.alt,
                    caption: slideData.attributes.field_caption ? slideData.attributes.field_caption.value : "",
                    link: slideData.attributes.field_website_link ? slideData.attributes.field_website_link.uri : "#",
                } as ISlide
            })

            return {
                slides,
                galleryType: this.getGalleryType(gallery)
            } as ISliderGalContState
        } catch (e) {
            return {
                slides: [],
                galleryType: undefined
            } as ISliderGalContState
        }
    }
    checkUrlForEntity(link: ITopicBoxWebsiteLink) {
        if (link !== null && link.uri !== null) {
            if (link.uri.substr(0, 11) === "entity:node") {
                return true
            }
        }
        return false
    }

    checkUrlStringForEntity(link: string) {
        if (link.substr(0, 11) === "entity:node") {
            return true
        }
        return false
    }

    getGalleryType(gallery: ISliderGalleryPageData): GalleryType | undefined {
        if (gallery.relationships.field_image_gallery_type !== undefined) {
            if (gallery.relationships.field_image_gallery_type.data !== undefined) {
                switch (gallery.relationships.field_image_gallery_type.data.id) {
                    case ("921f9001-3bea-46e5-a6a4-a8262fedf907"):
                        return "Vertical"
                    case ("b4a047c2-0397-4c6c-a8e7-2318b7eb41cc"):
                    default:
                        return "Horizontal"
                }
            }
        }
        return undefined
    }

    async getUrl(link: ITopicBoxWebsiteLink) {
        const map = this.props.siteDomain
            .replace("https://", "")
            .replace("//", "")
            .replace(".com", "")

        if (this.checkUrlForEntity(link)) {
            const sitemap = await GetSiteMap(map)
            const matchedPage = sitemap.find((page) =>
                page.attributes.nid.toString() === link.uri.substr(12))
            if (typeof matchedPage !== "undefined") {
                return matchedPage.attributes.path.alias
            }

            const newsSitemap = await GetSiteMap("news")
            const matchedNewsPage = newsSitemap.find((page) =>
                page.attributes.nid.toString() === link.uri.substr(12))
            if (typeof matchedNewsPage !== "undefined") {
                return matchedNewsPage.attributes.path.alias
            }

        }
        return (link !== null && link.uri !== null) ? link.uri : null
    }

    getClassName() {
        let className = 'slider-gallery'
        if (this.props.className) {
            className = " " + this.props.className
        }
        if (this.state.galleryType) {
            className += ` slider-gallery-article-${this.state.galleryType.toLowerCase()}`
        }
        return className
    }
    getDecorator(): any {
        if (this.props.decorators) {
            return this.props.decorators
        }

        if (this.state.galleryType === "Horizontal") {
            return ArticlePhotoGalleryDecorators
        }
        if (this.state.galleryType === "Vertical") {
            return ArticlePhotoGalleryDecoratorsVertical
        }

        return DefaultDecorators
    }

}

export default SliderGalleryContainer

export interface ISliderGalContProps {
    includedRelationships: Array<Page.IPageData | ISliderGalleryPageData | any>
    paragraphId: string
    siteDomain: string
    carousel?: React.ComponentClass<any>
    slide?: React.Component
    decorators?: IDecorators[]
    className?: string
    autoplay?: boolean
}

export interface ICarouselProps {
    decorators?: IDecorators[],
    wrapAround?: boolean
}

export interface ISliderGalContState {
    slides: ISlide[]
    galleryType?: GalleryType
}

export interface ISlide {
    imageUrl: string,
    imageAlt: string,
    caption: string,
    link: string,
}

export interface ISliderGalleryData extends Page.IType {
    data: ISliderGalleryPageData
}

export interface ISliderGalleryPageData extends Page.IPageData {
    relationships: ISliderGalleryRelationships,
}

export interface ISliderGalleryRelationships extends Page.IRelationships {
    field_image_gallery_type?: Page.IType
    field_slider: {
        data: ISlideData[],
    }
}

export interface ISlideData extends Page.IPageData {
    attributes: ISlideAttributes,
    relationships: ISlideRelationships
}

export interface ISlideAttributes extends Page.IAttributes {
    field_caption: {
        value: string,
        format: string,
    } | null,
    field_website_link?: ITopicBoxWebsiteLink
}

export interface ISlideRelationships extends Page.IRelationships {
    field_image: Page.IField_Image
}

export interface IDecorators {
    component?: React.StatelessComponent,
    position?: decoratorPosition,
    style?: React.CSSProperties,
}

export type decoratorPosition = "TopLeft" | "TopCenter" | "TopRight" | "CenterLeft" | "CenterCenter" | "CenterRight" | "BottomLeft" | "BottomCenter" | "BottomRight"

export type GalleryType = "Horizontal" | "Vertical"