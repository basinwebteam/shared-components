import * as React from "react"
import { MouseEvent } from "react"
import { IDecorators } from "nuka-carousel"

export const LeftButton: React.StatelessComponent<IProps> = (props) => {
  const handleClick = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault()
    props.previousSlide()
  }

  const getButtonStyles = (disabled: boolean) => {
    return {
      border: 0,
      background: "rgba(0,159,223,0.75)",
      color: "white",
      padding: 16,
      outline: 0,
      opacity: disabled ? 0.3 : 1,
      cursor: "pointer",
    }
  }

  return (
    <button 
      className="slick-prev"
      style={getButtonStyles(props.currentSlide === 0 && !props.wrapAround)}
      onClick={handleClick}
    >
        <span className="prev">PREV</span>
    </button>
  )
}

const RightButton: React.StatelessComponent<IProps> = (props) => {
  const handleClick = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault()
    props.nextSlide()
  }

  const getButtonStyles = (disabled: boolean) => {
    return {
      border: 0,
      background: "rgba(0,159,223,0.75)",
      color: "white",
      padding: 16,
      outline: 0,
      opacity: disabled ? 0.3 : 1,
      cursor: "pointer",
    }
  }

  return (
    <button
      className="slick-next"
      style={getButtonStyles(props.currentSlide + props.slidesToScroll >= props.slideCount && !props.wrapAround)}
      onClick={handleClick}
    >
      <span className="next">NEXT</span>
    </button>
  )
}

const DefaultDecorators = [
  {
    component: LeftButton,
    position: "CenterLeft",
  },
  {
    component: RightButton,
    position: "CenterRight",
  },
] as IDecorators[]

export default DefaultDecorators

export interface IProps {
  nextSlide: () => void,
  previousSlide: () => void,
  currentSlide: number,
  wrapAround: boolean,
  slidesToScroll: number,
  slideCount: number,
}
