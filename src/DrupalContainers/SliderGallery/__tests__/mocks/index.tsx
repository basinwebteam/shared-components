// tslint:disable:max-line-length

export const pageData = {
    "data": {
        "type": "node--basin_electric_basic_page",
        "id": "d02322d3-030f-4ac5-b597-fdf60de15a83",
        "attributes": {
            "nid": 2541,
            "uuid": "d02322d3-030f-4ac5-b597-fdf60de15a83",
            "vid": 20826,
            "langcode": "en",
            "status": true,
            "title": "Home",
            "created": 1501680797,
            "changed": 1517431869,
            "promote": false,
            "sticky": false,
            "revision_timestamp": 1517431869,
            "revision_log": null,
            "revision_translation_affected": true,
            "default_langcode": true,
            "path": {
                "alias": "/basin-home",
                "pid": 3381
            },
            "field_meta_tags": "a:0:{}",
            "field_summary": null
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "c1953c9b-859c-42d1-a2ae-a74572456c0c"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/type"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "db4a17fc-2cab-463e-881f-3243f7d239a1"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/uid"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/revision_uid"
                }
            },
            "menu_link": {
                "data": null
            },
            "moderation_state": {
                "data": null
            },
            "field_aside_content": {
                "data": []
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--slider_gallery",
                        "id": "ed3fce20-a1dc-4bf7-8f74-c732335f932e",
                        "meta": {
                            "target_revision_id": "65436"
                        }
                    },
                    {
                        "type": "paragraph--topic_box_row",
                        "id": "5258216f-f20a-4606-8de0-2a64b9f83c3f",
                        "meta": {
                            "target_revision_id": "65471"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/field_content"
                }
            },
            "field_related_videos": {
                "data": null
            },
            "field_web_layout": {
                "data": {
                    "type": "taxonomy_term--web_layouts",
                    "id": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/relationships/field_web_layout",
                    "related": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83/field_web_layout"
                }
            },
            "scheduled_update": {
                "data": []
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83"
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/basin_electric_basic_page/d02322d3-030f-4ac5-b597-fdf60de15a83?include=field_content%2Cfield_aside_content%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_view%2Cfield_content.field_biography_reference%2Cfield_biography_reference.field_image%2Cfield_content.field_content%2Cfield_content.field_content.field_image%2Cfield_content.field_title_color%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_content.field_form_id%2Cfield_content.field_topic_row%2Cfield_content.field_topic_row.field_title_color%2Cfield_content.field_topic_row.field_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_content.field_gallery_image%2Cfield_content.field_gallery_image.field_image%2Cfield_content.field_calendar%2Cfield_content.field_gallery_playlist_videos%2Cfield_content.field_gallery_playlist_videos.field_video_gallery_videos%2Cfield_content.field_board%2Cfield_content.field_board.field_content%2Cfield_web_layout"
    },
    "included": [
        {
            "type": "paragraph--slider_gallery",
            "id": "ed3fce20-a1dc-4bf7-8f74-c732335f932e",
            "attributes": {
                "id": 5221,
                "uuid": "ed3fce20-a1dc-4bf7-8f74-c732335f932e",
                "revision_id": 65436,
                "langcode": "en",
                "status": true,
                "created": 1501769464,
                "parent_id": "2541",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "602b51dd-969b-4048-906d-6d5ded3799a6"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_gallery/ed3fce20-a1dc-4bf7-8f74-c732335f932e/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_gallery/ed3fce20-a1dc-4bf7-8f74-c732335f932e/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_gallery/ed3fce20-a1dc-4bf7-8f74-c732335f932e/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_gallery/ed3fce20-a1dc-4bf7-8f74-c732335f932e/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_gallery/ed3fce20-a1dc-4bf7-8f74-c732335f932e/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_gallery/ed3fce20-a1dc-4bf7-8f74-c732335f932e/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_slider": {
                    "data": [
                        {
                            "type": "paragraph--slider",
                            "id": "c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8",
                            "meta": {
                                "target_revision_id": "65426"
                            }
                        },
                        {
                            "type": "paragraph--slider",
                            "id": "1da482b8-3911-4d4d-b040-2bb61e807515",
                            "meta": {
                                "target_revision_id": "65431"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_gallery/ed3fce20-a1dc-4bf7-8f74-c732335f932e/relationships/field_slider",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_gallery/ed3fce20-a1dc-4bf7-8f74-c732335f932e/field_slider"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/slider_gallery/ed3fce20-a1dc-4bf7-8f74-c732335f932e"
            }
        },
        {
            "type": "paragraph--topic_box_row",
            "id": "5258216f-f20a-4606-8de0-2a64b9f83c3f",
            "attributes": {
                "id": 10091,
                "uuid": "5258216f-f20a-4606-8de0-2a64b9f83c3f",
                "revision_id": 65471,
                "langcode": "en",
                "status": true,
                "created": 1504820095,
                "parent_id": "2541",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "9c5559bf-55b6-49ca-810f-5fd8cbd68a6b"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box_row/5258216f-f20a-4606-8de0-2a64b9f83c3f/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box_row/5258216f-f20a-4606-8de0-2a64b9f83c3f/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box_row/5258216f-f20a-4606-8de0-2a64b9f83c3f/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box_row/5258216f-f20a-4606-8de0-2a64b9f83c3f/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box_row/5258216f-f20a-4606-8de0-2a64b9f83c3f/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box_row/5258216f-f20a-4606-8de0-2a64b9f83c3f/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_topic_row": {
                    "data": [
                        {
                            "type": "paragraph--topic_box",
                            "id": "e2fce84d-3e8a-4324-83e6-f83445059979",
                            "meta": {
                                "target_revision_id": "65446"
                            }
                        },
                        {
                            "type": "paragraph--topic_box",
                            "id": "4b18cc07-d1cd-4513-a94c-16b71abc0d2c",
                            "meta": {
                                "target_revision_id": "65456"
                            }
                        },
                        {
                            "type": "paragraph--topic_box",
                            "id": "24dcde43-05f8-4023-ad54-c71611b26c63",
                            "meta": {
                                "target_revision_id": "65466"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box_row/5258216f-f20a-4606-8de0-2a64b9f83c3f/relationships/field_topic_row",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box_row/5258216f-f20a-4606-8de0-2a64b9f83c3f/field_topic_row"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box_row/5258216f-f20a-4606-8de0-2a64b9f83c3f"
            }
        },
        {
            "type": "taxonomy_term--web_layouts",
            "id": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb",
            "attributes": {
                "tid": 606,
                "uuid": "f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb",
                "langcode": "en",
                "name": "Full Page",
                "description": null,
                "weight": 0,
                "changed": 1502982012,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "9481b046-bd2c-4ef8-ba1d-acde28597ffc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb/vid"
                    }
                },
                "parent": {
                    "data": []
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/web_layouts/f7bbc0d1-fab6-4d3a-bcf6-898ae7f039eb"
            }
        },
        {
            "type": "paragraph--slider",
            "id": "c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8",
            "attributes": {
                "id": 5206,
                "uuid": "c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8",
                "revision_id": 65426,
                "langcode": "en",
                "status": true,
                "created": 1501769464,
                "parent_id": "5221",
                "parent_type": "paragraph",
                "parent_field_name": "field_slider",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_caption": {
                    "value": "<p>Dakota Gasification Company holds great value to Basin Electric's membership.</p>\r\n",
                    "format": "rich_text"
                },
                "field_website_link": {
                    "uri": "entity:node/5741",
                    "title": "",
                    "options": []
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "fa320ba7-39a3-4a11-8af4-8e3564ec7e04"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider/c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider/c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider/c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider/c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider/c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider/c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "5e3eda34-a52e-4f93-ac76-06611e342282",
                        "meta": {
                            "alt": "Great Plains Synfuels Plant",
                            "title": "",
                            "width": "1152",
                            "height": "504"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider/c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider/c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/slider/c0c2535d-dd91-44cc-aaab-2e5d2a05c3a8"
            }
        },
        {
            "type": "paragraph--slider",
            "id": "1da482b8-3911-4d4d-b040-2bb61e807515",
            "attributes": {
                "id": 5216,
                "uuid": "1da482b8-3911-4d4d-b040-2bb61e807515",
                "revision_id": 65431,
                "langcode": "en",
                "status": true,
                "created": 1501769619,
                "parent_id": "5221",
                "parent_type": "paragraph",
                "parent_field_name": "field_slider",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_caption": {
                    "value": "<p>Basin Electric's recent completion of its Headquarters expansion in Bismarck, ND, reinforces the co-op's commitment to the future of the cooperative workforce, its membership, and to the community of Bismarck.</p>\r\n",
                    "format": "rich_text"
                },
                "field_website_link": {
                    "uri": "entity:node/5751",
                    "title": "",
                    "options": []
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "fa320ba7-39a3-4a11-8af4-8e3564ec7e04"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider/1da482b8-3911-4d4d-b040-2bb61e807515/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider/1da482b8-3911-4d4d-b040-2bb61e807515/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider/1da482b8-3911-4d4d-b040-2bb61e807515/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider/1da482b8-3911-4d4d-b040-2bb61e807515/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider/1da482b8-3911-4d4d-b040-2bb61e807515/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider/1da482b8-3911-4d4d-b040-2bb61e807515/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "7cc30510-6f00-4ed5-9303-41e94c2cc2f3",
                        "meta": {
                            "alt": "Headquarters expansion",
                            "title": "",
                            "width": "1152",
                            "height": "504"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider/1da482b8-3911-4d4d-b040-2bb61e807515/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider/1da482b8-3911-4d4d-b040-2bb61e807515/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/slider/1da482b8-3911-4d4d-b040-2bb61e807515"
            }
        },
        {
            "type": "file--file",
            "id": "5e3eda34-a52e-4f93-ac76-06611e342282",
            "attributes": {
                "fid": 8621,
                "uuid": "5e3eda34-a52e-4f93-ac76-06611e342282",
                "langcode": "en",
                "filename": "2017-11-12-DGC-home.jpg",
                "uri": "public://images/home-hero/2017-11-12-DGC-home.jpg",
                "filemime": "image/jpeg",
                "filesize": 102928,
                "status": true,
                "created": 1515453141,
                "changed": 1515453322,
                "url": "/sites/CMS/files/images/home-hero/2017-11-12-DGC-home.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/5e3eda34-a52e-4f93-ac76-06611e342282/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/5e3eda34-a52e-4f93-ac76-06611e342282/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/5e3eda34-a52e-4f93-ac76-06611e342282"
            }
        },
        {
            "type": "file--file",
            "id": "7cc30510-6f00-4ed5-9303-41e94c2cc2f3",
            "attributes": {
                "fid": 8626,
                "uuid": "7cc30510-6f00-4ed5-9303-41e94c2cc2f3",
                "langcode": "en",
                "filename": "2017-11-12-HDQ-home.jpg",
                "uri": "public://images/home-hero/2017-11-12-HDQ-home.jpg",
                "filemime": "image/jpeg",
                "filesize": 124040,
                "status": true,
                "created": 1515453284,
                "changed": 1515453322,
                "url": "/sites/CMS/files/images/home-hero/2017-11-12-HDQ-home.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/7cc30510-6f00-4ed5-9303-41e94c2cc2f3/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/7cc30510-6f00-4ed5-9303-41e94c2cc2f3/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/7cc30510-6f00-4ed5-9303-41e94c2cc2f3"
            }
        },
        {
            "type": "paragraph--topic_box",
            "id": "e2fce84d-3e8a-4324-83e6-f83445059979",
            "attributes": {
                "id": 10076,
                "uuid": "e2fce84d-3e8a-4324-83e6-f83445059979",
                "revision_id": 65446,
                "langcode": "en",
                "status": true,
                "created": 1504820095,
                "parent_id": "10091",
                "parent_type": "paragraph",
                "parent_field_name": "field_topic_row",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": "Spend a day in the life of Basin Electric Performance Engineer Josh Raynes",
                "field_topic_box_width": 0,
                "field_website_link": {
                    "uri": "entity:node/5111",
                    "title": "",
                    "options": []
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "a2f35e84-04b8-470f-a191-35a9a15b98b5"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_content": {
                    "data": [
                        {
                            "type": "paragraph--image_topic_box",
                            "id": "ca7b502e-821f-441a-b967-9cd9f1aa51d5",
                            "meta": {
                                "target_revision_id": "65441"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/relationships/field_content",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/field_content"
                    }
                },
                "field_title_color": {
                    "data": {
                        "type": "taxonomy_term--basin_primary_colors",
                        "id": "db06ef35-b62b-44d2-bb44-a888b3be1c2a"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/relationships/field_title_color",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979/field_title_color"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/e2fce84d-3e8a-4324-83e6-f83445059979"
            }
        },
        {
            "type": "paragraph--topic_box",
            "id": "4b18cc07-d1cd-4513-a94c-16b71abc0d2c",
            "attributes": {
                "id": 10086,
                "uuid": "4b18cc07-d1cd-4513-a94c-16b71abc0d2c",
                "revision_id": 65456,
                "langcode": "en",
                "status": true,
                "created": 1504820278,
                "parent_id": "10091",
                "parent_type": "paragraph",
                "parent_field_name": "field_topic_row",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": "Wind subsidiaries to merge into Basin Electric",
                "field_topic_box_width": 0,
                "field_website_link": {
                    "uri": "entity:node/5681",
                    "title": "",
                    "options": []
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "a2f35e84-04b8-470f-a191-35a9a15b98b5"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_content": {
                    "data": [
                        {
                            "type": "paragraph--image_topic_box",
                            "id": "09b463e4-9bba-42c4-97a1-51b9893e3cc7",
                            "meta": {
                                "target_revision_id": "65451"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/relationships/field_content",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/field_content"
                    }
                },
                "field_title_color": {
                    "data": {
                        "type": "taxonomy_term--basin_primary_colors",
                        "id": "db06ef35-b62b-44d2-bb44-a888b3be1c2a"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/relationships/field_title_color",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c/field_title_color"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/4b18cc07-d1cd-4513-a94c-16b71abc0d2c"
            }
        },
        {
            "type": "paragraph--topic_box",
            "id": "24dcde43-05f8-4023-ad54-c71611b26c63",
            "attributes": {
                "id": 10101,
                "uuid": "24dcde43-05f8-4023-ad54-c71611b26c63",
                "revision_id": 65466,
                "langcode": "en",
                "status": true,
                "created": 1504820428,
                "parent_id": "10091",
                "parent_type": "paragraph",
                "parent_field_name": "field_topic_row",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "field_title": "Basin Electric issues Request for Proposal for power supply",
                "field_topic_box_width": 0,
                "field_website_link": {
                    "uri": "entity:node/5786",
                    "title": "",
                    "options": []
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "a2f35e84-04b8-470f-a191-35a9a15b98b5"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_content": {
                    "data": [
                        {
                            "type": "paragraph--image_topic_box",
                            "id": "9c3b53ad-06e4-4e40-bede-7e31e9cb8e51",
                            "meta": {
                                "target_revision_id": "65461"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/relationships/field_content",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/field_content"
                    }
                },
                "field_title_color": {
                    "data": {
                        "type": "taxonomy_term--basin_primary_colors",
                        "id": "db06ef35-b62b-44d2-bb44-a888b3be1c2a"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/relationships/field_title_color",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63/field_title_color"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/topic_box/24dcde43-05f8-4023-ad54-c71611b26c63"
            }
        },
        {
            "type": "paragraph--image_topic_box",
            "id": "ca7b502e-821f-441a-b967-9cd9f1aa51d5",
            "attributes": {
                "id": 10321,
                "uuid": "ca7b502e-821f-441a-b967-9cd9f1aa51d5",
                "revision_id": 65441,
                "langcode": "en",
                "status": true,
                "created": 1505769494,
                "parent_id": "10076",
                "parent_type": "paragraph",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "0914fff7-5d11-465e-9a62-efe71306b633"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/ca7b502e-821f-441a-b967-9cd9f1aa51d5/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/ca7b502e-821f-441a-b967-9cd9f1aa51d5/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/ca7b502e-821f-441a-b967-9cd9f1aa51d5/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/ca7b502e-821f-441a-b967-9cd9f1aa51d5/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/ca7b502e-821f-441a-b967-9cd9f1aa51d5/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/ca7b502e-821f-441a-b967-9cd9f1aa51d5/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "8fe92a4b-9a7a-4b01-a35e-be9664103402",
                        "meta": {
                            "alt": "Josh Raynes",
                            "title": "",
                            "width": "325",
                            "height": "180"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/ca7b502e-821f-441a-b967-9cd9f1aa51d5/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/ca7b502e-821f-441a-b967-9cd9f1aa51d5/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/ca7b502e-821f-441a-b967-9cd9f1aa51d5"
            }
        },
        {
            "type": "taxonomy_term--basin_primary_colors",
            "id": "db06ef35-b62b-44d2-bb44-a888b3be1c2a",
            "attributes": {
                "tid": 581,
                "uuid": "db06ef35-b62b-44d2-bb44-a888b3be1c2a",
                "langcode": "en",
                "name": "Blue",
                "description": null,
                "weight": 0,
                "changed": 1502914799,
                "default_langcode": true,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "19c8e71b-14c4-47b2-8eb6-4e768ae6f606"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/basin_primary_colors/db06ef35-b62b-44d2-bb44-a888b3be1c2a/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/basin_primary_colors/db06ef35-b62b-44d2-bb44-a888b3be1c2a/vid"
                    }
                },
                "parent": {
                    "data": []
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/basin_primary_colors/db06ef35-b62b-44d2-bb44-a888b3be1c2a"
            }
        },
        {
            "type": "paragraph--image_topic_box",
            "id": "09b463e4-9bba-42c4-97a1-51b9893e3cc7",
            "attributes": {
                "id": 10081,
                "uuid": "09b463e4-9bba-42c4-97a1-51b9893e3cc7",
                "revision_id": 65451,
                "langcode": "en",
                "status": true,
                "created": 1504820316,
                "parent_id": "10086",
                "parent_type": "paragraph",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "0914fff7-5d11-465e-9a62-efe71306b633"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/09b463e4-9bba-42c4-97a1-51b9893e3cc7/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/09b463e4-9bba-42c4-97a1-51b9893e3cc7/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/09b463e4-9bba-42c4-97a1-51b9893e3cc7/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/09b463e4-9bba-42c4-97a1-51b9893e3cc7/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "6a62af53-1704-4f1e-ac25-5d31f35d9193"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/09b463e4-9bba-42c4-97a1-51b9893e3cc7/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/09b463e4-9bba-42c4-97a1-51b9893e3cc7/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "deffbdb7-ed6d-4174-b465-628bc6a1fe62",
                        "meta": {
                            "alt": "wind",
                            "title": "",
                            "width": "325",
                            "height": "180"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/09b463e4-9bba-42c4-97a1-51b9893e3cc7/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/09b463e4-9bba-42c4-97a1-51b9893e3cc7/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/09b463e4-9bba-42c4-97a1-51b9893e3cc7"
            }
        },
        {
            "type": "paragraph--image_topic_box",
            "id": "9c3b53ad-06e4-4e40-bede-7e31e9cb8e51",
            "attributes": {
                "id": 11801,
                "uuid": "9c3b53ad-06e4-4e40-bede-7e31e9cb8e51",
                "revision_id": 65461,
                "langcode": "en",
                "status": true,
                "created": 1508939428,
                "parent_id": "10101",
                "parent_type": "paragraph",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "0914fff7-5d11-465e-9a62-efe71306b633"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/9c3b53ad-06e4-4e40-bede-7e31e9cb8e51/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/9c3b53ad-06e4-4e40-bede-7e31e9cb8e51/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/9c3b53ad-06e4-4e40-bede-7e31e9cb8e51/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/9c3b53ad-06e4-4e40-bede-7e31e9cb8e51/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/9c3b53ad-06e4-4e40-bede-7e31e9cb8e51/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/9c3b53ad-06e4-4e40-bede-7e31e9cb8e51/revision_uid"
                    }
                },
                "moderation_state": {
                    "data": null
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "b608effe-26d3-465a-9f7d-18c3a0b75e9f",
                        "meta": {
                            "alt": "Request for Proposal",
                            "title": "",
                            "width": "325",
                            "height": "180"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/9c3b53ad-06e4-4e40-bede-7e31e9cb8e51/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/9c3b53ad-06e4-4e40-bede-7e31e9cb8e51/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/image_topic_box/9c3b53ad-06e4-4e40-bede-7e31e9cb8e51"
            }
        }
    ]
}

export const articleSliderPageDataHorizontal = {
    "data": {
        "type": "node--news_page",
        "id": "a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce",
        "attributes": {
            "nid": 6021,
            "uuid": "a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce",
            "vid": 26286,
            "langcode": "en",
            "revision_timestamp": 1531162352,
            "revision_log": null,
            "status": true,
            "title": "Employees graduate from inaugural BE Leaders program",
            "created": 1519767675,
            "changed": 1531162352,
            "promote": false,
            "sticky": false,
            "default_langcode": true,
            "revision_translation_affected": true,
            "metatag": null,
            "path": {
                "alias": "/news-center/publications/basin-today/employees-graduate-inaugural-be-leaders-program",
                "pid": 5081,
                "langcode": "en"
            },
            "field_byline": "Tammy Langerud",
            "field_meta_tags": "a:0:{}",
            "field_published_date": "2018-02-27",
            "field_summary": "A major component of the program was a leadership development project designed to positively impact the co-op’s employees, a process, or procedure."
        },
        "relationships": {
            "type": {
                "data": {
                    "type": "node_type--node_type",
                    "id": "8913bdc6-27fb-474e-bfb1-d8994bca68dd"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/type",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/type"
                }
            },
            "revision_uid": {
                "data": {
                    "type": "user--user",
                    "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/revision_uid",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/revision_uid"
                }
            },
            "uid": {
                "data": {
                    "type": "user--user",
                    "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/uid",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/uid"
                }
            },
            "menu_link": {
                "data": null,
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/menu_link",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/menu_link"
                }
            },
            "field_aside_content": {
                "data": [
                    {
                        "type": "paragraph--title",
                        "id": "02998ba9-9795-40d1-9c20-4fd25b93ecb2",
                        "meta": {
                            "target_revision_id": "85611"
                        }
                    },
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "63efcc1b-71ae-4fc3-8464-049699eaed9b",
                        "meta": {
                            "target_revision_id": "85616"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/field_aside_content",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/field_aside_content"
                }
            },
            "field_basin_today_thumb": {
                "data": {
                    "type": "file--file",
                    "id": "3ebb0cef-1b86-4ad2-beb9-96ac2196d50b",
                    "meta": {
                        "alt": "BE Leaders class",
                        "title": "",
                        "width": "350",
                        "height": "200"
                    }
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/field_basin_today_thumb",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/field_basin_today_thumb"
                }
            },
            "field_content": {
                "data": [
                    {
                        "type": "paragraph--basic_paragraph",
                        "id": "074c4720-fa0f-4217-8921-9c5c144c3eb3",
                        "meta": {
                            "target_revision_id": "85621"
                        }
                    },
                    {
                        "type": "paragraph--in_content_image_gallery",
                        "id": "9e8cb779-8fac-4cf1-aa45-5ade8d358e49",
                        "meta": {
                            "target_revision_id": "85626"
                        }
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/field_content",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/field_content"
                }
            },
            "field_news_class": {
                "data": {
                    "type": "taxonomy_term--news_class",
                    "id": "7945bfdd-7c7c-402f-8249-b3e493a64863"
                },
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/field_news_class",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/field_news_class"
                }
            },
            "field_news_thumb": {
                "data": null,
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/field_news_thumb",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/field_news_thumb"
                }
            },
            "field_news_top_image": {
                "data": null,
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/field_news_top_image",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/field_news_top_image"
                }
            },
            "field_publish_to_sites": {
                "data": [
                    {
                        "type": "taxonomy_term--news_shared_domains",
                        "id": "213e34f1-0b4a-4770-92f3-959fc1cf2def"
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/field_publish_to_sites",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/field_publish_to_sites"
                }
            },
            "field_related_news": {
                "data": [
                    {
                        "type": "taxonomy_term--related_news",
                        "id": "d9de2b73-cb76-4c75-a5a3-7c0c9acee756"
                    }
                ],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/field_related_news",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/field_related_news"
                }
            },
            "field_related_videos": {
                "data": null,
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/field_related_videos",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/field_related_videos"
                }
            },
            "scheduled_update": {
                "data": [],
                "links": {
                    "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/relationships/scheduled_update",
                    "related": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce/scheduled_update"
                }
            }
        },
        "links": {
            "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce"
        }
    },
    "jsonapi": {
        "version": "1.0",
        "meta": {
            "links": {
                "self": "http://jsonapi.org/format/1.0/"
            }
        }
    },
    "links": {
        "self": "http://cms.bepc.com/jsonapi/node/news_page/a846e5f5-f65a-4316-bbb6-6a5f23c1e5ce?include=field_content%2Cfield_aside_content%2Cfield_related_videos%2Cfield_related_videos.field_tags%2Cfield_aside_content.field_block%2Cfield_aside_content.field_block.field_employee_contact%2Cfield_content.field_image%2Cfield_content.field_image_aside%2Cfield_content.field_slider%2Cfield_content.field_slider.field_image%2Cfield_news_class%2Cfield_news_thumb%2Cfield_news_top_image%2Cfield_publish_to_sites%2Cfield_related_news"
    },
    "included": [
        {
            "type": "paragraph--title",
            "id": "02998ba9-9795-40d1-9c20-4fd25b93ecb2",
            "attributes": {
                "id": 14216,
                "uuid": "02998ba9-9795-40d1-9c20-4fd25b93ecb2",
                "revision_id": 85611,
                "langcode": "en",
                "status": true,
                "created": 1519767844,
                "parent_id": "6021",
                "parent_type": "node",
                "parent_field_name": "field_aside_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_title": "The 2017 participants"
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "1e5a7347-50e5-4f3a-af40-6724fa1bf928"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/title/02998ba9-9795-40d1-9c20-4fd25b93ecb2/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/title/02998ba9-9795-40d1-9c20-4fd25b93ecb2/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/title/02998ba9-9795-40d1-9c20-4fd25b93ecb2/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/title/02998ba9-9795-40d1-9c20-4fd25b93ecb2/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/title/02998ba9-9795-40d1-9c20-4fd25b93ecb2/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/title/02998ba9-9795-40d1-9c20-4fd25b93ecb2/revision_uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/title/02998ba9-9795-40d1-9c20-4fd25b93ecb2"
            }
        },
        {
            "type": "paragraph--basic_paragraph",
            "id": "63efcc1b-71ae-4fc3-8464-049699eaed9b",
            "attributes": {
                "id": 14221,
                "uuid": "63efcc1b-71ae-4fc3-8464-049699eaed9b",
                "revision_id": 85616,
                "langcode": "en",
                "status": true,
                "created": 1519767854,
                "parent_id": "6021",
                "parent_type": "node",
                "parent_field_name": "field_aside_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_body": {
                    "value": "<p><strong>Headquarters</strong><br />\r\nRyan Anderson, Brittany Bauer, Shawn Carlson, Erin Dukart, Dustin Erhardt, Jeff Hansen, Melissa Hatzenbuhler, Jolene Hoffman, Dana Jensen, Mike Kraft, Jennifer Krogstad, Ryan Lang, JP Maddock, Eric Nimmo, Shawna Piatz, Jeff Przybylski, Aaron Ramsdell, RD Reimers, Dave Roberts, Jeremy Severson, Jim Sheldon, Will Spooner, Tara Vesey, Vickie Volk, Jeremy Wittenberg, Kristi Wuitschick</p>\r\n\r\n<p><strong>DGC/LOS/AVS/TSM</strong><br />\r\nJason Batke, Travis Bauer, Brad Bergstad, Robin Braun,&nbsp;Ed Dillman, Jacob Dow, Colin Entringer, Sarah Feist, Rachel Focht, Brian Heinert, Scott Hellman, Kyle Hochalter, Dennis Horning, Eric Hyttinen, Kim Jackson, Collin Kittelson, Doug Leidholm, Troy Maas, Josh Mindt, Todd Pederson,&nbsp;Evan Pippenger, Walt Regan, Paul Remmick, Chris Roemmich, Keri Schiferl, Cody Striefel, Bill Tryon, Angela Wick</p>\r\n\r\n<p><strong>LRS/DFS/TSM</strong><br />\r\nScott Aurich, Gary Anderson, Brian Bartow, Cort Cundy, Bear Delgado, Justin Evans, Mike Goddard, Jareb Goodrich, Jay Houx, Terry Jelle, Colby Lebsack, Kori Lower, Reena McCoy, Levi Mickelsen, Ed Min, Kip Pontarolo, Jaime Riddle, Terri Seyfang, Darrel Vaughn, Mike Wilhelm, Amy Windmeier, Travis Witt</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p><strong>Headquarters</strong><br />\nRyan Anderson, Brittany Bauer, Shawn Carlson, Erin Dukart, Dustin Erhardt, Jeff Hansen, Melissa Hatzenbuhler, Jolene Hoffman, Dana Jensen, Mike Kraft, Jennifer Krogstad, Ryan Lang, JP Maddock, Eric Nimmo, Shawna Piatz, Jeff Przybylski, Aaron Ramsdell, RD Reimers, Dave Roberts, Jeremy Severson, Jim Sheldon, Will Spooner, Tara Vesey, Vickie Volk, Jeremy Wittenberg, Kristi Wuitschick</p>\n<p><strong>DGC/LOS/AVS/TSM</strong><br />\nJason Batke, Travis Bauer, Brad Bergstad, Robin Braun, Ed Dillman, Jacob Dow, Colin Entringer, Sarah Feist, Rachel Focht, Brian Heinert, Scott Hellman, Kyle Hochalter, Dennis Horning, Eric Hyttinen, Kim Jackson, Collin Kittelson, Doug Leidholm, Troy Maas, Josh Mindt, Todd Pederson, Evan Pippenger, Walt Regan, Paul Remmick, Chris Roemmich, Keri Schiferl, Cody Striefel, Bill Tryon, Angela Wick</p>\n<p><strong>LRS/DFS/TSM</strong><br />\nScott Aurich, Gary Anderson, Brian Bartow, Cort Cundy, Bear Delgado, Justin Evans, Mike Goddard, Jareb Goodrich, Jay Houx, Terry Jelle, Colby Lebsack, Kori Lower, Reena McCoy, Levi Mickelsen, Ed Min, Kip Pontarolo, Jaime Riddle, Terri Seyfang, Darrel Vaughn, Mike Wilhelm, Amy Windmeier, Travis Witt</p>\n"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/63efcc1b-71ae-4fc3-8464-049699eaed9b/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/63efcc1b-71ae-4fc3-8464-049699eaed9b/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/63efcc1b-71ae-4fc3-8464-049699eaed9b/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/63efcc1b-71ae-4fc3-8464-049699eaed9b/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/63efcc1b-71ae-4fc3-8464-049699eaed9b/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/63efcc1b-71ae-4fc3-8464-049699eaed9b/revision_uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/63efcc1b-71ae-4fc3-8464-049699eaed9b"
            }
        },
        {
            "type": "paragraph--basic_paragraph",
            "id": "074c4720-fa0f-4217-8921-9c5c144c3eb3",
            "attributes": {
                "id": 14226,
                "uuid": "074c4720-fa0f-4217-8921-9c5c144c3eb3",
                "revision_id": 85621,
                "langcode": "en",
                "status": true,
                "created": 1519767771,
                "parent_id": "6021",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null,
                "field_body": {
                    "value": "<p>For 76 Basin Electric employees, 2017 was a year dedicated to exploring various aspects of leadership through the co-op’s inaugural session&nbsp;of its BE Leaders program.</p>\r\n\r\n<p>Launched in January 2017, the 12-month program empowers Basin Electric’s employee-participants to explore their leadership potential, while working to positively influence others, and examine the <a href=\"/about-us/organization/Governance-Model\">co-op philosophy</a>.</p>\r\n\r\n<p>BE Leaders was created not only as a proactive measure about future retirement projections, but also to let employees know they have opportunities to grow personally and professionally within the cooperative.&nbsp;</p>\r\n\r\n<p>“When employees begin their career at Basin Electric, we encourage them to look at professional and personal growth opportunities,” says Kristi Pfliger-Keller, Basin Electric learning and development administrator. “Maybe it’s not a leadership position they’re interested in, but BE Leaders will help them learn about other areas in the cooperative, too.”</p>\r\n\r\n<p>Through an application process, the program is open to any Basin Electric employee interested in personal and professional development.&nbsp;</p>\r\n\r\n<p>Participants were divided into three groups, limited to 28 employees each.&nbsp;<br />\r\n• &nbsp;Employees from Headquarters<br />\r\n• &nbsp;Employees from <a href=\"https://www.dakotagas.com\">Dakota Gasification Company</a>,&nbsp;<a href=\"/Facilities/Antelope-Valley\">Antelope Valley Station</a>, <a href=\"/Facilities/Leland-Olds\">Leland Olds Station</a>, and Transmission System Maintenance (TSM)<br />\r\n• &nbsp;Employees from <a href=\"/Facilities/Laramie-River\">Laramie River Station</a>, <a href=\"/Facilities/Dry-Fork\">Dry&nbsp;Fork Station</a>, and TSM</p>\r\n\r\n<p>The program’s curriculum covers topics like leadership philosophy, StrengthsFinder, communication skills, business ethics, conflict management, change management, coaching and mentoring, and networking. It also covers accounting, finance, and budgeting.</p>\r\n\r\n<p>Participants at each location meet in person quarterly and complete monthly individual assignments. The outside classroom assignments include a variety of coursework, such as reading assignments, having conversations with supervisors/others, taking a skills assessment, and job shadowing in a different department or facility.&nbsp;</p>\r\n\r\n<p>As a way to help participants network with various managers at Basin Electric, lunch-and-learn panel discussions are incorporated throughout the year-long program, giving them access to various management at Headquarters, Dakota Gas, Antelope Valley Station, Dry Fork Station, Laramie River Station, Leland Olds Station, and TSM.</p>\r\n\r\n<p>A major component of the program was a leadership development project designed to positively impact the co-op’s employees, a process, or procedure. Working in teams of three to seven, each group identified a topic and worked on the project from scratch. The project was also an opportunity for participants to leverage and apply the knowledge and skillsets from BE Leaders.</p>\r\n\r\n<p>“BE Leaders impacts and touches so many more people than the participants and opens up more doors outside of their core group,” says Pammie Lipford, learning and development administrator.</p>\r\n\r\n<p>“Participants talked to their supervisors and others as part of their assignments. They also worked with Basin Electric’s senior staff regarding their projects. Interactions were so positive,” she says.</p>\r\n\r\n<p>The inaugural BE Leaders class graduated in December 2017, and the 2018 class started in January.</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>For 76 Basin Electric employees, 2017 was a year dedicated to exploring various aspects of leadership through the co-op’s inaugural session of its BE Leaders program.</p>\n<p>Launched in January 2017, the 12-month program empowers Basin Electric’s employee-participants to explore their leadership potential, while working to positively influence others, and examine the <a href=\"/about-us/organization/Governance-Model\">co-op philosophy</a>.</p>\n<p>BE Leaders was created not only as a proactive measure about future retirement projections, but also to let employees know they have opportunities to grow personally and professionally within the cooperative. </p>\n<p>“When employees begin their career at Basin Electric, we encourage them to look at professional and personal growth opportunities,” says Kristi Pfliger-Keller, Basin Electric learning and development administrator. “Maybe it’s not a leadership position they’re interested in, but BE Leaders will help them learn about other areas in the cooperative, too.”</p>\n<p>Through an application process, the program is open to any Basin Electric employee interested in personal and professional development. </p>\n<p>Participants were divided into three groups, limited to 28 employees each. <br />\n•  Employees from Headquarters<br />\n•  Employees from <a href=\"https://www.dakotagas.com\">Dakota Gasification Company</a>, <a href=\"/Facilities/Antelope-Valley\">Antelope Valley Station</a>, <a href=\"/Facilities/Leland-Olds\">Leland Olds Station</a>, and Transmission System Maintenance (TSM)<br />\n•  Employees from <a href=\"/Facilities/Laramie-River\">Laramie River Station</a>, <a href=\"/Facilities/Dry-Fork\">Dry Fork Station</a>, and TSM</p>\n<p>The program’s curriculum covers topics like leadership philosophy, StrengthsFinder, communication skills, business ethics, conflict management, change management, coaching and mentoring, and networking. It also covers accounting, finance, and budgeting.</p>\n<p>Participants at each location meet in person quarterly and complete monthly individual assignments. The outside classroom assignments include a variety of coursework, such as reading assignments, having conversations with supervisors/others, taking a skills assessment, and job shadowing in a different department or facility. </p>\n<p>As a way to help participants network with various managers at Basin Electric, lunch-and-learn panel discussions are incorporated throughout the year-long program, giving them access to various management at Headquarters, Dakota Gas, Antelope Valley Station, Dry Fork Station, Laramie River Station, Leland Olds Station, and TSM.</p>\n<p>A major component of the program was a leadership development project designed to positively impact the co-op’s employees, a process, or procedure. Working in teams of three to seven, each group identified a topic and worked on the project from scratch. The project was also an opportunity for participants to leverage and apply the knowledge and skillsets from BE Leaders.</p>\n<p>“BE Leaders impacts and touches so many more people than the participants and opens up more doors outside of their core group,” says Pammie Lipford, learning and development administrator.</p>\n<p>“Participants talked to their supervisors and others as part of their assignments. They also worked with Basin Electric’s senior staff regarding their projects. Interactions were so positive,” she says.</p>\n<p>The inaugural BE Leaders class graduated in December 2017, and the 2018 class started in January.</p>\n"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "3ccb0f25-4d7d-4e61-be87-8bf232034654"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/074c4720-fa0f-4217-8921-9c5c144c3eb3/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/074c4720-fa0f-4217-8921-9c5c144c3eb3/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/074c4720-fa0f-4217-8921-9c5c144c3eb3/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/074c4720-fa0f-4217-8921-9c5c144c3eb3/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "4d109916-a4c9-4b60-b399-f64e5e6da968"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/074c4720-fa0f-4217-8921-9c5c144c3eb3/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/074c4720-fa0f-4217-8921-9c5c144c3eb3/revision_uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/basic_paragraph/074c4720-fa0f-4217-8921-9c5c144c3eb3"
            }
        },
        {
            "type": "paragraph--in_content_image_gallery",
            "id": "9e8cb779-8fac-4cf1-aa45-5ade8d358e49",
            "attributes": {
                "id": 15721,
                "uuid": "9e8cb779-8fac-4cf1-aa45-5ade8d358e49",
                "revision_id": 85626,
                "langcode": "en",
                "status": true,
                "created": 1529615259,
                "parent_id": "6021",
                "parent_type": "node",
                "parent_field_name": "field_content",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": null
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "ca09331d-0b58-4e2e-b75e-331813d4ab19"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/revision_uid"
                    }
                },
                "field_image_gallery_type": {
                    "data": {
                        "type": "taxonomy_term--image_gallery_types",
                        "id": "b4a047c2-0397-4c6c-a8e7-2318b7eb41cc"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/relationships/field_image_gallery_type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/field_image_gallery_type"
                    }
                },
                "field_slider": {
                    "data": [
                        {
                            "type": "paragraph--slider_for_in_context_image_gall",
                            "id": "061e0289-814c-4796-8941-26085cf3d736",
                            "meta": {
                                "target_revision_id": "85591"
                            }
                        },
                        {
                            "type": "paragraph--slider_for_in_context_image_gall",
                            "id": "c28be2fd-3c40-46da-9ba9-f39a1b3a47ba",
                            "meta": {
                                "target_revision_id": "85596"
                            }
                        },
                        {
                            "type": "paragraph--slider_for_in_context_image_gall",
                            "id": "db6c2097-ba9d-4d5a-b184-68abdd88a331",
                            "meta": {
                                "target_revision_id": "85601"
                            }
                        }
                    ],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/relationships/field_slider",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49/field_slider"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/in_content_image_gallery/9e8cb779-8fac-4cf1-aa45-5ade8d358e49"
            }
        },
        {
            "type": "taxonomy_term--news_class",
            "id": "7945bfdd-7c7c-402f-8249-b3e493a64863",
            "attributes": {
                "tid": 356,
                "uuid": "7945bfdd-7c7c-402f-8249-b3e493a64863",
                "langcode": "en",
                "name": "Basin Today",
                "description": {
                    "value": "<p>we</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>we</p>\n"
                },
                "weight": 0,
                "changed": 1507131760,
                "default_langcode": true,
                "metatag": null,
                "path": {
                    "alias": "/news-center/news-articles/basin-today",
                    "pid": 3336,
                    "langcode": "en"
                },
                "field_article_url_alias": "/news-center/publications/basin-today/"
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "937fb620-a348-4ec3-8665-c82ba7c83369"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/news_class/7945bfdd-7c7c-402f-8249-b3e493a64863/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/news_class/7945bfdd-7c7c-402f-8249-b3e493a64863/vid"
                    }
                },
                "parent": {
                    "data": [],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/news_class/7945bfdd-7c7c-402f-8249-b3e493a64863/relationships/parent",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/news_class/7945bfdd-7c7c-402f-8249-b3e493a64863/parent"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/news_class/7945bfdd-7c7c-402f-8249-b3e493a64863"
            }
        },
        {
            "type": "taxonomy_term--news_shared_domains",
            "id": "213e34f1-0b4a-4770-92f3-959fc1cf2def",
            "attributes": {
                "tid": 336,
                "uuid": "213e34f1-0b4a-4770-92f3-959fc1cf2def",
                "langcode": "en",
                "name": "Basin Public",
                "description": null,
                "weight": 0,
                "changed": 1500477572,
                "default_langcode": true,
                "metatag": null,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "09588dc1-475f-41f2-8d54-0febe36a1b6c"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/news_shared_domains/213e34f1-0b4a-4770-92f3-959fc1cf2def/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/news_shared_domains/213e34f1-0b4a-4770-92f3-959fc1cf2def/vid"
                    }
                },
                "parent": {
                    "data": [],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/news_shared_domains/213e34f1-0b4a-4770-92f3-959fc1cf2def/relationships/parent",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/news_shared_domains/213e34f1-0b4a-4770-92f3-959fc1cf2def/parent"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/news_shared_domains/213e34f1-0b4a-4770-92f3-959fc1cf2def"
            }
        },
        {
            "type": "taxonomy_term--related_news",
            "id": "d9de2b73-cb76-4c75-a5a3-7c0c9acee756",
            "attributes": {
                "tid": 391,
                "uuid": "d9de2b73-cb76-4c75-a5a3-7c0c9acee756",
                "langcode": "en",
                "name": "employee",
                "description": null,
                "weight": 0,
                "changed": 1501180091,
                "default_langcode": true,
                "metatag": null,
                "path": null
            },
            "relationships": {
                "vid": {
                    "data": {
                        "type": "taxonomy_vocabulary--taxonomy_vocabulary",
                        "id": "87a34d54-fa60-43a4-b3b8-8d1650034040"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/related_news/d9de2b73-cb76-4c75-a5a3-7c0c9acee756/relationships/vid",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/related_news/d9de2b73-cb76-4c75-a5a3-7c0c9acee756/vid"
                    }
                },
                "parent": {
                    "data": [],
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/taxonomy_term/related_news/d9de2b73-cb76-4c75-a5a3-7c0c9acee756/relationships/parent",
                        "related": "http://cms.bepc.com/jsonapi/taxonomy_term/related_news/d9de2b73-cb76-4c75-a5a3-7c0c9acee756/parent"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/taxonomy_term/related_news/d9de2b73-cb76-4c75-a5a3-7c0c9acee756"
            }
        },
        {
            "type": "paragraph--slider_for_in_context_image_gall",
            "id": "061e0289-814c-4796-8941-26085cf3d736",
            "attributes": {
                "id": 15706,
                "uuid": "061e0289-814c-4796-8941-26085cf3d736",
                "revision_id": 85591,
                "langcode": "en",
                "status": true,
                "created": 1529615259,
                "parent_id": "15721",
                "parent_type": "paragraph",
                "parent_field_name": "field_slider",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_caption": {
                    "value": "<p>Basin employees discussing leadership.</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Basin employees discussing leadership.</p>\n"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "e2e61b69-c044-4b23-9124-3b797879490b"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/061e0289-814c-4796-8941-26085cf3d736/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/061e0289-814c-4796-8941-26085cf3d736/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/061e0289-814c-4796-8941-26085cf3d736/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/061e0289-814c-4796-8941-26085cf3d736/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/061e0289-814c-4796-8941-26085cf3d736/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/061e0289-814c-4796-8941-26085cf3d736/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "8244c55a-ad17-4971-83a8-a43301ce9c33",
                        "meta": {
                            "alt": "Basin employees discussing leadership.",
                            "title": "",
                            "width": "800",
                            "height": "457"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/061e0289-814c-4796-8941-26085cf3d736/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/061e0289-814c-4796-8941-26085cf3d736/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/061e0289-814c-4796-8941-26085cf3d736"
            }
        },
        {
            "type": "paragraph--slider_for_in_context_image_gall",
            "id": "c28be2fd-3c40-46da-9ba9-f39a1b3a47ba",
            "attributes": {
                "id": 15711,
                "uuid": "c28be2fd-3c40-46da-9ba9-f39a1b3a47ba",
                "revision_id": 85596,
                "langcode": "en",
                "status": true,
                "created": 1529615377,
                "parent_id": "15721",
                "parent_type": "paragraph",
                "parent_field_name": "field_slider",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_caption": {
                    "value": "<p>Basin employees learning leadership.</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Basin employees learning leadership.</p>\n"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "e2e61b69-c044-4b23-9124-3b797879490b"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/c28be2fd-3c40-46da-9ba9-f39a1b3a47ba/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/c28be2fd-3c40-46da-9ba9-f39a1b3a47ba/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/c28be2fd-3c40-46da-9ba9-f39a1b3a47ba/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/c28be2fd-3c40-46da-9ba9-f39a1b3a47ba/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/c28be2fd-3c40-46da-9ba9-f39a1b3a47ba/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/c28be2fd-3c40-46da-9ba9-f39a1b3a47ba/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "9aed9179-9046-41a1-b005-0450a3edea47",
                        "meta": {
                            "alt": "Basin employees discussing leadership.",
                            "title": "",
                            "width": "800",
                            "height": "457"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/c28be2fd-3c40-46da-9ba9-f39a1b3a47ba/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/c28be2fd-3c40-46da-9ba9-f39a1b3a47ba/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/c28be2fd-3c40-46da-9ba9-f39a1b3a47ba"
            }
        },
        {
            "type": "paragraph--slider_for_in_context_image_gall",
            "id": "db6c2097-ba9d-4d5a-b184-68abdd88a331",
            "attributes": {
                "id": 15716,
                "uuid": "db6c2097-ba9d-4d5a-b184-68abdd88a331",
                "revision_id": 85601,
                "langcode": "en",
                "status": true,
                "created": 1529615466,
                "parent_id": "15721",
                "parent_type": "paragraph",
                "parent_field_name": "field_slider",
                "behavior_settings": "a:0:{}",
                "default_langcode": true,
                "revision_translation_affected": true,
                "field_caption": {
                    "value": "<p>Be Leadership program at Basin Electric.</p>\r\n",
                    "format": "rich_text",
                    "processed": "<p>Be Leadership program at Basin Electric.</p>\n"
                }
            },
            "relationships": {
                "type": {
                    "data": {
                        "type": "paragraphs_type--paragraphs_type",
                        "id": "e2e61b69-c044-4b23-9124-3b797879490b"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/db6c2097-ba9d-4d5a-b184-68abdd88a331/relationships/type",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/db6c2097-ba9d-4d5a-b184-68abdd88a331/type"
                    }
                },
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/db6c2097-ba9d-4d5a-b184-68abdd88a331/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/db6c2097-ba9d-4d5a-b184-68abdd88a331/uid"
                    }
                },
                "revision_uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/db6c2097-ba9d-4d5a-b184-68abdd88a331/relationships/revision_uid",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/db6c2097-ba9d-4d5a-b184-68abdd88a331/revision_uid"
                    }
                },
                "field_image": {
                    "data": {
                        "type": "file--file",
                        "id": "44f4b559-761d-4dd2-9fcd-3f8ef4433038",
                        "meta": {
                            "alt": "Basin employees discussing leadership.",
                            "title": "",
                            "width": "800",
                            "height": "457"
                        }
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/db6c2097-ba9d-4d5a-b184-68abdd88a331/relationships/field_image",
                        "related": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/db6c2097-ba9d-4d5a-b184-68abdd88a331/field_image"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/paragraph/slider_for_in_context_image_gall/db6c2097-ba9d-4d5a-b184-68abdd88a331"
            }
        },
        {
            "type": "file--file",
            "id": "8244c55a-ad17-4971-83a8-a43301ce9c33",
            "attributes": {
                "fid": 10376,
                "uuid": "8244c55a-ad17-4971-83a8-a43301ce9c33",
                "langcode": "en",
                "filename": "HDQ-BE-Leaders-8.jpg",
                "uri": {
                    "value": "public://images/HDQ-BE-Leaders-8_0.jpg",
                    "url": "/sites/CMS/files/images/HDQ-BE-Leaders-8_0.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 130862,
                "status": true,
                "created": 1531162297,
                "changed": 1531162352,
                "url": "/sites/CMS/files/images/HDQ-BE-Leaders-8_0.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/8244c55a-ad17-4971-83a8-a43301ce9c33/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/8244c55a-ad17-4971-83a8-a43301ce9c33/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/8244c55a-ad17-4971-83a8-a43301ce9c33"
            }
        },
        {
            "type": "file--file",
            "id": "9aed9179-9046-41a1-b005-0450a3edea47",
            "attributes": {
                "fid": 10381,
                "uuid": "9aed9179-9046-41a1-b005-0450a3edea47",
                "langcode": "en",
                "filename": "HDQ-BE-Leaders-14.jpg",
                "uri": {
                    "value": "public://images/HDQ-BE-Leaders-14_0.jpg",
                    "url": "/sites/CMS/files/images/HDQ-BE-Leaders-14_0.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 103327,
                "status": true,
                "created": 1531162304,
                "changed": 1531162352,
                "url": "/sites/CMS/files/images/HDQ-BE-Leaders-14_0.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/9aed9179-9046-41a1-b005-0450a3edea47/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/9aed9179-9046-41a1-b005-0450a3edea47/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/9aed9179-9046-41a1-b005-0450a3edea47"
            }
        },
        {
            "type": "file--file",
            "id": "44f4b559-761d-4dd2-9fcd-3f8ef4433038",
            "attributes": {
                "fid": 10386,
                "uuid": "44f4b559-761d-4dd2-9fcd-3f8ef4433038",
                "langcode": "en",
                "filename": "HDQ-BE-Leaders-77.jpg",
                "uri": {
                    "value": "public://images/HDQ-BE-Leaders-77_0.jpg",
                    "url": "/sites/CMS/files/images/HDQ-BE-Leaders-77_0.jpg"
                },
                "filemime": "image/jpeg",
                "filesize": 75903,
                "status": true,
                "created": 1531162309,
                "changed": 1531162352,
                "url": "/sites/CMS/files/images/HDQ-BE-Leaders-77_0.jpg"
            },
            "relationships": {
                "uid": {
                    "data": {
                        "type": "user--user",
                        "id": "cf5604bd-3199-4388-b143-9aa839fb6e10"
                    },
                    "links": {
                        "self": "http://cms.bepc.com/jsonapi/file/file/44f4b559-761d-4dd2-9fcd-3f8ef4433038/relationships/uid",
                        "related": "http://cms.bepc.com/jsonapi/file/file/44f4b559-761d-4dd2-9fcd-3f8ef4433038/uid"
                    }
                }
            },
            "links": {
                "self": "http://cms.bepc.com/jsonapi/file/file/44f4b559-761d-4dd2-9fcd-3f8ef4433038"
            }
        }
    ]
}