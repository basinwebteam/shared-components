import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import ArticlePhotoGalleryDecorators from "./articlePhotoGalleryDecorators"
import * as Carousel from "nuka-carousel"

import { SliderGalleryContainer } from "../SliderGallery"
import { pageData, articleSliderPageDataHorizontal } from "./__tests__/mocks"

const description = `Displays a Data View from Drupal.`
const siteDomain = '//cms.bepc.com'
import "./style.scss"


storiesOf("Slider Gallery", module)
    .add("Home Page News Slider",
        withInfo(`${description} a slider for images & text.`)(() => {
            return (
                <div style={{ maxWidth: "800px", position: "relative" }}>
                    <SliderGalleryContainer
                        paragraphId="ed3fce20-a1dc-4bf7-8f74-c732335f932e"
                        includedRelationships={pageData.included}
                        siteDomain={siteDomain}
                        carousel={Carousel as React.ComponentClass<any>}
                    />
                </div>
            )
        })
    )
    .add("Vertical Article Photo Gallery Slider",
        withInfo(`${description} a slider for images & text.`)(() => {
            return (
                <div style={{ maxWidth: "800px", position: "relative" }}>
                    <div>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti provident voluptatum ullam consequuntur quasi tempora alias illo.
                        Minima optio aspernatur, amet sapiente minus, odit ipsam molestias, temporibus quibusdam possimus aliquam.</p>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            Id culpa, quia nemo, enim explicabo harum accusantium cupiditate doloribus adipisci distinctio perspiciatis aperiam autem fuga maxime sequi officia laboriosam illum animi?
                        </p>
                    </div>
                    <div>
                        <SliderGalleryContainer
                            paragraphId="9e8cb779-8fac-4cf1-aa45-5ade8d358e49"
                            includedRelationships={articleSliderPageDataHorizontal.included}
                            siteDomain={siteDomain}
                            carousel={Carousel as React.ComponentClass<any>}
                            autoplay={false}
                        />
                    </div>
                    <div>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.

                            Magnam sequi quam sunt dolore illum saepe itaque provident, molestiae deserunt eaque at voluptatum in ratione, animi nobis! Tempore sequi corrupti dolorum?

                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus esse rem quos facere quidem consectetur perferendis, illum iusto fuga nihil!

                            A eum recusandae fuga consequatur omnis exercitationem laboriosam corrupti itaque?
                        </p>
                    </div>
                </div>
            )
        })
    )
    .add("Horizontal Article Photo Gallery Slider",
        withInfo(`${description} a slider for images & text.`)(() => {
            return (
                <div style={{ maxWidth: "800px", position: "relative" }}>
                    <div>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti provident voluptatum ullam consequuntur quasi tempora alias illo.
                        Minima optio aspernatur, amet sapiente minus, odit ipsam molestias, temporibus quibusdam possimus aliquam.</p>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            Id culpa, quia nemo, enim explicabo harum accusantium cupiditate doloribus adipisci distinctio perspiciatis aperiam autem fuga maxime sequi officia laboriosam illum animi?
                        </p>
                    </div>
                    <div>
                        <SliderGalleryContainer
                            paragraphId="9e8cb779-8fac-4cf1-aa45-5ade8d358e49"
                            includedRelationships={articleSliderPageDataHorizontal.included}
                            siteDomain={siteDomain}
                            carousel={Carousel as React.ComponentClass<any>}
                            autoplay={false}
                        />
                    </div>
                    <div>
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.

                            Magnam sequi quam sunt dolore illum saepe itaque provident, molestiae deserunt eaque at voluptatum in ratione, animi nobis! Tempore sequi corrupti dolorum?

                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus esse rem quos facere quidem consectetur perferendis, illum iusto fuga nihil!

                            A eum recusandae fuga consequatur omnis exercitationem laboriosam corrupti itaque?
                        </p>
                    </div>
                </div>
            )
        })
    )
