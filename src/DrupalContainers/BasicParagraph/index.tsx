import { AxiosResponse, default as axios } from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"

export class BasicParagraph extends React.Component<IBasicParagraphProps, IBasicParagraphState> {
    constructor(props: IBasicParagraphProps) {
        super(props)
        this.state = this.getDefaultState()
    }

    public render() {
        return (
            <div dangerouslySetInnerHTML={{ __html: this.state.html }} />
        )
    }

    public componentDidMount() {
        this.getData()
    }

    public componentDidUpdate(prevProps: IBasicParagraphProps, prevState: IBasicParagraphState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.getData()
        }
    }

    public async getData() {
        if (typeof this.props.data === "undefined") {
            this.setState({
                html: await this.queryData(),
            } as IBasicParagraphState)
        } else {
            this.setState({
                html: this.props.data.attributes.field_body.value,
            } as IBasicParagraphState)
        }
    }

    public async queryData() {
        const response: IAxiosBasicParagraphResponse =
            await axios.get("//api.bepc.com/api/cms/json/paragraph/basic_paragraph/" + this.props.paragraphId)

        return response.data.data.attributes.field_body.value
    }

    public getDefaultState() {
        /* This tries to get the block and related child data if its part of the props blockData value.

        If any of it is missing, this returns an empty state. */
        try {
            const html = this.props.data ? this.props.data.attributes.field_body.value : ""
            return {
                html,
            } as IBasicParagraphState
        } catch (e) {
            return {
                html: "",
            } as IBasicParagraphState
        }
    }

}

export default BasicParagraph

export interface IBasicParagraphProps {
    paragraphId: string,
    data: IBasicParagraphPageData | undefined,
    siteDomain: string,
}

export interface IBasicParagraphState {
    html: string
}

export interface IAxiosBasicParagraphResponse extends AxiosResponse {
    data: IBasicParagraphData
}

export interface IBasicParagraphData extends Page.IType {
    data: IBasicParagraphPageData
}

export interface IBasicParagraphPageData extends Page.IPageData {
    attributes: IBasicParagraphAttributes,
}

export interface IBasicParagraphAttributes extends Page.IAttributes {
    field_body: IFieldBody,
}

export interface IFieldBody {
    value: string
}
