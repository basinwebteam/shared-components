import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import VimeoVideo from "../../components/Vimeo"

export const Vimeo: React.StatelessComponent<IVimeoProps> = (props) => (
    <VimeoVideo link={getVideoUrl(props)} />
)

const getVideoUrl = (props: IVimeoProps) => {
    const data = props.includedRelationships.find((item) => item.id === props.paragraphId) as IVimeoData
    return data.attributes.field_vimeo_video
}

export default Vimeo

export interface IVimeoProps {
    includedRelationships: Page.IPageData[],
    paragraphId: string,
}

export interface IVimeoData extends Page.IPageData {
    attributes: IVimeoAttributes,
}

export interface IVimeoAttributes extends Page.IAttributes {
    field_vimeo_video: string,
}
