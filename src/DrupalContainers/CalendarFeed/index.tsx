import {AxiosResponse, default as axios} from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import {CalendarFeed as Calendar} from "../../components/CalendarFeed"
import {IEventProps} from "../../components/CalendarFeed/Event"

export class CalendarContainer extends React.Component<ICalendarFeedProps, ICalendarFeedState> {
    constructor(props: ICalendarFeedProps) {
        super(props)
        this.state = {
            events: [],
        }
    }

    public render() {
        return (
            <Calendar events={this.state.events} />
        )
    }

    public componentDidMount() {
        this.updateComponent()
    }

    public componentDidUpdate(prevProps: ICalendarFeedProps, prevState: ICalendarFeedState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateComponent()
        }
    }

    public async updateComponent() {
        const feedResponse = await axios.get(this.getFeedUrl()) as IResponse
        const events = feedResponse.data.d.results.map((event) => {
            return {
                id: event.Id.toString(),
                title: event.Title,
                location: event.Location,
                date: event.EventDate,
                description: event.Description.trim(),
            } as IEventProps
        })
        this.setState({
            events,
        })
    }

    private getFeedUrl() {
        const paragraphData = this.props.includedRelationships.find((paragraph) => paragraph.id === this.props.paragraphId) as ICalendarParagraphData
        const taxonomyData = this.props.includedRelationships.find((paragraph) => paragraph.id === paragraphData.relationships.field_calendar.data.id) as ICalendarTaxonomy
        return taxonomyData.attributes.field_url
    }

}

export default CalendarContainer

export interface ICalendarFeedProps {
    paragraphId: string,
    includedRelationships: Page.IPageData[],
}

export interface ICalendarFeedState {
    events: IEventProps[],
}

interface IEventFeedEvent {
    Id: number,
    Title: string,
    Location: string,
    EventDate: string,
    EndDate: string,
    Description: string,
    fAllDayEvent: boolean,
    fRecurrence: boolean,
    Category: string,
    Contact_x0020_for_x0020_more_x00: string,
}

interface IResponse extends AxiosResponse {
    data: {
        d: {
            results: IEventFeedEvent[]
        }
    }
}

interface ICalendarParagraphData extends Page.IPageData {
    relationships: ICalendarParagraphRelationships
}

interface ICalendarParagraphRelationships extends Page.IRelationships {
    field_calendar: Page.IType
}

interface ICalendarTaxonomy extends Page.ITaxonomyData {
    attributes: ICalendarTaxonomyAttributes
}

interface ICalendarTaxonomyAttributes extends Page.ITaxonomyAttributes {
    field_url: string
}
