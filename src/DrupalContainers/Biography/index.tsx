import { AxiosResponse, default as axios } from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import { IImageFileData, ImageContainer as Image } from "../Image"
import Biography from "../../components/Biography"

export class BiographyContainer extends React.Component<IPropsBiography, IStateBiography> {
    constructor(props: IPropsBiography) {
        super(props)
        this.state = this.getDefaultState()
    }

    public render() {
        return (
            <Biography
                firstName={this.state.firstName}
                lastName={this.state.lastName}
                title={this.state.employeeTitle}
                biographyDescription={this.getBiography()}
                emailAddress={this.state.employeeEmail}
                phoneNumber={this.getPhoneNumbers()}
            >
                {(this.state.showImage) ? this.getImage() : ""}
            </Biography>
        )
    }

    public componentDidMount() {
        this.getBiographyData()
    }

    public componentDidUpdate(prevProps: IPropsBiography, prevState: IStateBiography) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.getBiographyData()
        }
    }

    private getBiography() {
        if (this.state.biography !== null && this.state.biography.length > 0) {
            return this.state.biography
        }
        return ""
    }

    private getImage() {
        if (this.state.showImage) {
            return (
                <figure>
                    <Image
                        paragraphId={this.state.contactId}
                        includedRelationships={this.state.includedImageData}
                        siteDomain={this.props.siteDomain}
                    />
                </figure>
            )
        }
        return ""
    }

    private async getBiographyData() {
        let paragraph = this.findParagraphData()
        let queriedData = new Array<IImageFileData | IEmployeeContactData>()
        if (typeof paragraph === "undefined") {
            const paragraphResponse = await this.queryParagraph()
            paragraph = paragraphResponse.data
            queriedData = paragraphResponse.included as IImageFileData[]
        }
        let biography = this.findBiographyData(paragraph)
        if (typeof biography === "undefined" ||
            typeof biography.relationships.field_image === "undefined") {
            const biographyResponse = await this.queryBiography(paragraph.relationships.field_biography_reference.data.id)
            biography = biographyResponse.data
            queriedData = biographyResponse.included
            queriedData.push(biographyResponse.data)
        }
        this.setState({
            employeeTitle: biography.attributes.field_employee_title,
            firstName: biography.attributes.field_first_name,
            lastName: biography.attributes.field_last_name,
            employeeEmail: biography.attributes.field_employee_e_mail,
            employeePhoneNumber: biography.attributes.field_employee_phone_number,
            biography: (biography.attributes.field_biography !== null) ? biography.attributes.field_biography.value : "",
            contactId: biography.id,
            includedImageData: (queriedData.length === 0) ? this.props.includedRelationships : queriedData,
            showImage: (biography.relationships.field_image.data !== null) ? true : false,
        })
    }

    private async queryParagraph() {
        const response = await axios.get
            (`//api.bepc.com/api/cms/json/paragraph/biography/${this.props.paragraphId}?include=field_biography_reference,field_biography_reference.field_image`) as IAxiosResponseBioParagraph
        return response.data
    }

    private async queryBiography(id: string) {
        const response = await axios.get(`//api.bepc.com/api/cms/json/node/basin_contacts/${id}?include=field_image`) as IAxiosResponseContactInfo
        return response.data
    }

    private findParagraphData() {
        return this.props.includedRelationships.find((includedData) =>
            includedData.id === this.props.paragraphId,
        ) as IBioParagraphData
    }

    private findBiographyData(paragraphData: IBioParagraphData) {
        return this.props.includedRelationships.find((includedData) =>
            includedData.id === paragraphData.relationships.field_biography_reference.data.id,
        ) as IEmployeeContactData
    }

    private getPhoneNumbers() {
        if (this.state.employeePhoneNumber !== null && this.state.employeePhoneNumber.length > 0) {
            return this.state.employeePhoneNumber.join(" or ")
        }
        return ""
    }

    private getDefaultState() {
        /*
            This tries to get the biography paragraph data and related contact data.
            If any of it is missing, this returns an empty state.
        */
        try {
            const paragraphData = this.findParagraphData()
            const bio = this.findBiographyData(paragraphData)
            if (typeof bio.relationships.field_image === "undefined") {
                throw Error("Field Image data missing")
            }
            return {
                title: bio.attributes.title,
                employeeTitle: bio.attributes.field_employee_title,
                firstName: bio.attributes.field_first_name,
                lastName: bio.attributes.field_last_name,
                employeeEmail: bio.attributes.field_employee_e_mail,
                employeePhoneNumber: bio.attributes.field_employee_phone_number,
                biography: bio.attributes.field_biography.value,
                contactId: bio.id,
                includedImageData: this.props.includedRelationships,
                showImage: bio.relationships.field_image.data !== null ? true : false,
            } as IStateBiography
        } catch (e) {
            return {
                title: "",
                employeeTitle: "",
                firstName: "",
                lastName: "",
                employeeEmail: "",
                employeePhoneNumber: [],
                biography: "",
                contactId: "",
                includedImageData: this.props.includedRelationships,
                showImage: false,
            } as IStateBiography
        }
    }
}

export default BiographyContainer

export interface IPropsBiography {
    paragraphId: string,
    includedRelationships: Page.IPageData[],
    siteDomain: string,
}

export interface IStateBiography {
    title: string
    employeeTitle: string,
    firstName: string,
    lastName: string,
    employeeEmail: string,
    employeePhoneNumber: Array<string | undefined>,
    biography: string,
    includedImageData: Array<Page.IPageData | IImageFileData>,
    contactId: string,
    showImage: boolean,
}

interface IBioParagraphData extends Page.IPageData {
    relationships: IBioParagraphRelationships,
}

interface IBioParagraphRelationships extends Page.IRelationships {
    field_biography_reference: Page.IType,
}

interface IAxiosResponseBioParagraph extends AxiosResponse {
    data: IAxiosResponseBioParagraphData,
}

interface IAxiosResponseBioParagraphData extends Page.IPage {
    data: IBioParagraphData,
}

interface IAxiosResponseContactInfo extends AxiosResponse {
    data: IAxiosResponseContactInfoData,
}

interface IAxiosResponseContactInfoData extends Page.IPage {
    data: IEmployeeContactData,
    included: IImageFileData[]
}

interface IEmployeeContactData extends Page.IPageData {
    attributes: IEmployeeContactAttribute,
    relationships: IEmployeeContactRelationships
}

interface IEmployeeContactAttribute extends Page.IAttributes {
    field_biography: IBiographyObject,
    field_employee_e_mail: string,
    field_first_name: string,
    field_last_name: string,
    field_employee_phone_number: string[],
    field_employee_title: string,
}

interface IBiographyObject {
    value: string
}

interface IEmployeeContactRelationships extends Page.IRelationships {
    field_image: Page.IField_Image
}
