import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import {getUrl} from "../../DrupalAPI/sitemap"
import Button from "../../components/Button"

export class ButtonContainer extends React.Component<ButtonContainerProps, ButtonContainerState>{
    constructor(props: ButtonContainerProps) {
        super(props)
        this.state = {
            url: "",
            title: "",
        }
    }

    displayName = "ButtonContainer"

    async componentDidMount() {
        const {paragraphId, includedRelationships, siteDomain} = this.props
        const {url, title} = await this.getButtonData(paragraphId, includedRelationships, siteDomain)
        this.setState({
            url,
            title
        })
    }

    render() {
        const {url, title} = this.state
        return (
            <Button url={url} title={title} />
        )
    }

    async getButtonData(paragraphId: string, includedRelationships: Page.IPageData[], siteDomain: string) {
        const buttonParagraph = includedRelationships.find((items) => items.id === paragraphId) as ButtonRelationships
        if(typeof buttonParagraph !== "undefined"){
            const link = buttonParagraph.attributes.field_website_link
            return {
                url: await getUrl(link.uri, siteDomain),
                title: link.title
            }
        }
        return {
            url: "",
            title: ""
        }
    }
} 


export default ButtonContainer

export interface ButtonContainerState {
    title: string,
    url: string,
}

export interface ButtonContainerProps {
    paragraphId: string,
    includedRelationships: Page.IPageData[],
    siteDomain: string
}

export interface ButtonRelationships extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_website_link: Page.ILinkField
    }
}
