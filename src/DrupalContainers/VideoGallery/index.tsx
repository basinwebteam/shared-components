import { AxiosResponse, AxiosPromise, default as axios } from "axios"
import * as React from "react"
import * as Page from "../../DrupalContentTypes/PageInterface"
import { getVideoId, YouTubeVideo } from "../../components/YouTube"
import { IVideoPlaylistProps as IPlaylistProps, IVideo, Playlist } from "../../components/VideoGallery/Playlist"
import { IPlaylist, VideoTOC } from "../../components/VideoGallery/TableOfContents"

export class VideoGallery extends React.Component<IVideoGalleryProps, IVideoGalleryState> {
    constructor(props: IVideoGalleryProps) {
        super(props)
        this.state = {
            currentVideoId: "",
            autoplay: false,
            playlists: [],
        }
        this.playVideoClickHandler = this.playVideoClickHandler.bind(this)
    }

    public render() {
        return (
            <section className="videoGallery">
                <div className="row">
                    <div className="videoPlayerWrapper" id="videoPlayer">
                        <YouTubeVideo autoplay={this.state.autoplay} link={this.state.currentVideoId} />
                    </div>
                    <VideoTOC
                        playlists={this.state.playlists.map((playlist, id) => {
                            return {
                                id: id.toString(),
                                title: playlist.title,
                            } as IPlaylist
                        })}
                    />
                </div>
                <div className="row">
                    <div className="vid-list column">
                        <div className="videoPlaylist">
                            {this.state.playlists
                                .map((playlist, i) => <Playlist key={i} {...playlist} instance={i} />)
                            }
                        </div>
                    </div>
                </div>
            </section>
        )
    }

    public componentDidMount() {
        this.updateComponent()
    }

    public componentDidUpdate(prevProps: IVideoGalleryProps, prevState: IVideoGalleryState) {
        if (this.props.paragraphId !== prevProps.paragraphId) {
            this.updateComponent(true)
        }
    }

    public async updateComponent(autoplay: boolean = false) {
        const playlists = await this.getVideoPlaylists()
        const defaultVideo = (this.props.includedRelationships.find((paragraph) => paragraph.id === this.props.paragraphId) as IVideoGallery).attributes.field_default_video

        this.setState({
            autoplay,
            currentVideoId: defaultVideo,
            playlists,
        })
    }

    public playVideoClickHandler(videoId: string) {
        this.setState({
            autoplay: true,
            currentVideoId: videoId,
        })
    }

    private async getVideoPlaylists() {
        const videoGalleriesData = this.getVideoGalleriesData(this.getVideoGalleryParagraph())
        const playlists = Promise.all(videoGalleriesData.map(async (gallery) => {
            let videos = [] as IVideo[]
            switch (gallery.relationships.field_video_gallery_videos.data.type) {
                case ("paragraph--related_video_playlist"):
                    videos = await this.getVideosFromYouTubePlaylist(gallery.relationships.field_video_gallery_videos.data.id)
                    break
                case ("paragraph--related_video_tags"):
                    videos = await this.getVideosFromYouTubeTags(gallery.relationships.field_video_gallery_videos.data.id)
                    break
                case ("paragraph--video_youtube_url_collection"):
                    videos = await this.getVideosFromYoutubeUrlCollection(gallery.relationships.field_video_gallery_videos.data.id)
                    break
            }
            return {
                title: gallery.attributes.field_title,
                videos,
                playVideoClickHandler: this.playVideoClickHandler,
                showMorePlaylistClickHandler: () => { console.log("test 1") },
            } as IPlaylistProps
        }))
        return playlists
    }

    private getVideoGalleryParagraph() {
        return this.props.includedRelationships.find((paragraph) => paragraph.id === this.props.paragraphId) as IVideoGallery
    }

    private getVideoGalleriesData(videoGalleryParagraph: IVideoGallery) {
        return videoGalleryParagraph.relationships.field_gallery_playlist_videos.data
            .map((data) => this.props.includedRelationships
                .find((includedItem) => includedItem.id === data.id)) as IVideoGalleryItem[]
    }

    private async getVideosFromYouTubePlaylist(paragraphId: string) {
        const playlistParagraph = this.props.includedRelationships.find((paragraph) => paragraph.id === paragraphId) as IRelatedVideoPlaylistParagraph
        const youtubeVideos = await axios.get("//api.bepc.com/api/youtube/videosByPlaylist/" + playlistParagraph.attributes.field_video_playlist_id) as IYouTubePlaylistResponse

        return youtubeVideos.data.map((video, i) => {
            return {
                id: video.id + i + paragraphId,
                title: video.title,
                url: video.id,
                thumb: video.thumbnailStandard,
            } as IVideo
        })
    }

    private async getVideosFromYoutubeUrlCollection(paragraphId: string) {
        const queries = [] as AxiosPromise[]

        const collectionParagraph = this.props.includedRelationships.find((paragraph) => paragraph.id === paragraphId) as IYouTubeCollection

        collectionParagraph.attributes.field_video_youtube.map((url) => {
            const data = axios.get("//api.bepc.com/api/youtube/videosById/" + getVideoId(url))
            queries.push(data)
        })

        const response = await axios.all(queries) as IYouTubeCollectionAxiosResponse[]
        return response
            .filter((video) => video.data.items.length > 0)
            .map((video, i) => {
                const item = video.data.items[0]
                return {
                    id: item.id + i + paragraphId,
                    title: item.snippet.title,
                    url: item.id,
                    thumb: item.snippet.thumbnails.medium.url,
                } as IVideo
            })
    }

    private async getVideosFromYouTubeTags(paragraphId: string) {
        const concatenatedTags = await this.getConcatenatedTagNamesFromCMS(paragraphId)
        const youtubeVideos = await axios.get("//api.bepc.com/api/youtube/videosByTag/" + concatenatedTags) as IYouTubePlaylistResponse

        return youtubeVideos.data.map((video, i) => {
            return {
                id: video.id + i + paragraphId,
                title: video.title,
                url: video.id,
                thumb: video.thumbnailStandard,
            } as IVideo
        })
    }

    private async getConcatenatedTagNamesFromCMS(paragraphId: string) {
        const relatedVideoTags = await axios.get("//api.bepc.com/api/cms/json/taxonomy_term/related_videos/") as IRelatedVideoTagTaxonomyResponse
        const paragraphData = this.props.includedRelationships.find((paragraph) => paragraph.id === paragraphId) as IRelatedVideoTagsParagraph

        return paragraphData.relationships.field_tags.data
            .reduce((prev, cur) => {
                const tagData = relatedVideoTags.data.data.find((data) => data.id === cur.id)
                if (typeof tagData !== "undefined") {
                    return (prev.length > 0) ? "," + tagData.attributes.name : tagData.attributes.name
                }
                return ""
            }, "")
    }

}

export default VideoGallery

export interface IVideoGalleryProps {
    paragraphId: string,
    includedRelationships: Page.IPageData[],
    siteDomain: string,
}

export interface IVideoGalleryState {
    currentVideoId: string,
    autoplay: boolean,
    playlists: IPlaylistProps[]
}

export interface IVideoGallery extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_default_video: string
    }
    relationships: Page.IRelationships & {
        field_gallery_playlist_videos: Page.ITypeMultipleData
    }
}

export interface IVideoGalleryItem extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_title: string
    },
    relationships: Page.IRelationships & {
        field_video_gallery_videos: Page.IType
    }
}

export interface IYouTubeCollection extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_video_youtube: string[]
    }
}

export interface IYouTubeCollectionAxiosResponse extends AxiosResponse {
    data: IYouTubeAPIVideosById
}

export interface IYouTubeAPIVideosById {
    kind: string,
    etag: string,
    nextPageToken: null,
    regionCode: null,
    pageInfo: {
        totalResults: number,
        resultsPerPage: number
    },
    items: IYouTubeAPIVideoItems[]
}

export interface IYouTubeAPIVideoItems {
    kind: string,
    etag: string,
    id: string,
    snippet: {
        publishedAt: string,
        channelId: string,
        title: string,
        description: string,
        thumbnails: {
            default: {
                url: string,
                width: number,
                height: number,
            },
            medium: {
                url: string,
                width: number,
                height: number,
            },
            high: {
                url: string,
                width: number,
                height: number,
            }
        },
        channelTitle: string,
        tags: string[],
        liveBroadcastContent: string,
    }
}

export interface IRelatedVideoTagTaxonomyResponse extends AxiosResponse {
    data: {
        data: IRelatedVideoTagTaxonomyData[]
    }
}

export interface IRelatedVideoTagTaxonomyData extends Page.IPageData {
    attributes: Page.IAttributes & {
        name: string
    }
}

export interface IRelatedVideoTagsParagraph extends Page.IPageData {
    relationships: Page.IRelationships & {
        field_tags: Page.ITypeMultipleData
    }
}

export interface IYouTubePlaylistResponse extends AxiosResponse {
    data: IYouTubePlaylistData[]
}

export interface IYouTubePlaylistData {
    id: string,
    title: string,
    description: string,
    publishedAt: string,
    thumbnailDefault: string,
    thumbnailStandard: string,
    thumbnailMaxRes: string,
    tag: string,
}

export interface IRelatedVideoPlaylistParagraph extends Page.IPageData {
    attributes: Page.IAttributes & {
        field_video_playlist_id: string
    }
}
