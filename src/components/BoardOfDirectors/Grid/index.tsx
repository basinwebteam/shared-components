import * as React from "react"

export const Grid: React.StatelessComponent<IProps> = ({ people, ...props }) => (
        <div className="board-grid">
            <ul className="small-block-grid-2 large-block-grid-3">
                {people.map(({ name, district, image, onClick }: IPeople, i) => (
                    <li className="board-member" onClick={onClick} key={i}>
                        <img src={image} />
                        <div className="dir-name">{name}</div>
                        <div className="dir-district">{district == "0" ? "External" : "District " + district}</div>
                    </li>
                ))}
            </ul>
        </div>
)

export default Grid

export interface IProps {
    people: IPeople[]
}

export interface IPeople {
    name: string,
    image: string,
    district: string,
    onClick?: () => void,
}