import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import {Grid, IPeople} from "../Grid"
import boardsMockData from "../__tests__/mocks/boardData"

import "../Grid/style.scss"


const description = `Shows grid board of directors that a user can click on.`

storiesOf("Board of Directors", module)
    .add("Grid",
        withInfo(`${description}`)(() => {
            const people = boardsMockData[0].members.map((member: IPeople) => {
                const {name, image, district} = member
                return {
                    name,
                    image,
                    district,
                    onClick: () => {console.log(name)}
                }
            }) as IPeople[]

            return (
                <Grid people={people}  />
            )
        })
    )