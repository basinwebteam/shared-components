export const boardsMockData = [
  {
     "name":"Basin Electric",
     "members":[
        {
           "name":"Wayne Peltier",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Peltier-Wayne-160px-Portrait-2017.jpg",
           "title":"President of the Basin Electric board",
           "district":"9",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Kermit Pearson",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Pearson-Kermit-160px-Portrait-2017.jpg",
           "title":"Vice president of the Basin Electric board",
           "district":"1",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Allen Thiessen",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Thiessen-Allen-160px-Portrait-2017.jpg",
           "title":"Chairman of the Dakota Gasification Company board",
           "district":"8",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Charlie Gilbert",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Gilbert-Charlie-160px-Portrait-2017.jpg",
           "title":"Vice chairman of the Dakota Gasification Company board",
           "district":"11",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Paul Baker",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Baker-Paul-160px-Portrait-2017.jpg",
           "title":"Vice chairman of the Dakota Coal Company board",
           "district":"10",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Troy Presser",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Presser-Troy-160px-Portrait-2017.jpg",
           "title":"Chairman of the Dakota Coal board",
           "district":"3",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Leo Brekel",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Brekel-Leo-160px-Portrait-2017.jpg",
           "title":"Assistant Secretary of the Basin Electric board",
           "district":"5",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Mike McQuistion",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/McQuiston-Mike-160px-Portrait-2017.jpg",
           "title":"Director on the Basin Electric board",
           "district":"7",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"David Meschke",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Meschke-David-160px-Portrait-2017.jpg",
           "title":"Director on the Basin Electric board",
           "district":"2",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Tom Wagner",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Wagner-Tom-160px-Portrait-2017.jpg",
           "title":"Treasurer on the Dakota Coal board",
           "district":"4",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Dan Gliko",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Gliko-Dan-160px-Portrait-2017.jpg",
           "title":"Director on the Basin Electric board",
           "district":"6",
           "description": "Lorem ipsum.... "
        }
     ]
  },
  {
     "name":"Dakota Gas board",
     "members":[
        {
           "name":"Allen Thiessen",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Thiessen-Allen-160px-Portrait-2017.jpg",
           "title":"Chairman of the Dakota Gas board",
           "district":"8",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Charlie Gilbert",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Gilbert-Charlie-160px-Portrait-2017.jpg",
           "title":"Vice Chairman of the Dakota Gas board",
           "district":"11",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Kermit Pearson",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Pearson-Kermit-160px-Portrait-2017.jpg",
           "title":"Treasurer of the Dakota Gas board",
           "district":"1",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Tom Wagner",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Wagner-Tom-160px-Portrait-2017.jpg",
           "title":"Director on the Dakota Gas board",
           "district":"4",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Leo Brekel",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Brekel-Leo-160px-Portrait-2017.jpg",
           "title":"Director on the Dakota Gas board",
           "district":"5",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Mike McQuistion",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/McQuiston-Mike-160px-Portrait-2017.jpg",
           "title":"Director on the Dakota Gas board",
           "district":"7",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Jim Geringer",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Geringer-Jim-160px-Portrait-2017.jpg",
           "title":"External director on the Dakota Gas board",
           "district":"1",
           "description": "Lorem ipsum.... "
        },
        {
           "name":"Alan Klein",
           "image":"https://basinelectric.com/sites/CMS/files/images/bios/Klein-Alan-160px-Portrait-2017.jpg",
           "title":"External director on the Dakota Gas board",
           "district":"1",
           "description": "Lorem ipsum.... "
        }
     ]
  }
]
export default boardsMockData