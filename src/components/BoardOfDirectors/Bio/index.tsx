import * as React from "react"
import { MouseEvent } from "react"

export const Bio: React.StatelessComponent<IProps> = ({ name, title, image, district, description, className = "", onClose = (() =>("")) }) => {
    const onClick = function(event: MouseEvent<HTMLDivElement> | MouseEvent<HTMLButtonElement>) {
        const target = event.target as HTMLElement
        if(target.classList.contains("board-bio-wrapper") ||
            target.classList.contains("close-button")) {
            onClose()
        }
    }
        
    return (
        <div className={`board-bio-wrapper ${className}`} onClick={onClick}>
            <div className="board-bio">
                <button className="close-button" onClick={onClick} type="button" aria-label="Close alert" >
                    <span aria-hidden="true">×</span>
                </button>
                <span className="board-bio-pic">
                    <img src={image} />
                </span>
                <span className="board-bio-details">
                    <div className="name">{name}</div>
                    <div className="district">{district == "0" ? "External" : "District " + district}</div>
                    <div className="description" dangerouslySetInnerHTML={{__html: description}} />
                </span>
            </div>
        </div>
    )
}

export default Bio

export interface IProps {
    name: string,
    title: string,
    image: string,
    district: string,
    description: string,
    className?: string,
    onClose?: () => void,
}
