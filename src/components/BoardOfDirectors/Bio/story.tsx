import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import Bio from "../Bio"
import boardsMockData from "../__tests__/mocks/boardData"

import "../Bio/style.scss"


const description = `Shows grid board of directors that a user can click on.`

storiesOf("Board of Directors", module)
    .add("Biography",
        withInfo(`${description}`)(() => {
            const { name, image, title, district } = boardsMockData[0].members[0]

            return (
                <Bio name={name} image={image} title={title} district={district} description={"Lorem ipsum.... Lorem ipsum.... Lorem ipsum.... Lorem ipsum.... Lorem ipsum....Lorem ipsum.... Lorem ipsum.... Lorem ipsum.... Lorem ipsum....Lorem ipsum...."}  />
            )
        })
    )