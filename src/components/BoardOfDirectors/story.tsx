import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import {BoardOfDirectors, IBoard} from "../BoardOfDirectors"
import {boardsMockData} from "./__tests__/mocks/boardData"
import "./style.scss"


const description = `Shows grid board of directors that a user can click on.`

storiesOf("Board of Directors", module)
    .add("Full Layout",
        withInfo(`${description}`)(() => {
            return (
                <BoardOfDirectors boards={boardsMockData as IBoard[]}  />
            )
        })
    )