import * as React from "react"
import Grid from "./Grid"
import BoardMenu from "./Menu"
import Table from "./Table"
import Bio from "./Bio"

export class BoardOfDirectors extends React.Component<IBoardOfDirectorsProps, IBoardOfDirectorsState> {
    constructor(props: IBoardOfDirectorsProps) {
        super(props)
        this.showBio = this.showBio.bind(this)
        this.closeBio = this.closeBio.bind(this)
    }

    state = this.getDefaultState()

    render() {
        const currentBoard = this.props.boards[this.state.currentBoard]
        const boardMenuBoards = this.getBoardMenuBoardsProperty()

        currentBoard.members = currentBoard.members.map(({name, image, title, district, description}) => ({
            name,
            image,
            title,
            district,
            description,
            onClick: () => this.showBio(name, image, title, district, description)
        }))
       
        const bio = this.state.currentBoardMember
        
        return (
            <div className="board-of-directors">
                <div className="row">
                    <div className="medium-6 large-4 columns">
                        <BoardMenu boards={boardMenuBoards} activeIndex={this.state.currentBoard} />
                        <Table name={currentBoard.name} people={currentBoard.members} />
                    </div>
                    <div className="medium-6 large-8 columns">
                        <Grid people={currentBoard.members} />
                    </div>
                </div>
                <Bio
                    name={bio.name}
                    title={bio.title}
                    image={bio.image}
                    district={bio.district}
                    description={bio.description}
                    className={this.state.showBoardMember ? "show" : "hide"}
                    onClose={this.closeBio}
                />
        </div>
        )
    }

    getBoardMenuBoardsProperty() {
        return this.props.boards.map((board, i) => 
            ({
                name: board.name,
                onClick: () => {
                    this.setState({
                        currentBoard: i
                    })
                    this.scrollToTopIfMobile()
                }
            })
        )
    }

    scrollToTopIfMobile() {
        if(window.pageYOffset > 200) {
            window.scrollTo(0,0)
        }
    }

    closeBio() {
        this.setState(() => ({showBoardMember: false}))
    }

    showBio(name: string, image: string, title: string, district: string, description: string = "") {
        this.setState((prevState, {boards}) => ({
            currentBoardMember: {
                name,
                title,
                image,
                district,
                description
            },
            showBoardMember: true
        }))
    }

    getDefaultState() {

        if(this.props.boards.length > 0){
            return {
                currentBoard: 0,
                currentBoardMember: {
                    name: "",
                    title: "",
                    image: "",
                    district: "",
                    description: ""
                },
                showBoardMember: false,
            }
        }

        return {
            currentBoard: 0,
            currentBoardMember: {
                name: "",
                title: "",
                image: "",
                district: "",
                description: ""
            },
            showBoardMember: false,
        }
    }
}

export default BoardOfDirectors

export interface IBoardOfDirectorsProps {
    boards: IBoard[]
}

export interface IBoardOfDirectorsState {
    currentBoard: number,
    currentBoardMember: ICurrentBoardMember,
    showBoardMember: boolean
}

export interface IPeople {
    name: string,
    image: string,
    title: string,
    district: string,
    description?: string,
    onClick?: () => void,
}

export interface ICurrentBoardMember {
    name: string,
    title: string,
    image: string,
    district: string,
    description: string    
}

export interface IBoard {
    name: string,
    members: IPeople[],
    onClick?: () => void,
}