import * as React from "react"

export const BoardMenu: React.StatelessComponent<IProps> = (props) => {
    const isActive = (index:number, activeIndex:number) => activeIndex === index ? "disabled" : ""
    const className = props.className || "button expand"

    return (
        <div className="row">
            <div className="medium-12 column">
                <div className="board-menu">
                    <ul>
                        {props.boards.map(({name, onClick}, i) => 
                            <li
                                className={`${className} ${isActive(i, props.activeIndex)}`} 
                                onClick={onClick} 
                                key={i}
                            >
                            {name}
                        </li>
                        )}
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default BoardMenu

export interface IProps {
    boards: IBoard[],
    activeIndex: number,
    className?: string,
}

export interface IBoard {
    name: string,
    onClick?: () => void
}