import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import {BoardMenu, IBoard} from "../Menu"

import "../Menu/style.scss"


const description = `Shows menu for board of directors that a user can click on. You can pass your own link component or use the default.`

storiesOf("Board of Directors", module)
    .add("Board Menu Buttons",
        withInfo(`${description}`)(() => {
            const boards = [
                {name: "Basin Electric board",},
                {name: "Dakota Coal board"},
            ] as IBoard[]


            return (
                <BoardMenu boards={boards} activeIndex={0}  />
            )
        })
    )