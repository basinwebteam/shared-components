import * as React from "react"

export const Table: React.StatelessComponent<IProps> = ({people, name, ...props}) => (
    <div className="row">
        <div className="medium-12 column">
            <div className="board-table">
                <h3>{name} Board</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Name</th>
                            <th>District</th>
                        </tr>
                    </thead>
                    <tbody>
                        {people.map(({title, name, district, onClick}: IPeople, i) => (
                            <tr key={i}>
                                <td className="title">{title}</td>
                                {/* Add OnClick for name. */}
                                <td onClick={onClick} className="name">{name}</td>
                                <td className="district">{district == "0" ? "External" : district}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
)

export default Table

export interface IProps {
    name: string,
    people: IPeople[]
}

export interface IPeople {
    title: string,
    name: string,
    district: string,
    onClick?: () => void,
}