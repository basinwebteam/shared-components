import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import {Table, IPeople as ITablePeople} from "../Table"
import boardsMockData from "../__tests__/mocks/boardData"


import "../Table/style.scss"

const description = `Shows menu for board of directors that a user can click on. You can pass your own link component or use the default.`

storiesOf("Board of Directors", module)
    .add("Table",
        withInfo(`${description}`)(() => {
            const people = boardsMockData[0].members.map((member: ITablePeople) => {
                const {name, title, district} = member
                return {
                    name,
                    title,
                    district,
                    onClick: () => {console.log(name)}
                }
            }) as ITablePeople[]


            return (
                <Table name="Basin Electric Board" people={people}  />
            )
        })
    )