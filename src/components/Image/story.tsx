import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'
import Image from "../Image"
import "./style.scss"

const description = `Displays an image `

storiesOf("Image", module)
    .addDecorator(withKnobs)
    .add("with caption", 
        withInfo(`${description} with a caption.`)(() => {
            const url=text("URL", "http://via.placeholder.com/400x300")
            const altText=text("Alt Text", "lorem ipsum")
            const caption=text("Caption", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua")

            return (
                <Image
                    url={url}
                    altText={altText}
                    caption={caption}
                />
            )
        })
    )
    .add("without caption", 
        withInfo(`${description} without a caption.`)(() => {
            const url=text("URL", "http://via.placeholder.com/400x300")
            const altText=text("Alt Text", "lorem ipsum")

            return (
                <Image
                    url={url}
                    altText={altText}
                />
            )
        })
    )