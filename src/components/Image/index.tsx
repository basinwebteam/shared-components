import * as React from "react"

export const Image: React.SFC<IImageProps> = (props) => {
    const className = (props.className) ? props.className : ""
    
    if(props.caption) {
        return (
            <figure className={className}>
                <img src={props.url} alt={props.altText} />
                <figcaption dangerouslySetInnerHTML={{__html: props.caption}} />
            </figure>
        )
    }
    return <img className={className} src={props.url} alt={props.altText} />
}

Image.displayName = "Image"

export default Image

export interface IImageProps {
    altText: string,
    url: string,
    caption?: string,
    className?: string,
}
