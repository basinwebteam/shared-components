import * as React from "react"

export const YouTubeVideo: React.StatelessComponent<IYouTubeProps> = (props) => {
    const autoplay = (props.autoplay) ? "1" : "0"
    return (
        <div className="video-youtube" style={styles.videoWrapper}>
            <iframe
                style={styles.iframe}
                width="560"
                height="315"
                src={`https://www.youtube.com/embed/${getVideoId(props.link)}?rel=0&autoplay=${autoplay}`}
                frameBorder="0"
                allowFullScreen={true}
            />
        </div>
    )
}

const styles = {
    videoWrapper: {
        position: "relative",
        paddingBottom: "56.25%", /* 16:9 */
        paddingTop: "25px",
        height: 0,
    } as React.CSSProperties,
    iframe: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
    } as React.CSSProperties,
}

export const getVideoId = (linkStringArray: string|string[]) => {
    let link = ""
    if (Array.isArray(linkStringArray)) {
        link = linkStringArray[0]
    } else {
        link = linkStringArray
    }
    if (link.startsWith("https://youtu.be/")) {
        return link.substr(17, 11)
    }
    if (link.startsWith("https://www.youtube.com/watch?v=")) {
        return link.substr(32, 11)
    }
    if (link.startsWith("https://www.youtube.com/embed")) {
        return link.substr(30, 11)
    }
    return link
}

export default YouTubeVideo

export interface IYouTubeProps {
    link: string,
    autoplay?: boolean
}
