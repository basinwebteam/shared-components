import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'
import YouTube from "../YouTube"

const description = `Displays a responsive YouTube video. It should work with either a variety of YouTube links or just the video ID itself.`

storiesOf("YouTube", module)
    .addDecorator(withKnobs)
    .add("Video",
        withInfo(`${description}`)(() => {
            const link = text("Link", "Ak6xkdY_1Jo")
            const autoplay = boolean("Autoplay", false)
            return (
                <div style={{maxWidth: "1000px"}}>
                    <YouTube
                        link={link}
                        autoplay={autoplay}
                    />
                </div>
            )
        })
    )
