import * as React from "react"
import { format } from "d3-format"
import { GenerationData } from "../EnergyPortfolioPieChart"

export const EnergyPortfolioResourceTable: React.SFC<IEPResourceResourceTableProps> = (props) => {
    const data = calculateResourceData(props.data)
    return (
        <div>
            <h2>Basin Electric Generation Portfolio</h2>
            <table>
                <thead>
                    <tr>
                        <th>Total</th>
                        <th>Summer (MW)</th>
                        <th>Winter (MW)</th>
                        <th>Summer Capacity (%)</th>
                        <th>Winter Capacity (%)</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.resources.map(({ name, mw, mwSummer, summerCap, winterCap }, i) => (
                            <tr key={i}>
                                <td>{name}</td>
                                <td className="text-right">{format(",")(mwSummer)}</td>
                                <td className="text-right">{format(",")(mw)}</td>
                                <td className="text-right">{format(".1f")(summerCap * 100)}%</td>
                                <td className="text-right">{format(".1f")(winterCap * 100)}%</td>
                            </tr>
                        ))}
                    <tr>
                        <td><strong>Total generation</strong></td>
                        <td className="text-right"><strong>{format(",.1f")(data.totalSummerMW)}</strong></td>
                        <td className="text-right"><strong>{format(",.1f")(data.totalWinterMW)}</strong></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Owned Generation</td>
                        <td className="text-right">{format(",.1f")(parseFloat(props.ownedGenSummer))}</td>
                        <td className="text-right">{format(",.1f")(parseFloat(props.ownedGenWinter))}</td>
                        <td className="text-right">{format(".1f")(parseFloat(props.ownedGenSummer)/data.totalSummerMW*100)}%</td>
                        <td className="text-right">{format(".1f")(parseFloat(props.ownedGenWinter)/data.totalWinterMW*100)}%</td>
                    </tr>
                    <tr>
                        <td>Purchased Generation</td>
                        <td className="text-right">{format(",.1f")(parseFloat(props.purchasedGenSummer))}</td>
                        <td className="text-right">{format(",.1f")(parseFloat(props.purchasedGenWinter))}</td>
                        <td className="text-right">{format(".1f")(parseFloat(props.purchasedGenSummer)/data.totalSummerMW*100)}%</td>
                        <td className="text-right">{format(".1f")(parseFloat(props.purchasedGenWinter)/data.totalWinterMW*100)}%</td>
                    </tr>
                    <tr>
                        <td>Operated Generation</td>
                        <td className="text-right">{format(",")(parseFloat(props.operatedGenSummer))}</td>
                        <td className="text-right">{format(",")(parseFloat(props.operatedGenWinter))}</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
export function calculateResourceData(data: GenerationData[]) {
    const totalSummerMW = data.reduce((a, b) => (a + (b.mwSummer || 0)), 0)
    const totalWinterMW = data.reduce((a, b) => (a + (b.mw || 0)), 0)
    const resources = data.map(d => ({
        name: d.name,
        mw: d.mw,
        mwSummer: d.mwSummer || 0,
        summerCap: (d.mwSummer || 0) / totalSummerMW,
        winterCap: (d.mw || 0) / totalWinterMW
    }) as CalculatedDataResource)

    return {
        totalSummerMW,
        totalWinterMW,
        resources
    } as CalculatedData
}

export default EnergyPortfolioResourceTable

export interface IEPResourceResourceTableProps {
    data: GenerationData[]
    operatedGenSummer: string,
    operatedGenWinter: string,
    ownedGenSummer: string,
    ownedGenWinter: string,
    purchasedGenSummer: string,
    purchasedGenWinter: string
}

export interface CalculatedData {
    totalSummerMW: number,
    totalWinterMW: number,
    resources: CalculatedDataResource[]
}

export interface CalculatedDataResource {
    name: string,
    mw: number,
    mwSummer: number,
    summerCap: number,
    winterCap: number
}