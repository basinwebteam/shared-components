import * as React from "react"

export const Title: React.StatelessComponent = (props: IProps) => (
    <h2 className="paragraph-title">{props.children}</h2>
)

Title.displayName = "Title"

export default Title

interface IProps {
    children: string
}
