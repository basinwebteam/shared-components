import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'
import Title from "../Title"
import "./style.scss"

const description = `Displays a paragraph title. `

storiesOf("Title", module)
    .addDecorator(withKnobs)
    .add("default", 
        withInfo(`${description} with a caption.`)(() => {
            const titleText=text("Text", "About Us")

            return (
                <Title>{titleText}</Title>
            )
        })
    )