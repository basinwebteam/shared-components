import * as React from 'react'
import MenuLink from "./MenuLink"
import { MobileMenuTrigger } from "./MobileMenuTrigger"

export class Menu extends React.Component<IMenuProps, IMenuState> {
    public defaultProps: Partial<IMenuProps> = {
        widthToChange: 840,
        linkHandler: (url: string, key: string, e: React.MouseEvent<HTMLElement>) => {
            if (typeof window !== "undefined") {
                window.location.href = (e.target as HTMLAnchorElement).href
            }
        }
    }

    constructor(props: IMenuProps) {
        super(props)
        this.state = {
            menuOpen: false,
            menuVersion: this.determineMenuVersion(),
            activeId: "",
        }
        this.toggleMenuOpenClosed = this.toggleMenuOpenClosed.bind(this)
        this.closeMenu = this.closeMenu.bind(this)
        this.clickLink = this.clickLink.bind(this)
        this.clickBack = this.clickBack.bind(this)
        this.resizeEventListener = this.resizeEventListener.bind(this)
    }

    public render() {
        return (
            <div className={this.menuClass()}>
                <MobileMenuTrigger display={this.state.menuVersion === "Mobile"} onClick={this.toggleMenuOpenClosed} />
                <nav role="navigation">
                    <ul>
                        {this.getLinks()}
                    </ul>
                </nav>
            </div>
        )
    }

    public menuClass() {
        const classList = ["mega-menu"]
        classList.push(this.state.menuVersion.toLowerCase())
        if (this.state.menuOpen) { classList.push("menu-open") }
        if (this.state.activeId) { classList.push("default") }
        return classList.join(" ")
    }


    public componentDidMount() {
        if (typeof window !== "undefined") {
            window.addEventListener("resize", this.resizeEventListener)
            window.addEventListener("reset_mobile_menu", this.closeMenu)
        }
    }

    public componentWillUnmount() {
        if (typeof window !== "undefined") {
            window.removeEventListener("resize", this.resizeEventListener)
            window.removeEventListener("reset_mobile_menu", this.closeMenu)
        }
    }

    public resizeEventListener() {
        this.setState({
            menuVersion: this.determineMenuVersion()
        })
    }

    public getLinks() {
        return this.props.links.map((link, i) => {
            const { url, title, key, children = [] } = link
            return (
                <MenuLink
                    key={i}
                    id={key}
                    path={url}
                    title={title}
                    children={children}
                    activeId={this.state.activeId}
                    className={`top-menu-item-${i}`}
                    activeForce={this.state.activeId === ""}
                    onLinkClick={this.clickLink}
                    onBackClick={this.clickBack}
                />
            )
        })
    }

    public clickLink(e: React.MouseEvent<HTMLElement>, key: string) {
        switch (e.type) {
            case "touchstart":
            default:
                return false
            case "touchend":
            case "click":
                //target could be a span or a tag on mobile. Make sure it's the a tag.
                let target = e.target as HTMLAnchorElement
                if (target.classList.contains("more")) {
                    target = target.parentElement as HTMLAnchorElement
                }

                if (this.state.menuVersion === "Mobile") {
                    if (this.hasChildren(target)) {
                        this.setState({ activeId: key })
                        return false
                    }
                }
                if (this.state.menuVersion === "Large") {
                    if (this.hasNoAncestors(key) && this.isNotOverviewLink(target)) {
                        this.setState({ activeId: key })
                        return false
                    }
                }
                if (this.props.linkHandler) {
                    this.props.linkHandler(target.href, key, e)
                }
                return false
        }
    }

    public clickBack(e: React.MouseEvent<HTMLElement>) {
        const parentId = this.findParentId(this.props.links, this.state.activeId)
        this.setState({ activeId: parentId })
    }

    public findParentId(links: ILinks[], idToFind: string, parentId = "") {
        links.find((link) => {
            if (idToFind === link.key) {
                return true
            }
            if (typeof link.children !== "undefined") {
                parentId = this.findParentId(link.children, idToFind)
                if (parentId !== "") {
                    return true
                }
            }
            return false
        })
        return parentId
    }

    public hasChildren(linkElement: HTMLElement) {
        return linkElement.children.length > 0
            && linkElement.children[0].classList.contains("more")
    }

    public isNotOverviewLink(linkElement: HTMLElement) {
        return linkElement.childElementCount !== 0
    }

    public hasNoAncestors(key: string) {
        return typeof this.props.links.find((link) => link.key === key) !== "undefined"
    }

    public toggleMenuOpenClosed() {
        this.setState({
            menuOpen: !this.state.menuOpen,
            activeId: "",
        })
    }

    public closeMenu() {
        this.setState({
            menuOpen: false,
            activeId: "",
        })
    }

    public determineMenuVersion(): MenuVersion {
        const widthToChange = (this.props as PropsWithDefaults).widthToChange
        if (typeof window !== "undefined") {
            return window.innerWidth >= widthToChange ? "Large" : "Mobile"
        }
        return "Large"
    }
}

export default Menu

export interface IMenuProps {
    links: Array<ILinks>,
    widthToChange?: number,
    linkHandler?: (url: string, key: string, e: React.MouseEvent<HTMLElement>) => void,
}

export interface IMenuState {
    menuOpen: boolean,
    menuVersion: MenuVersion,
    activeId: string
}

export interface IDefaultProps {
    widthToChange: number,
    linkHandler: (url: string, key: string, e: React.MouseEvent<HTMLElement>) => void,
}

export interface ILinks {
    key: string,
    title: string,
    url: string,
    children?: Array<ILinks>
}

export type MenuVersion = "Large" | "Mobile"
export type PropsWithDefaults = IMenuProps & IDefaultProps