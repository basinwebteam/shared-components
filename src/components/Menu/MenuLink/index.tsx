import * as React from 'react'
import { ILinks } from "../../Menu"

export class MenuLink extends React.Component<PropsWithDefaultsMenuLink, IMenuLinkState> {
    constructor(props: PropsWithDefaultsMenuLink) {
        super(props)
        this.state = {
            active: this.isActive(),
            activeChildren: false
        }
        this.handleLinkClick = this.handleLinkClick.bind(this)
    }

    public static defaultProps: Partial<IMenuLinkProps> = {
        activeId: "",
        className: "",
        children: []
    }

    public render(): JSX.Element {
        return (
            <li className={this.listItemClass()}>
                <a href={this.props.path} onClick={this.handleLinkClick}>
                    {this.props.title}
                    {this.props.children.length > 0 ? <span className="more" /> : ""}
                </a>
                {this.props.children.length > 0 ? this.getChildren() : ""}
            </li>
        )
    }

    public listItemClass() {
        const classList = ["menu-link"]
        classList.push(this.props.className ? this.props.className : "")
        classList.push(this.isActive() ? "active" : "")
        classList.push(this.activeIdBelongsToDescendant() ? "active-parent" : "")
        classList.push(this.props.activeForce ? "active-forced" : "")
        return classList.join(" ").trim()
    }

    public handleLinkClick(e: React.MouseEvent<HTMLElement>) {
        e.stopPropagation()
        e.preventDefault()
        return this.props.onLinkClick(e, this.props.id)
    }

    public getLinks(links: Array<ILinks>) {
        return links.map((link, i) => {
            const { url, title, key, children = [] } = link
            return (
                <MenuLink
                    key={i}
                    id={key}
                    path={url}
                    title={title}
                    children={children}
                    activeId={this.props.activeId}
                    activeForce={false}
                    onLinkClick={this.props.onLinkClick}
                    onBackClick={this.props.onBackClick}
                />
            )
        })
    }

    private isActive() {
        return this.props.activeId === this.props.id
    }

    private activeIdBelongsToDescendant() {
        const descendant = this.findDescendantIfContainsActiveId(this.props.children)

        if (typeof descendant !== "undefined") {
            return true
        }
        return false
    }

    private findDescendantIfContainsActiveId(children: Array<ILinks>) {
        return children.find((child) => {
            if (child.key === this.props.activeId) {
                return true
            }
            if (typeof child.children !== "undefined") {
                const descendant = this.findDescendantIfContainsActiveId(child.children)
                if (typeof descendant !== "undefined") {
                    return true
                }
            }
            return false
        })
    }

    private getChildren() {
        const title = this.props.title + " Overview"
        const { onBackClick, path, children } = this.props
        return (
            <ul>
                <li className="back" onClick={onBackClick}>
                    <span>Back</span>
                </li>
                <li className="menu-overview">
                    <a href={path}>{title}</a>
                    <ul className="menu-inner-wrapper">
                        <li>
                            <ul>
                                {this.getLinks(children)}
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        )
    }

}

export default MenuLink

export interface IPropsRequired {
    id: string,
    path: string,
    title: string,
    onLinkClick: (e: React.MouseEvent<HTMLElement>, id: string) => boolean,
    onBackClick: (e: React.MouseEvent<HTMLElement>) => void,
}

export interface IMenuLinkProps extends IPropsRequired {
    activeId?: string,
    children?: Array<ILinks>,
    className?: string,
    activeForce?: boolean
}

export interface IMenuLinkState {
    active: boolean,
    activeChildren: boolean,
}

export interface IDefaultMenuLinkProps {
    activeId: string,
    children: Array<ILinks>,
    className?: string,
    activeForce: boolean
}

export type PropsWithDefaultsMenuLink = IMenuLinkProps & IDefaultMenuLinkProps
