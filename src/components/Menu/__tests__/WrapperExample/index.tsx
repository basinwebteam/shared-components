import * as React from 'react'
import {ILinks, Menu} from "../../../Menu"
import rawJSONFromDrupalApi from "./mockData"

export class MenuWrapper extends React.Component<{}, IState> {
    constructor(props: {}) {
        super(props)
        this.state = {
            links: []
        }
    }

    public render() {
        return (
            <Menu links={this.state.links} />
        )
    }

    public componentDidMount() {
        this.convertMenu()
    }

    private convertMenu() {
        const links = (rawJSONFromDrupalApi).map((rootItem) => {
            return {
                "key": rootItem.key,
                "title": rootItem.title,
                "url": rootItem.alias,
                "children": this.getChildren(rootItem as ImportedData)
            }
        })
        this.setState({links})
    }
    
    private getChildren(parent: ImportedData): Array<ILinks> {
        if (parent.hasOwnProperty("below")) {
            return Object.keys(parent.below).map((id) => {
                return {
                    "key": parent.below[id].key,
                    "title": parent.below[id].title,
                    "url": parent.below[id].alias,
                    "children": this.getChildren(parent.below[id] as ImportedData)
                }
            })
        }
        return []
    }
}


export default MenuWrapper

interface IState {
    links: Array<ILinks>,
}

interface PartialImportedData {
    key: string,
    title: string,
    alias: string,
    below?: Dictionary
}

interface ImportedData extends PartialImportedData {
    key: string,
    title: string,
    alias: string,
    below: Dictionary
}

interface Dictionary {
    [key: string]: PartialImportedData
}