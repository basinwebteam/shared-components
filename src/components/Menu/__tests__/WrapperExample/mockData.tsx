export const rawJSONFromDrupalApi = [
    {
        "key": "c2d9bfa7-04e0-4281-bbf5-1fbb99b2b15e",
        "title": "About Us",
        "alias": "about-us",
        "below": {
            "79c37e76-e78f-4639-96f8-65896a0a12c2": {
                "key": "79c37e76-e78f-4639-96f8-65896a0a12c2",
                "title": "Career Center",
                "alias": "about-us/career-center",
                "below": {
                    "b24b8e6c-5d3d-4044-8420-0ca546ffb6c3": {
                        "key": "b24b8e6c-5d3d-4044-8420-0ca546ffb6c3",
                        "title": "Benefits and wellness",
                        "alias": "about-us/career-center/benefits-and-wellness"
                    },
                    "7339bb32-fd3a-4f81-8d1f-9191da450796": {
                        "key": "7339bb32-fd3a-4f81-8d1f-9191da450796",
                        "title": "Employees in Military Service",
                        "alias": "about-us/career-center/employees-military-service"
                    },
                    "53e825c6-e3f5-405c-895d-6f298d812ce8": {
                        "key": "53e825c6-e3f5-405c-895d-6f298d812ce8",
                        "title": "First day of work",
                        "alias": "about-us/career-center/first-day-work-0"
                    },
                    "f59b2af7-3a3b-4d42-81b9-123d2e0ff577": {
                        "key": "f59b2af7-3a3b-4d42-81b9-123d2e0ff577",
                        "title": "Internships",
                        "alias": "about-us/career-center/internships-0"
                    },
                    "e5933d9e-7b7d-4088-9330-c12d185d83ae": {
                        "key": "e5933d9e-7b7d-4088-9330-c12d185d83ae",
                        "title": "Professional Development",
                        "alias": "about-us/career-center/professional-development-0"
                    },
                    "91b5aeec-69fb-419e-8c57-d394356852af": {
                        "key": "91b5aeec-69fb-419e-8c57-d394356852af",
                        "title": "Communities",
                        "alias": "about-us/career-center/communities"
                    },
                    "0fa85a63-59d7-47e5-b467-e9a266c66cea": {
                        "key": "0fa85a63-59d7-47e5-b467-e9a266c66cea",
                        "title": "Recruiters",
                        "alias": "about-us/career-center/recruiters-0"
                    }
                }
            },
            "682c88b9-080b-4e0c-84e9-1b11c62189de": {
                "key": "682c88b9-080b-4e0c-84e9-1b11c62189de",
                "title": "Organization",
                "alias": "about-us/organization",
                "below": {
                    "8cd70005-9bf0-450b-b1e7-aa4e45001158": {
                        "key": "8cd70005-9bf0-450b-b1e7-aa4e45001158",
                        "title": "At a Glance",
                        "alias": "about-us/organization/at-a-glance"
                    },
                    "5b29ee8a-19b1-4fdc-b15b-0e06ea55814e": {
                        "key": "5b29ee8a-19b1-4fdc-b15b-0e06ea55814e",
                        "title": "Boards of Directors",
                        "alias": "about-us/organization/boards-directors"
                    },
                    "e6d1058e-7a48-4f6b-a4f1-376ffd01c967": {
                        "key": "e6d1058e-7a48-4f6b-a4f1-376ffd01c967",
                        "title": "Executives",
                        "alias": "about-us/organization/Executives"
                    },
                    "46e2b4d0-ccdb-4a05-848d-dcd73b71b3a4": {
                        "key": "46e2b4d0-ccdb-4a05-848d-dcd73b71b3a4",
                        "title": "Financial",
                        "alias": "about-us/organization/financial"
                    },
                    "27deb7fb-c960-4761-b329-30ac107f6d58": {
                        "key": "27deb7fb-c960-4761-b329-30ac107f6d58",
                        "title": "History",
                        "alias": "about-us/organization/history",
                        "below": {
                            "31e98447-e954-4031-956e-f50ede152ddc": {
                                "key": "31e98447-e954-4031-956e-f50ede152ddc",
                                "title": "1958-1960",
                                "alias": "about-us/organization/history/1958-1960"
                            },
                            "d97b6d3b-ef1e-4609-adb8-4b604c4b19f3": {
                                "key": "d97b6d3b-ef1e-4609-adb8-4b604c4b19f3",
                                "title": "1961-1984",
                                "alias": "about-us/organization/history/1961-1984"
                            },
                            "45a1d6e4-cbed-414a-af98-db42c6b613c6": {
                                "key": "45a1d6e4-cbed-414a-af98-db42c6b613c6",
                                "title": "1985-1999",
                                "alias": "about-us/organization/history/1985-1999"
                            },
                            "1b03a403-5734-4011-91c0-60149fd4852a": {
                                "key": "1b03a403-5734-4011-91c0-60149fd4852a",
                                "title": "2000-2012",
                                "alias": "about-us/organization/history/2000-2012"
                            },
                            "fb865111-3448-45e8-8c32-7fa36ca04328": {
                                "key": "fb865111-3448-45e8-8c32-7fa36ca04328",
                                "title": "2013 to present",
                                "alias": "about-us/organization/history/2013-present"
                            },
                            "0da4af6d-5699-4ba1-8ca3-32c4f94d94f7": {
                                "key": "0da4af6d-5699-4ba1-8ca3-32c4f94d94f7",
                                "title": "Hall of Fame",
                                "alias": "about-us/organization/history/hall-fame"
                            },
                            "bd7b4d4c-b1ae-4f30-8dea-8fccf2370185": {
                                "key": "bd7b4d4c-b1ae-4f30-8dea-8fccf2370185",
                                "title": "Original Incorporators",
                                "enabled": true
                            },
                            "d90bdb53-a348-4e34-96a1-363b34433878": {
                                "key": "d90bdb53-a348-4e34-96a1-363b34433878",
                                "title": "Rural Electrification",
                                "alias": "about-us/organization/history/rural-electrification"
                            }
                        }
                    },
                    "dae6f978-b610-4e40-bbe4-cf19ba5f1199": {
                        "key": "dae6f978-b610-4e40-bbe4-cf19ba5f1199",
                        "title": "Procurement",
                        "alias": "about-us/organization/procurement"
                    },
                    "f73c7f8c-eeef-45b0-b724-8f900292bd65": {
                        "key": "f73c7f8c-eeef-45b0-b724-8f900292bd65",
                        "title": "Safety and Health",
                        "alias": "about-us/organization/safety-and-health"
                    },
                    "52a8cfad-2e5c-4013-8f87-959cdd540857": {
                        "key": "52a8cfad-2e5c-4013-8f87-959cdd540857",
                        "title": "Subsidiaries",
                        "alias": "about-us/organization/subsidiaries"
                    },
                    "cfd6ff4a-8476-4df5-9a24-921bd1c2e1ee": {
                        "key": "cfd6ff4a-8476-4df5-9a24-921bd1c2e1ee",
                        "title": "Marketing and Asset Management",
                        "alias": "about-us/organization/marketing-and-asset-management"
                    }
                }
            },
            "427e4779-c763-4917-a641-63ca4e67fd82": {
                "key": "427e4779-c763-4917-a641-63ca4e67fd82",
                "title": "Social Responsibility",
                "alias": "about-us/Social-Responsibility",
                "below": {
                    "7cd481af-9696-4a62-95f2-9a96b04cc008": {
                        "key": "7cd481af-9696-4a62-95f2-9a96b04cc008",
                        "title": "Charitable Giving",
                        "alias": "about-us/social-responsibility/charitable-giving"
                    },
                    "46bec7dc-bfea-4b24-8d1f-4996ec171738": {
                        "key": "46bec7dc-bfea-4b24-8d1f-4996ec171738",
                        "title": "Service Programs",
                        "alias": "about-us/social-responsibility/service-programs"
                    },
                    "b4fe0da7-0ff3-4fd2-ae17-0d592ae7aae8": {
                        "key": "b4fe0da7-0ff3-4fd2-ae17-0d592ae7aae8",
                        "title": "Medora Musical",
                        "alias": "about-us/social-responsibility/medora-musical"
                    },
                    "180a7ebf-1b96-4574-9c56-c2ce27da7326": {
                        "key": "180a7ebf-1b96-4574-9c56-c2ce27da7326",
                        "title": "United Way",
                        "alias": "about-us/social-responsibility/united-way"
                    },
                    "4c45b776-2aea-4dbf-98f3-c76c14e26e4a": {
                        "key": "4c45b776-2aea-4dbf-98f3-c76c14e26e4a",
                        "title": "Norsk Hostfest",
                        "alias": "about-us/social-responsibility/norsk-hostfest"
                    },
                    "384d3e67-8bbc-41a1-a0ec-73a4d571ed82": {
                        "key": "384d3e67-8bbc-41a1-a0ec-73a4d571ed82",
                        "title": "Brave the Shave",
                        "alias": "about-us/social-responsibility/brave-shave"
                    },
                    "cae51a09-b0ff-4870-8abc-932f805cfb15": {
                        "key": "cae51a09-b0ff-4870-8abc-932f805cfb15",
                        "title": "Youth Development",
                        "alias": "about-us/social-responsibility/youth-development"
                    }
                }
            },
            "f765e931-6d6b-4ca9-ad55-1e2a17b30d38": {
                "key": "f765e931-6d6b-4ca9-ad55-1e2a17b30d38",
                "title": "Members",
                "alias": "about-us/members"
            },
            "beac4adb-7da6-473c-9176-306f4661939d": {
                "key": "beac4adb-7da6-473c-9176-306f4661939d",
                "title": "Annual Meeting",
                "alias": "about-us/annual-meeting",
                "below": {
                    "6708b597-6d50-4969-91b5-028106200755": {
                        "key": "6708b597-6d50-4969-91b5-028106200755",
                        "title": "Agenda",
                        "alias": "about-us/annual-meeting/agenda"
                    },
                    "5ad955a2-ab1d-4631-976a-5f19971fe742": {
                        "key": "5ad955a2-ab1d-4631-976a-5f19971fe742",
                        "title": "Bismarck-Mandan Hotels",
                        "alias": "about-us/annual-meeting/bismarck-mandan-hotels"
                    }
                }
            }
        }
    },
    {
        "key": "073aad01-804f-4a6a-a3c5-6b0071f595ca",
        "title": "News Center",
        "alias": "news-center",
        "below": {
            "3f34c076-10bc-47f8-83c9-3de88269a486": {
                "key": "3f34c076-10bc-47f8-83c9-3de88269a486",
                "title": "Media Resources",
                "alias": "news-center/media-resources",
                "below": {
                    "b9f68317-90f6-4cfe-a109-181dd272a6f0": {
                        "key": "b9f68317-90f6-4cfe-a109-181dd272a6f0",
                        "title": "Speakers Bureau",
                        "alias": "News-Center/Media-Resources/Speakers"
                    },
                    "0563cf0d-bdf6-4fdb-9456-443b4f17e411": {
                        "key": "0563cf0d-bdf6-4fdb-9456-443b4f17e411",
                        "title": "Tours",
                        "alias": "News-Center/Media-Resources/Tours"
                    },
                    "b823e927-fcd8-4f1d-8cf0-8af6708ada98": {
                        "key": "b823e927-fcd8-4f1d-8cf0-8af6708ada98",
                        "title": "Photo Request",
                        "alias": "news-center/media-resources/photo-request"
                    }
                }
            },
            "92a5459a-6ef4-45ee-99c6-7b3927484b5d": {
                "key": "92a5459a-6ef4-45ee-99c6-7b3927484b5d",
                "title": "Legislation",
                "alias": "news-center/legislation",
                "below": {
                    "cc42b6d1-ec82-43e6-b415-921dcebebfbf": {
                        "key": "cc42b6d1-ec82-43e6-b415-921dcebebfbf",
                        "title": "EPA Regulations",
                        "alias": "news-center/legislation/epa-regulations"
                    },
                    "b7445623-97cb-4bc3-bdaf-36e2603e681f": {
                        "key": "b7445623-97cb-4bc3-bdaf-36e2603e681f",
                        "title": "Government Contacts",
                        "alias": "news-center/legislation/government-contacts"
                    },
                    "27413918-a1a5-41a2-b4b9-816fa87ccfde": {
                        "key": "27413918-a1a5-41a2-b4b9-816fa87ccfde",
                        "title": "Federal Legislation",
                        "alias": "news-center/legislation/federal-legislation"
                    },
                    "c7aaa5f9-02af-49c2-9382-f43fd5e797f4": {
                        "key": "c7aaa5f9-02af-49c2-9382-f43fd5e797f4",
                        "title": "State Legislative Sessions",
                        "alias": "news-center/legislation/state-legislative-sessions"
                    }
                }
            },
            "b05332aa-19b0-4590-8dfb-171f243ff002": {
                "key": "b05332aa-19b0-4590-8dfb-171f243ff002",
                "title": "Publications",
                "alias": "news-center/publications-0",
                "below": {
                    "e3b96f0e-6c5c-42de-bde7-ffadec245af3": {
                        "key": "e3b96f0e-6c5c-42de-bde7-ffadec245af3",
                        "title": "Annual Report",
                        "alias": "news-center/publications/annual-report"
                    },
                    "eeeb06a3-9707-4b3a-ab7d-4916e3a5f099": {
                        "key": "eeeb06a3-9707-4b3a-ab7d-4916e3a5f099",
                        "title": "CEO Talk",
                        "alias": "news-center/publications/ceo-talk"
                    },
                    "b1ed06c2-7293-468c-96d5-af5e3d994e2f": {
                        "key": "b1ed06c2-7293-468c-96d5-af5e3d994e2f",
                        "title": "Fact Sheets",
                        "alias": "news-center/publications/fact-sheets"
                    },
                    "af5d8277-de6e-4c1d-bbdb-d230e3485eca": {
                        "key": "af5d8277-de6e-4c1d-bbdb-d230e3485eca",
                        "title": "Basin Today",
                        "alias": "news-center/publications/basin-today"
                    }
                }
            },
            "0d0bce26-6e5c-460b-95a8-ba070a042199": {
                "key": "0d0bce26-6e5c-460b-95a8-ba070a042199",
                "title": "Industry Links",
                "alias": "news-center/industry-links"
            },
            "2cd6a57b-1ae6-4905-878b-1edc9c489f0b": {
                "key": "2cd6a57b-1ae6-4905-878b-1edc9c489f0b",
                "title": "Bakken News",
                "alias": "news-center/bakken-news"
            },
            "7f9cdb79-6475-4d54-92ba-210bc86ca421": {
                "key": "7f9cdb79-6475-4d54-92ba-210bc86ca421",
                "title": "Events",
                "alias": "news-center/events"
            },
            "9c4f2fa1-3c96-4996-bde2-4473d5a1508d": {
                "key": "9c4f2fa1-3c96-4996-bde2-4473d5a1508d",
                "title": "News Releases",
                "alias": "news-center/news-releases"
            },
            "30a4f940-a567-4b12-ac4d-3967b5dbebe0": {
                "key": "30a4f940-a567-4b12-ac4d-3967b5dbebe0",
                "title": "News Briefs",
                "alias": "news-center/news-briefs"
            },
            "959d1772-fa38-4d78-b900-a3aa48ab9d4c": {
                "key": "959d1772-fa38-4d78-b900-a3aa48ab9d4c",
                "title": "In the News",
                "alias": "news-center/news"
            }
        }
    },
    {
        "key": "b04c36b1-5382-4a2c-9a10-7615a83d25e0",
        "title": "Projects",
        "alias": "projects",
        "below": {
            "4e70a888-4497-4f3f-b64d-2ff36eebfaaf": {
                "key": "4e70a888-4497-4f3f-b64d-2ff36eebfaaf",
                "title": "Antelope Valley Station to Neset",
                "alias": "projects/antelope-valley-station-neset-transmission"
            },
            "8758c856-25ac-4e2f-a99a-a4837a30bad1": {
                "key": "8758c856-25ac-4e2f-a99a-a4837a30bad1",
                "title": "Urea Production Facility",
                "alias": "projects/urea-production-facility"
            }
        }
    },
    {
        "key": "f79b5411-a6f6-4160-9000-ea3a992cef87",
        "title": "Facilities",
        "alias": "facilities",
        "below": {
            "bff146b9-9f48-4724-a334-f6abe9950f6b": {
                "key": "bff146b9-9f48-4724-a334-f6abe9950f6b",
                "title": "Antelope Valley Station",
                "alias": "Facilities/Antelope-Valley"
            },
            "8af62eaa-f266-43c5-989e-38c91d38aef3": {
                "key": "8af62eaa-f266-43c5-989e-38c91d38aef3",
                "title": "Transmission",
                "alias": "Facilities/Transmission",
                "below": {
                    "3c8824cb-4728-469d-a23f-4737468ba7fa": {
                        "key": "3c8824cb-4728-469d-a23f-4737468ba7fa",
                        "title": "FERC Standards of Conduct",
                        "alias": "facilities/transmission/ferc-standards-conduct"
                    },
                    "261214a5-92dc-4149-b47f-b402e4e5295b": {
                        "key": "261214a5-92dc-4149-b47f-b402e4e5295b",
                        "title": "Common Use System",
                        "alias": "Facilities/Transmission/Common-Use-System"
                    },
                    "dcbefe7f-8377-4b68-b87f-b0830efed60a": {
                        "key": "dcbefe7f-8377-4b68-b87f-b0830efed60a",
                        "title": "Southwest Power Pool - Upper Missouri Zone",
                        "alias": "Facilities/Transmission/SPP-Upper-Missouri-Zone"
                    },
                    "85e638d2-52e5-45b3-bddd-98cc1b2bc1b1": {
                        "key": "85e638d2-52e5-45b3-bddd-98cc1b2bc1b1",
                        "title": "MBPP Transmission",
                        "alias": "Facilities/Transmission/MBPP-Transmission"
                    },
                    "4f28a9d5-5376-4f68-b0a6-7b2574c3faf4": {
                        "key": "4f28a9d5-5376-4f68-b0a6-7b2574c3faf4",
                        "title": "Regulation",
                        "alias": "Facilities/Transmission/Regulation"
                    },
                    "d0e2a823-51a3-465a-ada0-934522010d11": {
                        "key": "d0e2a823-51a3-465a-ada0-934522010d11",
                        "title": "Vegetation Management Program",
                        "alias": "Facilities/Transmission/Vegetation-Management-Program"
                    }
                }
            },
            "ff27d42e-d36a-487c-91d6-12802651dbc8": {
                "key": "ff27d42e-d36a-487c-91d6-12802651dbc8",
                "title": "Culbertson Station",
                "alias": "Facilities/Culbertson"
            },
            "fd5c278f-631a-474f-9337-9df2c843c54f": {
                "key": "fd5c278f-631a-474f-9337-9df2c843c54f",
                "title": "Deer Creek Station",
                "alias": "Facilities/Deer-Creek"
            },
            "e962b977-0bdd-4395-999e-c5514e98cb91": {
                "key": "e962b977-0bdd-4395-999e-c5514e98cb91",
                "title": "Dry Fork Station",
                "alias": "Facilities/Dry-Fork"
            },
            "0b22b675-1497-4e45-916b-0a3f7d09d24b": {
                "key": "0b22b675-1497-4e45-916b-0a3f7d09d24b",
                "title": "Wisdom Generating Station",
                "alias": "Facilities/Wisdom"
            },
            "0088224e-4de6-4683-83a8-90e08a2530a9": {
                "key": "0088224e-4de6-4683-83a8-90e08a2530a9",
                "title": "Groton Generation Station",
                "alias": "Facilities/Groton"
            },
            "36c63237-d769-4cb5-82f6-d6660b36542d": {
                "key": "36c63237-d769-4cb5-82f6-d6660b36542d",
                "title": "Laramie River Station",
                "alias": "Facilities/Laramie-River"
            },
            "22d259c4-3a25-4b95-b36e-6b8c5bd854d3": {
                "key": "22d259c4-3a25-4b95-b36e-6b8c5bd854d3",
                "title": "Leland Olds Station",
                "alias": "Facilities/Leland-Olds"
            },
            "3fc01d2b-ce4c-4746-b76b-24809be56f91": {
                "key": "3fc01d2b-ce4c-4746-b76b-24809be56f91",
                "title": "Lonesome Creek Station",
                "alias": "Facilities/Lonesome-Creek"
            },
            "e1aaf37b-3606-4844-8f21-a613da988d74": {
                "key": "e1aaf37b-3606-4844-8f21-a613da988d74",
                "title": "Pioneer Generation Station",
                "alias": "Facilities/Pioneer"
            },
            "e9e3aefb-81e1-4658-b55d-b7d43208bdd4": {
                "key": "e9e3aefb-81e1-4658-b55d-b7d43208bdd4",
                "title": "Recovered Energy Generation",
                "alias": "Facilities/Recovered-Energy"
            },
            "79a1f3ec-91e1-447f-a7a4-c7932b4ed9b9": {
                "key": "79a1f3ec-91e1-447f-a7a4-c7932b4ed9b9",
                "title": "Spirit Mound Station",
                "alias": "Facilities/Spirit-Mound"
            },
            "70264be6-9894-493a-9698-e9b30d6172cb": {
                "key": "70264be6-9894-493a-9698-e9b30d6172cb",
                "title": "Wyoming Distributed Generation",
                "alias": "Facilities/WY-Distributed-Gen"
            },
            "d5215882-454c-416f-98cf-1635ed4a3faa": {
                "key": "d5215882-454c-416f-98cf-1635ed4a3faa",
                "title": "PrairieWinds 1 and Minot Wind",
                "alias": "Facilities/PrairieWinds-1"
            },
            "51b6d4f6-f430-4011-846b-d8fe034fe86a": {
                "key": "51b6d4f6-f430-4011-846b-d8fe034fe86a",
                "title": "Crow Lake Wind",
                "alias": "Facilities/Crow-Lake-Wind"
            },
            "fd8ef763-888c-485a-8a2e-f5a53c059a2c": {
                "key": "fd8ef763-888c-485a-8a2e-f5a53c059a2c",
                "title": "Wind",
                "alias": "facilities/wind"
            },
            "1ff85af2-711b-4d71-8f45-807c2a8525c5": {
                "key": "1ff85af2-711b-4d71-8f45-807c2a8525c5",
                "title": "Facility Contacts",
                "alias": "facilities/facility-contacts"
            }
        }
    },
    {
        "key": "147aa657-8cd2-4aa3-9344-f5dd40a8348f",
        "title": "Environment",
        "alias": "environment",
        "below": {
            "ce22bfd2-a895-449f-9f94-afe0d9dbb6ff": {
                "key": "ce22bfd2-a895-449f-9f94-afe0d9dbb6ff",
                "title": "Commitment and Compliance",
                "alias": "environment/commitment-and-compliance"
            },
            "b5d37fde-9b8f-4661-9462-c99b1bc65cde": {
                "key": "b5d37fde-9b8f-4661-9462-c99b1bc65cde",
                "title": "Reclamation",
                "alias": "Environment/Reclamation"
            },
            "35f104de-98bf-4dfd-86c1-6a33e4ec9b4e": {
                "key": "35f104de-98bf-4dfd-86c1-6a33e4ec9b4e",
                "title": "Energy Policy",
                "alias": "environment/energy-policy"
            },
            "41b6189a-4536-4499-9fa7-aa1d80d070e3": {
                "key": "41b6189a-4536-4499-9fa7-aa1d80d070e3",
                "title": "Coal Combustion Residuals (CCR) Rule Compliance Data and Information",
                "enabled": true
            }
        }
    },
    {
        "key": "555a63ca-f480-4a65-b2a0-3bb823d27d34",
        "title": "Contacts",
        "alias": "contacts",
        "below": {
            "32a19135-782f-4504-97b8-b1a581a4114f": {
                "key": "32a19135-782f-4504-97b8-b1a581a4114f",
                "title": "Bismarck Map",
                "alias": "contact-us/bismarck-map"
            },
            "176ae455-1e70-4f4f-bd92-24cbb78a497b": {
                "key": "176ae455-1e70-4f4f-bd92-24cbb78a497b",
                "title": "Driving Directions",
                "alias": "contact-us/driving-directions"
            },
            "3489841c-5ed0-4fdd-9c04-2058d3fe65d7": {
                "key": "3489841c-5ed0-4fdd-9c04-2058d3fe65d7",
                "title": "Human Resources Contact",
                "alias": "contact-us/human-resources-contact"
            },
            "1216245d-f149-42a9-91ae-d34573066d54": {
                "key": "1216245d-f149-42a9-91ae-d34573066d54",
                "title": "Media Contact",
                "alias": "contacts/media-contact"
            }
        }
    }
]

export default rawJSONFromDrupalApi