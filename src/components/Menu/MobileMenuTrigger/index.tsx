import * as React from "react"

export const MobileMenuTrigger: React.StatelessComponent<IProps> = (props) => (
    <div className="mobile-menu-trigger-wrapper" style={{display: props.display ? "block" : "none"}}>
        <div className="mobile-menu-trigger" title="Menu Button" onClick={props.onClick}>
            <span />Menu
        </div>
    </div>
)

export interface IProps {
    display: boolean,
    onClick: () => void
}
