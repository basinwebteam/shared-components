import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withKnobs, number } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'

import Menu from "../Menu"
import "../Menu/style.scss"
import {MenuMockData} from "./__tests__/mocks/menuMockData"

storiesOf("Menu", module)
    .addDecorator(withKnobs)
    .add("with width property",
        withInfo('Links is an array of menu link data. Width property adjusts when to change between mobile & desktop menus. A window resize event is required to make this switch.')(() => {
            const widthToChange = number("Change to Desktop Width (requires resizing frame/window)", 300)
            return (
                <Menu 
                    links={MenuMockData}
                    widthToChange={widthToChange}
                />
            )
        })
    )
    .add("with link handler property, no width property",
        withInfo('Links is an array of menu link data. LinkHandler accepts a function to call when a link is clicked. This example outputs data to the console.')(() => {
            return (
                <Menu
                    links={MenuMockData}
                    linkHandler={menuLinkHandler}
                />
            )
        })
    )


/**
 * HELPERS
 */

const menuLinkHandler = (url: string, key: string, e: React.MouseEvent<HTMLElement>) => {
  console.log(url)
  console.log(key)
}
  