import * as moment from "moment"
import * as React from "react"

export const Event: React.SFC<IEventProps> = ({
    id,
    date,
    title,
    location="",
    description=""
}) => {
    const mdate = moment(date)
    return (
        <div className="row event-item" id={`id${id}`}>
            <div className="small-12 large-2 columns">
                <div className="event-date-box">
                    <span className="event-month">{mdate.format("MMM")}</span>
                    <span className="event-day">{mdate.format("D")}</span>
                </div>
            </div>
            <div className="small-12 large-10 columns">
                <p className="event-title">{title}</p>
                <p className="event-date-string">{mdate.format("ddd, MMM D, YYYY")}</p>
                {location!=="" && <p className="event-location">{location}</p>}
                {description!=="" && <p className="event-desc" dangerouslySetInnerHTML={{__html: description}} />}
            </div>
        </div>
    )
}

export default Event

export interface IEventProps {
    id: string,
    date: string,
    title: string,
    location?: string,
    description: string,
    link?: string,
}
