import {IEventProps} from "../../Event"

export const data = [
    {
        id: "2636",
        title: "Iowa Association of Electric Cooperatives Annual Meeting",
        location: "West Des Moines, IA",
        date: "2017-11-30T00:00:00",
        description: "<div>Registration&#58; 8 AM</div><div>Business Meeting&#58; 8&#58;45 AM</div><div>Breakfast is provided</div>"
    },
    {
        id: "3007",
        title: "NREA Annual Meeting",
        location: "Kearney, NE",
        date: "2017-11-30T00:00:00",
        description: "<div>For more information, visit their Website&#58;<br><a href=\"http&#58;//nrea.org/\">http&#58;//nrea.org/</a></div>"
    },
    {
        id: "2999",
        title: "MWECA Annual Meeting",
        location: "Denver, CO",
        date: "2017-12-12T00:00:00",
        description: "<div>Register Online&#58; <a href=\"http&#58;//meconsumers.com/registration/\">http&#58;//meconsumers.com/registration/</a></div>"
    },
    {
        id: "3004",
        title: "South Dakota Rural Electric Association Annual Meeting",
        location: "Pierre, SD",
        date: "2018-01-11T00:00:00",
        description: "<div>Register with Karla Steel at <a href=\"mailto&#58;karla.steele@sdrea.coop\">karla.steele@sdrea.coop</a></div>"
    },
    {
        id: "2674",
        title: "NDAREC Annual Meeting",
        location: "Bismarck, ND",
        date: "2018-01-16T00:00:00",
        description: "<div>TBD</div>"
     }
] as IEventProps[]

export default data
