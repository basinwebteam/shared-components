import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import CalendarFeed from '../CalendarFeed'
import data from "./__tests__/mocks/calendarFeed"
import "./style.scss"

storiesOf("Calendar Feed", module)
    .add("Without Links",
    withInfo("A calendar feed from SharePoint or another source displayed as a verticla list. Has a mobile and desktop style.")(() => {
        return (
            <CalendarFeed events={data} />
        )
    })
    )
    .add("With Links",
    withInfo("A calendar feed from SharePoint or another source displayed as a verticla list. Has a mobile and desktop style.")(() => {
        return (
            <CalendarFeed events={data.map((d) => ({...d, link: "/"}))} />
        )
    })
    )
