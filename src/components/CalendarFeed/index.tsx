import * as React from "react"
import { Event, IEventProps } from "./Event"

export const CalendarFeed: React.SFC<ICalendarFeedProps> = ({events, className=""}) => {
    const wrapperClassName = "cal-holder" + className
    return (
        <div className={wrapperClassName}>
            <div className="calendarList">
                {displayEvents(events)}
            </div>
        </div>
    )
}

CalendarFeed.displayName = "CalendarFeed"

export default CalendarFeed

export const displayEvents = (events: IEventProps[]) => (
    events.map((event, i) => {
        if (event.link && event.link !== "") {
            return (
                <a href={event.link}>
                    <Event
                        key={event.id + i}
                        id={event.id}
                        date={event.date}
                        title={event.title}
                        location={event.location}
                        description={event.description}
                    />
                </a>
            )
        } else {
            return (
                <Event
                    key={event.id + i}
                    id={event.id}
                    date={event.date}
                    title={event.title}
                    location={event.location}
                    description={event.description}
                />
            )
        }
    })
)

export interface ICalendarFeedProps {
    events: IEventProps[]
    className?: string
}