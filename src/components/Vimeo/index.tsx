import * as React from "react"

export const VimeoVideo: React.StatelessComponent<IVimeoProps> = (props) => (
    <div className="video-vimeo" style={styles.videoWrapper}>
        <iframe
            style={styles.iframe}
            allowFullScreen={true}
            frameBorder="0"
            height="281"
            src={props.link}
            width="500"
        />
    </div>
)

const styles = {
    videoWrapper: {
        position: "relative",
        paddingBottom: "56.25%", /* 16:9 */
        paddingTop: "25px",
        height: 0,
    } as React.CSSProperties,
    iframe: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
    } as React.CSSProperties,
}

export default VimeoVideo

export interface IVimeoProps {
    link: string
}
