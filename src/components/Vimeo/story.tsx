import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import { withKnobs, text } from '@storybook/addon-knobs'
import Vimeo from "../Vimeo"

const description = `Displays a responsive Vimeo video. It does not do anything special with the link. It requires the link to be the same link Vimeo gives you to embed it.`

storiesOf("Vimeo", module)
    .addDecorator(withKnobs)
    .add("Video",
        withInfo(`${description}`)(() => {
            const link = text("Link", "https://player.vimeo.com/video/248021362?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;autopause=0&amp;player_id=0")
            return (
                <div style={{maxWidth: "1000px"}}>
                    <Vimeo
                        link={link}
                    />
                </div>
            )
        })
    )
