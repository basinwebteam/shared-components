# Components

These are generic, re-usable components that should work with React in any platform.

* Components are created using TypeScript.
* Styles are created using Sass.

Components should be strictly presentational and named index.tsx or consist of a *presentation* & a *container* directory.

*Presentation* - Refers to a component without a state. It only receives values passed to it. It does not store & later change values. A designer without coding/developer knowledge should be able to make sense out of these components & modify them.

*Container* - Refers to a component with a state. It can receive values passed to it, store values & modify those values. A typical pattern would be to create a container component with one or more presentation components within the render method. The container component would handle items like HTTP requests and the state of itself &/or child components. These require at least some coding/developer knowledge.

## Stories

For every component created in this section, please create at least one corresponding story in `/stories/` that explains how to use it.

## Tests

TODO: Create tests for each component.

## Updating

If you update the code in here, please update the version number in `/package.json`. Please follow the *Semver* standard.

* Bug fixes and other minor changes: Patch release, increment the last number, e.g. 1.0.1.
  * *Example: Found a typo, fixed a bug that doesn't require altering code that uses the component*
* New features which don't break existing features: Minor release, increment the middle number, e.g. 1.1.0.
  * *Example: Added a new component*
* Changes which break backwards compatibility: Major release, increment the first number, e.g. 2.0.0.
  * *Example: Any code that uses the new version needs to thoroughly be tested & probably altered*
