import * as React from "react"

export const TopicBox: React.StatelessComponent<IProps> = ({
    title,
    className = "",
    image = "",
    paragraph = "",
    color = "blue",
    url = null
}) => {

    if(url === null) {
        return (
            <div className={"column box "+ className}>
                <div className={"box-title title-" + color}>{title}</div>
                {image}
                {paragraph}
            </div>
        )            
    } else {
        return (
            <div className={"column box "+ className}>
                <a href={url}>
                    <div className={"box-title title-" + color}>{title}</div>
                    {image}
                </a>
                {paragraph}
            </div>
        )
    }
}


TopicBox.displayName = "TopicBox"

export default TopicBox

export interface IProps {
    title: string,
    url?: string,
    color?: string,
    className?: string,
    image?: any,
    paragraph?: any,
}
