import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'
import TopicBox from "../TopicBox"
import "./style.scss"

const description = `Shows a Topic Box. `

storiesOf("Topic Box", module)
  .addDecorator(withKnobs)
  .add("without image", 
    withInfo(`${description} This is the default topic box.`)(() => {
      const title=text("Title", "Test Topic Box")
      const color=text("Color", "blue")
      const paragraph = <div>Some text.. Lorem ipsum...</div>
      const className = text("Class", "medium-12")
      return (
        <div style={{maxWidth: "400px"}}>
          <TopicBox
            title={title}
            color={color}
            className={className}
            paragraph={paragraph}
          />
        </div>
      )
    })
  )
  .add("with image", 
    withInfo(`${description} This is the default topic box.`)(() => {
      const title=text("Title", "Test Topic Box")
      const color=text("Color", "blue")
      const paragraph = <div>Some text.. Lorem ipsum...</div>
      const className = text("Class", "medium-12")
      const image = <img src="https://basinelectric.com/sites/CMS/files/images/home-ads/Wilton-Wind-Farm-325px.jpg" />
      return (
        <div style={{maxWidth: "400px"}}>
          <TopicBox
            title={title}
            color={color}
            className={className}
            paragraph={paragraph}
            image={image}
          />
        </div>
      )
    })
  )
    .add("multiple", 
    withInfo(`${description} This is the default topic box.`)(() => {
      const title=text("Title", "Test Topic Box")
      const color=text("Color", "blue")
      const paragraph = <div>Some text.. Lorem ipsum...</div>
      const className = text("Class", "medium-4")
      const image = <img src="https://basinelectric.com/sites/CMS/files/images/home-ads/Wilton-Wind-Farm-325px.jpg" />
      return (
        <div className="row topic-box-row">
          <TopicBox
            title={title}
            color={color}
            className={className}
            paragraph={paragraph}
            image={image}
          />
          <TopicBox
            title={title}
            color={color}
            className={className}
            paragraph={paragraph}
            image={image}
          />
          <TopicBox
            title={title}
            color={color}
            className={className}
            paragraph={paragraph}
            image={image}
          />
        </div>
      
      )
    })
  )
