import { AxiosResponse, default as axios } from "axios"
import * as React from "react"
import Image from "../Image"
import TopicBox from "../../components/TopicBox"
import { GenerationDataPlants } from "../EnergyPortfolioPieChart"

export const EnergyPortfolioResourceDetails: React.SFC<IEPResourceDetailsProps> = ({ name, photo, plants, briefInfo, videos }) => (
    <div className="aside">
        <h2 className="paragraph-title">{`${name}`}</h2>
        <div className="aside-body">
            {photo}
            <div>
                {createPlantsContent(plants, name)}
                {name === "Resource Details" && (<p>Click a resource on the pie chart to view details.</p>)}
                {briefInfo}
                {videos}
            </div>
        </div>
    </div>
)

export function createPlantsContent(plants: GenerationDataPlants[], name: string) {
    if (plants.length > 0) {
        return (
            <div>
                <h3 style={{ marginTop: "1em" }}>{`${name} Facilities`}</h3>
                <div className="aside-body">
                    <ul>
                        {plants.map((p, i) => (
                            <li key={i}>
                                <a href={p.url}>{p.name}</a>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        )
    }
    return ""
}

export default EnergyPortfolioResourceDetails

export interface IEPResourceDetailsProps {
    name: string,
    photo: JSX.Element | string,
    videos: JSX.Element | string,
    plants: GenerationDataPlants[]
    briefInfo: JSX.Element | string
}