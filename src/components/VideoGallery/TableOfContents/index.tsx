import * as React from "react"

export const VideoTOC: React.StatelessComponent<IVideoTOCProps> = (props) => {
    return (
        <div className="videoFeed">
            {props.playlists.map((item, i) =>
                <a key={i} className="button expand" href={`#playlist-${item.id}`}>{item.title}</a>
            )}
        </div>
    )
}

VideoTOC.displayName = "VideoTOC"

export default VideoTOC

export interface IVideoTOCProps {
    playlists: IPlaylist[]
}

export interface IPlaylist {
    id: string,
    title: string,
}
