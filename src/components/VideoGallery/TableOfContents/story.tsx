import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import TableOfContents from "../TableOfContents"
import "./style.scss"

const description = `Displays a playlist `

storiesOf("Video Gallery", module)
    .add("Table of Contents", 
        withInfo(`${description}`)(() => {
            const playlists = [
                {
                    id: "abc",
                    title: "Financials"
                },
                {
                    id: "cbd",
                    title: "Annual Meeting",
                },
                {
                    id: "xyz",
                    title: "Community"
                }
            ]

            return (
                <TableOfContents
                    playlists={playlists}
                />
            )
        })
    )
