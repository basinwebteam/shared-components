import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import Playlist from "../Playlist"
import "./style.scss"
import "../Thumbnail/style.scss"
import {playlistMockData} from "../Playlist/__tests__/mocks"

const description = `Displays a playlist `

storiesOf("Video Gallery", module)
    .add("Playlist", 
        withInfo(`${description}`)(() => {
            const {instance, title, videos, playVideoClickHandler} = playlistMockData
            return (
                <Playlist
                    instance={instance}
                    title={title}
                    videos={videos}
                    playVideoClickHandler={playVideoClickHandler}
                />
            )
        })
    )
