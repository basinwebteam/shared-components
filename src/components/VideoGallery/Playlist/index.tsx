import * as React from "react"
import Thumbnail from "../Thumbnail"

export const Playlist: React.StatelessComponent<IVideoPlaylistProps> = (props) => {
    const videos = props.videos.map((video) => ({
        playVideoClickHandler: props.playVideoClickHandler,
        ...video
    }))

    return (
        <div className="videoList" id={"playlist-" + props.instance}>
            <h2>{props.title}</h2>
            <ul className="vid-gal-list-item block-grid-4">
                {videos.map((video, i) =>
                    <Thumbnail
                        key={props.instance.toString() + i.toString()}
                        id={video.id}
                        title={video.title}
                        url={video.url}
                        thumb={video.thumb}
                        playVideoClickHandler={video.playVideoClickHandler}
                    />
                )}
                {/* <li key={props.videos.length} className="show">
                    <div className="js-show button" role="button" onClick={props.showMorePlaylistClickHandler}>Show More +</div>
                </li> */}
            </ul>
        </div>
    )
}

Playlist.displayName = "Playlist"

export default Playlist

export interface IVideoPlaylistProps {
    instance: number,
    title: string,
    videos: IVideo[],
    playVideoClickHandler: (videoId: string) => void,
    showMorePlaylistClickHandler?: () => void,
}

export interface IVideo {
    id: string,
    title: string,
    url: string,
    thumb: string,
}
