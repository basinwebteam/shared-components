export const videos = [
    {
        id: "Ak6xkdY_1Jo",
        url: "Ak6xkdY_1Jo",
        title: "Wind subsidiaries to merge into Basin Electric",
        thumb: "https://i.ytimg.com/vi/Ak6xkdY_1Jo/mqdefault.jpg",
    },
    {
        id: "nejsNW5pUOQ",
        url: "nejsNW5pUOQ",
        title: "Basin Electric and subsidiary boards approve 2018 budgets",
        thumb: "https://i.ytimg.com/vi/nejsNW5pUOQ/mqdefault.jpg",
    },
    {
        id: "GE4eLR-U1Bw",
        url: "GE4eLR-U1Bw",
        title: "CFO report: Steve Johnson, Basin Electric 2017 Annual Meeting",
        thumb: "https://i.ytimg.com/vi/GE4eLR-U1Bw/mqdefault.jpg",
    },
    {
        id: "IhGrFzVMHsw",
        url: "IhGrFzVMHsw",
        title: "Matt Washburn of NIPCO on keeping Basin Electric margins stable",
        thumb: "https://i.ytimg.com/vi/IhGrFzVMHsw/mqdefault.jpg",
    },
    {
        id: "y_l2ZscFwaw",
        url: "y_l2ZscFwaw",
        title: "Basin Electric looks into impacts of 2018 U.S. Fiscal budget and more",
        thumb: "https://i.ytimg.com/vi/y_l2ZscFwaw/mqdefault.jpg",
    }
]

export const playlistMockData = {
    instance: 1,
    title:  "Video Playlist",
    videos,
    playVideoClickHandler: (id: string) => {console.log(id)}
}
