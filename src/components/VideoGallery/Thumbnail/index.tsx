import * as React from "react"

export const Thumbnail: React.StatelessComponent<IProps> = ({id, title, url, thumb, playVideoClickHandler}) => {
    const clickHandler = () => playVideoClickHandler(url)

    return (
        <li key={id}>
            <a className="vid-gal-list-link rel-vid-item" onClick={clickHandler} href="#videoPlayer">
                <div className="rel-vid-icon" aria-label="Play Button" />
                <div className="rel-vid-thumb">
                    <img
                        data-source={thumb}
                        src={thumb}
                    />
                    <small>{title}</small>
                </div>
            </a>
        </li>
    )
}

Thumbnail.displayName = "Thumbnail"

export default Thumbnail

export interface IProps {
    id: string,
    title: string,
    url: string,
    thumb: string,
    playVideoClickHandler: (videoId: string) => void,
}