import * as React from "react"

export const Button: React.StatelessComponent<IProps> = (props) => {
    const className = (props.className) ? props.className : "default"

    return (
        <div>
            <a href={props.url} className={"button " + className}>{props.title}</a>
        </div>
    )
}


Button.displayName = "Button"

export default Button

export interface IProps {
    url: string,
    title: string,
    className?: string,
}
