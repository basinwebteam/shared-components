import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'
import Button from "../Button"
import "./style.scss"

const description = `Shows a button. `

storiesOf("Button", module)
  .addDecorator(withKnobs)
  .add("without additional class", 
    withInfo(`${description} This is the default button.`)(() => {
      const title=text("Title", "Default Button")
      const url=text("URL", "//basinelectric.com/static/img/logos/bepc-header-logo.gif")
      
      return (
        <Button
          title={title}
          url={url}
        />
      )
    })
  )
  .add("with alert class", 
    withInfo(`${description} This is an alert butotn.`)(() => {
      const title=text("Title", "Alert Button")
      const url=text("URL", "//basinelectric.com/static/img/logos/bepc-header-logo.gif")
      const className=text("Class", "alert")
      
      return (
        <Button
          title={title}
          url={url}
          className={className}
        />
      )
    })
  )
  .add("with success class", 
    withInfo(`${description} This is an success butotn.`)(() => {
      const title=text("Title", "Success Button")
      const url=text("URL", "//basinelectric.com/static/img/logos/bepc-header-logo.gif")
      const className=text("Class", "success")
      
      return (
        <Button
          title={title}
          url={url}
          className={className}
        />
      )
    })
  )     