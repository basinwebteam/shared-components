import * as React from "react"

export class EnergyPortfolioInfo extends React.Component<IPropsEnergyPortfolioInfo, IPropsEnergyPortfolioInfo> {
    constructor(props: IPropsEnergyPortfolioInfo){
        super(props)
    }
    /** 
     * Create Fake Data version of Energy Portfolio. Use old data. Create as a block. Have Tracie update it. Use to test.
     */

    render() {
        return `<a href="#js-at-a-glance-chart" class="button hide-for-large-up">Back to chart</a>
		    <div class="contentBlock fuel-main-info">
			    <div class="aside-block">
				    <div class="aside-header">${longName}</div>${(photo)?`<img src="${photo}" class="fuel-image">`:``}
				    <div class="aside-body">
                        ${briefInfo}
				    </div>
			    </div>
			    <div class="aside-block fuel-plant-info ${plants.length==0?'hide':''}">
				    <div class="aside-header">${name} Plants</div>
				    <div class="aside-body">
					    <ul>
                            ${plantsHTMLList}
					    </u>
				    </div>
			    </div>
			    <div class="aside-block fuel-video-info ${videoIDs.length == 0? 'hide' : ''}"">
				    <div class="aside-header">${name}  Videos </div>
				    <div class="videoPlaylist"></div>
			    </div>
		    </div>`
    }

    componentDidMount(){

    }

}

export interface IPropsEnergyPortfolioInfo {

}
export interface IPropsEnergyPortfolioInfo {

}