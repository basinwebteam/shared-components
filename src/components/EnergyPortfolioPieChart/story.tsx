import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'
import EnergyPortfolioPieChart from "../EnergyPortfolioPieChart"
import {data as genData, renewablesData} from "./__tests__/mocks/mockData"
// import "./style.scss"

const description = `Displays an Energ yPortfolio Pie Chart `

storiesOf("EnergyPortfolioPieChart", module)
    .addDecorator(withKnobs)
    .add("Pie Chart Only", 
        withInfo(`${description} with a caption.`)(() => {
            // const totalMW=text("Total MW", "6,302")

            return (
                <EnergyPortfolioPieChart
                    size={[500,500]}
                    generationData={genData}
                    generationRenewableData={renewablesData}
                    clickHandler={clickHandler}
                />
            )
        })
    )

function clickHandler(n: string){
    console.log(n)
}