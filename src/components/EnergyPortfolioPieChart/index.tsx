//tslint:disable TS6133
declare let window: IWindow

import * as React from "react"
import { scaleLinear, scaleOrdinal, ScaleOrdinal } from 'd3-scale'
import { arc, pie, DefaultArcObject } from "d3-shape"
import { format } from "d3-format"
import { max } from 'd3-array'
import { select } from 'd3-selection'
import { SlicePosition } from "./SlicePosition"
import { Image } from "../Image"

export class EnergyPortfolioPieChart extends React.Component<IPropsEnergyPortfolio> {
    node: SVGElement
    basinColors = [
        [
            '#D22630',
            '#dc575f',
            '#009FDF',
            '#ED8B00',
            '#004967',
            '#78BE20',
            '#b3b5b6',
            '#4b4d4f',
        ],
        [
            '#86181f',
            '#985900',
            '#4d7a14',
            '#00668f',
        ]
    ]
    generationPieColor = scaleOrdinal().range(this.basinColors[0])
    generationRenewablesPieColor = scaleOrdinal().range(this.basinColors[1])
    basinColorsCombined = this.basinColors[0].concat(this.basinColors[1])
    generationColorsCombined = scaleOrdinal().range(this.basinColorsCombined)
    textStyle = {
        'font-family': 'nimbus-sans, Helvetica, Arial, sans-serif',
        'font-weight': 'bold',
    }
    totalMW: number
    chartRadius: number
    renewablePieEndAngle: number
    orderedGenData: GenerationData[] = new Array()

    constructor(props: IPropsEnergyPortfolio) {
        super(props)
        this.orderResources()
        this.totalMW = this.orderedGenData.reduce((a, b) => a + b.mw, 0)
        this.renewablePieEndAngle = Math.round(this.props.generationRenewableData[0].mw / this.totalMW * 360)
        this.chartRadius = Math.min(this.props.size[0], this.props.size[1]) / 2.4
    }

    render() {
        return (
            <div>
                <div className="small-12 column">
                    <svg
                        ref={node => this.node = node!}
                        width={this.props.size[0]}
                        height={this.props.size[1]}
                        style={{ cursor: "pointer" }}
                    />
                </div>
                <div className="small-12 column" id="js-at-a-glance-chart-legend">
                    <div />
                    <ul />
                </div>
                <div className="small-12 column">
                    <p><strong>Total = {format(".1f")(this.totalMW)} MW</strong></p>
                </div>
            </div>
        )
    }
    componentDidMount() {
        this.createChart()
    }

    orderResources() {
        [
            "Wind",
            "Recovered",
            "Coal",
            "Hydro",
            "Nuclear",
            "Natural",
            "Oil",
            "Unspecified"
        ].forEach(resource => this.addResource(resource))
    }

    addResource(name: string) {
        const resource = this.props.generationData.find(d => d.name.startsWith(name))
        if (resource !== undefined) {
            this.orderedGenData.push(resource)
        }
    }

    createChart() {
        const node = this.node

        let generationArc = arc()
            .outerRadius(this.chartRadius - 10)
            .innerRadius(0)

        let generationRenewableArc = arc()
            .outerRadius(this.chartRadius - 50)
            .innerRadius(0)

        let generationPie: any = pie()
            .sort(null)
            .value((data: any) => data.mw)

        // Create a partial pie to lay over generationPie
        let generationRenewablePie: any = pie()
            .sort(null)
            .value((data: any) => data.mw)
            .startAngle(0)
            .endAngle(this.renewablePieEndAngle * (Math.PI / 180))

        let generationSVG = select(node)
            //following helps make chart responsive
            .attr('viewBox', `20 0 ${this.props.size[0]} ${this.props.size[1]} `)
            .attr('preserveAspectRatio', 'xMinYMin meet')
            .append('g')
            .attr('transform', `translate( ${this.props.size[0] / 2}, ${this.props.size[1] / 2} )`)

        let generationPieSlices = generationSVG.selectAll('.generationSlice')
            .data(generationPie(this.orderedGenData))
            .enter()
            .append('g')
            .attr('class', 'generationSlice')


        generationPieSlices.append("path")
            .attr('d', generationArc)
            .style("fill", (d: any): string => this.generationPieColor(d.data.name) as string)

        function translateByNameHack(d: any, axis: string, line: number): number {
            if (d.data.name == 'Oil' && axis == 'x' && line == 1) { return -25 }
            if (d.data.name == 'Oil' && axis == 'x' && line == 2) { return -15 }
            if (d.data.name == 'Oil' && axis == 'y' && line == 1) { return 3 }
            if (d.data.name == 'Unspecified' && axis == 'x' && line == 2) { return 23 }
            return 0
        }

        const generationPieSliceLine1Attributes: { [key: string]: any } = {
            "transform": function (d: any) {
                let slicePosition: SlicePosition = new SlicePosition(d.startAngle, d.endAngle)
                let yPaddingAmount: number = slicePosition.returnValue({
                    'top': () => -24,
                    'bottom': () => 0,
                    'left': () => 0,
                    'right': () => 0,
                })
                let c = generationArc.centroid(d)
                return "translate(" + (c[0] * 2.1 + (5) + (translateByNameHack(d, 'x', 1))) + "," + (c[1] * 2.1 + yPaddingAmount + (translateByNameHack(d, 'y', 1))) + ")"
            },
            "text-anchor": function (d: any) {
                let slicePosition: SlicePosition = new SlicePosition(d.startAngle, d.endAngle)
                return slicePosition.returnValue({
                    'top': () => 'middle',
                    'bottom': () => 'middle',
                    'left': () => 'end',
                    'right': () => 'start',
                })
            },
            "dy": ".35em",
        }

        //Small Devices
        generationPieSlices.append("text")
            .attr("transform", generationPieSliceLine1Attributes.transform)
            .attr("text-anchor", generationPieSliceLine1Attributes["text-anchor"])
            .attr("dy", generationPieSliceLine1Attributes.dy)
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .classed('hide-for-medium-up', true)
            .text((d: any) => {
                return this.round10((d.data.mw / this.totalMW) * 100, -1) + '%'
            })
        //Larger Devices
        generationPieSlices.append("text")
            .attr("transform", generationPieSliceLine1Attributes.transform)
            .attr("text-anchor", generationPieSliceLine1Attributes["text-anchor"])
            .attr("dy", generationPieSliceLine1Attributes.dy)
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .classed('hide-for-small-only', true)
            .text(function (d: any) {
                return d.data.name
            })

        generationPieSlices.append("text")
            .attr(
                "transform", (d: any) => {
                    let mwPercentage: string = this.round10((d.data.mw / this.totalMW) * 100, -1) + '%'
                    //Get Y Padding
                    let slicePosition: SlicePosition = new SlicePosition(d.startAngle, d.endAngle)
                    let yPaddingAmount: number = slicePosition.returnValue({
                        'top': () => -24,
                        'bottom': () => 0,
                        'left': () => 0,
                        'right': () => 0,
                    })
                    //Get X Padding
                    let xPaddingAmount: number = (d.data.name.length - mwPercentage.length) ? Math.round((d.data.name.length - mwPercentage.length) / 2) : 0
                    if (slicePosition.isLeftHalf()) {
                        xPaddingAmount = xPaddingAmount * -1
                    }

                    let c = generationArc.centroid(d)
                    return "translate(" + (c[0] * 2.1 + (xPaddingAmount * 5) + (translateByNameHack(d, 'x', 2))) + "," + (c[1] * 2.1 + 16 + yPaddingAmount + (translateByNameHack(d, 'y', 2))) + ")"
                }
            )
            .attr("dy", ".35em")
            .attr("text-anchor", function (d: any) {
                let slicePosition: SlicePosition = new SlicePosition(d.startAngle, d.endAngle)
                //return slicePosition.isLeftHalf() ? "end" : "start"
                return slicePosition.returnValue({
                    'top': () => 'middle',
                    'bottom': () => 'middle',
                    'left': () => 'end',
                    'right': () => 'start',
                })

            })
            .classed('hide-for-small-only', true)
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .text((d: any) => {
                let mwPercentage: string = this.round10((d.data.mw / this.totalMW) * 100, -1) + '%'
                return mwPercentage
            })

        const generationRenewablePieSlices = generationSVG.selectAll('.generationRenewableSlice')
            .data(generationRenewablePie(this.props.generationRenewableData))
            .enter()
            .append('g')
            .attr('class', 'generationRenewableSlice')

        generationRenewablePieSlices.append("path")
            .attr('d', generationRenewableArc)
            .style("fill", (d: any): string => this.generationRenewablesPieColor(d.data.name) as string)

        generationRenewablePieSlices.append("text")
            .attr(
                "transform", function (d: DefaultArcObject) {
                    let c = generationArc.centroid(d)
                    return "translate(" + c[0] * 1.2 + "," + c[1] * 1.2 + ")"
                }
            )
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .style('fill', '#FFF')
            .classed('hide-for-small-only', true)
            .text(function (d: any) {
                return d.data.name
            })

        generationRenewablePieSlices.append("text")
            .attr(
                "transform", function (d: DefaultArcObject) {
                    let c = generationArc.centroid(d)
                    return "translate(" + c[0] * 1.2 + "," + (c[1] * 1.2 + 16) + ")"
                }
            )
            .attr("dy", ".35em")
            .attr("text-anchor", "middle")
            .style("font-family", this.textStyle["font-family"])
            .style("font-weight", this.textStyle["font-weight"])
            .style('fill', '#FFF')
            .text((d: any) => {
                return this.round10((d.data.mw / this.totalMW) * 100, -1) + '%'
            })

        let legendRectSize = 14
        let legendSpacing = 4
        let legendContainer = select('#js-at-a-glance-chart-legend')
        legendContainer.select('div')
            .style('font-weight', 'bold')
            .text('Maximum winter capability in MW')
        legendContainer.select('ul')
            .style('margin-left', 0)
        let legendList = select('#js-at-a-glance-chart-legend ul')

        let legend = legendList.selectAll('.legend')
            .data(this.orderedGenData.concat(this.props.generationRenewableData))
            .enter()
            .append('li')
            .attr('class', 'legend')
            .style('float', 'left')
            .style('list-style-type', 'none')
            .style('margin-right', '25px')


        legend.append('div')
            .style('width', legendRectSize + 'px')
            .style('height', legendRectSize + 'px')
            .style('display', 'inline-block')
            .style('margin-right', '5px')
            .style('background-color', (d: any) => this.generationColorsCombined(d.mw) as string)

        legend.append('span')
            .style('font-weight', 'bold')
            .style('font-size', '14px')
            .text(function (d: any) {
                return d.longName + ' - ' + d.mw.toLocaleString()
            })

        legend.on("click", (data: any) => this.dispatchEvent(data))

        generationRenewablePieSlices.on('click', (data: { data: GenerationData }) => this.dispatchEvent(data.data))
        generationPieSlices.on('click', (data: { data: GenerationData }) => this.dispatchEvent(data.data))
    }

    dispatchEvent(data: GenerationData) {
        this.props.clickHandler(data.name)
    }

    decimalAdjust(type: string, value: any, exp: number): number {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value)
        }
        value = +value
        exp = +exp
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN
        }
        // Shift
        value = value.toString().split('e')
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)))
        // Shift back
        value = value.toString().split('e')
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp))
    }

    round10(value: any, exp: number) {
        return this.decimalAdjust('round', value, exp)
    }
    floor10(value: any, exp: number) {
        return this.decimalAdjust('floor', value, exp)
    }
    ceil10(value: any, exp: number) {
        return this.decimalAdjust('ceil', value, exp)
    }

}

export default EnergyPortfolioPieChart

export interface IPropsEnergyPortfolio {
    // totalMW: string,
    size: number[],
    generationData: GenerationData[]
    generationRenewableData: GenerationData[],
    clickHandler: (name: string) => void
}

export interface GenerationData {
    name: string
    mw: number
    mwSummer?: number
    longName: string
    photo?: JSX.Element | string
    videos?: JSX.Element | string
    plants?: GenerationDataPlants[]
    briefInfo: JSX.Element | string
}

export interface GenerationDataPlants {
    name: string,
    url: string
}

interface IWindow extends Window {
    CustomEvent: (event: any, props: any) => CustomEvent
}
