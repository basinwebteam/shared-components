import {GenerationData} from "../../../EnergyPortfolioPieChart/"

export const data: GenerationData[] = [
    {
        name: 'Wind',
        mw: 810.7,
        longName: 'Wind',
        photo: '',
        videos: "",
        plants: [
        ],
        briefInfo: '',
    },
    {
        name: 'Recovered',
        mw: 44.0,
        longName: 'Recovered',
        photo: '',
        videos: "",
        plants: [
            {
                name: '',
                url: ''
            },
            {
                name: '',
                url: ''
            }
        ],
        briefInfo: '',
    },
    {
        name: 'Coal',
        mw: 3154.1,
        longName: 'Coal-based',
        photo: 'http://www.basinelectric.com/files/img/facility-details/avs.jpg',
        videos: "",
        plants: [
            {
                name: 'Antelope Valley Station',
                url: 'http://www.basinelectric.com/Facilities/Antelope-Valley/index.html'
            },
            {
                name: 'Dry Fork Station',
                url: 'http://www.basinelectric.com/Facilities/Dry-Fork/index.html'
            },
            {
                name: 'Laramie River Station',
                url: 'http://www.basinelectric.com/Facilities/Laramie-River/index.html'
            },
            {
                name: 'Leland Olds Station',
                url: 'http://www.basinelectric.com/Facilities/Leland-Olds/index.html'
            }
        ],
        briefInfo: '<ul><li>Low cost, reliable generation</li><li>Plants run 24/7</li><li>The "workhorses" of the fleet</li></ul>',
    },
    {
        name: 'Hydro',
        mw: 315.7,
        longName: 'Hydro',
        photo: '',
        videos: "",
        plants: [
            {
                name: '',
                url: ''
            },
            {
                name: '',
                url: ''
            }
        ],
        briefInfo: '',
    },
    {
        name: 'Nuclear',
        mw: 62.2,
        longName: 'Nuclear',
        photo: '',
        videos: "",
        plants: [
            {
                name: '',
                url: ''
            },
            {
                name: '',
                url: ''
            }
        ],
        briefInfo: '',

    },
    {
        name: 'Natural Gas',
        mw: 1026.5,
        longName: 'Natural Gas',
        photo: '',
        videos: "",
        plants: [
            {
                name: '',
                url: ''
            },
            {
                name: '',
                url: ''
            }
        ],
        briefInfo: '',

    },
    {
        name: 'Oil',
        mw: 180.8,
        longName: 'Oil, diesel and jet fuel',
        photo: '',
        videos: "",
        plants: [
            {
                name: '',
                url: ''
            },
            {
                name: '',
                url: ''
            }
        ],
        briefInfo: '',

    },
]

export const renewablesData: GenerationData[] = [
    {
        name: 'Renewables',
        mw: 854.7,
        longName: 'Renewables (Recovered & Wind)',
        photo: '',
        videos: "",

        plants: [
            {
                name: '',
                url: ''
            },
            {
                name: '',
                url: ''
            }
        ],
        briefInfo: '',
    }
]