import * as React from 'react'
import 'reset-css/reset.css'
import { storiesOf } from '@storybook/react'
import { withKnobs, text } from '@storybook/addon-knobs'
import { withInfo } from '@storybook/addon-info'
import Biography from "../Biography"
import "./style.scss"

const description = `Shows contact info in a card-like display. 
Used on the board and executive pages as well as some contact pages.`

storiesOf("Biography", module)
  .addDecorator(withKnobs)
  .add("with image", 
    withInfo(`${description} An image or other data can be placed in between the Biography tags.`)(() => {
      const firstName=text("First Name", "Jane")
      const lastName=text("Last Name", "Doe")
      const title=text("Title", "Founder")
      const biographyDescription=text("Biography Description (HTML)", "<p>lorem ipsum.... </p>")
      const emailAddress=text("Email Address", "jane@doe.com")
      const phoneNumber=text("Phone Number", "701-555-2918")
      
      return (
        <Biography
          firstName={firstName}
          lastName={lastName}
          title={title}
          biographyDescription={biographyDescription}
          emailAddress={emailAddress}
          phoneNumber={phoneNumber}
        >
          <figure>
            <img src="http://via.placeholder.com/200x300" />
          </figure>
        </Biography>
      )
    })
  )
  .add("without image",
    withInfo(`${description} Biography tag without additional child elements.`)(() => {
      const firstName=text("First Name", "Jane")
      const lastName=text("Last Name", "Doe")
      const title=text("Title", "Founder")
      const biographyDescription=text("Biography Description (HTML)", "<p>lorem ipsum.... </p>")
      const emailAddress=text("Email Address", "jane@doe.com")
      const phoneNumber=text("Phone Number", "701-555-2918")
      
      return (
        <Biography
          firstName={firstName}
          lastName={lastName}
          title={title}
          biographyDescription={biographyDescription}
          emailAddress={emailAddress}
          phoneNumber={phoneNumber}
        />
      )}
    )
  )
 