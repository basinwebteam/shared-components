import * as React from "react"

export const Biography: React.StatelessComponent<IBiographyProps> = (props) => {
    const emailAddress = props.emailAddress || ""
    const phoneNumber = props.phoneNumber || ""
    const wrapperClass = props.children ? "biography biography-plus" : "biography"
    
    return (
        <div className={wrapperClass}>
            <p className="bio-name">{props.firstName} {props.lastName}</p>
            {props.children}
            <div className="bio-info">
                <p className="bio-title">{props.title}</p>
                {emailAddress !== "" && (<div className="bio-email">Email: <a href={`mailto:${emailAddress}`}>{props.firstName} {props.lastName}</a></div>)}
                {phoneNumber !== "" && (<div className="bio-phone">Phone: {phoneNumber}</div>)}
                <div className="bio-description" dangerouslySetInnerHTML={{__html: props.biographyDescription}} />
            </div>
        </div>
    )
}

Biography.displayName = "Biography"

export default Biography

export interface IBiographyProps {
    firstName: string,
    lastName: string,
    title: string,
    biographyDescription: string,
    emailAddress?: string,
    phoneNumber?: string
}