/// <reference types="react" />
import * as React from 'react'

export declare class Carousel extends React.Component<IProps, IState> {
    constructor(props: IProps)
    render(): JSX.Element
}
export default Carousel

export interface IProps {
    afterSlide?: () => void,
    autoplay?: boolean,
    autoplayInterval?: number,
    beforeSlide?: () => void,
    cellAlign?: cellAlign,
    cellSpacing?: number,
    data?: () => void,
    decorators?: IDecorators[],
    dragging?: boolean,
    easing?: string,
    edgeEasing?: string,
    framePadding?: string,
    frameOverflow?: string,
    initialSlideHeight?: number,
    initialSlideWidth?: number,
    slideIndex?: number,
    slidesToShow?: number,
    slidesToScroll?: slidesToScroll,
    slideWidth?: string | number,
    speed?: number,
    swiping?: boolean,
    vertical?: boolean,
    width?: string,
    wrapAround?: boolean
}

export interface IState {

}

export interface IDecorators {
    component?: React.StatelessComponent,
    position?: decoratorPosition,
    style?: React.CSSProperties,
}

export type cellAlign = "left" | "center" | "right"

export type slidesToScroll = number | "auto"

export type decoratorPosition = "TopLeft" | "TopCenter" | "TopRight" | "CenterLeft" | "CenterCenter" | "CenterRight" | "BottomLeft" | "BottomCenter" | "BottomRight"