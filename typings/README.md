# Purpose of this Directory

Occassionaly an imported module does not have TypeScript definitions. Temporary ones are placed here until some are publicly available.

Ideally we should share these via GitHub/NPM.

To add a new typing, add the directory in `/typings/` and then type something similar to: `typings install --save file:typings/nuka-carousel/index.d.ts`

## Nuka Carousel

This one has a pull request for types. Not sure of the status.